$(document).ready(function(){

	$("#img-upload").on("change", function(){

		var files = !!this.files ? this.files : [];
	   	if (!files.length || !window.FileReader) return; // Check if File is selected, or no FileReader support
	   	if (/^image/.test( files[0].type)){ //  Allow only image upload
	    var ReaderObj = new FileReader(); // Create instance of the FileReader
	    ReaderObj.readAsDataURL(files[0]); // read the file uploaded
	    ReaderObj.onloadend = function(){ 
            $('#img-label').css('background-image', 'url('+this.result+')');
//            $('#error-message').text('Image selected!');
//            $('#error-message').css('color', 'green');
         }
     	}
     	else{
            $('#img-label').css('background-image', 'url("assets/images/camera.jpg")');
            $('#error-message').text('File is not an image!');
            $('#error-message').css('color', 'red');
     	}
	});
        
        new getDefaultData();
});

function getDefaultData(){
    
    var url_string = window.location.href;
    var params = new URL(url_string);
    var username = params.searchParams.get("name");
    var checkpointID = params.searchParams.get("id");
    
    var protocol = window.location.protocol;
    var url = protocol + '//' + window.location.hostname + '/api/Customer/single-page?username='+ username +'&checkpoint_id=' + checkpointID;
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            var jsonToArray = JSON.parse(this.response);
            var img_thumb = jsonToArray.data.company.logo;
            var checkpointName = jsonToArray.data.checkpoint.name;
            var p_location = jsonToArray.data.checkpoint.p_location;
            var c_location = jsonToArray.data.checkpoint.c_location;

            document.getElementById("logo_img").src = img_thumb;
            document.getElementById("checkpoint_name").innerHTML = 'How do you feel about ' + checkpointName + '?';
            document.getElementById("p_location").innerHTML = p_location;
            document.getElementById("c_location").innerHTML = c_location;
            
            setTimeout(function () {
                $('.content-loader-2').hide();
            }, 1000);
        }
    };
    xmlHttp.open( "GET", url, false ); // false for synchronous request
    xmlHttp.send();
}

function saveDataLoader(){
    $('.content-loader-2').show();
    setTimeout(function () {
        $('.content-loader-2').hide();
    }, 10000);
}