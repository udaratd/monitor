<?php

require 'Email.class.php';
require 'support-functions.php';


if(isset($_POST['rating-field'])) {

	$email_to = "info@myroomsonline.com";
    $name_to = "Admin";
    $email_subject = "Feedback from washroom user";

    function died($error) {
        // your error code can go here
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
        echo "These errors appear below.<br /><br />";
        echo $error."<br /><br />";
        echo "Please go back and fix these errors.<br /><br />";
        die();
    }

    if(!isset($_POST['feedback'])) {
        die('We are sorry, but there appears to be a problem with the form you submitted.');       
    }

    $feedback = '';
    $name = 'Anonymous';
    $phone = '-';

    $uploadOk = 0;
    $moveFile = 0;

    $target_dir = "uploads/";
    $file_size = $_FILES["uploadPic"]["size"];

    if (isset($_POST['rating-field'])) {
    	$rating = $_POST['rating-field'];
    }

    if (isset($_POST['feedback'])) {
    	$feedback = $_POST['feedback'];
    }

    if (isset($_POST['reviewerName'])) {
    	$name = $_POST['reviewerName'];
    }

    if (isset($_POST['reviewerPhone'])) {
    	$phone = $_POST['reviewerPhone'];
    }

    if ($file_size > 0) {
        
        $img_name = $_FILES["uploadPic"]["name"];
        $target_file = $target_dir . basename($_FILES["uploadPic"]["name"]);
        $tmp_name = $_FILES["uploadPic"]["tmp_name"];

        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is an actual image
        $uploadOk = check_image(getimagesize($tmp_name));
        
        // Check file size
        if ($file_size > 10000000) {
            $uploadOk = 0;
        }

        // Allow certain file formats
        $uploadOk = check_file_type($imageFileType);
        
        //Reduce file size
        $image_min = min_file_size($tmp_name, 75);

        //Moves the file to "uploads/" directory
        if ($uploadOk == 1) {
            $moveFile = move_to_dir($image_min, $target_file);
        }
    }

    $email_message = "Form details below.<br><br>";

    $email_message .= "Rating: ".$rating."<br><br>";
    $email_message .= "Reviewer Name: ".$name."<br>";
    $email_message .= "Phone: ".$phone."<br>";
    $email_message .= "Feedback: ".$feedback."<br>";

    $fromEmail = 'hemas-feedback@domedia.lk';
    $fromName = "Hemas Feedback Form";

    $send = new SimpleMail();

    $send->setTo($email_to, $name_to);
    $send->setFrom($fromEmail, $fromName);
    $send->setSubject($email_subject);
    $send->setMessage($email_message);
    
    if ($moveFile) {
        $send->addAttachment($target_file);
    }
    else
        $send->addGenericHeader('Content-Type', 'text/html; charset="utf-8"');

    $send->send();

    header("Location: thankyou-page.php");
    die();
}
else
    echo "Please select a reaction feedback before form submission.";
?>