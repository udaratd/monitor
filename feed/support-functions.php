<?php

function min_file_size($source, $quality){

	$info = getimagesize($source);
	$destination = $source;

	if ($info['mime'] == 'image/jpeg') 
		$image = imagecreatefromjpeg($source);

	elseif ($info['mime'] == 'image/gif') 
		$image = imagecreatefromgif($source);

	elseif ($info['mime'] == 'image/png') 
		$image = imagecreatefrompng($source);

	imagejpeg($image, $destination, $quality);

	return $destination;
}

function check_image($check){
	if($check !== false) {
        return 1;
    } else {
        return 0;
    }
}

function check_file_type($imageFileType){
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	    && $imageFileType != "gif" ) {
	        return 0;
	}
	else
		return 1;
}

function move_to_dir($image_min, $target_file){

	if (move_uploaded_file($image_min, $target_file)) {
        return 1;
    }
    else
       return 0;
}

?>