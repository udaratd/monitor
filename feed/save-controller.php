<?php

$postData = $_POST;
$getData = $_GET;

if(empty($getData['name'])){
    echo 'Company username is required';
    die();
}
if(empty($getData['id'])){
    echo 'Checkpoint id is tequired';
    die();
}
if(empty($postData['rating-field'])){
    echo "Please select a reaction feedback before form submission.";
    die();
}

$userName = $getData['name'];
$checkpointId = $getData['id'];
$reactionText = $postData['rating-field'];
$feedback = '';
$name = '';
$phone = '';
$image = '';
$reaction = 0;

if($reactionText === 'Bad'){
    $reaction = 1;
}elseif ($reactionText === 'Good') {
    $reaction = 2;
} else {
    $reaction = 3;
}

if(isset($postData['feedback'])){
    $feedback = $postData['feedback'];
}
if(isset($postData['reviewerName'])){
    $name = $postData['reviewerName'];
}
if(isset($postData['reviewerPhone'])){
    $phone = $postData['reviewerPhone'];
}
if(isset($_FILES['uploadPic'])){
    $image = imageToBase64();
}

uploadData($userName, $checkpointId, $reaction, $feedback, $name, $phone, $image);

function imageToBase64(){
    $base64 = null;
    $errors=array();
    $allowed_ext= array('jpg','jpeg','png','gif');
    $file_name =$_FILES['uploadPic']['name'];
    $file_ext = strtolower( end(explode('.',$file_name)));

    $file_size=$_FILES['uploadPic']['size'];
    $file_tmp= $_FILES['uploadPic']['tmp_name'];

    $data = file_get_contents($file_tmp);
    $base64 = base64_encode($data);

    if(in_array($file_ext,$allowed_ext) === false){
        $errors[]='File type is not supported, Please try again';
    }
    if($file_size > 2097152){
        $errors[]= 'File size must be under 2mb';
    }
    if(!empty($errors)){
        foreach($errors as $error){
            echo $error , '<br/>'; 
        }
    } else {
        return $base64;
    }   
}

function uploadData($userName, $checkpointId, $reaction, $feedback, $name, $phone, $image){  
    $curl = curl_init();
    
    $requestScheme = $_SERVER['REQUEST_SCHEME'];
    $host = $_SERVER['HTTP_HOST'];
    $url = $requestScheme.'://'.$host.'/api/Customer/add';
       
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => array('username' => $userName,'checkpoint_id' => $checkpointId,'reaction' => $reaction,'feedback' => $feedback,'name' => $name,'phone' => $phone,'image' => $image),
    ));

    $responseJson = curl_exec($curl);

    curl_close($curl);
    
    $response = json_decode($responseJson, true);  
    if($response['response']['statusCode'] === 200 && $response['response']['success']){
        header("Location: thankyou-page.php");
        die();
    }
}