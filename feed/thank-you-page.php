<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="shortcut icon" type="image/png" href="assets/images/favicon.png" />
    <title>Feedback | Thank You</title>

</head>
<body>
	<div class="section-one">
		<div class="check-image-wrapper">
			<img src="assets/images/check.png" alt="white tick on blue field">
		</div>
		
	</div>

	<div class="thank-you-text-wrapper font-roboto-bold">
		<h2 class="top-text">Thanks</h2>
		<h3 class="bottom-text">for your feedback</h3>
	</div>
</body>

</html>