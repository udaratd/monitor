<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/all.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css?v=0.0.1">
    <link rel="shortcut icon" type="image/png" href="assets/images/favicon.png" />
    <title>Feedback | Physical Monitoring System</title>
</head>

<body class="index">
        
	<div class="section-wrapper">
            
                <div class="content-loader-2"></div>
                               
		<div class="section-one">
			<div class="logo-image-wrapper">
				<img id="logo_img">				
			</div>
		</div>

		<div class="section-two">
                        <h2 id="checkpoint_name" class="main-text font-roboto-bold">
			
			</h2>
			<div class="sub-text font-roboto-regular">
                                <div id="p_location" class="floor"></div>
				<div class="right-chevron"><img src="assets/images/right-chevron.png"></div>
				<div id="c_location" class="room"></div>
			</div>
			<div class="rating-wrapper font-roboto-regular">
				<div class="rating-hr"></div>
				<div class="bad-face rating-faces">
					<input class="emoji-radio" form="feedback-form" type="radio" id="imojiOne" name="rating-field" value="Bad">
					<div class="rating-face-inner">

						<label for="imojiOne">
							<i class="fas fa-frown"></i>
						</label>
							
					</div>

					<p class="rating-text">
						Bad
					</p>
				</div>
				<div class="good-face rating-faces">
					<input class="emoji-radio" form="feedback-form" type="radio" id="imoji-2" name="rating-field" value="Good">
					<div class="rating-face-inner">
						<label for="imoji-2">
							<i class="fas fa-smile"></i>
						</label>
						
					</div>
					<p class="rating-text">
						Good
					</p>
				</div>
				<div class="great-face rating-faces">
					<input class="emoji-radio" checked form="feedback-form" type="radio" id="imoji-3" name="rating-field" value="Great">
					<div class="rating-face-inner">
						<label for="imoji-3">
							<i class="fas fa-laugh-beam"></i>
						</label>
						
					</div>
					<p class="rating-text">
						Great
					</p>
				</div>
			</div>
		</div>

		<div class="section-three font-roboto-regular">
			<h3 class="section-three-header">Thanks. Anything else you'd like to add?</h3>
			<div class="section-three-form">
                            <form id="feedback-form" name="feedback-form" action="save-controller.php?id=<?php echo $_GET['id'];?>&name=<?php echo $_GET['name'];?>" method="post" enctype="multipart/form-data" onsubmit="saveDataLoader();">
					<textarea class="feedback-textarea" name="feedback" placeholder="Write a Feedback"></textarea>
					<div class="form-bottom">
						<div class="form-bottom-left">
							<input class="feedback-name" type="text" name="reviewerName" placeholder="Name">
							<input class="feedback-phone" type="text" name="reviewerPhone" placeholder="Phone">
						</div>
						<div class="form-bottom-right">
							<input id="img-upload" type="file" name="uploadPic" hidden />
							<label id="img-label" class="feedback-upload" for="img-upload" style="background-image: url('assets/images/camera.jpg');">Upload Photo</label>
							<!-- <button type="file" class="feedback-upload" name="uploadPic">Upload Photo</button> -->
						</div>
					</div>
					<p id="error-message"></p>
                                        <input class="feedback-submit" type="submit" name="feedbackSubmit" value="Submit Feedback">
				</form>
			</div>
		</div>
	</div>
</body>

<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/script.js"></script>

</html>