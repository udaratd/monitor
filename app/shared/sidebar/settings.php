<div class="sidebar-header">
    <div class="pull-left">
        <span class="sidebar-title"><i class="fa fa-cog" aria-hidden="true"></i> My Profile</span>
    </div>
    <div class="pull-right">
        <a href="" class="sidebar-closer">&times;</a>
    </div>
</div>

<div class="sidebar-content">
<!--    <h3 class="p-l-10">Coming Soon..</h3>-->
    
    <div class="sb-user-heading">
        <div class="image">
            <img alt="{{logged_user_fname}}" src="{{logged_user_avatar}}" ng-if="logged_user_avatar != null && logged_user_avatar != 'null'">
            <img alt="{{logged_user_fname}}" src="/assets/images/user-default.jpg" ng-if="logged_user_avatar == null || logged_user_avatar == 'null'">
        </div>
        <div class="content">
            <div class="title">{{logged_user_fname}} {{logged_user_lname}}</div>
            <div class="sub-title">{{logged_user_role_name}}</div>
        </div>
    </div>

    <div class="sb-user-content">
        <div class="tbl-data-view">
            <table>
                <tbody>
                    <tr>
                        <td class="col-md-4 col-sm-5"><label>NIC <span class="dash-seperator">:</span></label></td>
                        <td>{{logged_user_nic}}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4 col-sm-5"><label>Email <span class="dash-seperator">:</span></label></td>
                        <td>{{logged_user_email}}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4 col-sm-5"><label>Phone <span class="dash-seperator">:</span></label></td>
                        <td>{{logged_user_phone}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    
</div>