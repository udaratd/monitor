<div class="sidebar-header">
    <div class="pull-left">
        <span class="sidebar-title"><i class="fa fa-bell" aria-hidden="true"></i> Notifications</span>
    </div>
    <div class="pull-right">
        <a href="" class="sidebar-closer">&times;</a>
    </div>
</div>

<div class="sidebar-content scroll-container">
    
    <ul class="notifi-list" ng-if="all_noti_list != null">
        <li ng-repeat="noti in all_noti_list" ng-class="{'unread': noti.notification == 0}">
            <a href="" ng-click="readNotification(noti.workID, $event)">
                <div class="info-image">
                    <div class="image">
                        <i class="fa fa-repeat text-danger" aria-hidden="true" ng-if="noti.rework != null"></i>
                        <i class="fa fa-ellipsis-h text-notice" aria-hidden="true" ng-if="noti.rework == null"></i>
                    </div>
                    <div class="content">
                        <div class="title">{{noti.staff_fName}} {{noti.staff_lName}} submitted a work</div>
                        <div class="sub-title">{{noti.loc_parentName}} <i class="fa fa-angle-right" aria-hidden="true"></i> {{noti.loc_childName}} <i class="fa fa-angle-right" aria-hidden="true"></i> {{noti.checkpointName}}</div>
                    </div>
                </div>
            </a>
        </li>
    </ul>
    
    <h3 class="p-l-10" ng-if="all_noti_list == null">No any notifications.</h3>
    
</div>