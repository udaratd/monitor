<!-- Sidebar Notifications -->
<div class="app-sidebar" id="sidebar-notifications">
    <?php include_once 'sidebar/notifications.php'; ?>
</div>

<!-- Sidebar Help -->
<div class="app-sidebar" id="sidebar-help">
    <div class="sidebar-header">
        <div class="pull-left">
            <span class="sidebar-title"><i class="fa fa-question-circle" aria-hidden="true"></i> Help</span>
        </div>
        <div class="pull-right">
            <a href="" class="sidebar-closer">&times;</a>
        </div>
    </div>
    
    <div class="sidebar-content">
        <h3 class="p-l-10">Coming Soon..</h3>
    </div>
</div>

<!-- Sidebar My Settings -->
<div class="app-sidebar" id="sidebar-settings">
    <?php include_once 'sidebar/settings.php'; ?>
</div>