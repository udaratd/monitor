<div class="toolbar-top">
    <div class="container-fluid full-height">
        <div class="row full-height vertical-center">
            <div class="col-md-3 col-sm-3">
                <a href="/" class="company-logo">
                    <!--                    <img src="/assets/images/moni-logo.png" alt="Company Logo">-->
                    <img alt="Company Logo" src="{{company_logo}}" ng-if="company_logo != null && company_logo != 'null'">
                    <img alt="Company Logo" src="/assets/images/logo-moni.png" ng-if="company_logo == null || company_logo == 'null'">
                </a>
            </div>

            <div class="col-md-6 col-sm-4 full-height">
                <div class="toolbar-search">
                    <form action="" method="GET" class="full-width">
                        <div class="form-group m-b-0">
                            <input type="text" name="app_search" placeholder="Search.." autofocus class="form-control">
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-md-3 col-sm-5">
                <ul class="toolbar-nav pull-right">
                    <!--                    <li><a href="#" class="search-toggle" title="Search"><i class="fa fa-search" aria-hidden="true"></i></a></li>-->
                    <li>
                        <a href="#" title="Notifications" class="sidebar-triggerer" data-sidebar="sidebar-notifications">
                            <i class="fa fa-bell" aria-hidden="true"></i>
                            <span class="toolbar-badge" ng-if="notification_count > 0">{{notification_count}}</span>
                        </a>
                    </li>

                    <li><a href="#" title="Help" class="sidebar-triggerer" data-sidebar="sidebar-help"><i class="fa fa-question-circle"
                                aria-hidden="true"></i></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-user-circle f-s-30 m-r-10" ng-if="logged_user_avatar == null || logged_user_avatar == 'null'"
                                aria-hidden="true"></i>
                            <img alt="" src="{{logged_user_avatar}}" ng-if="logged_user_avatar != null && logged_user_avatar != 'null'"
                                class="toolbar-icon-user">
                            <span class="user-name m-r-5">{{logged_user_fname}}</span> <i class="fa fa-angle-down"
                                aria-hidden="true"></i>
                        </a>
                        <ul class="dropdown-menu right">
                            <li><a href="" class="sidebar-triggerer" data-sidebar="sidebar-settings">My Profile</a></li>
                            <li><a href="/settings/general">Account Setup</a></li>
                            <li><a href="" class="sidebar-triggerer" data-sidebar="sidebar-help">Help Center</a></li>
                            <li><a href="" ng-click="userLogout()" class="btn-loading" id="link-logout">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="toolbar-bottom">
    <ul class="toolbar-nav">
        <li><a href="/dashboard" active-link="active"><i class="fa fa-home" aria-hidden="true"></i> <span>Dashboard</span></a></li>
        <li>
            <a href="/review" active-link="active"><i class="fa fa-eye" aria-hidden="true"></i> <span>Review Activity</span>
                <span class="toolbar-badge" ng-if="review_count > 0">{{review_count}}</span>
            </a>
        </li>
        <li><a href="/work-list" active-link="active"><i class="fa fa-list" aria-hidden="true"></i> <span>Work List</span></a></li>
        <li><a href="/schedules" active-link="active"><i class="fa fa-tasks" aria-hidden="true"></i> <span>Task &
                    Schedules</span></a></li>
        <li><a href="/checkpoints" active-link="active"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>Checkpoints</span></a></li>
        <li><a href="/feedback" active-link="active"><i class="fa fa-comments-o" aria-hidden="true"></i> <span>Feedbacks</span>
                <span class="toolbar-badge" ng-if="unread_feedback_count > 0">{{unread_feedback_count}}</span>
            </a>
        </li>
        <li><a href="/reports" active-link="active"><i class="fa fa-file-text-o" aria-hidden="true"></i> <span>Reports</span></a></li>
        <li><a href="/settings/general" active-link="active"><i class="fa fa-cog" aria-hidden="true"></i> <span>Settings</span></a></li>
    </ul>
</div>