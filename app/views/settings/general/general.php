<div class="panel" ng-controller="MainCtrl" ng-init="initCompany()">

    <?php include_once '../settings-header.php'; ?>

    <div class="content-wrapper p-t-15">
        <div class="row">
            <div class="col-md-5 col-sm-5">
                <div class="form-group">
                    <label for="user-name">Company User Name</label>
                    <input type="text" class="form-control" placeholder="Company User name" ng-model="logged_company_uname"
                        id="user-name" disabled>
                </div>
            </div>
        </div>
        <form name="formUpdateCompany" novalidate ng-submit="updateCompany(upd_company)">
            <div class="row">
                <div class="col-md-5 col-sm-5">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="update-company-name">Company Name</label>
                                <input type="text" class="form-control" placeholder="Company name" ng-model="upd_company.name"
                                    id="update-company-name">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="fld-label">Company Logo</label>
                                <div class="uploader-area">
                                    <div class="uploader-wrapper view-only">
                                        <input type="file" id="update-company-logo" ng-model="upd_company.image"
                                            app-file-model="" accept="image/*">
                                        <label for="update-company-logo" class="btn"><i class="fa fa-upload"
                                                aria-hidden="true"></i> Change Logo</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Overlap Schedule</label>
                                <span class="checkbox inline-block">
                                    <input type="checkbox" ng-model="skip_validation" id="skip-schedule-validation">
                                    <label for="skip-schedule-validation">&nbsp;</label>
                                </span>
                            </div> 
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <p class="import-hint-text">Please contact system administrator to edit these details.</p>
                        </div>
                        <div class="col-sm-4">
                            <!-- <button class="btn btn-loading" type="submit" ng-disabled="formUpdateCompany.$invalid" id="btn-update-company">Update
                                Details</button> -->

                        </div>
                        <!-- <div class="col-sm-8">
                            <div class="response-message-text" id="msg-update-company"></div>
                            <div class="spacer visible-xs"></div>
                        </div> -->
                    </div>

                </div>
            </div>
        </form>
    </div>

</div>