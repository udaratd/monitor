<div class="panel" ng-controller="LocationCtrl" ng-init="initAdd()">

    <?php include_once '../settings-header.php'; ?>

    <div class="panel-title">
        <div class="row">
            <div class="col-md-5">
                <h1 class="inline-block">List of Locations</h1>
                <a href="/settings/location/add" class="btn btn-primary pull-right">Add Location</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <?php include 'location-list.php'; ?>
        </div>

        <div class="col-md-7">
            <div class="box-bordered p-20">
                <h2 class="m-t-0">Add Location</h2>
                <form name="formAddLocation" novalidate ng-submit="addLocation(loc)">

                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="settings-loc-name" class="col-sm-2">Name *</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" ng-model="loc.name" placeholder="ex: Floor 1"
                                    id="settings-loc-name" required>
                            </div>
                        </div>

                        <div class="form-group hide" id="settings-loc-parent-wrapper">
                            <label for="settings-loc-parent" class="col-sm-2">Parent</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" ng-model="loc.parentName" placeholder="ex: Floor 1"
                                    id="settings-loc-parent" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="settings-loc-desc" class="col-sm-2">Description</label>
                            <div class="col-sm-10">
                                <textarea class="form-control no-resize" ng-model="loc.description" placeholder="Add Note.."
                                    id="settings-loc-desc"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="settings-location-sample-photo" class="col-sm-2">Photo</label>
                            <div class="col-sm-10">
                                <div class="droppable-area content-horizontal" style="height: 120px;">
                                    <div class="uploader-wrapper">
                                        <input type="file" id="settings-location-sample-photo" ng-model="loc.image"
                                            app-file-model="" accept="image/*">
                                        <label for="settings-location-sample-photo" class="btn btn-dark m-b-0">Select
                                            Photo</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-2 col-sm-push-2">
                                <button type="submit" ng-disabled="formAddLocation.$invalid" class="btn btn-primary btn-loading"
                                    id="btn-add-location"><span>Save</span></button>
                            </div>
                            <div class="col-sm-8 col-sm-push-2">
                                <div class="response-message-text" id="msg-add-location"></div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

</div>