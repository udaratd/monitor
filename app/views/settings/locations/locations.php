<div class="panel" ng-controller="LocationCtrl" ng-init="init()">

    <?php include_once '../settings-header.php'; ?>
    
    <div class="panel-title">
        <div class="row">
            <div class="col-md-5">
                <h1 class="inline-block">List of Locations</h1>
                <a href="/settings/location/add" class="btn btn-primary pull-right">Add Location</a>
            </div>
        </div>
    </div>
    
    <div class="row min-h-200">
        <div class="col-md-5">
            <?php include 'location-list.php'; ?>
        </div>
    </div>
    
</div>