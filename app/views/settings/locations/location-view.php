<div class="panel" ng-controller="LocationCtrl" ng-init="initView()">

    <?php include_once '../settings-header.php'; ?>
    
    <div class="panel-title">
        <div class="row">
            <div class="col-md-5">
                <h1 class="inline-block">List of Locations</h1>
                <a href="/settings/location/add" class="btn btn-primary pull-right">Add Location</a>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-5">
            <?php include 'location-list.php'; ?>
        </div>
        
        <div class="col-md-7">
            <div class="box-bordered p-20">
                <div class="row vertical-center m-b-10">
                    <div class="col-md-4 col-sm-4">
                        <h2 class="m-t-0 m-b-0">{{location_single.name}}</h2>
                    </div>
                    <div class="col-md-8 col-sm-8 text-right">
                        <ul class="horizontal-action-group">
                            <li><a href="/settings/location/edit/{{location_single.ID}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a></li>
                            <li><a href="" ng-click="deleteLocation(location_single.ID)" class="text-muted btn-loading btn-text-loading" id="btn-delete-location"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a></li>
                        </ul>
                    </div>
                </div>
                <div class="line-seperator m-t-0"></div>
                
                <div class="row">
                    <div class="col-sm-6">
                        <div class="single-image h-200 contain">
                            <img src="{{location_single.img_large}}" ng-if="location_single.img_large != null" alt="{{location_single.name}}">
                            <img src="/assets/images/default-pic-medium.png" ng-if="location_single.img_large == null" alt="{{location_single.name}}">
                        </div>
                    </div>
                </div>
                
                <div class="tbl-data-view m-t-15">
                    <table>
                        <tbody>
                            <tr ng-if="location_single.parentName != 'null'">
                                <td class="col-sm-3"><span class="f-w-500">Parent Location</span><span class="dash-seperator">:</span></td>
                                <td>{{location_single.parentName}}</td>
                            </tr>
                            <tr>
                                <td class="col-sm-3"><span class="f-w-500">Description</span><span class="dash-seperator">:</span></td>
                                <td><p class="text-justify">{{location_single.description}}</p></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
    
</div>