<div class="scroll-container max-h-600" ng-if="!location_list_no_data">
    <ul class="location-accordian">
        <li class="has-children {{location.active}}" ng-repeat="location in location_list">
            <div class="item-strip">
                <i class="fa fa-angle-right animate-item" aria-hidden="true"></i><a href="/settings/locations/{{location.ID}}" class="item-label">{{location.name}}</a>
            </div>
            <ul class="accodian-submenu">
                <li ng-repeat="location_child in location.childs" class="{{location_child.active}}">
                    <div class="item-strip">
                        <a href="/settings/locations/{{location_child.ID}}" class="item-label">{{location_child.name}}</a>
                    </div>
                </li>
                <li><a href="/settings/location/add?parent={{location.ID}}&pname={{location.name}}">Add Child Location</a></li>
            </ul>
        </li>
    </ul>
</div>

<div class="m-t-30 m-b-15" ng-if="location_list_no_data"><h3>No Locations found</h3></div>

<div class="content-loader edged" content-loader=""></div>