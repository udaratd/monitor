<div class="model-wrapper" id="model-add-user" ng-controller="UserCtrl" ng-init="initAddUser()">
    <div class="model-dialog model-medium-small">

        <div class="model-header">
            <!-- <div class="model-closer"><a href="/settings/users">&times;</a></div> -->
            <div class="model-closer2"><a href="" ng-click="addUserPopupShowHide()">&times;</a></div>
            <h3 class="text-center">Add User</h3>
        </div>

        <div class="model-content">
            <form name="formAddUser" novalidate ng-submit="addUser(user)">
                <div class="row">
                    <div class="col-md-6 col-sm-12">

                        <div class="row half-gap">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="settings-user-first-name">First Name *</label>
                                    <input class="form-control" type="text" ng-model="user.firstName" placeholder="ex: Saman"
                                        id="settings-user-first-name" required>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="settings-user-last-name">Last Name *</label>
                                    <input class="form-control" type="text" ng-model="user.lastName" placeholder="ex: Perera"
                                        id="settings-user-last-name" required>
                                </div>
                            </div>
                        </div>
                        <div class="row half-gap">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>NIC Number *</label>
                                    <input class="form-control" type="text" ng-model="user.nic" placeholder="ex: 849775645V"
                                        required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="settings-user-phone">Phone Number</label>
                                    <input class="form-control" type="text" ng-model="user.phone" placeholder="ex: 0777547894"
                                        id="settings-user-phone">
                                </div>
                            </div>
                        </div>

                        <div class="form-group" ng-class="{'has-error': formAddUser.$error.email}">
                            <label>Email *</label>
                            <input class="form-control" type="email" ng-model="user.email" placeholder="ex: kamal@gmail.com"
                                required>
                        </div>

                        <div class="row half-gap">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label>Password *</label>
                                    <input class="form-control" type="password" ng-model="user.password" required>
                                </div>
                            </div>
                            <!-- <div class="col-sm-7" style="padding-top: 22px;">
                                <a href="#" class="btn btn-dark disabled">Auto Fill</a>
                            </div> -->
                        </div>
                        <div class="row half-gap">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="settings-user-role">Job Role *</label>

                                    <div class="select-wrapper" ng-if="logged_user_role == 2">
                                        <select ng-model="user.jobRole" id="settings-user-role" class="form-control"
                                            required>
                                            <option value="" selected="selected">--Select Role--</option>
                                            <option value="3" selected="selected">Staff</option>
                                        </select>
                                    </div>

                                    <div class="select-wrapper" ng-if="logged_user_role != 2">
                                        <select ng-options="role.id as role.label for role in role_list | orderBy : '-id'"
                                            ng-model="user.jobRole" id="settings-user-role" class="form-control"
                                            ng-change="getSupervisors(user.jobRole)" required ng-disabled="logged_user_role == 2">
                                            <option value="" selected="selected">--Select Role--</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group disable" ng-if="user.jobRole == '3' && logged_user_role != 2">
                                    <label for="settings-user-supervisor">Supervisor</label>
                                    <div class="search-selection">
                                        <input type="text" id="settings-user-supervisor" placeholder="All" class="form-control output"
                                            readonly>
                                        <div class="search-dropdown">
                                            <input type="text" placeholder="Search.." ng-model="filter_superviser_by_name"
                                                class="form-control search-filter">
                                            <ul class="result-list scroll-container">
                                                <li ng-repeat="supervisor in user_list | filter:filter_superviser_by_name">
                                                    <label for="chk-sup-{{supervisor.ID}}">
                                                        <span>{{supervisor.firstName}} {{supervisor.lastName}}</span>
                                                        <input type="radio" id="chk-sup-{{supervisor.ID}}" ng-model="user.supervisor_id"
                                                            name="user_supervisor[]" value="{{supervisor.ID}}" required>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12">

                        <div class="form-group">
                            <label for="checkpoint-description">Notes</label>
                            <textarea class="form-control no-resize" id="user-note" ng-model="user.note" placeholder="Add Note"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="settings-user-photo">Upload Staff Photo</label>
                            <div class="droppable-area" style="height: 145px;">
                                <!--                                <h3 class="uploader-title m-t-0 m-b-0 text-left">Drop files here to <br>start uploading</h3>
                                <h3 class="m-t-0 m-b-0 p-l-25 p-r-25">or</h3>-->

                                <div class="uploader-wrapper">
                                    <input type="file" ng-model="user.imageString" app-file-model="" id="settings-user-photo"
                                        accept="image/*">
                                    <label for="settings-user-photo" class="btn btn-dark m-b-0">Select Photo</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row text-right">
                    <div class="col-md-10 col-sm-9">
                        <div class="response-message-text" id="msg-add-user"></div>
                    </div>
                    <div class="col-md-2 col-sm-3">
                        <button type="submit" ng-disabled="formAddUser.$invalid" class="btn btn-primary btn-loading" id="btn-add-user"><span>Save</span></button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>