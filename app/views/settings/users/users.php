<div class="panel" ng-controller="UserCtrl" ng-init="init()">

    <?php include_once '../settings-header.php'; ?>

    <div class="content-wrapper p-t-15">
        <div class="page-toolbar m-t-0">

            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="form-group">
                        <div class="search-wrapper">
                            <input type="search" name="term" class="search-control  form-control" ng-model="filter_users_by_keyword"
                                placeholder="Search by ID, Name or Contact Number">
                            <span class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-5 col-sm-5">
                    <div class="form-group group-inline">
                        <label for="user-job-role">Job Role</label>
                        <div class="select-wrapper">
                            <select class="form-control" id="user-job-role" ng-options="role.id as role.label for role in role_list"
                                ng-model="user.role" ng-change="getUserList(user.role)">
                                <option value="" selected="selected">--Select Role--</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-3 text-right">
                    <a href="" ng-click="addUserPopupShow()" class="btn btn-primary">Add Staff</a>
                </div>
            </div>

        </div>

        <div class="table-responsive" ng-if="!user_list_no_data">
            <table class="table table-bordered table-sortable table-condensed table-striped content-align-top">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>NIC Number</th>
                        <th>Contact Number</th>
                        <th>Job Role</th>
                        <th class="w-260">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="user in user_list | filter:filter_users_by_keyword">
                        <td class="condensed-column">
                            <div class="info-image">
                                <div class="image">
                                    <img src="{{user.img_thumbnail}}" alt="{{user.firstName}}" ng-if="user.img_thumbnail !== null">
                                    <img src="/assets/images/user-default.jpg" alt="{{user.firstName}}" ng-if="user.img_thumbnail === null">
                                </div>
                                <div class="content">
                                    <div class="title">{{user.firstName}} {{user.lastName}}</div>
                                </div>
                            </div>
                        </td>
                        <td>{{user.nic}}</td>
                        <td><a href="tel:{{user.phone}}" ng-if="user.phone !== null">{{user.phone}}</a><span ng-if="user.phone === null">-</span></td>
                        <td>{{user.role}}</td>
                        <td>
                            <ul class="horizontal-action-group">
                                <li><a href="" ng-click="prepareUserUpdate(user.ID)"><i class="fa fa-pencil-square-o"
                                            aria-hidden="true"></i> View / Edit</a></li>
                                <!--                                <li><a href="" drawer-execute="user-single-drawer" class="disabled"><i class="fa fa-eye" aria-hidden="true"></i> View</a></li>-->
                                <li ng-if="users_count>1 || user.role!='Admin'"><a href="" ng-click="deleteUser(user.ID)"
                                        class="text-muted btn-loading btn-text-loading"><i class="fa fa-trash"
                                            aria-hidden="true"></i> Delete</a></li>
                            </ul>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>

        <div class="text-center m-t-30 m-b-15" ng-if="user_list_no_data">
            <h3>No Users found</h3>
        </div>

        <?php include_once 'user-single.php'; ?>

    </div>

    <div class="content-loader" content-loader=""></div>

    <?php include_once 'edit-user.php'; ?>
</div>