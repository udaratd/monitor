<div class="model-wrapper" id="model-edit-user" ng-controller="UserCtrl">
    <div class="model-dialog model-medium-small">

        <div class="model-header">
            <div class="model-closer"><a href="/settings/users">&times;</a></div>
            <h3 class="text-center">Edit User</h3>
        </div>

        <div class="model-content">
            <form name="formUpdateUser" novalidate ng-submit="updateUser(updUser)">
                <div class="row">
                    <div class="col-md-6 col-sm-12">

                        <div class="row half-gap">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="upd-user-first-name">First Name *</label>
                                    <input class="form-control" type="text" ng-model="updUser.firstName" placeholder="ex: Saman"
                                        id="upd-user-first-name" required>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="upd-user-last-name">Last Name *</label>
                                    <input class="form-control" type="text" ng-model="updUser.lastName" placeholder="ex: Perera"
                                        id="upd-user-last-name" required>
                                </div>
                            </div>
                        </div>
                        <div class="row half-gap">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="upd-user-nic">NIC Number *</label>
                                    <input class="form-control" type="text" ng-model="updUser.nic" placeholder="ex: 849775645V"
                                        id="upd-user-nic" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="upd-user-phone">Phone Number</label>
                                    <input class="form-control" type="text" ng-model="updUser.phone" placeholder="ex: 0777547894"
                                        id="upd-user-phone">
                                </div>
                            </div>
                        </div>
                        <div class="form-group" ng-class="{'has-error': formUpdateUser.$error.email}">
                            <label>Email *</label>
                            <input class="form-control" type="email" ng-model="updUser.email" placeholder="ex: kamal@gmail.com"
                                required>
                        </div>

                        <div class="row half-gap">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="upd-user-password">New Password (If you are willing to change)</label>
                                    <input class="form-control" type="password" ng-model="updUser.password" id="upd-user-password">
                                </div>
                            </div>
                            <!--                            <div class="col-sm-7" style="padding-top: 22px;">
                                <a href="#" class="btn btn-dark disabled">Auto Fill</a>
                            </div>-->
                        </div>
                        <div class="row half-gap">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="upd-user-role">Job Role</label>
                                    <input class="form-control" type="text" ng-model="upd_role_name" placeholder="Staff"
                                        id="upd-user-role" disabled>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group" ng-if="updUser.role == '3'">
                                    <label for="upd-user-supervisor">Supervisor</label>
                                    <div class="search-selection">
                                        <input type="text" id="upd-user-supervisor" value="{{updUser.supervisorName}}"
                                            placeholder="Supervisor Name" class="form-control output" readonly>
                                        <div class="search-dropdown">
                                            <input type="text" placeholder="Search.." ng-model="filter_superviser_by_name"
                                                class="form-control search-filter">
                                            <ul class="result-list scroll-container">
                                                <li ng-repeat="supervisor in supervisor_list | filter:filter_superviser_by_name">
                                                    <label for="chk-sup-{{supervisor.ID}}">
                                                        <span>{{supervisor.firstName}} {{supervisor.lastName}}</span>
                                                        <input type="radio" id="chk-sup-{{supervisor.ID}}" ng-model="updUser.supervisor_id"
                                                            name="user_supervisor[]" value="{{supervisor.ID}}" required>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="checkpoint-description">Notes</label>
                            <textarea class="form-control no-resize" id="user-note" ng-model="updUser.note" placeholder="Add Note"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="upd-user-photo">Upload Staff Photo</label>
                            <div class="droppable-area" style="height: 145px;">
                                <!--                                <h3 class="uploader-title m-t-0 m-b-0 text-left">Drop files here to <br>start uploading</h3>
                                <h3 class="m-t-0 m-b-0 p-l-25 p-r-25">or</h3>-->

                                <div class="uploader-wrapper">
                                    <input type="file" ng-model="updUser.imageString" app-file-model="" id="upd-user-photo"
                                        accept="image/*">
                                    <img src="{{upd_image_prev}}" alt="Preview" class="prev-thumb" ng-if="upd_image_prev != null && upd_image_prev != '' && upd_image_prev != undefined">
                                    <label for="upd-user-photo" class="btn btn-dark m-b-0">Select Photo</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row text-right">
                    <div class="col-md-10 col-sm-9">
                        <div class="response-message-text" id="msg-update-user"></div>
                    </div>
                    <div class="col-md-2 col-sm-3">
                        <button type="submit" ng-disabled="formUpdateUser.$invalid" class="btn btn-primary btn-loading"
                            id="btn-update-user"><span>Update</span></button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>