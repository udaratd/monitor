<!-- Push Content -->
<div class="push-content" id="user-single-drawer">
    <a href="" class="content-drawer"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
    
    <div class="scroll-container scroll-x-hidden p-r-15 p-l-10 full-height">
        <h3>Coming Soon..</h3>
<!--        <div class="row half-gap vertical-bottom">
            <div class="col-sm-2">
                <div class="single-image h-100 contain">
                    <img src="/assets/images/sample/staff-member-1.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-10">
                <div class="row vertical-bottom">
                    <div class="col-sm-9">
                        <h1 class="f-w-500 m-t-0 m-b-0">M.G Priyantha Perera</h1>
                        <p class="m-t-0 m-b-0 f-s-20">Staff</p>
                    </div>
                    <div class="col-sm-3 text-right">
                        <a href=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    </div>
                </div>
                <div class="line-seperator m-b-0 m-t-5"></div>
            </div>
        </div>

        <div class="tbl-data-view m-t-15">
            <table>
                <tbody>
                    <tr>
                        <td class="col-md-3 col-sm-4">NIC<span class="dash-seperator">:</span></td>
                        <td><span class="f-w-500">785898644V</span></td>
                    </tr>
                    <tr>
                        <td class="col-md-3 col-sm-4">Phone Number<span class="dash-seperator">:</span></td>
                        <td><span class="f-w-500">0778974567</span></td>
                    </tr>
                    <tr>
                        <td class="col-md-3 col-sm-4">Job Role<span class="dash-seperator">:</span></td>
                        <td><span class="f-w-500">Staff</span></td>
                    </tr>
                    <tr>
                        <td class="col-md-3 col-sm-4">Assigned Checkpoints<span class="dash-seperator">:</span></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="row clear-after-3">

                                <div class="col-sm-4">
                                    <div class="checkpoint-card">
                                        <h3 class="title f-w-500 m-t-0">1st Floor Room 1</h3>
                                        <ul class="checkpoint-list">
                                            <li>Bed Sheet Laying</li>
                                            <li>TV Table Cleaning</li>
                                            <li>Table Cleaning</li>
                                            <li>Toilet Cleaning</li>
                                            <li>Window Cleaning</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="checkpoint-card">
                                        <h3 class="title f-w-500 m-t-0">1st Floor Room 2</h3>
                                        <ul class="checkpoint-list">
                                            <li>Bed Sheet Laying</li>
                                            <li>TV Table Cleaning</li>
                                            <li>Table Cleaning</li>
                                            <li>Toilet Cleaning</li>
                                            <li>Window Cleaning</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="checkpoint-card">
                                        <h3 class="title f-w-500 m-t-0">1st Floor Room 3</h3>
                                        <ul class="checkpoint-list">
                                            <li>Bed Sheet Laying</li>
                                            <li>TV Table Cleaning</li>
                                            <li>Table Cleaning</li>
                                            <li>Toilet Cleaning</li>
                                            <li>Window Cleaning</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="checkpoint-card">
                                        <h3 class="title f-w-500 m-t-0">2nd Floor Room 1</h3>
                                        <ul class="checkpoint-list">
                                            <li>Bed Sheet Laying</li>
                                            <li>TV Table Cleaning</li>
                                            <li>Table Cleaning</li>
                                            <li>Toilet Cleaning</li>
                                            <li>Table Cleaning</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="checkpoint-card">
                                        <h3 class="title f-w-500 m-t-0">2nd Floor Room 1</h3>
                                        <ul class="checkpoint-list">
                                            <li>Bed Sheet Laying</li>
                                            <li>TV Table Cleaning</li>
                                            <li>Table Cleaning</li>
                                            <li>Toilet Cleaning</li>
                                            <li>Window Cleaning</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="checkpoint-card">
                                        <h3 class="title f-w-500 m-t-0">2nd Floor Room 1</h3>
                                        <ul class="checkpoint-list">
                                            <li>Bed Sheet Laying</li>
                                            <li>TV Table Cleaning</li>
                                            <li>Table Cleaning</li>
                                            <li>Toilet Cleaning</li>
                                            <li>Window Cleaning</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>-->
    </div>
    
</div>