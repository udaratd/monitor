<div class="panel" ng-controller="UserCtrl" ng-init="initLog()">
    <?php include_once 'settings-header.php'; ?>
    <div class="content-wrapper p-t-15">

        <div class="page-toolbar m-t-0" ng-show="!user_log_no_data">

            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="form-group">
                        <div class="search-wrapper">
                            <input type="search" name="term" class="search-control  form-control" ng-model="filter_logs_by_keyword"
                                placeholder="Search by Name or Activity">
                            <span class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-8 text-right">
                    <div class="inline-item">
                        <div class="form-group">
                            <div class="calendar-component">
                                <button type="button" class="calendar-navigator nav-prepend"><i class="fa fa-angle-left"
                                        aria-hidden="true"></i></button>
                                <div class="calendar-trigger">
                                    <input type="text" class="form-control fld-datepicker" jq-date-range-picker=""
                                        ng-model="filter_userlog_by_date" value="" readonly="" ng-change="filterUserLog()">
                                </div>
                                <button type="button" class="calendar-navigator nav-append"><i class="fa fa-angle-right"
                                        aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="table-responsive" ng-if="!user_log_no_data">
            <table class="table table-bordered table-sortable table-condensed table-striped content-align-top">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Activity</th>
                        <th>Role</th>
                        <th>Date</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="log in user_log_list | filter:filter_logs_by_keyword">
                        <td>{{log.by_whome_name}}</td>
                        <td>{{log.activity}}</td>
                        <td>{{log.role}}</td>
                        <td>{{log.date | date}}</td>
                        <td>{{log.formatted_time}}</td>
                    </tr>

                </tbody>
            </table>
        </div>
        <div class="text-center m-t-30 m-b-15" ng-if="user_log_no_data">
            <h3>No Logs found</h3>
        </div>
        <list-pagination ng-if="!user_log_no_data" page-count="{{userlog_page_count}}" page-number="{{userlog_page_number}}"
            callback-fn="filterUserLog(null,page_number)"></list-pagination>
    </div>
</div>
<div class="content-loader" content-loader=""></div>
</div>