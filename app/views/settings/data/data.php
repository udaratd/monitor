<div class="panel" ng-controller="MainCtrl">

    <?php include_once '../settings-header.php'; ?>

    <div class="panel-title">

        <h2 class="m-b-0"><i class="fa fa-upload" aria-hidden="true"></i> Import Data</h2>
        <p class="import-hint-text">Please use sample document to import data</p>
        <div class="row m-b-30">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-3 col-sm-4" ng-controller="UserCtrl">
                        <div class="uploader-wrapper">
                            <input type="file" id="import-users-fld" ng-model="import.user" app-file-model=""
                                upload-mode="full" preview="false" accept=".xlsx, .xls, .csv" callback-fn="importUsers(import)"
                                required>
                            <label for="import-users-fld" class="btn btn-loading w-160" id="btn-import-users"><i class="fa fa-upload"
                                    aria-hidden="true"></i> Import Users</label>

                        </div>
                        <a href="/api/User/sample" class="block sample-doc-link" target="_blank"><i class="fa fa-info-circle"
                                aria-hidden="true"></i>Sample
                            Document</a>
                        <div class="response-message-text" id="msg-import-users"></div>
                    </div>

                    <div class="col-md-3 col-sm-4" ng-controller="CheckpointCtrl">
                        <div class="uploader-wrapper">
                            <input type="file" id="import-checkpoint-fld" ng-model="import.checkpoint" app-file-model=""
                                upload-mode="full" preview="false" accept=".xlsx, .xls, .csv" callback-fn="importCheckpoints(import)"
                                required>
                            <label for="import-checkpoint-fld" class="btn btn-loading" id="btn-import-checkpoints"><i
                                    class="fa fa-upload" aria-hidden="true"></i> Import Checkpoints</label>
                        </div>
                        <a href="/api/Checkpoint/sample" class="block sample-doc-link" target="_blank"><i class="fa fa-info-circle"
                                aria-hidden="true"></i>Sample
                            Document</a>
                        <div class="response-message-text" id="msg-import-checkpoints"></div>
                    </div>

                    <div class="col-md-3 col-sm-4" ng-controller="LocationCtrl">
                        <div class="uploader-wrapper">
                            <input type="file" id="import-location-fld" ng-model="import.location" app-file-model=""
                                upload-mode="full" preview="false" accept=".xlsx, .xls, .csv" callback-fn="importLocations(import)"
                                required>
                            <label for="import-location-fld" class="btn btn-loading" id="btn-import-location"><i class="fa fa-upload"
                                    aria-hidden="true"></i> Import Locations</label>
                        </div>
                        <a href="/api/Location/sample" class="block sample-doc-link" target="_blank"><i class="fa fa-info-circle"
                                aria-hidden="true"></i>Sample
                            Document</a>
                        <div class="response-message-text" id="msg-import-location"></div>
                    </div>
                </div>
            </div>
        </div>

        <h2><i class="fa fa-download" aria-hidden="true"></i> Export Data</h2>
        <div class="row m-b-30">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <a href="/api/User/export" target="_blank" class="btn btn-primary w-160">Export Users</a>
                        <div class="spacer visible-sm"></div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <a href="/api/Checkpoint/export" target="_blank" class="btn btn-primary w-160">Export
                            Checkpoints</a>
                        <div class="spacer visible-sm"></div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <a href="/api/Location/export" target="_blank" class="btn btn-primary w-160">Export Locations</a>
                        <div class="spacer visible-sm"></div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <a href="/api/Schedule/export" target="_blank" class="btn btn-primary w-160">Export Schedules</a>
                    </div>
                </div>
            </div>
        </div>

        <h3><i class="fa fa-qrcode" aria-hidden="true"></i> Checkpoint QR Code Export</h3>
        <div class="row m-b-30">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <a href="/api/Checkpoint/qr_export" target="_blank" class="btn btn-primary w-160">Export QR Codes</a>
                        <div class="spacer visible-sm"></div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <a href="/api/Checkpoint/feed_qr_export" target="_blank" class="btn btn-primary w-160">Feedback QR Codes</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>