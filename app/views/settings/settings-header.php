<div class="">
        <ul class="as-tabs m-l-15 m-r-15">
            <li><a href="/settings/users" active-settings-header=""><i class="fa fa-users" aria-hidden="true"></i> Users</a></li>
            <li><a href="/settings/locations" active-settings-header=""><i class="fa fa-location-arrow" aria-hidden="true"></i> Locations</a></li>
            <li><a href="/settings/data" active-settings-header=""><i class="fa fa-rocket" aria-hidden="true"></i> Import / Export Data</a></li>
            <li><a href="/settings/general" active-settings-header=""><i class="fa fa-cog" aria-hidden="true"></i> Account Setup</a></li>
            <li ng-if="logged_user_role == 1"><a href="/settings/log" active-settings-header=""><i class="fa fa-book" aria-hidden="true"></i>User Log</a></li>
        </ul>
        <div class="line-seperator"></div>
</div>