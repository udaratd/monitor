<div class="model-wrapper" id="model-reschedule" ng-controller="ScheduleCtrl" ng-init="initReschedule()">
    <div class="model-dialog model-medium">
        
        <div class="model-header">
            <div class="model-closer"><a href="/schedules">&times;</a></div>
            <h3 class="text-center">Reschedule Staff</h3>
        </div>
        
        <div class="model-content">
            <form name="formReschedule" ng-submit="reSchedule(reschedule)" novalidate>
                <div class="row">
                    
                    <div class="col-md-7">
                        <div class="panel panel-gap-25">
                            
                            <div class="form-group">
                                <div class="search-wrapper">
                                    <input type="search" ng-model="filter_users_by_keyword" class="search-control form-control" placeholder="Search by Staff..">
                                    <span class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></span>
                                </div>
                            </div>
                            
                            <div class="scroll-container max-h-350">
                                <ul class="vertical-linklist list-stripped list-clickable">
                                    <li ng-repeat="member in staff_list | filter:filter_users_by_keyword">
                                        <div class="row vertical-center">
                                            <div class="col-md-1 w-30">
                                                <span class="radio">
                                                    <input type="radio" value="{{member.ID}}" ng-model="reschedule.newUser_id" name="reschedule_staff" id="rdo-reuser-{{member.ID}}" required>
                                                    <label for="rdo-reuser-{{member.ID}}">&nbsp;</label>
                                                </span>
                                            </div>
                                            <div class="col-md-11">
                                                <div class="info-image">
                                                    <div class="image">
                                                        <img src="{{member.img_thumbnail}}" alt="{{member.firstName}} {{member.lastName}}" ng-if="member.img_thumbnail !== null">
                                                        <img src="/assets/images/user-default.jpg" alt="{{member.firstName}} {{member.lastName}}" ng-if="member.img_thumbnail === null">
                                                    </div>
                                                    <div class="content">
                                                        <div class="title f-w-500">{{member.firstName}} {{member.lastName}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            
                        </div> 
                        <div class="h-20 visible-sm"></div>
                    </div>
                    
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="reschedule_start_date">Start Date</label>
                            <div class="field-icon-wrapper">
                                <input class="form-control" type="text" name="reschedule_start_date" placeholder="22 Feb 2018" jq-date-picker="" ng-model="reschedule.startDate" id="reschedule_start_date" min-date="0" ng-disabled="reschedule.scheduleStatus == 0" ng-required="reschedule.scheduleStatus != 0">
                                <span class="field-icon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="reschedule_end_date">End Date</label>
                            <div class="field-icon-wrapper">
                                <input class="form-control" type="text" name="reschedule_end_date" placeholder="1 Mar 2018" jq-date-picker="" ng-model="reschedule.endDate" id="reschedule_end_date" min-date="0" ng-disabled="reschedule.scheduleStatus == 0" ng-required="reschedule.scheduleStatus != 0">
                                <span class="field-icon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                            </div>
                        </div>
                        
                        <div class="form-group m-t-30">
                            <span class="checkbox">
                                <input type="checkbox" ng-true-value="0" ng-false-value="1" ng-model="reschedule.scheduleStatus" id="reschedule-permanent">
                                <label for="reschedule-permanent">Permanent</label>
                            </span>
                        </div>
                    </div>
                    
                </div>
                
                <div class="row text-right">
                    <div class="col-md-9 col-sm-8">
                        <div class="response-message-text" id="msg-re-schedule"></div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <button type="submit" ng-disabled="formReschedule.$invalid" class="btn btn-primary btn-loading" id="btn-re-schedule"><span>Reschedule</span></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>