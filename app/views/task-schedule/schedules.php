<div class="panel" ng-controller="ScheduleCtrl" ng-init="init()">

    <div class="page-toolbar">

        <div class="row">

            <div class="col-md-10 col-sm-9">
                <div class="toolbar-inline">

                    <div class="inline-item w-220">
                        <div class="form-group">
                            <label>Search in schedules</label>
                            <div class="search-wrapper">
                                <input type="search" name="term" class="search-control form-control" ng-model="filter_schedule_list_by_keyword"
                                    placeholder="Search by Staff..">
                                <span class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="inline-item">
                        <div class="form-group">
                            <label for="schedule-location-1">Location 1</label>
                            <div class="search-selection w-160">
                                <input type="text" id="schedule-location-1" placeholder="All" class="form-control output"
                                    readonly>
                                <div class="search-dropdown">
                                    <input type="text" placeholder="Search.." ng-model="filter_schedule_by_location_level_1"
                                        class="form-control search-filter">
                                    <ul class="result-list scroll-container">
                                        <li><label ng-click="filterScheduleList('all')"><span>All</span></label></li>
                                        <li ng-repeat="location in location_list | filter:filter_schedule_by_location_level_1">
                                            <label ng-click="filterScheduleList(location.ID)">
                                                <span>{{location.name}}</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="inline-item hide" id="schedule-location-2-item">
                        <div class="form-group">
                            <label for="schedule-location-2">Location 2</label>
                            <div class="search-selection w-160">
                                <input type="text" id="schedule-location-2" placeholder="All" class="form-control output"
                                    readonly>
                                <div class="search-dropdown">
                                    <input type="text" placeholder="Search.." ng-model="filter_schedule_by_location_level_2"
                                        class="form-control search-filter">
                                    <ul class="result-list scroll-container">
                                        <li><label ng-click="filterScheduleList(null, 'all')"><span>All</span></label></li>
                                        <li ng-repeat="location in second_level_location_list | filter:filter_schedule_by_location_level_2">
                                            <label ng-click="filterScheduleList(null, location.ID)">
                                                <span>{{location.name}}</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="inline-item hide" id="schedule-checkpoint-item">
                        <div class="form-group">
                            <label for="schedule-checkpoint">Checkpoint</label>
                            <div class="search-selection w-160">
                                <input type="text" id="schedule-checkpoint" placeholder="All" class="form-control output"
                                    readonly>
                                <div class="search-dropdown">
                                    <input type="text" placeholder="Search.." ng-model="filter_schedule_by_checkpoint"
                                        class="form-control search-filter">
                                    <ul class="result-list scroll-container">
                                        <li><label ng-click="filterScheduleList(null, null, 'all')"><span>All</span></label></li>
                                        <li ng-repeat="checkpoint in checkpoint_list | filter:filter_schedule_by_checkpoint">
                                            <label ng-click="filterScheduleList(null, null, checkpoint.ID)">
                                                <span>{{checkpoint.name}}</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="inline-item">
                        <div class="form-group">
                            <label for="schedule-day-count">Show Until</label>
                            <div class="select-wrapper" id="schedule-day-count">
                                <select class="form-control" ng-model="filter_schedule_by_days" ng-change="filterScheduleList(null, null, null, filter_schedule_by_days)">
                                    <option value="7">7 Days</option>
                                    <option value="14" ng-selected="true">14 Days</option>
                                    <option value="21">21 Days</option>
                                    <option value="30">30 Days</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-2 col-sm-3 text-right p-t-22">
                <a href="/schedule/add" class="btn btn-primary">Add Schedule</a>
            </div>
        </div>

    </div>

    <div class="table-responsive" ng-if="!schedule_list_no_data">
        <table class="table table-bordered table-sortable table-condensed table-striped content-align-top">
            <thead>
                <tr>
                    <th>Staff</th>
                    <th>Checkpoints</th>
                    <th><a href="#">Date</a></th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Status</th>
                    <th ng-if="logged_user_role == 1 || logged_user_role == 2">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="schedule in schedule_list | filter:filter_schedule_list_by_keyword">
                    <td class="condensed-column">
                        <div class="info-image user">
                            <div class="image">
                                <img src="{{schedule.img_thumbnail}}" ng-if="schedule.img_thumbnail != null" alt="{{schedule.firstName}} {{schedule.lastName}}">
                                <img src="/assets/images/default-pic-mini.png" ng-if="schedule.img_thumbnail == null"
                                    alt="{{schedule.firstName}} {{schedule.lastName}}">
                            </div>
                            <div class="content">
                                <div class="title">{{schedule.firstName}} {{schedule.lastName}}</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <ul class="list-plain">
                            <li ng-repeat="schedule_item in schedule.schedule">
                                {{schedule_item.parentName}} <i class="fa fa-angle-right" aria-hidden="true"></i>
                                {{schedule_item.childName}} <i class="fa fa-angle-right" aria-hidden="true"></i>
                                {{schedule_item.checkpointName}}
                            </li>
                        </ul>
                    </td>
                    <td>
                        <ul class="list-plain w-space-normal w-150">
                            <li ng-repeat="schedule_item in schedule.schedule">{{schedule_item.repeat}}</li>
                        </ul>
                    </td>
                    <td>
                        <ul class="list-plain">
                            <li ng-repeat="schedule_item in schedule.schedule">{{schedule_item.startTime}}</li>
                        </ul>
                    </td>
                    <td>
                        <ul class="list-plain">
                            <li ng-repeat="schedule_item in schedule.schedule">{{schedule_item.endTime}}</li>
                        </ul>
                    </td>
                    <td>
                        <a href="#" ng-click="prepareReschedule(schedule)" ng-if="schedule.rescheduled != true" class="no-wrap"><i
                                class="fa fa-clock-o m-r-5" aria-hidden="true"></i> Reschedule</a>
                        <span class="text-danger no-wrap" ng-if="schedule.rescheduled == true"><i class="fa fa-clock-o m-r-5"
                                aria-hidden="true"></i> Rescheduled</span>
                        <span class="block text-danger f-s-11 m-t-5 p-l-20 no-wrap" ng-if="schedule.rescheduledStartTime != null">From
                            {{schedule.rescheduledStartTime}}<br>To {{schedule.rescheduledEndTime}}</span>
                    </td>
                    <td ng-if="logged_user_role == 1 || logged_user_role == 2">
                        <ul class="list-plain">
                            <li ng-repeat="schedule_item in schedule.schedule">
                                <ul class="horizontal-action-group">
                                    <!-- <li><a href="" ng-click="prepareUserUpdate(user.ID)"><i class="fa fa-pencil-square-o"
                                                aria-hidden="true"></i> View / Edit</a></li> -->
                                    <!--                                <li><a href="" drawer-execute="user-single-drawer" class="disabled"><i class="fa fa-eye" aria-hidden="true"></i> View</a></li>-->
                                    <li><a href="" ng-click="deleteSchedule(schedule_item.ID)" class="text-muted btn-loading btn-text-loading"><i
                                                class="fa fa-trash" aria-hidden="true"></i> Delete</a></li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>

    <div class="text-center m-t-30 m-b-15" ng-if="schedule_list_no_data">
        <h3>No Schedules found</h3>
    </div>

    <div class="content-loader" content-loader=""></div>

    <?php include_once 'reschedule.php'; ?>

</div>