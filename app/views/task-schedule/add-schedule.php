<div class="panel" ng-controller="ScheduleCtrl" ng-init="initAddSchedule()">

    <form name="formAddSchedule" novalidate ng-submit="addSchedule(schedule)">
        <div class="row">

            <div class="col-md-4">
                <h2 class="m-b-10">User</h2>
                <div class="panel min-h-670-md">
                    <div class="form-group">
                        <div class="search-wrapper">
                            <input type="search" class="search-control form-control" ng-model="filter_users_by_keyword"
                                placeholder="Search by Staff..">
                            <span class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></span>
                        </div>
                    </div>

                    <div class="scroll-container scroll-x-hidden max-h-600">
                        <ul class="vertical-linklist list-stripped">

                            <li ng-repeat="member in staff_list | filter:filter_users_by_keyword">
                                <div class="row vertical-center">
                                    <div class="col-md-1 w-30">
                                        <span class="radio">
                                            <input type="radio" value="{{member.ID}}" ng-model="schedule.user_id" name="schedule_staff"
                                                id="rdo-user-{{member.ID}}" required>
                                            <label for="rdo-user-{{member.ID}}">&nbsp;</label>
                                        </span>
                                    </div>
                                    <div class="col-md-11">
                                        <div class="info-image">
                                            <div class="image">
                                                <img src="{{member.img_thumbnail}}" alt="{{member.firstName}} {{member.lastName}}"
                                                    ng-if="member.img_thumbnail !== null">
                                                <img src="/assets/images/user-default.jpg" alt="{{member.firstName}} {{member.lastName}}"
                                                    ng-if="member.img_thumbnail === null">
                                            </div>
                                            <div class="content">
                                                <div class="title f-w-500">{{member.firstName}} {{member.lastName}}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <h2 class="m-b-10">Craft</h2>
                <div class="panel min-h-670-md">
                    <div class="row half-gap hide">
                        <div class="col-sm-6">
                            <div drag-drop="drop" class="droppable-area h-150">
                                <p class="f-w-500">Drag & Drop Staff <br>Members Here</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="droppable-area h-150">
                                <p class="f-w-500">Drag & Drop Staff <br>Members Here</p>
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal m-t-30">
                        <div class="form-group">
                            <label for="schedule-start-time" class="col-sm-4">Start Time</label>
                            <div class="col-sm-8">
                                <input class="app-time-picker" type="text" ng-model="schedule.startTime"
                                    app-time-picker="" start-hour="1" start-min="0" time-period='{"am":1,"pm":1}'
                                    callback-fn="filterEndTime(filter_array)">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="schedule-end-time" class="col-sm-4">End Time</label>
                            <div class="col-sm-8 " id="schedule-end-time-wrapper">
                                <input id="schedule-end-input" class="app-time-picker" type="text" ng-model="schedule.endTime"
                                    app-time-picker="" start-hour="1" start-min="endTimeMin" time-period='{"am":1,"pm":1}'
                                    callback-fn="filterEndTime(filter_array)">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="schedule-repeat" class="col-sm-4">Repeat</label>
                            <div class="col-sm-8">
                                <div id="schedule-repeat" class="select-wrapper">
                                    <select class="form-control" ng-options="type.id as type.label for type in repeat_types"
                                        ng-model="schedule.repeatType_id" required></select>
                                </div>

                                <div ng-if="schedule.repeatType_id == '
                                    2'" class="m-t-30">
                                    <span class="checkbox inline-block">
                                        <input type="checkbox" ng-true-value="1" ng-false-value="0" ng-model="schedule.sunday"
                                            id="schedule-repeat-sun">
                                        <label for="schedule-repeat-sun">Sun</label>
                                    </span>

                                    <span class="checkbox inline-block">
                                        <input type="checkbox" ng-true-value="1" ng-false-value="0" ng-model="schedule.monday"
                                            id="schedule-repeat-mon">
                                        <label for="schedule-repeat-mon">Mon</label>
                                    </span>

                                    <span class="checkbox inline-block">
                                        <input type="checkbox" ng-true-value="1" ng-false-value="0" ng-model="schedule.tuesday"
                                            id="schedule-repeat-tue">
                                        <label for="schedule-repeat-tue">Tue</label>
                                    </span>

                                    <span class="checkbox inline-block">
                                        <input type="checkbox" ng-true-value="1" ng-false-value="0" ng-model="schedule.wednesday"
                                            id="schedule-repeat-wed">
                                        <label for="schedule-repeat-wed">Wed</label>
                                    </span>

                                    <span class="checkbox inline-block">
                                        <input type="checkbox" ng-true-value="1" ng-false-value="0" ng-model="schedule.thursday"
                                            id="schedule-repeat-thu">
                                        <label for="schedule-repeat-thu">Thu</label>
                                    </span>

                                    <span class="checkbox inline-block">
                                        <input type="checkbox" ng-true-value="1" ng-false-value="0" ng-model="schedule.friday"
                                            id="schedule-repeat-fri">
                                        <label for="schedule-repeat-fri">Fri</label>
                                    </span>

                                    <span class="checkbox inline-block">
                                        <input type="checkbox" ng-true-value="1" ng-false-value="0" ng-model="schedule.saturday"
                                            id="schedule-repeat-sat">
                                        <label for="schedule-repeat-sat">Sat</label>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" ng-if="schedule.repeatType_id == '3'">
                            <label for="schedule-one-time" class="col-sm-4">One Time Date</label>
                            <div class="col-sm-8">
                                <div class="field-icon-wrapper">
                                    <input class="form-control" type="text" ng-model="schedule.oneTimeDate" placeholder="22 Feb 2018"
                                        jq-date-picker="" min-date="0" id="schedule-one-time">
                                    <span class="field-icon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="schedule-note" class="col-sm-4">Description</label>
                            <div class="col-sm-8">
                                <textarea class="form-control no-resize" ng-model="schedule.description" placeholder="Add Note"
                                    id="schedule-note"></textarea>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-8 col-sm-7">
                            <div class="response-message-text" id="msg-add-schedule"></div>
                        </div>
                        <div class="col-md-4 col-sm-5 text-right">
                            <button type="submit" ng-disabled="formAddSchedule.$invalid" class="btn btn-primary btn-loading"
                                id="btn-add-schdule"><span>Save</span></button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <h2 class="m-b-10">Checkpoints</h2>
                <div class="panel min-h-670-md">

                    <div class="row quarter-gap" ng-show="!checkpoint_list_no_data">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="search-wrapper">
                                    <input type="search" class="search-control form-control" ng-model="filter_checkpoints_by_keyword"
                                        placeholder="Search by Checkpoint..">
                                    <span class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="select-wrapper disabled">
                                <select class="form-control">
                                    <option value="">All</option>
                                    <option value="">Unassigned</option>
                                    <option value="">Assigned</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="scroll-container scroll-x-hidden max-h-600" ng-if="!checkpoint_list_no_data">
                        <div class="row quarter-gap">

                            <div class="col-sm-12" ng-repeat="checkpoint in checkpoint_list | filter:filter_checkpoints_by_keyword">
                                <div class="strip-card-checkpoint">
                                    <h2>{{checkpoint.checkpointName}}</h2>
                                    <p>{{checkpoint.location_parentName}} > {{checkpoint.location_childName}}</p>
                                    <input type="radio" ng-value="checkpoint.checkpointID" ng-model="schedule.checkpoint_id"
                                        class="tst-rdo-ab-top-rt" name="schedule_checkpoint[]" required>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="m-t-30 m-b-15" ng-if="checkpoint_list_no_data">
                        <h3>No Checkpoints found</h3>
                    </div>

                </div>
            </div>

        </div>
    </form>

</div>