<div class="model-wrapper" id="model-edit-checkpoint" ng-controller="CheckpointCtrl">
    <div class="model-dialog model-medium-small">
        
        <div class="model-header">
            <div class="model-closer"><a href="/checkpoints">&times;</a></div>
            <h3 class="text-center">Edit Checkpoint</h3>
        </div>
        
        <div class="model-content">
            <form name="formUpdateCheckpoint" novalidate ng-submit="updateCheckpoint(updCheckpoint)">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="upd-checkpoint-name">Checkpoint Name *</label>
                            <input class="form-control" type="text" ng-model="updCheckpoint.checkName" placeholder="ex: Bed Sheet Laying" id="upd-checkpoint-name" required>
                        </div>
                        
                        <div class="form-group">
                            <label for="upd-checkpoint-location">Location *</label>
<!--                            <div class="search-selection">
                                <input type="text" id="upd-checkpoint-location" placeholder="--Select Location--" class="form-control output" readonly>
                                <div class="search-dropdown">
                                    <input type="text" placeholder="Search.." ng-model="filter_locations" class="form-control search-filter">
                                    <ul class="result-list scroll-container">
                                        <li ng-repeat="location in location_list | filter:filter_locations">
                                            <label for="chk-loc-{{location.ID}}">
                                                <span>{{location.name}}</span>
                                                <input type="radio" id="chk-loc-{{location.ID}}" ng-model="updCheckpoint.locationid" name="checkpoint_location[]" value="{{location.ID}}" required>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>-->
                            
                            <div class="select-wrapper" id="upd-checkpoint-location">
                                <select ng-model="updCheckpoint.locationid" class="form-control" required>
                                    <option ng-repeat="location in location_list" value="{{location.ID}}">{{location.name}}</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="upd-checkpoint-description">Description</label>
                            <textarea class="form-control no-resize" id="upd-checkpoint-description" ng-model="updCheckpoint.description" placeholder="Add Note"></textarea>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12">   
                        <div class="form-group">
                            <label for="upd-checkpoint-sample-photo">Upload Sample Photo</label>
                            <div class="droppable-area content-horizontal" style="height: 255px;">
<!--                                <img src="/assets/images/upload.png" class="pic-default" alt="">
                                <h3 class="uploader-title m-b-5">Drop files here to start uploading</h3>
                                <h3 class="m-t-0 m-b-10">or</h3>-->

                                <div class="uploader-wrapper">
                                    <input type="file" id="upd-checkpoint-sample-photo" ng-model="updCheckpoint.image" app-file-model="" accept="image/*">
                                    <img src="{{upd_image_prev}}" alt="Preview" class="prev-thumb" ng-if="upd_image_prev != null && upd_image_prev != '' && upd_image_prev != undefined">
                                    <label for="upd-checkpoint-sample-photo" class="btn btn-dark m-b-0">Select Photo</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row text-right">
                    <div class="col-md-8 col-sm-7">
                        <div class="response-message-text" id="msg-update-checkpoint"></div>
                    </div>
                    <div class="col-md-4 col-sm-5">
                        <button type="submit" ng-disabled="formUpdateCheckpoint.$invalid" class="btn btn-primary btn-loading" id="btn-update-checkpoint"><span>Update</span></button>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
</div>