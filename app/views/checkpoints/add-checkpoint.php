<div class="model-wrapper" id="model-add-checkpoint" ng-controller="CheckpointCtrl" ng-init="initAddCheckpoint()">
    <div class="model-dialog model-medium-small">

        <div class="model-header">
            <!-- <div class="model-closer"><a href="/checkpoints">&times;</a></div> -->
            <div class="model-closer2"><a href="" ng-click="hideAddCheckpoint()">&times;</a></div>

            <h3 class="text-center">New Checkpoint</h3>
        </div>

        <div class="model-content">
            <form name="formAddCheckpoint" novalidate ng-submit="addCheckpoint(checkpoint)">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="checkpoint-name">Checkpoint Name *</label>
                            <input class="form-control" type="text" ng-model="checkpoint.checkName" placeholder="ex: Bed Sheet Laying"
                                id="checkpoint-name" required>
                        </div>

                        <div class="form-group">
                            <label for="checkpoint-location">Location *</label>
                            <div class="search-selection">
                                <input type="text" id="checkpoint-location" placeholder="--Select Location--" class="form-control output"
                                    readonly>
                                <div class="search-dropdown">
                                    <input type="text" placeholder="Search.." ng-model="filter_locations" class="form-control search-filter">
                                    <ul class="result-list scroll-container">
                                        <li ng-repeat="location in location_list | filter:filter_locations">
                                            <label for="chk-loc-{{location.ID}}">
                                                <span>{{location.name}}</span>
                                                <input type="radio" id="chk-loc-{{location.ID}}" ng-model="checkpoint.locationid"
                                                    name="checkpoint_location[]" value="{{location.ID}}" required>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="checkpoint-description">Description</label>
                            <textarea class="form-control no-resize" id="checkpoint-description" ng-model="checkpoint.description"
                                placeholder="Add Note"></textarea>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="checkpoint-sample-photo">Upload Sample Photo</label>
                            <div class="droppable-area content-horizontal" style="height: 255px;">
                                <!--                                <img src="/assets/images/upload.png" class="pic-default" alt="">
                                <h3 class="uploader-title m-b-5">Drop files here to start uploading</h3>
                                <h3 class="m-t-0 m-b-10">or</h3>-->

                                <div class="uploader-wrapper">
                                    <input type="file" id="checkpoint-sample-photo" ng-model="checkpoint.image"
                                        app-file-model="" accept="image/*">
                                    <label for="checkpoint-sample-photo" class="btn btn-dark m-b-0">Select Photo</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row text-right">
                    <div class="col-md-8 col-sm-7">
                        <div class="response-message-text" id="msg-add-checkpoint"></div>
                    </div>
                    <div class="col-md-4 col-sm-5">
                        <!--                    <a href="/checkpoints/add/success" class="btn btn-primary">Done & View QR Code</a>-->
                        <button type="submit" ng-disabled="formAddCheckpoint.$invalid" class="btn btn-primary btn-loading"
                            id="btn-add-checkpoint"><span>Save</span></button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>