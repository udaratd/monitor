<div class="model-wrapper" id="model-checkpoint-success">

    <div class="model-dialog model-medium-small">
        <div class="model-header">
            <div class="model-closer"><a href="/checkpoints">&times;</a></div>
            <h3 class="text-center">Checkpoint Saved Successfully</h3>
        </div>
        
        <div class="model-content">
            
            <div class="checkpoint-success-card">
                <div class="row">
                    <div class="col-md-4">
                        <div class="single-image">
                            <img src="/assets/images/QR-code-sample.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="tbl-data-view">
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="col-sm-4">Checkpoint<span class="dash-seperator">-</span></td>
                                        <td>Bed</td>
                                    </tr>
                                    <tr>
                                        <td class="col-sm-4">Location<span class="dash-seperator">-</span></td>
                                        <td>1st Floor > room 1</td>
                                    </tr>
                                    <tr>
                                        <td class="col-sm-4">Staff<span class="dash-seperator">-</span></td>
                                        <td>Eranga Supun</td>
                                    </tr>
                                    <tr>
                                        <td class="col-sm-4">Supervisor<span class="dash-seperator">-</span></td>
                                        <td>Darshana Perera</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="text-right m-t-20">
                <a href="" class="btn btn-primary">Schedule</a>
                <a href="" class="btn btn-light">Print</a>
            </div>
            
        </div>
    </div>
</div>