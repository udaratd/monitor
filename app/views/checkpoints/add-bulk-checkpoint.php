<div class="model-wrapper in" id="model-add-bulk-checkpoint" ng-show="showBulkCheckpointPop">
    <div class="model-dialog model-mini">

        <div class="model-header">
            <div class="model-closer2"><a href="" ng-click="showBulkCheckpointPop = false">&times;</a></div>
            <h3 class="text-center m-b-25">Add Bulk Checkpoints</h3>
        </div>

        <div class="model-content">
            <form name="formAddBulkCheckpoints" novalidate ng-submit="addBulkCheckpoints(checkpoint)">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="checkpoint-count">Number of checkpoints *</label>
                            <span class="field-hint">You can just add a set of checkpoints and they will automatically assign to a default location</span>
                            <input class="form-control" type="text" ng-model="checkpoint.count" id="checkpoint-count" placeholder="100" ng-pattern="/^([1-9][0-9]{0,2}|1000)$/" required> <!-- Pattern validates numbers between 1-1000 -->
                        </div>
                    </div>
                </div>

                <div class="row text-right">
                    <div class="col-md-9 col-sm-8">
                        <div class="response-message-text" id="msg-add-bulk-checkpoints"></div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <button type="submit" ng-disabled="formAddBulkCheckpoints.$invalid" class="btn btn-primary btn-loading" id="btn-add-bulk-checkpoints"><span>Add</span></button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>