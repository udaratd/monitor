<div class="panel" ng-controller="CheckpointCtrl" ng-init="init()">

    <div class="page-toolbar">

        <div class="row">
            <div class="col-md-3 col-sm-4 hidden-sm">
                <div class="form-group">
                    <div class="search-wrapper">
                        <input type="search" name="term" ng-model="filter_checkpoints_by_keyword" class="search-control form-control"
                            placeholder="Search..">
                        <span class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></span>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-5">
                <div class="form-group group-inline">
                    <label for="checkpoint-location">Location</label>
                    <div class="search-selection">
                        <input type="text" id="checkpoint-location" placeholder="All" class="form-control output"
                            readonly>
                        <div class="search-dropdown">
                            <input type="text" placeholder="Search.." ng-model="filter_locations" class="form-control search-filter">
                            <ul class="result-list scroll-container">
                                <li><label ng-click="filterCheckpointList('All',null)"><span>All</span></label></li>
                                <li ng-repeat="location in location_list | filter:filter_locations">
                                    <label ng-click="filterCheckpointList(location.ID,null)"><span>{{location.name}}</span></label>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-5 col-sm-7 text-right">
                <a href="" ng-click="showAddCheckpoint()" class="btn btn-primary">Add Checkpoint</a>
                <a href="" ng-click="showBulkCheckpointPop = true" class="btn btn-primary">Add Bulk Checkpoints</a>
            </div>
        </div>

    </div>

    <div class="row half-gap" ng-if="!checkpoint_list_no_data">

        <div ng-repeat="checkpoint in checkpoint_list | filter:filter_checkpoints_by_keyword" class="col-md-5ths col-sm-4">
            <div class="hover-box">
                <a href="" ng-click="getSingleCheckpoint(checkpoint.checkPID, $event)">
                    <div class="bg-image">
                        <img src="{{checkpoint.img_medium}}" ng-if="checkpoint.img_medium != null" class="animate-item"
                            alt="{{checkpoint.img_medium}}">
                        <img src="/assets/images/default-pic-medium.png" ng-if="checkpoint.img_medium == null" class="animate-item"
                            alt="{{checkpoint.checkPName}}">
                    </div>
                    <div class="gradient-overlay"><i class="fa fa-eye" aria-hidden="true"></i></div>
                    <div class="content">
                        <h3 class="title">{{checkpoint.checkPName}}</h3>
                        <p class="sub-title">{{checkpoint.Loc_parentName}} <i class="fa fa-angle-right" aria-hidden="true"></i>
                            {{checkpoint.Loc_childName}}</p>
                        <p class="sub-title">Last Activity - <span ng-if="checkpoint.staffFname != null">{{checkpoint.staffFname}}
                                {{checkpoint.staffLname}}</span><span ng-if="checkpoint.staffFname == null">N/A</span></p>
                    </div>
                </a>
                <a href="" ng-click="prepareCheckpointUpdate(checkpoint)" class="link-action animate-item"><i class="fa fa-pencil-square-o"
                        aria-hidden="true"></i> Edit</a>
            </div>
        </div>


    </div>

    <div class="text-center m-t-30 m-b-15" ng-if="checkpoint_list_no_data">
        <h3>No Checkpoints found</h3>
    </div>
    <list-pagination ng-if="!checkpoint_list_no_data" page-count="{{checkpoint_page_count}}" page-number="{{checkpoint_page_number}}"
        callback-fn="filterCheckpointList(null,page_number)"></list-pagination>
    <div class="content-loader" content-loader=""></div>

    <?php 
        include_once 'view-checkpoint.php';
        include_once 'edit-checkpoint.php';
        include_once 'add-bulk-checkpoint.php';
    ?>

</div>