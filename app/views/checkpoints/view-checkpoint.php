<div class="model-wrapper" id="model-checkpoint-view" ng-controller="CheckpointCtrl">
    <div class="model-dialog model-medium-small">
        
        <div class="model-header">
            <div class="model-closer"><a href="/checkpoints">&times;</a></div>
            <h3 class="text-center">Checkpoint - {{checkpoint_single.name}}</h3>
        </div>
        
        <div class="model-content">
            <div class="row">
                <div class="col-md-5">
                    <div class="single-image contain h-300 no-border">
                        <img src="{{checkpoint_single.img_large}}" alt="{{checkpoint_single.name}}" ng-if="checkpoint_single.img_large !== null">
                        <img src="/assets/images/default-pic-medium.png" alt="{{checkpoint_single.name}}" ng-if="checkpoint_single.img_large === null">
                    </div>
                    <div class="h-15 visible-sm visible-xs"></div>
                </div>
                <div class="col-md-7">
                    <div class="tbl-data-view">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="col-md-4 col-sm-5"><span class="text-light">Checkpoint</span><span class="dash-seperator">-</span></td>
                                    <td>{{checkpoint_single.name}}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-4 col-sm-5"><span class="text-light">Location</span><span class="dash-seperator">-</span></td>
                                    <td>{{checkpoint_single.p_location}} <i class="fa fa-angle-right" aria-hidden="true"></i> {{checkpoint_single.c_location}}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-4 col-sm-5"><span class="text-light">Description</span><span class="dash-seperator">-</span></td>
                                    <td><p class="text-justify">{{checkpoint_single.description}}</p></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="m-t-15">
                        <ul class="horizontal-action-group">
<!--                            <li><a href="" ng-click="prepareCheckpointUpdate(checkpoint_single)"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a></li>-->
                            <li><a href="" class="text-danger btn-loading btn-text-loading" ng-click="deleteCheckpoint(checkpoint_single.ID)" id="btn-delete-checkpoint"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a></li>
                        </ul>
                    </div>
                    
<!--                    <div class="text-right">
                        <a href="" class="btn btn-primary">Schedule</a>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>