<div class="panel" ng-controller="ReportCtrl" ng-init="initDashboard()">
    
    <div class="panel-title">
        <h1 class="f-w-500">Welcome to Hotel X Physical Monitoring System</h1>
    </div>
   
    <h2>This Week</h2>
    <div class="row">
        <div class="col-md-3 col-sm-6" ng-if="dashboard.completedTask.completedTaskCount != null && dashboard.completedTask.completedTaskCount != undefined">
            <div class="rr-strip">
                <div class="strip-content">
                    <div class="left">
                        <h2>Completed Tasks</h2>
                        <p class="hide">Previous Period 75</p>
                    </div>
                    <div class="right">
                        <div class="progress-circle progress-{{dashboard.completedTask.completedTaskCount}}"><span>{{dashboard.completedTask.completedTaskCount}}</span></div> 
                        <div class="strip-result-percent" ng-class="dashboard.completedTask.status == 'Positive' ? 'up' : 'down'" ng-if="dashboard.completedTask.status != null && dashboard.completedTask.status != undefined">{{dashboard.completedTask.percentage}}%</div>
                    </div>
                </div>
            </div>   
        </div>
        
        <div class="col-md-3 col-sm-6" ng-if="dashboard.ontime.onTimeTasksPercentage != null && dashboard.ontime.onTimeTasksPercentage != undefined">
            <div class="rr-strip">
                <div class="strip-content">
                    <div class="left">
                        <h2>On Time</h2>
                        <p class="hide">Previous Period 95%</p>
                    </div>
                    <div class="right">
                        <div class="progress-circle progress-{{dashboard.ontime.onTimeTasksPercentage}}"><span>{{dashboard.ontime.onTimeTasksPercentage}}%</span></div> 
                        <div class="strip-result-percent" ng-class="dashboard.ontime.status == 'Positive' ? 'up' : 'down'" ng-if="dashboard.ontime.status != null && dashboard.ontime.status != undefined">{{dashboard.ontime.percentage}}%</div>
                    </div>
                </div>
            </div>   
        </div>
        
        <div class="col-md-3 col-sm-6" ng-if="dashboard.reworkRate.reworkPercentage != null && dashboard.reworkRate.reworkPercentage != undefined">
            <div class="rr-strip">
                <div class="strip-content">
                    <div class="left">
                        <h2>Rework Rate</h2>
                        <p class="hide">Previous Period 15%</p>
                    </div>
                    <div class="right">
                        <div class="progress-circle progress-{{dashboard.reworkRate.reworkPercentage}}"><span>{{dashboard.reworkRate.reworkPercentage}}%</span></div> 
                        <div class="strip-result-percent" ng-class="dashboard.reworkRate.status == 'Positive' ? 'up' : 'down'" ng-if="dashboard.reworkRate.status != null && dashboard.reworkRate.status != undefined">{{dashboard.reworkRate.percentage}}%</div>
                    </div>
                </div>
            </div>   
        </div>
        
        <div class="col-md-3 col-sm-6 hide">
            <div class="rr-strip data-sample" title="Sample Data">
                <div class="strip-content">
                    <div class="left">
                        <h2>Avg Time to Clear Location</h2>
                    </div>
                    <div class="right">
                        <div class="progress-circle progress-60"><span>20 min</span></div> 
                        <div class="strip-result-percent up">60%</div>
                    </div>
                </div>
            </div>   
        </div>
    </div>
    
    <div class="row m-t-15">
        <div class="col-md-5 col-sm-12">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <tr class="row-heading">
                        <td><i class="fa fa-link text-themed m-r-10" aria-hidden="true"></i> <span class="text">Quick Links</span></td>
                    </tr>
                    <tr class="row-quicklinks">
                        <td><a href="/settings/users/add"><i class="fa fa-user-plus" aria-hidden="true"></i> <span class="text">Add Staff</span></a></td>
                    </tr>
                    <tr class="row-quicklinks">
                        <td><a href="/checkpoints/add"><i class="fa fa-map-marker" aria-hidden="true"></i> <span class="text">Add Checkpoint</span></a></td>
                    </tr>
                    <tr class="row-quicklinks">
                        <td><a href="/settings/location/add"><i class="fa fa-location-arrow" aria-hidden="true"></i> <span class="text">Add Location</span></a></td>
                    </tr>
                    <tr class="row-quicklinks">
                        <td><a href="/schedule/add"><i class="fa fa-tasks" aria-hidden="true"></i> <span class="text">Add Schedule</span></a></td>
                    </tr>
                </table>
            </div>
            <div class="visible-sm h-20"></div>
        </div>
        
        <div class="col-md-7 col-sm-12">
            <div class="scroll-container max-h-400 box-bordered">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr class="row-heading">
                                <td colspan="4"><i class="fa fa-repeat text-danger-great m-r-10" aria-hidden="true"></i> <span class="text">Reworked Checkpoints</span></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-if="dashboard.reworkedCheckpoint[0] !== null && dashboard.reworkedCheckpoint[0] !== undefined" ng-repeat="item in dashboard.reworkedCheckpoint">
                                <td><span class="f-w-500 f-s-16 no-wrap">{{item.checkpointName}}</span></td>
                                <td>{{item.location}}</td>
                                <td><span class="text-themed">{{item.staffName}}</span></td>
                                <td><span class="no-wrap">{{item.uploadServerDate}}</span></td>
                            </tr>
                            <tr ng-if="dashboard.reworkedCheckpoint[0] === null || dashboard.reworkedCheckpoint[0] === undefined">
                                <td colspan="4" class="condensed-column">No data found.</td>   
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</div>