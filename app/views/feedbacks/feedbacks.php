<div class="panel pg-worklist" ng-controller="FeedbackCtrl" ng-init="init()">
    
    <div class="page-toolbar m-t-0">

        <div class="row">
            <div class="col-md-3 col-sm-4">
                <div class="form-group">
                    <div class="search-wrapper">
                        <input type="search" name="term" class="search-control  form-control" ng-model="filter_feedback_by_keyword"
                            placeholder="Search">
                        <span class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></span>
                    </div>
                </div>
            </div>

            <div class="col-md-5 col-sm-5">
                <div class="form-group group-inline">
                    <label for="user-job-role">Reaction</label>
                    <div class="select-wrapper">
                        <select class="form-control" id="user-job-role" name="feedback_reaction" ng-options="reaction.id as reaction.label for reaction in feedback_reaction_list track by reaction.id"
                            ng-init="filter_feedback_by_reaction = feedback_reaction_list[0]" ng-model="filter_feedback_by_reaction"
                            ng-change="filterFeedbackList(filter_feedback_by_reaction)">                            
                        </select>
                    </div>
                </div>
            </div>

        </div>

    </div>
    
    <div class="table-responsive" ng-if="!feedback_list_no_data">
        <table class="table table-bordered table-sortable table-condensed table-striped content-align-top">
            <thead>
                <tr>
                    <th>Checkpoint</th>
                    <th><a href="#">Customer Name</a></th>
                    <th><a href="#">Reaction</a></th>
                    <th><a href="#">Phone</a></th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

                <tr ng-repeat="feedbacklist in feedback_list | filter:filter_feedback_by_keyword">
                    <td class="condensed-column">
                        <div class="info-image">
                            <div class="image">
                                <img src="{{feedbacklist.img_thumbnail}}" alt="{{feedbacklist.checkpoint_name}}" ng-if="feedbacklist.img_thumbnail !== null">
                                <img src="/assets/images/default-pic-mini.jpg" alt="{{feedbacklist.checkpoint_name}}" ng-if="feedbacklist.img_thumbnail === null">
                            </div>
                            <div class="content">
                                <div class="title">{{feedbacklist.checkpoint_name}}</div>
                                <div class="sub-title">
                                    {{feedbacklist.p_location}} <i class="fa fa-angle-right" aria-hidden="true"></i> {{feedbacklist.c_location}}
                                </div>
                            </div>
                        </div>
                    </td>

                    <td>{{feedbacklist.name}}</td>
                    <td>{{feedbacklist.reaction_txt}}</td>
                    <td><span class="text-themed">{{feedbacklist.phone}}</span></td>
                    <td>
                        <ul class="horizontal-action-group">
                            <li><a href="/feedback/{{feedbacklist.id}}"> View </a></li>
                        </ul>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
    
    <div class="text-center m-t-30 m-b-15" ng-if="feedback_list_no_data">
        <h3>No Feedback found</h3>
    </div>
    <list-pagination ng-if="!feedback_list_no_data" page-count="{{feedback_page_count}}" page-number="{{feedback_page_number}}"
        callback-fn="filterFeedbackList(null,page_number)"></list-pagination>
    <div class="content-loader" content-loader=""></div>
</div>