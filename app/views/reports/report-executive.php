<div class="panel" ng-controller="ReportCtrl" ng-init="initExecutive()">

    <div class="page-toolbar">
        <div class="row">
            <div class="col-md-10 col-sm-9">
                <div class="toolbar-inline">
                    
                    <div class="inline-item">
                        <div class="form-group">
                            <div class="select-wrapper">
                                <select class="form-control" ng-model="filter_change_report" ng-change="changeView(filter_change_report)">
                                    <option value="executive" ng-selected="true">Executive Summary Report</option>
                                    <option value="comparison">Comparison Report</option>
                                    <option value="rework">Process Rework Rate Report</option>
                                    <option value="customer">Customer Report</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="inline-item">
                        <div class="form-group">
                            <div class="calendar-component">
                                <div class="calendar-trigger">
                                    <input type="text" class="form-control fld-datepicker" jq-date-range-picker="" ng-model="filter_ex_report_by_date" value="" readonly="" ng-change="filterExecutiveReport(filter_ex_report_by_date); viewDateRange(filter_ex_report_by_date);">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- <div class="inline-item">
                        <div class="form-group">
                            <div class="select-wrapper">
                                <select class="form-control" ng-model="filter_ex_report_by_days" ng-change="filterExecutiveReport(filter_ex_report_by_days); viewDateRange(filter_ex_report_by_days);">
                                    <option value="7" ng-selected="true">Last 7 Days</option>
                                    <option value="14">Last 14 Days</option>
                                    <option value="21">Last 21 Days</option>
                                    <option value="30">Last 30 Days</option>
                                </select>
                            </div>
                        </div>
                    </div> -->
                    
                    <div class="inline-item hide">
                        <div class="form-group">
                            <div class="search-selection">
                                <input type="text" placeholder="All" class="form-control output" readonly>
                                <div class="search-dropdown">
                                    <input type="text" placeholder="Search.." ng-model="filter_ex_report_by_checkpoint" class="form-control search-filter">
                                    <ul class="result-list scroll-container">
                                        <li><label ng-click="filterExecutiveReport()"><span>All</span></label></li>
                                        <li ng-repeat="checkpoint in checkpoint_list | filter:filter_ex_report_by_checkpoint">
                                            <label ng-click="filterExecutiveReport(null, checkpoint.ID)"><span>{{checkpoint.name}}</span></label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
            
<!--            <div class="col-md-2 col-sm-3 text-right">
                <a href=""><i class="fa fa-print" aria-hidden="true"></i> Print</a>
            </div>-->
        </div>
    </div>
    
    <div class="panel-title">
        <h1 class="f-w-500 m-b-5">Executive Summary Report</h1>
        <span class="text-themed" ng-bind="date_range_view"></span>
    </div>
    
    <div class="row half-gap m-t-25">
        <div class="col-md-5ths col-sm-6" ng-if="executive.completedTask.completedTaskCount != null && executive.completedTask.completedTaskCount != undefined">
            <div class="rr-strip">
                <div class="strip-content">
                    <div class="left">
                        <h2>Completed Tasks</h2>
                    </div>
                    <div class="right">
                        <div class="progress-circle progress-{{executive.completedTask.completedTaskCount}}"><span>{{executive.completedTask.completedTaskCount}}</span></div> 
                        <div class="strip-result-percent" ng-class="executive.completedTask.status == 'Positive' ? 'up' : 'down'" ng-if="executive.completedTask.status != null && executive.completedTask.status != undefined">{{executive.completedTask.percentage}}%</div>
                    </div>
                </div>
            </div>   
        </div>
        
        <div class="col-md-5ths col-sm-6" ng-if="executive.ontime.onTimeTasksPercentage != null && executive.ontime.onTimeTasksPercentage != undefined">
            <div class="rr-strip">
                <div class="strip-content">
                    <div class="left">
                        <h2>On Time</h2>
                    </div>
                    <div class="right">
                        <div class="progress-circle progress-{{executive.ontime.onTimeTasksPercentage}}"><span>{{executive.ontime.onTimeTasksPercentage}}%</span></div> 
                        <div class="strip-result-percent" ng-class="executive.ontime.status == 'Positive' ? 'up' : 'down'" ng-if="executive.ontime.status != null && executive.ontime.status != undefined">{{executive.ontime.percentage}}%</div>
                    </div>
                </div>
            </div>   
        </div>
        
        <div class="col-md-5ths col-sm-6" ng-if="executive.reworkRate.reworkPercentage != null && executive.reworkRate.reworkPercentage != undefined">
            <div class="rr-strip">
                <div class="strip-content">
                    <div class="left">
                        <h2>Rework Rate</h2>
                    </div>
                    <div class="right">
                        <div class="progress-circle progress-{{executive.reworkRate.reworkPercentage}}"><span>{{executive.reworkRate.reworkPercentage}}%</span></div> 
                        <div class="strip-result-percent" ng-class="executive.reworkRate.status == 'Positive' ? 'up' : 'down'" ng-if="executive.reworkRate.status != null && executive.reworkRate.status != undefined">{{executive.reworkRate.percentage}}%</div>
                    </div>
                </div>
            </div>   
        </div>
        
        <div class="col-md-5ths col-sm-6 hide">
            <div class="rr-strip">
                <div class="strip-content">
                    <div class="left">
                        <h2>Avg Time to Finish</h2>
                    </div>
                    <div class="right">
                        <div class="progress-circle progress-60"><span>20 min</span></div> 
                        <div class="strip-result-percent up">60%</div>
                    </div>
                </div>
            </div>   
        </div>
        
        <div class="col-md-5ths col-sm-6" ng-if="executive.flaggedRate != null && executive.flaggedRate != undefined">
            <div class="rr-strip">
                <div class="strip-content">
                    <div class="left">
                        <h2>Flagged Rate</h2>
                    </div>
                    <div class="right">
                        <div class="progress-circle progress-{{executive.flaggedRate.flaggedPercentage}}" ng-if="executive.flaggedRate.flaggedPercentage != null && executive.flaggedRate.flaggedPercentage != undefined"><span>{{executive.flaggedRate.flaggedPercentage}}%</span></div> 
                        <div class="strip-result-percent" ng-class="executive.flaggedRate.status == 'Positive' ? 'up' : 'down'" ng-if="executive.flaggedRate.status != null && executive.flaggedRate.status != undefined">{{executive.flaggedRate.percentage}}%</div>
                    </div>
                </div>
            </div>   
        </div>
    </div>
    
    <div class="row m-t-10">
        <div class="col-sm-12 col-md-6">
            <h2 class="f-w-500">Top 10 Rework Staff</h2>
            
            <div class="scroll-container max-h-350 box-bordered">
                <div class="table-responsive">
                    <table class="table table-condensed table-striped content-align-top">
                        <thead>
                            <tr>
                                <th>Staff Member</th>
                                <th>Reworked Checkpoint Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-if="executive.reworkStaff[0] !== null && executive.reworkStaff[0] !== undefined" ng-repeat="staff in executive.reworkStaff">
                                <td class="condensed-column">
                                    <div class="info-image">
                                        <div class="image">
                                            <img src="{{staff.img_profile}}" alt="" ng-if="staff.img_profile != null">
                                            <img src="/assets/images/user-default.jpg" alt="" ng-if="staff.img_profile == null">
                                        </div>
                                        <div class="content">
                                            <div class="title">{{staff.fullName}}</div>
                                        </div>
                                    </div>
                                </td>                  
                                <td>{{staff.reworkCheckpointCount}}</td>    
                            </tr>
                            <tr ng-if="executive.reworkStaff[0] === null || executive.reworkStaff[0] === undefined">
                                <td colspan="2" class="condensed-column">No data found.</td>   
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
        
        <div class="col-sm-12 col-md-6">
            <h2 class="f-w-500">Top 10 Reworked Checkpoints</h2>
            
            <div class="scroll-container max-h-350 box-bordered">
                <div class="table-responsive">
                    <table class="table table-condensed table-striped content-align-top">
                        <thead>
                            <tr>
                                <th>Checkpoint</th>
                                <th>Reworked Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-if="executive.reworkCheckpoint[0] !== null && executive.reworkCheckpoint[0] !== undefined" ng-repeat="item in executive.reworkCheckpoint">
                                <td class="condensed-column">{{item.checkpointName}}</td>                  
                                <td>{{item.reworkCheckpointCount}}</td>    
                            </tr>
                            <tr ng-if="executive.reworkCheckpoint[0] === null || executive.reworkCheckpoint[0] === undefined">
                                <td colspan="2" class="condensed-column">No data found.</td>   
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </div>
    
    <div class="row m-t-20">
        <div class="col-sm-12 col-md-6">
            <h2 class="f-w-500">Late Submitted Checkpoints (Most Recent)</h2>
            
            <div class="scroll-container max-h-350 box-bordered">
                <div class="table-responsive">
                    <table class="table table-condensed table-striped content-align-top">
                        <thead>
                            <tr>
                                <th>Checkpoint</th>
                                <th>Time After Expiry</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-if="executive.lateSubmitedCheckpoint[0] !== null && executive.lateSubmitedCheckpoint[0] !== undefined" ng-repeat="item in executive.lateSubmitedCheckpoint">
                                <td class="condensed-column">{{item.checkpointName}}</td>                  
                                <td>{{item.timeDiff}}</td>    
                            </tr>
                            <tr ng-if="executive.lateSubmitedCheckpoint[0] === null || executive.lateSubmitedCheckpoint[0] === undefined">
                                <td colspan="2" class="condensed-column">No data found.</td>   
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <div class="col-sm-12 col-md-6">
            <h2 class="f-w-500">Checkpoints with more than 1 Rework</h2>
            
            <div class="scroll-container max-h-350 box-bordered">
                <div class="table-responsive">
                    <table class="table table-condensed table-striped content-align-top">
                        <thead>
                            <tr>
                                <th>Checkpoint</th>
                                <th>Reworks</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-if="executive.moreThanOneRework !== null && executive.moreThanOneRework !== undefined" ng-repeat="item in executive.moreThanOneRework">
                                <td class="condensed-column">{{item.fullName}}</td>                  
                                <td>{{item.reworkCount}}</td>    
                            </tr>
                            <tr ng-if="executive.moreThanOneRework[0] === null || executive.moreThanOneRework[0] === undefined">
                                <td colspan="2" class="condensed-column">No data found.</td>   
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    <div class="content-loader" content-loader=""></div>
    
</div>