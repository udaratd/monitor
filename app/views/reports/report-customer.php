<div class="panel" ng-controller="ReportCtrl" ng-init="initRepCustomer()">

    <div class="page-toolbar">
        <div class="row">
            <div class="col-md-10 col-sm-9">
                <div class="toolbar-inline">
                    
                    <div class="inline-item">
                        <div class="form-group">
                            <div class="select-wrapper">
                                <select class="form-control" ng-model="filter_change_report" ng-change="changeView(filter_change_report)">
                                    <option value="executive">Executive Summary Report</option>
                                    <option value="comparison">Comparison Report</option>
                                    <option value="rework">Process Rework Rate Report</option>
                                    <option value="customer" ng-selected="true">Customer Report</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="inline-item">
                        <div class="form-group">
                            <div class="calendar-component">
                                <div class="calendar-trigger">
                                    <input type="text" class="form-control fld-datepicker" jq-date-range-picker="" ng-model="filter_customer_report_by_date" value="" readonly="" ng-change="filterCustomerReport(filter_customer_report_by_date); viewDateRange(filter_customer_report_by_date);">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- <div class="inline-item">
                        <div class="form-group">
                            <div class="select-wrapper">
                                <select class="form-control" ng-model="filter_customer_report_by_days" ng-change="filterCustomerReport(filter_customer_report_by_days); viewDateRange(filter_customer_report_by_days);">
                                    <option value="7" ng-selected="true">Last 7 Days</option>
                                    <option value="14">Last 14 Days</option>
                                    <option value="21">Last 21 Days</option>
                                    <option value="30">Last 30 Days</option>
                                </select>
                            </div>
                        </div>
                    </div> -->
                    
                </div>
            </div>
            
            <div class="col-md-2 col-sm-3 text-right">
                
            </div>
        </div>
    </div>
    
    <div class="panel-title">
        <h1 class="f-w-500 m-b-5">Customer Report</h1>
        <span class="text-themed" ng-bind="date_range_view"></span>
    </div>
    
    <div class="box-bordered p-15 m-t-15" ng-if="rep_cus_checkpoint_data">
        
        <div class="m-b-10 bordered-group" ng-repeat="checkpoint_item in rep_cus_checkpoint_data">
            <h2 class="f-w-500 m-t-0">{{checkpoint_item.date}}</h2>
            <div class="row" ng-if="checkpoint_item.checkpoints[0] != undefined">
                <div class="col-sm-12 col-md-6 m-b-30" ng-repeat="checkpoint in checkpoint_item.checkpoints">
                    <h3 class="f-w-500 m-t-0 m-b-5">{{checkpoint.checkpointName}}</h3>
                    <h4 class="m-t-0 m-b-10">Location : {{checkpoint.location}}</h4>
                    <div class="image-group">
                        <div class="image-item" ng-if="checkpoint.sample != undefined">
                            <img src="{{checkpoint.sample}}" alt="">
                            <div class="caption">Sample</div>
                        </div>
                        <div class="image-item" ng-if="checkpoint.before != undefined">
                            <img src="{{checkpoint.before}}" alt="">
                            <div class="caption">Before</div>
                        </div>
                        <div class="image-item" ng-if="checkpoint.after != undefined">
                            <img src="{{checkpoint.after}}" alt="">
                            <div class="caption">After</div>
                        </div>
                    </div>
                </div>
            </div>
            <p ng-if="checkpoint_item.checkpoints[0] == undefined">No Checkpoints to show.</p> <!-- If checkpoints not found -->
        </div>
        
    </div>
    
    <p ng-if="!rep_cus_checkpoint_data">No data found.</p> <!-- If data not received -->
    
    <div class="content-loader" content-loader=""></div>
    
</div>