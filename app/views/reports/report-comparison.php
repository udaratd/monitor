<div class="panel" ng-controller="ReportCtrl" ng-init="initComparison()">

    <div class="page-toolbar">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="toolbar-inline">

                    <div class="inline-item">
                        <div class="form-group">
                            <div class="select-wrapper">
                                <select class="form-control" ng-model="filter_change_report" ng-change="changeView(filter_change_report)">
                                    <option value="executive">Executive Summary Report</option>
                                    <option value="comparison" ng-selected="true">Comparison Report</option>
                                    <option value="rework">Process Rework Rate Report</option>
                                    <option value="customer">Customer Report</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="inline-item">
                        <div class="form-group">
                            <div class="calendar-component">
                                <div class="calendar-trigger">
                                    <input type="text" class="form-control fld-datepicker" jq-date-range-picker="" ng-model="filter_comp_report_by_date" value="" readonly="" ng-change="filterComparisonReport(filter_comp_report_by_date); viewDateRange(filter_comp_report_by_date);">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="inline-item">
                        <div class="form-group">
                            <div class="select-wrapper">
                                <select class="form-control" ng-model="filter_comp_report_by_days" ng-change="filterComparisonReport(filter_comp_report_by_days); viewDateRange(filter_comp_report_by_days);">
                                    <option value="7" ng-selected="true">Last 7 Days</option>
                                    <option value="14">Last 14 Days</option>
                                    <option value="21">Last 21 Days</option>
                                    <option value="30">Last 30 Days</option>
                                </select>
                            </div>
                        </div>
                    </div> -->

                    <div class="inline-item">
                        <div class="form-group group-inline">
                            <label>Staff 1</label>
                            <!-- <div class="search-selection">
                                <input type="text" id="checkpoint-location" placeholder="--Select Location--" class="form-control output"
                                    readonly>
                                <div class="search-dropdown">
                                    <input type="text" placeholder="Search.." ng-model="filter_locations" class="form-control search-filter">
                                    <ul class="result-list scroll-container">
                                        <li ng-repeat="location in location_list | filter:filter_locations">
                                            <label for="chk-loc-{{location.ID}}">
                                                <span>{{location.name}}</span>
                                                <input type="radio" id="chk-loc-{{location.ID}}" ng-model="checkpoint.locationid"
                                                    name="checkpoint_location[]" value="{{location.ID}}" required>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div> -->

                            <div class="search-selection">
                                <input type="text" placeholder="Ex: Priyantha Perera" class="form-control output" value="{{staff_list[0].firstName}} {{staff_list[0].lastName}}"
                                    readonly>
                                <div class="search-dropdown">
                                    <input type="text" placeholder="Search.." ng-model="filter_comp_report_by_staff_1"
                                        class="form-control search-filter">
                                    <ul class="result-list scroll-container">
                                        <li ng-repeat="member in staff_list | filter:filter_comp_report_by_staff_1"
                                            ng-if="filterParams.staff_2!=member.ID">
                                            <label ng-click="filterComparisonReport(null, member.ID, null)">
                                                <span>{{member.firstName}} {{member.lastName}}</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="inline-item">
                        <div class="form-group group-inline">
                            <label>Staff 2</label>
                            <div class="search-selection">
                                <input type="text" placeholder="Ex: Priyantha Perera" value="{{staff_list[1].firstName}} {{staff_list[1].lastName}}"
                                    class="form-control output" readonly>
                                <div class="search-dropdown">
                                    <input type="text" placeholder="Search.." ng-model="filter_comp_report_by_staff_2"
                                        class="form-control search-filter">
                                    <ul class="result-list scroll-container">
                                        <li ng-repeat="member in staff_list | filter:filter_comp_report_by_staff_2"
                                            ng-if="filterParams.staff_1!=member.ID">
                                            <label ng-click="filterComparisonReport(null, null, member.ID)">
                                                <span>{{member.firstName}} {{member.lastName}}</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!-- <div class="col-md-2 col-sm-3 text-right">

            </div> -->
        </div>
    </div>

    <div class="panel-title">
        <h1 class="f-w-500 m-b-5">Comparison Report</h1>
        <span class="text-themed" ng-bind="date_range_view"></span>
    </div>

    <div class="row m-t-25">
        <div class="col-md-3 col-sm-6">
            <div class="info-image user">
                <div class="image bordered-hard border-blue">
                    <img src="{{compUsers[0].staff_1.img_thumbnail}}" alt="" ng-if="compUsers[0].staff_1.img_thumbnail != null">
                    <img src="/assets/images/user-default.jpg" alt="" ng-if="compUsers[0].staff_1.img_thumbnail == null">
                </div>
                <div class="content">
                    <div class="title f-w-500">Staff 1</div>
                    <div class="sub-title">{{compUsers[0].staff_1.firstName}} {{compUsers[0].staff_1.lastName}}</div>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6">
            <div class="info-image user">
                <div class="image bordered-hard border-green">
                    <img src="{{compUsers[1].staff_2.img_thumbnail}}" alt="" ng-if="compUsers[1].staff_2.img_thumbnail != null">
                    <img src="/assets/images/user-default.jpg" alt="" ng-if="compUsers[1].staff_2.img_thumbnail == null">
                </div>
                <div class="content">
                    <div class="title f-w-500">Staff 2</div>
                    <div class="sub-title">{{compUsers[1].staff_2.firstName}} {{compUsers[1].staff_2.lastName}}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="row m-t-10">
        <div class="col-sm-12 col-md-7">
            <div class="box-bordered p-15">
                <h2 class="f-w-500 m-t-0">Completed Checkpoints</h2>
                <div class="scroll-container">
                    <div id="chart-completed-checkpoints">No data found.</div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-5">
            <div class="m-t-30 visible-sm"></div>
            <div class="box-bordered p-15">
                <h2 class="f-w-500 m-t-0">Reworks</h2>
                <div class="scroll-container" ng-show="!chart_rework_no_data">
                    <div id="chart-reworks">No data found.</div>
                </div>
                <p ng-if="chart_rework_no_data">No data found for both Staff members.</p>
            </div>
        </div>
    </div>

    <div class="row m-t-30">
        <div class="col-sm-12 col-md-7">
            <div class="box-bordered p-15">
                <h2 class="f-w-500 m-t-0">On Time Completion Rate</h2>
                <div class="scroll-container" ng-show="!chart_ontime_no_data">
                    <div id="chart-ontime">No data found.</div>
                </div>
                <p ng-if="chart_ontime_no_data">No data found for both Staff members.</p>
            </div>
        </div>
    </div>

    <div class="content-loader" content-loader=""></div>

</div>