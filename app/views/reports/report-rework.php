<div class="panel" ng-controller="ReportCtrl" ng-init="initRework()">

    <div class="page-toolbar">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="toolbar-inline">
                    
                    <div class="inline-item">
                        <div class="form-group">
                            <div class="select-wrapper">
                                <select class="form-control" ng-model="filter_change_report" ng-change="changeView(filter_change_report)">
                                    <option value="executive">Executive Summary Report</option>
                                    <option value="comparison">Comparison Report</option>
                                    <option value="rework" ng-selected="true">Process Rework Rate Report</option>
                                    <option value="customer">Customer Report</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="inline-item">
                        <div class="form-group">
                            <div class="calendar-component">
                                <div class="calendar-trigger">
                                    <input type="text" class="form-control fld-datepicker" jq-date-range-picker="" ng-model="filter_rework_report_by_date" value="" readonly="" ng-change="filterReworkReport(filter_rework_report_by_date); viewDateRange(filter_rework_report_by_date);">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- <div class="inline-item">
                        <div class="form-group">
                            <div class="select-wrapper">
                                <select class="form-control" ng-model="filter_rework_report_by_days" ng-change="filterReworkReport(filter_rework_report_by_days); viewDateRange(filter_rework_report_by_days);">
                                    <option value="7" ng-selected="true">Last 7 Days</option>
                                    <option value="14">Last 14 Days</option>
                                    <option value="21">Last 21 Days</option>
                                    <option value="30">Last 30 Days</option>
                                </select>
                            </div>
                        </div>
                    </div> -->
                    
                    <div class="inline-item">
                        <div class="form-group group-inline">
                            <label>Staff</label>
                            <div class="search-selection">
                                <input type="text" placeholder="All Staff" value="All Staff" class="form-control output" readonly>
                                <div class="search-dropdown">
                                    <input type="text" placeholder="Search.." ng-model="filter_rework_report_by_staff" class="form-control search-filter">
                                    <ul class="result-list scroll-container">
                                        <li><label ng-click="filterReworkReport(null, '')"><span>All Staff</span></label></li>
                                        <li ng-repeat="member in staff_list | filter:filter_rework_report_by_staff">
                                            <label ng-click="filterReworkReport(null, member.ID)">
                                                <span>{{member.firstName}} {{member.lastName}}</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="inline-item">
                        <div class="form-group group-inline">
                            <label>Location</label>
                            <div class="search-selection">
                                <input type="text" placeholder="All Locations" class="form-control output" readonly>
                                <div class="search-dropdown">
                                    <input type="text" placeholder="Search.." ng-model="filter_rework_report_by_location" class="form-control search-filter">
                                    <ul class="result-list scroll-container">
                                        <li><label ng-click="filterReworkReport(null, null, '')"><span>All Locations</span></label></li>
                                        <li ng-repeat="location in location_list | filter:filter_rework_report_by_location">
                                            <label ng-click="filterReworkReport(null, null, location.ID)"><span>{{location.name}}</span></label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
<!--            <div class="col-md-2 col-sm-3 text-right">
                <a href="" ng-click="printReworkReport()"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
            </div>-->
        </div>
    </div>
    
    <div class="panel-title">
        <h1 class="f-w-500 m-b-5">Process Rework Rate Report</h1>
        <span class="text-themed" ng-bind="date_range_view"></span>
    </div>
    
    <div class="row m-t-10">
        <div class="col-sm-12 col-md-12">
            <div class="box-bordered p-15">
                <h2 class="f-w-500 m-t-0">Checkpoints</h2>
                <div class="scroll-container scroll-x" ng-show="chart_data_availability">
                    <div id="chart-rework-checkpoints">No data found.</div>
                </div>
                <p ng-if="!chart_data_availability">No data found.</p>
            </div>         
        </div>
    </div>
    
    <div class="row m-t-20">
        <div class="col-sm-12 col-md-12">
            <div class="box-bordered p-15">
                <h2 class="f-w-500 m-t-0">Staff</h2>
                <div class="scroll-container" ng-show="chart_data_availability">
                    <div id="chart-rework-staff">No data found.</div>
                </div>
                <p ng-if="!chart_data_availability">No data found.</p>
            </div>         
        </div>
    </div>
    
    <div class="content-loader" content-loader=""></div>
    
</div>