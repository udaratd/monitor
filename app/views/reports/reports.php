<div class="panel">

    <div class="panel-title">
        <h1 class="m-b-0">All Reports</h1>
        <div class="line-seperator m-t-10"></div>
    </div>

    <div class="row">
        <div class="col-sm-4 col-md-3">
            <div class="rep-link-box">
                <a href="/report/executive">
                    <div class="icon"><i class="fa fa-area-chart" aria-hidden="true"></i></div>
                </a>
                <h2 class="title"><a href="/report/executive">Executive Report</a></h2>
            </div>
        </div>
        <div class="col-sm-4 col-md-3">
            <div class="rep-link-box">
                <a href="/report/comparison">
                    <div class="icon"><i class="fa fa-balance-scale" aria-hidden="true"></i></div>
                </a>
                <h2 class="title"><a href="/report/comparison">Comparison Report</a></h2>
            </div>
        </div>
        <div class="col-sm-4 col-md-3">
            <div class="rep-link-box">
                <a href="/report/rework">
                    <div class="icon"><i class="fa fa-recycle" aria-hidden="true"></i></div>
                </a>
                <h2 class="title"><a href="/report/rework">Process Rework Rate Report</a></h2>
            </div>
        </div>
        <div class="col-sm-4 col-md-3">
            <div class="rep-link-box">
                <a href="/report/customer">
                    <div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                </a>
                <h2 class="title"><a href="/report/customer">Customer Report</a></h2>
            </div>
        </div>
    </div>

</div>