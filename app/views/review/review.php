<div class="panel full-container-height pg-review vertical-center horizontal-center full-width" ng-controller="ReviewCtrl" ng-init="initReviewCount()">
    <div class="panel-content text-center">
        <img src="/assets/images/picture-default.png" class="pic-default" alt="">
        <h1 class="text-themed f-s-35">You Have <span ng-if="review_summary.workCount > 0">{{review_summary.workCount}}</span><span ng-if="review_summary.workCount == 0">No</span> New Unseen images</h1>
        <a ng-href="/review/{{review_summary.firstWorkID}}" class="btn btn-primary btn-big min-w-130" ng-if="review_summary.workCount > 0">Start</a>
    </div>
	<div class="content-loader review-section" content-loader=""></div>
</div>