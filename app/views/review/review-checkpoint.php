<div class="panel p-0 pg-review" ng-controller="ReviewCtrl" ng-init="initReview()">

    <div class="row equal-height-md pg-review-cont">
        <div class="col-md-6">
            <div class="single-image no-border full-height review-zoom-image">
                <img src="{{review_single.w_img_original}}" alt="{{review_single.checkpointName}}" app-loading-image="">
                <div class="icon"><i class="fa fa-search" aria-hidden="true"></i></div>

                <div class="btn-group btn-work-toggle" ng-if="(review_single.originalWorkID == null && review_single.status == 'Rework' && review_single.rework != null && review_single.rework != false) || (review_single.originalWorkID != null)">
                    <a href="/review/{{review_single.originalWork.workID}}" class="btn" ng-if="review_single.originalWorkID != null">See
                        Original Work</a>
                    <a href="/review/{{review_single.rework.workID}}" class="btn" ng-if="review_single.originalWorkID == null">See
                        Rework</a>
                </div>

                <div class="seal-triangle" ng-if="review_single.originalWorkID != null">
                    <div><span><i class="fa fa-repeat" aria-hidden="true"></i></span><span>REWORKED</span></div>
                </div>
            </div>
        </div>

        <div class="col-md-6">

            <div class="panel-content scroll-container">
                <div class="row vertical-center m-b-10">
                    <div class="col-md-4 col-sm-4">
                        <h1 class="m-t-0 m-b-0 badge-status badge-inline">
                            <span class="badge-circle notice" ng-if="review_single.status == 'Pending'" title="Pending"><i
                                    class="fa fa-ellipsis-h" aria-hidden="true"></i></span>
                            <span class="badge-circle danger" ng-if="review_single.status == 'Rework'" title="Reworked"><i
                                    class="fa fa-repeat" aria-hidden="true"></i></span>
                            <span class="badge-circle warning" ng-if="review_single.status == 'Flagged'" title="Flagged"><i
                                    class="fa fa-flag" aria-hidden="true"></i></span>
                            <span class="badge-circle success" ng-if="review_single.status == 'Approved'" title="Approved"><i
                                    class="fa fa-check-square" aria-hidden="true"></i></span>
                            Details
                        </h1>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <div class="text-right f-w-500 vertical-center align-right" ng-class="review_single.early == 'YES' ? 'text-success':'text-danger'"
                            ng-if="review_single.scheduleStatus!=2">
                            <span class="m-r-5">{{review_single.timeDiff}} <span ng-if="review_single.early == 'YES'">Early</span><span
                                    ng-if="review_single.early == 'NO'">Late</span></span>
                            <span><i class="fa fa-clock-o f-s-25" aria-hidden="true"></i></span>
                        </div>
                        <div class="text-right f-w-500 vertical-center align-right text-success" ng-if="review_single.scheduleStatus==2">
                            This activity submitted from a QR Code
                        </div>
                    </div>
                </div>
                <div class="line-seperator m-t-0 m-l-15-minus-md"></div>

                <div class="tbl-data-view">
                    <table>
                        <tbody>
                            <tr>
                                <td class="col-sm-4">Checkpoint<span class="dash-seperator">-</span></td>
                                <td><span class="f-w-500">{{review_single.checkpointName}}</span></td>
                            </tr>
                            <tr>
                                <td class="col-sm-4">Location<span class="dash-seperator">-</span></td>
                                <td><span class="f-w-500">{{review_single.loc_parentName}} <i class="fa fa-angle-right"
                                            aria-hidden="true"></i> {{review_single.loc_childName}}</span></td>
                            </tr>
                            <tr>
                                <td class="col-sm-4">Uploaded Date / Time<span class="dash-seperator">-</span></td>
                                <td><span class="f-w-500">{{review_single.uploadFormattedDate}} at
                                        {{review_single.uploadServerTime}}</span></td>
                            </tr>
                            <tr>
                                <td class="col-sm-4">Staff<span class="dash-seperator">-</span></td>
                                <td><span class="text-themed">{{review_single.staff_fName}}
                                        {{review_single.staff_lName}}</span></td>
                            </tr>
                            <tr>
                                <td class="col-sm-4">Description<span class="dash-seperator">-</span></td>
                                <td>
                                    <p class="text-justify f-w-500">{{review_single.check_Description}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-sm-4">Schedule Description<span class="dash-seperator">-</span></td>
                                <td>
                                    <p class="text-justify f-w-500">{{review_single.schedule_Description}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-sm-4">Staff Comment<span class="dash-seperator">-</span></td>
                                <td>
                                    <p class="text-justify f-w-500">{{review_single.stf_comment}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-sm-4">Sample Image<span class="dash-seperator">-</span></td>
                                <td>
                                    <div class="single-image w-auto h-auto max-h-200 border-to-image review-sample-zoom">
                                        <img src="{{review_single.c_img_medium}}" class="max-h-200" alt="">
                                    </div>
                                </td>
                            </tr>
                            <tr id="review-comment-area">
                                <td class="col-sm-4">Comments<span class="dash-seperator">-</span></td>
                                <td>
                                    <p class="text-justify f-w-500 m-b-10" ng-if="review_single.comment != null">{{review_single.comment}}</p>
                                    <div class="form-group" ng-show="review_single.status != 'Rework' && review_single.status != 'Approved'">
                                        <textarea class="form-control no-resize h-60 min-h-60" ng-model="reviewUpdComment"
                                            placeholder="Add Comment.." id="review-comment"></textarea>
                                    </div>
                                    <div class="response-message-text" id="msg-add-comment"></div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="btn-group three-btn full-width btn-group-large">
                <button type="button" class="btn bg-danger btn-loading fade-in-hover" ng-click="updateReview(1, $event); goToByScroll('review-comment-area');"
                    id="btn-review-rework" ng-disabled="review_single.status == 'Rework' || review_single.status == 'Approved' || (review_single.originalWorkID != null && review_single.originalWork.status == 'Rework')"><i
                        class="fa fa-repeat m-r-5" aria-hidden="true"></i> Rework</button>
                <button type="button" class="btn bg-warning btn-loading fade-in-hover" ng-click="updateReview(2, $event); goToByScroll('review-comment-area');"
                    id="btn-review-flag" ng-disabled="review_single.status == 'Flagged' || review_single.status == 'Approved' || review_single.status == 'Rework'"><i
                        class="fa fa-flag m-r-5" aria-hidden="true"></i> Flag</button>
                <button type="button" class="btn bg-success btn-loading fade-in-hover" ng-click="updateReview(3, $event);"
                    id="btn-review-approve" ng-disabled="review_single.status == 'Approved' || review_single.status == 'Rework'"><i
                        class="fa fa-check-square m-r-5" aria-hidden="true"></i> Approve</button>
            </div>

        </div>
    </div>
    <div class="review-content-loader review-section"></div>
</div>