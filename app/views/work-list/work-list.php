<div class="panel pg-worklist" ng-controller="WorkListCtrl" ng-init="init()">

    <div class="page-toolbar">
        <div class="toolbar-inline">
            <div class="inline-item">
                <div class="form-group">
                    <div class="search-wrapper">
                        <input type="search" name="term" class="search-control form-control" placeholder="Search"
                            ng-model="filter_worklist_by_keyword">
                        <span class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></span>
                    </div>
                </div>
            </div>

            <div class="inline-item">
                <div class="form-group">
                    <div class="calendar-component">
                        <button type="button" class="calendar-navigator nav-prepend"><i class="fa fa-angle-left"
                                aria-hidden="true"></i></button>
                        <div class="calendar-trigger">
                            <input type="text" class="form-control fld-datepicker" jq-date-range-picker="" ng-model="filter_worklist_by_date"
                                value="" readonly="" ng-change="filterWorkList()">
                        </div>
                        <button type="button" class="calendar-navigator nav-append"><i class="fa fa-angle-right"
                                aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>

            <div class="inline-item">
                <div class="form-group group-inline">
                    <label for="worklist-staff">Staff</label>
                    <div class="search-selection w-160">
                        <input type="text" id="worklist-staff" placeholder="All" class="form-control output" readonly>
                        <div class="search-dropdown">
                            <input type="text" placeholder="Search.." ng-model="filter_worklist_by_staff" class="form-control search-filter">
                            <ul class="result-list scroll-container">
                                <li><label ng-click="filterWorkList('all')"><span>All</span></label></li>
                                <li ng-repeat="member in staff_list | filter:filter_worklist_by_staff">
                                    <label ng-click="filterWorkList(member.ID)">
                                        <span>{{member.firstName}} {{member.lastName}}</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="inline-item">
                <div class="form-group group-inline">
                    <label for="worklist-location">Location</label>
                    <div class="search-selection w-160">
                        <input type="text" id="worklist-location" placeholder="All" class="form-control output"
                            readonly>
                        <div class="search-dropdown">
                            <input type="text" placeholder="Search.." ng-model="filter_worklist_by_location" class="form-control search-filter">
                            <ul class="result-list scroll-container">
                                <li><label ng-click="getWorkList(null, 'all')"><span>All</span></label></li>
                                <li ng-repeat="location in location_list | filter:filter_worklist_by_location">
                                    <label ng-click="filterWorkList(null, location.ID)">
                                        <span>{{location.name}}</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="inline-item">
                <div class="form-group group-inline">
                    <label for="worklist-status">Status</label>
                    <div class="select-wrapper" id="worklist-status">
                        <select name="worklist_status" ng-options="status.id as status.label for status in work_status_list track by status.id"
                            ng-init="filter_worklist_by_status = work_status_list[0]" ng-model="filter_worklist_by_status"
                            class="form-control" ng-change="filterWorkList(null, null, filter_worklist_by_status)"></select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="table-responsive" ng-if="!worklist_list_no_data">
        <table class="table table-bordered table-sortable table-condensed table-striped content-align-top">
            <thead>
                <tr>
                    <th>Checkpoint</th>
                    <th><a href="#">Date</a></th>
                    <th><a href="#">Time</a></th>
                    <th><a href="#">Staff (Uploader)</a></th>
                    <th><a href="#">Supervisor</a></th>
                    <th>Status</th>
                    <th ng-if="logged_user_role == 1 && logged_debug_mode==2">Actions</th>
                </tr>
            </thead>
            <tbody>

                <tr ng-repeat="worklist in worklist_list | filter:filter_worklist_by_keyword">
                    <td class="condensed-column">
                        <div class="info-image">
                            <div class="image">
                                <img src="{{worklist.img_thumbnail}}" alt="{{worklist.checkpointName}}" ng-if="worklist.img_thumbnail !== null">
                                <img src="/assets/images/default-pic-mini.jpg" alt="{{worklist.checkpointName}}" ng-if="worklist.img_thumbnail === null">
                            </div>
                            <div class="content">
                                <div class="title"><a href="/review/{{worklist.ID}}">{{worklist.checkpointName}}</a></div>
                                <div class="sub-title">
                                    <a href="/review/{{worklist.ID}}">{{worklist.parentName}} <i class="fa fa-angle-right"
                                            aria-hidden="true"></i> {{worklist.childName}}</a>
                                </div>
                            </div>
                        </div>
                    </td>

                    <td>{{worklist.server_date}}</td>
                    <td>{{worklist.server_time}}</td>
                    <td><span class="text-themed">{{worklist.staff_FName}} {{worklist.staff_LName}}</span></td>
                    <td>{{worklist.supervisorFName}} {{worklist.supervisorLName}}</td>
                    <td>
                        <span class="text-success" ng-if="worklist.work_status === 'Approved'"><i class="fa fa-check-square m-r-5"
                                aria-hidden="true"></i> {{worklist.work_status}}</span>
                        <span class="text-notice" ng-if="worklist.work_status === 'Pending'"><i class="fa fa-ellipsis-h m-r-5"
                                aria-hidden="true"></i> {{worklist.work_status}}</span>
                        <span class="text-danger" ng-if="worklist.work_status === 'Rework'"><i class="fa fa-repeat m-r-5"
                                aria-hidden="true"></i> {{worklist.work_status}}</span>
                        <span class="text-warning" ng-if="worklist.work_status === 'Flagged'"><i class="fa fa-flag m-r-5"
                                aria-hidden="true"></i> {{worklist.work_status}}</span>

                        <div ng-if="worklist.work_status === 'Rework'" class="m-t-3">
                            <span class="text-notice" ng-if="worklist.Rework.rework != null"><i class="fa fa-ellipsis-h m-r-5"
                                    aria-hidden="true"></i> Rework not done yet</span>
                            <span class="text-notice" ng-if="worklist.Rework[0].work_status == 'Pending'"><i class="fa fa-ellipsis-h m-r-5"
                                    aria-hidden="true"></i> Rework Pending</span>
                            <span class="text-success" ng-if="worklist.Rework[0].work_status == 'Approved'"><i class="fa fa-check-square m-r-5"
                                    aria-hidden="true"></i> Rework Approved</span>
                            <span class="text-warning" ng-if="worklist.Rework[0].work_status == 'Flagged'"><i class="fa fa-flag m-r-5"
                                    aria-hidden="true"></i> Rework Flagged</span>
                        </div>
                    </td>
                    <td ng-if="logged_user_role == 1 && logged_debug_mode==2">
                        <ul class="horizontal-action-group">
                            <li><a href="" ng-click="viewDeviceInfo(worklist.ID)" class="text-muted btn-loading btn-text-loading"><i
                                        class="fa fa-mobile" aria-hidden="true"></i> Device info</a></li>
                        </ul>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>

    <div class="text-center m-t-30 m-b-15" ng-if="worklist_list_no_data">
        <h3>No Worklist found</h3>
    </div>
    <list-pagination ng-if="!worklist_list_no_data" page-count="{{worklist_page_count}}" page-number="{{worklist_page_number}}"
        callback-fn="filterWorkList(null,null,null,page_number)"></list-pagination>
    <div class="content-loader" content-loader=""></div>



    <!-- Device Infomation Model -->

    <div class="model-wrapper" id="model-device-info" style="display:block;" ng-if="device_info_popup">
        <div class="model-dialog model-small">

            <div class="model-header">
                <!-- <div class="model-closer"><a href="/settings/users">&times;</a></div> -->
                <div class="model-closer3"><a href="" ng-click="deviceInfoPopupHide()">&times;</a></div>
                <h3 class="text-center">Device Infomation</h3>
            </div>

            <div class="model-content">
                <div class="table-responsive" ng-if="!device_info_no_data">
                    <table class="table table-bordered table-sortable table-condensed table-striped ">
                        <tr>
                            <td>App Version</td>
                            <td>{{device_info.APP_VERSION}}</td>
                        </tr>
                        <tr>
                            <td>Model</td>
                            <td>{{device_info.MODEL}}</td>
                        </tr>
                        <tr>
                            <td>Manufacturer</td>
                            <td>{{device_info.MANUFACTURER}}</td>
                        </tr>
                        <tr>
                            <td>Brand</td>
                            <td>{{device_info.BRAND}}</td>
                        </tr>
                        <tr>
                            <td>SDK</td>
                            <td>{{device_info.SDK}}</td>
                        </tr>
                        <tr>
                            <td>Version Code</td>
                            <td>{{device_info.VERSION_CODE}}</td>
                        </tr>
                        <tr>
                            <td>Serial</td>
                            <td>{{device_info.SERIAL}}</td>
                        </tr>
                    </table>
                </div>
                <div class="text-center m-t-30 m-b-15" ng-if="device_info_no_data">
                    <h3>No Device Informations found</h3>
                </div>
            </div>
        </div>
    </div>

</div>