var app = angular.module('moniApp', ['ngRoute']);

app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/dashboard.php'
        })
        .when('/dashboard', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/dashboard.php'
        })
        .when('/work-list', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/work-list/work-list.php',
            controller: 'WorkListCtrl'
        })
        .when('/schedules', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                loadPopups: function (appServices) {
                    appServices.controllPopup(false, 'model-reschedule');
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/task-schedule/schedules.php',
            controller: 'ScheduleCtrl'
        })
        .when('/schedule/add', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/task-schedule/add-schedule.php',
            controller: 'ScheduleCtrl'
        })
        .when('/checkpoints', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                },
                loadPopups: function (appServices) {
                    appServices.controllPopup(false, 'model-add-checkpoint');
                },
                //            loadPopups: function($location, appServices){
                //                appServices.controllPopup(false, 'model-add-checkpoint');
                //                var urlParams = $location.search();
                //                if(urlParams.action === 'view'){
                //                    appServices.controllPopup(true, 'model-checkpoint-view');
                //                }
                //            }
            },
            templateUrl: 'app/views/checkpoints/checkpoints.php',
            controller: 'CheckpointCtrl'
        })
        .when('/checkpoints/add', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                loadPopups: function (appServices) {
                    appServices.controllPopup(true, 'model-add-checkpoint');
                    appServices.clearPopupImages("model-add-checkpoint");
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/checkpoints/checkpoints.php',
            controller: 'CheckpointCtrl'
        })
        //    .when('/checkpoint/:id', {
        //        resolve: {
        //            validateSession: function(appFactories){
        //                return appFactories.checkSession();
        //            },
        //            loadPopups: function(appServices){
        //                appServices.controllPopup(false, 'model-add-checkpoint');
        //                appServices.controllPopup(true, 'model-checkpoint-view');
        //            }
        //        },
        //        templateUrl: 'app/views/checkpoints/checkpoints.php',
        //        controller: 'CheckpointCtrl'
        //    })
        .when('/checkpoints/add/success', {
            resolve: {
                loadPopups: function (appServices) {
                    appServices.controllPopup(false, 'model-add-checkpoint');
                    appServices.controllPopup(true, 'model-checkpoint-success');
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/checkpoints/checkpoints.php'
        })
        .when('/review', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/review/review.php',
            controller: 'ReviewCtrl'
        })
        .when('/review/:id', {
            resolve: {
                // rework this
                // validateSession: function(appFactories){
                //     return appFactories.checkSession();
                // },
                // getNotifications: function (appFactories) {
                //     return appFactories.getNotifications();
                // }
            },
            templateUrl: 'app/views/review/review-checkpoint.php',
            controller: 'ReviewCtrl'
        })
        .when('/feedback', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/feedbacks/feedbacks.php',
            controller: 'FeedbackCtrl'
        })
        .when('/feedback/:id', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/feedbacks/review-feedback.php',
            controller: 'FeedbackCtrl'
        })
        .when('/settings', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/settings/general/general.php',
            controller: 'UserCtrl'
        })
        .when('/settings/users', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                loadPopups: function (appServices) {
                    appServices.controllPopup(false, 'model-add-user');
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/settings/users/users.php',
            controller: 'UserCtrl'
        })
        .when('/settings/users/add', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                loadPopups: function (appServices) {
                    appServices.controllPopup(true, 'model-add-user');
                    appServices.clearPopupImages("model-add-user");
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/settings/users/users.php',
            controller: 'UserCtrl'
        })
        .when('/settings/locations', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/settings/locations/locations.php',
            controller: 'LocationCtrl'
        })
        .when('/settings/locations/:id', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/settings/locations/location-view.php',
            controller: 'LocationCtrl'
        })
        .when('/settings/location/add', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/settings/locations/location-add.php',
            controller: 'LocationCtrl'
        })
        .when('/settings/location/edit/:id', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/settings/locations/location-edit.php',
            controller: 'LocationCtrl'
        })
        .when('/settings/general', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/settings/general/general.php'
        })
        .when('/settings/data', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/settings/data/data.php'
        })
        .when('/settings/log', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/settings/user-log.php'
        })
        .when('/reports', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/reports/reports.php'
        })
        .when('/report/executive', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/reports/report-executive.php',
            controller: 'ReportCtrl'
        })
        .when('/report/comparison', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/reports/report-comparison.php',
            controller: 'ReportCtrl'
        })
        .when('/report/rework', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/reports/report-rework.php',
            controller: 'ReportCtrl'
        })
        .when('/report/customer', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                },
                getNotifications: function (appFactories) {
                    return appFactories.getNotifications();
                }
            },
            templateUrl: 'app/views/reports/report-customer.php',
            controller: 'ReportCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
    $locationProvider.html5Mode(true);
}]);