app.controller('MainCtrl', ['$scope', '$rootScope', '$http', '$route', 'appServices', 'userServices', 'notificationServices', 'reviewServices', 'companyServices', 'feedbackServices', function ($scope, $rootScope, $http, $route, appServices, userServices, notificationServices, reviewServices, companyServices, feedbackServices) {

    var request_url = appServices.getRequestUrl();
    $scope.logged_company_uname = "";
    var lu_role = appServices.getSessionDataByName("role");
    if (lu_role != null) {
        $scope.logged_user_role = lu_role;
    }

    $scope.init = function () {
        $scope.getLoggedUserData();
        // $scope.getCompanyDetails();
    };

    $rootScope.initNotifications = function () {
        $scope.getAllNotifications();
        $scope.getNotificationCount();
        $scope.getReviewWorkCountGlobal();
        $scope.getUnreadFeedbackCountGlobal();
    };

    $scope.initCompany = function () {
        $scope.prepareCompanyUpdate();
        $scope.getLoggedUserData();
        // $scope.getCompanyDetails();
    };

    $scope.getLoggedUserData = function () {
        var lu_fname = appServices.getSessionDataByName("first_name");
        $scope.logged_user_lname = appServices.getSessionDataByName("last_name");
        $scope.logged_user_role_name = userServices.mapUserRoles(appServices.getSessionDataByName("role"));
        $scope.logged_user_email = appServices.getSessionDataByName("email");
        $scope.logged_user_nic = appServices.getSessionDataByName("nic");
        $scope.logged_user_phone = appServices.getSessionDataByName("phone");
        $scope.logged_user_avatar = appServices.getSessionDataByName("avatar");
        $scope.logged_company_uname = appServices.getSessionDataByName("company_uname");
        $scope.company_logo = appServices.getSessionDataByName("company_logo");
        if(appServices.getSessionDataByName("skip_validation") == 1){
            $scope.skip_validation = true;
        }else{
            $scope.skip_validation = false;
        }
        
console.log("Hello");
console.log($scope.skip_validation);
        if (lu_fname != null) {
            $scope.logged_user_fname = lu_fname;
        } else {
            $scope.logged_user_fname = 'Guest';
        }
    };

    // ------- Get Review Work Count ------- //
    $scope.getReviewWorkCountGlobal = function () {
        reviewServices.getReviewWorkCount().success(function (response) {
            if (response.response.success) {
                $scope.review_count = response.data[0].workCount;
                appServices.setReviewCount(response.data[0]);
            }
        });
    };

    // ------- Get Unread Feedback Count ------- //
    $scope.getUnreadFeedbackCountGlobal = function () {
        feedbackServices.getUnreadFeedbackCount().success(function (response) {
            if (response.response.success) {
                $scope.unread_feedback_count = response.data.unread_count;
            }
        });
    };

    // User Logout
    $scope.userLogout = function () {
        console.log("ss");
        $('#link-logout').addClass('spin-loader-mini');
        appServices.userLogout().success(function (response) {
            if (response.response.success) {
                console.log($scope.logged_company_uname);
                var cuname = appServices.getSessionDataByName("company_uname");
                if (typeof (Storage) !== "undefined") {
                    localStorage.removeItem("lu_id");
                    localStorage.removeItem("lu_fname");
                    localStorage.removeItem("lu_lname");
                    localStorage.removeItem("lu_role");
                    localStorage.removeItem("lu_email");
                    localStorage.removeItem("lu_nic");
                    localStorage.removeItem("lu_phone");
                    localStorage.removeItem("lu_avatar");
                    localStorage.removeItem("company_logo");
                    localStorage.removeItem("lu_company_logo");
                    localStorage.removeItem("company_name");
                    localStorage.removeItem("nextdata");
                    localStorage.removeItem("oldreviewdata");
                    localStorage.removeItem("reviewpending");
                    localStorage.removeItem("pendinglist");
                    localStorage.removeItem("quotaExceeded");
                    localStorage.removeItem("logout-event");
                    localStorage.removeItem("lu_debug_mode");
                    // localStorage.removeItem("lu_company_uname");
                }

                window.location.href = "/login/" + $scope.logged_company_uname;
                $('#link-logout').removeClass('spin-loader-mini');
            }
        });
    };

    // Get Notifications Count
    $scope.getNotificationCount = function () {
        notificationServices.getNotificationCount().success(function (response) {
            if (response.response.success) {
                $scope.notification_count = response.data.notificationCount;
            }
        });
    };

    // Get All Notifications
    $scope.getAllNotifications = function () {
        notificationServices.getAllNotifications().success(function (response) {
            if (response.response.success) {
                $scope.all_noti_list = response.data;
            }
        });
    };

    // Read notifications
    $scope.readNotification = function (id, event) {

        var elem = $(event.currentTarget);
        var params = {
            work_id: id
        };
        elem.addClass('spin-loader-mini');

        $http({
            method: 'POST',
            url: request_url + '/Notification/readingnotification',
            data: $.param(params),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function () {
            window.location.href = "/review/" + id;
        });
    };

    // Get Company Details
    $scope.getCompanyDetails = function () {
        companyServices.getCompanyDetails().success(function (response) {
            if (response.response.success) {
                var res_data = response.data;
                console.log(res_data);
                appServices.setSessionDataByName("company_name", res_data.companyName);
                if (res_data.image != undefined) {
                    appServices.setSessionDataByName("company_logo", res_data.image); // For further purpose (Ex: account details page)
                    $scope.company_logo = res_data.image; // For Header logo display
                }
            }
        });
    };

    // Prepare Company Update
    $scope.prepareCompanyUpdate = function () {
        $scope.upd_company = {};
        $scope.upd_company.name = appServices.getSessionDataByName("company_name");
        var company_logo = appServices.getSessionDataByName("company_logo");
        if (company_logo != undefined) {
            $scope.upd_company.image = appServices.getSessionDataByName("company_logo");
        }
    };

    // Update Company Details
    $scope.updateCompany = function (company) {
        company.id = appServices.getSessionDataByName("id");
        console.log(company);

        appServices.buttonLoader(true, 'btn-update-company');

        companyServices.updateCompany(company).then(function (response) {
            appServices.buttonLoader(false, 'btn-update-company');
            if (response.data.response.success) {
                appServices.responseMessage(true, 'msg-update-company', true, 'Company updated successfully. Please Wait..');
                setTimeout(function () {
                    location.reload();
                }, 1000);
            } else {
                appServices.responseMessage(true, 'msg-update-company', false, response.data.response.statusMsg);
            }

        }, function () {
            appServices.buttonLoader(false, 'btn-update-company');
            appServices.responseMessage(true, 'msg-update-company', false, 'Oops. Something went wrong with your connection.');
        });
    };

    // check for another tab login
    window.addEventListener('storage', function (event) {
        if (event.key == 'logout-event') {
            window.location.href = "/";
        }
    });

}]);