app.controller('ScheduleCtrl', ['$scope', '$http', '$location', '$route', '$routeParams', '$timeout', 'appServices', 'scheduleServices', 'userServices', 'checkpointServices', 'locationServices', function ($scope, $http, $location, $route, $routeParams, $timeout, appServices, scheduleServices, userServices, checkpointServices, locationServices) {

    var request_url = appServices.getRequestUrl();
    var lu_role = appServices.getSessionDataByName("role");
    if (lu_role != null) {
        $scope.logged_user_role = lu_role;
    }

    // Initialized when Schedule Page load
    $scope.init = function () {
        $scope.getScheduleList();
        //$scope.getScheduleCheckpointList();
        $scope.getLocationList();

        $scope.filterParams = {};
        $scope.filter_schedule_by_days = 14;
    };

    // Initialized when Add Schedule Page load
    $scope.initAddSchedule = function () {
        $('#schedule-end-time-wrapper').addClass('disabled');
        $('#btn-add-schdule').addClass('disabled');
        $scope.getStaffList();
        $scope.getScheduleCheckpointList();
        $scope.endTimeMin = 1;
        $scope.schedule = {};
        $scope.schedule.startTime = "00:00";
        $scope.schedule.endTime = "00:01";
        $scope.repeat_types = scheduleServices.listRepeatTypes();
        $scope.schedule.repeatType_id = 1; // Default select the Repeat type to 'Daily'
    };

    $scope.initReschedule = function () {
        $scope.getStaffList();
        $scope.reschedule = {};
    };


    // ------- Get Staff List ------- //
    $scope.getStaffList = function () {
        userServices.getStaffList().success(function (response) {
            if (response.response.success) {
                $scope.staff_list = response.data;
                return $scope.staff_list;
            }
        });
    };

    // ------- Get Location List ------- //
    $scope.getLocationList = function () {
        locationServices.getLocationList().success(function (response) {
            if (response.response.success) {
                $scope.location_list = response.data;
            }
        });
    };

    // ------- Get Checkpoint list by location id ------- //
    $scope.getCheckpointListByLocationId = function (id) {
        $scope.checkpoint_list = [];
        locationServices.getCheckpointListByLocationId(id).success(function (response) {
            if (response.response.success) {
                $scope.checkpoint_list = response.data;
            }
        });
    };

    // ------- Get Schedule Checkpoint List ------- //
    $scope.getScheduleCheckpointList = function () {
        checkpointServices.getScheduleCheckpointList().success(function (response) {
            if (response.response.success) {
                $scope.checkpoint_list = response.data;
                return $scope.checkpoint_list;
            }
        });
    };

    // ------- Get Schedule List ------- //
    $scope.getScheduleList = function (params) {
        scheduleServices.getScheduleList(params).success(function (response) {
            if (response.response.success && (response.data != null || response.data != undefined)) {
                angular.forEach(response.data.schedules, function (value) {

                    angular.forEach(value.schedule, function (sub_value) {
                        sub_value.startTime = appServices.timeConvert(sub_value.startTime);
                        sub_value.endTime = appServices.timeConvert(sub_value.endTime);

                        if (sub_value.sid != null && sub_value.sid != 'null' && sub_value.sid != '' && sub_value.sid != undefined) {
                            value.rescheduled = true;
                            value.rescheduledStartTime = sub_value.startDate;
                            value.rescheduledEndTime = sub_value.endDate;
                        }
                    });
                });
                $scope.schedule_list_no_data = false;
                $scope.schedule_list = response.data.schedules;
                console.log($scope.schedule_list);
            } else {
                $scope.schedule_list_no_data = true;
            }
        });
    };

    // ------- Add Schedule ------- //
    $scope.addSchedule = function (schedule) {
        console.log(schedule);

        if ($scope.formAddSchedule.$invalid) {
            return;
        }

        var url = request_url + '/schedule/';
        switch (schedule.repeatType_id) {
            case 1:
                url += 'createDaily';
                break;
            case 2:
                url += 'createWeekly';
                break;
            case 3:
                url += 'createOneDay';
                break;
            default:
                url += 'createDaily';
        }
        appServices.buttonLoader(true, 'btn-add-schdule');
        $http({
            method: 'POST',
            url: url,
            data: $.param(schedule),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function (response) {
            appServices.buttonLoader(false, 'btn-add-schdule');
            console.log(response);
            if (response.data.response.success) {
                appServices.responseMessage(true, 'msg-add-schedule', true, 'Schedule Added Successfully. Refreshing...');

                $timeout(function () {
                    $route.reload();
                    //appServices.responseMessage(false, 'msg-add-schedule');
                }, 2000);

            } else {
                appServices.responseMessage(true, 'msg-add-schedule', false, response.data.response.statusMsg);
            }
        }, function (response) {
            appServices.responseMessage(true, 'msg-add-schedule', false, 'Oops. Something went wrong with your connection.');
        });

    };

    // ------- Re Schedule ------- //
    $scope.prepareReschedule = function (schedule) {
        $routeParams.oldUser_id = schedule.ID;
        $routeParams.oldUser_firstName = schedule.firstName;
        $routeParams.oldUser_lastName = schedule.lastName;
        $('#model-reschedule').addClass('in');
    };

    $scope.reSchedule = function (reschedule) {
        var text_status = 'permanently';
        if (reschedule.scheduleStatus == undefined || reschedule.scheduleStatus == 1) {
            reschedule.scheduleStatus = 1;
            text_status = 'temporary';
        } else {
            text_status = 'permanently';
        }

        reschedule.oldUser_id = $routeParams.oldUser_id;
        console.log(reschedule);

        if ($scope.formReschedule.$invalid) {
            return;
        }

        if (confirm('This will ' + text_status + ' reschdule all the tasks done by ' + $routeParams.oldUser_firstName + ' ' + $routeParams.oldUser_lastName + '. Continue?')) {

            appServices.buttonLoader(true, 'btn-re-schedule');
            $http({
                method: 'POST',
                url: request_url + '/schedule/reschedule',
                data: $.param(reschedule),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                appServices.buttonLoader(false, 'btn-re-schedule');
                console.log(response);
                if (response.data.response.success) {
                    appServices.responseMessage(true, 'msg-re-schedule', true, 'Reschduled Successfully. Refreshing...');

                    $timeout(function () {
                        $route.reload();
                        //appServices.responseMessage(false, 'msg-add-schedule');
                    }, 2000);

                } else {
                    appServices.responseMessage(true, 'msg-re-schedule', false, response.data.response.statusMsg);
                }
            }, function (response) {
                appServices.buttonLoader(false, 'btn-re-schedule');
                appServices.responseMessage(true, 'msg-re-schedule', false, 'Oops. Something went wrong with your connection.');
            });

        }

    };

    // ------- Filter Schedule List ------- //
    $scope.filterScheduleList = function (location_id, location_id_2, checkpoint_id, day_count) {

        if (location_id == 'all') {
            delete $scope.filterParams.parent;
            delete $scope.filterParams.child;
            delete $scope.filterParams.checkName;
            clearLocationFilter();
            clearCheckpointFilter();
        }

        if (location_id_2 == 'all') {
            delete $scope.filterParams.child;
            delete $scope.filterParams.checkName;
            clearCheckpointFilter();
        }

        if (checkpoint_id == 'all') {
            delete $scope.filterParams.checkName;
        }

        if (location_id != null && location_id != undefined && location_id != 'all') {
            $scope.filterParams.parent = location_id;
            clearLocationFilter();
            clearCheckpointFilter();

            $scope.second_level_location_list = [];
            locationServices.getSecondLevelLocationList(location_id).success(function (response) {
                if (response.response.success) {
                    $scope.second_level_location_list = response.data;
                    $('#schedule-location-2-item').show();
                }
            });

        }

        if (location_id_2 != null && location_id_2 != undefined && location_id_2 != 'all') {
            $scope.filterParams.child = location_id_2;
            clearCheckpointFilter();

            $scope.checkpoint_list = [];
            locationServices.getCheckpointListByLocationId(location_id_2).success(function (response) {
                if (response.response.success) {
                    $scope.checkpoint_list = response.data;
                    $('#schedule-checkpoint-item').show();
                }
            });

        }

        if (checkpoint_id != null && checkpoint_id != undefined && checkpoint_id != 'all') {
            $scope.filterParams.checkName = checkpoint_id;
        }

        if (day_count != null && day_count != undefined) {
            $scope.filterParams.day_count = day_count;
        }
        $scope.getScheduleList($scope.filterParams);
    };

    function clearCheckpointFilter() {
        $scope.checkpoint_list = [];
        $('#schedule-checkpoint').val('');
        $('#schedule-checkpoint-item').hide();
    }

    function clearLocationFilter() {
        $scope.second_level_location_list = [];
        $('#schedule-location-2').val('');
        $('#schedule-location-2-item').hide();
    }
    $scope.et = false;

    $scope.filterEndTime = function (filterTime) {
        $('#schedule-end-time-wrapper').removeClass('disabled');

        $scope.endTimeMin = 0;
        $scope.$apply();


        var stime = $scope.schedule.startTime + ":00";
        var etime = $scope.schedule.endTime + ":00";
        if (stime >= etime) {
            console.log("error");
            $('#btn-add-schdule').addClass('disabled');
            appServices.responseMessage(true, 'msg-add-schedule', false, 'Error:End time is before Start time ');
        } else {
            console.log("no error");
            $('#btn-add-schdule').removeClass('disabled');
            appServices.responseMessage(false, 'msg-add-schedule', false, 'No Error');
        }

    }

    // ------- Delete Schedule ------- //
    $scope.deleteSchedule = function (id) {
        if (confirm('Are you sure you want to delete this Schedule?')) {
            var params = {
                ID: id,
                confirm: 2
            };

            $http({
                method: 'POST',
                url: request_url + '/schedule/delete',
                data: $.param(params),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                console.log(response.data);
                if (response.data.response.success == false) {
                    if (response.data.response.statusCode == 510) {
                        if (confirm(response.data.response.statusMsg)) {
                            params.confirm = 1;

                            $http({
                                method: 'POST',
                                url: request_url + '/schedule/delete',
                                data: $.param(params),
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            }).then(function (response) {
                                console.log(response.data);
                                if (response.data.response.success) {
                                    $route.reload();
                                }
                            });
                        }
                    }
                    if (response.data.response.statusCode == 511) {
                        alert(response.data.response.statusMsg);
                    } else {
                        $route.reload();
                    }
                } else {
                    $route.reload();
                }
            });
        }
    };

}]);