app.controller('WorkListCtrl', ['$scope', '$http', '$location', 'appServices', 'userServices', 'locationServices', 'worklistServices', function ($scope, $http, $location, appServices, userServices, locationServices, worklistServices) {
    var lu_role = appServices.getSessionDataByName("role");
    if (lu_role != null) {
        $scope.logged_user_role = lu_role;
    }
    var lu_debug = appServices.getSessionDataByName("debug");
    if (lu_debug != null) {
        $scope.logged_debug_mode = lu_debug;
    }

    // Initialized when Page load
    $scope.init = function () {
        $scope.worklist_page_count = 1;
        $scope.worklist_page_number = 1;
        $scope.getStaffList();
        $scope.getLocationList();
        $scope.getWorkList();
        $scope.work_status_list = worklistServices.getWorkStatusesWithAll();

        $scope.filterParams = {};
        appServices.resetDataLocal();

        $scope.device_info_popup = false;
    };

    // ------- Get Staff List ------- //
    $scope.getStaffList = function () {
        userServices.getStaffList().success(function (response) {
            if (response.response.success) {
                $scope.staff_list = response.data;
                return $scope.staff_list;
            }
        });
    };

    // ------- Get Location List ------- //
    $scope.getLocationList = function () {
        locationServices.getLocationList().success(function (response) {
            if (response.response.success) {
                $scope.location_list = locationServices.prepareLocationList(response.data);
                return $scope.location_list;
            }
        });
    };

    // ------- Get Work List ------- //
    $scope.getWorkList = function (params) {        
        worklistServices.getWorkList(params).success(function (response) {
            if (response.response.success) {
                $scope.worklist_list_no_data = false;
                console.log(response.data);
                angular.forEach(response.data.worklist, function (value) {
                    value.server_time = appServices.timeConvert(value.server_time);
                });
                $scope.worklist_list = response.data.worklist;
                if ((response.data).hasOwnProperty('meta')) {
                    $scope.worklist_page_count = response.data.meta.numberOfPages;
                    $scope.worklist_page_number = response.data.meta.pageNumber;
                }
            } else {
                $scope.worklist_list_no_data = true;
            }
        });
    };

    // ------- Filter Work List ------- //
    $scope.filterWorkList = function (staff_id, location_id, status, pageNumber) {

        if (staff_id == 'all') {
            delete $scope.filterParams.staff_id;
        }
        if (location_id == 'all') {
            delete $scope.filterParams.location_id;
        }

        if (staff_id != null && staff_id != undefined && staff_id != 'all') {
            $scope.filterParams.staff_id = staff_id;
        }
        if (location_id != null && location_id != undefined && location_id != 'all') {
            $scope.filterParams.location_id = location_id;
        }

        if ($scope.filter_worklist_by_date != null && $scope.filter_worklist_by_date != undefined && $scope.filter_worklist_by_date != '') {
            var date_object = JSON.parse($scope.filter_worklist_by_date);
            $scope.filterParams.s_date = date_object.start;
            $scope.filterParams.e_date = date_object.end;
        } else {
            delete $scope.filterParams.s_date;
            delete $scope.filterParams.e_date;
        }

        if (status != null && status != undefined) {
            $scope.filterParams.status = status;
        }
        if (pageNumber != null && pageNumber != undefined) {
            $scope.filterParams.page_number = pageNumber;
        }
        console.log($scope.filterParams);

        $scope.getWorkList($scope.filterParams);
    };

    $scope.viewDeviceInfo = function (id) {
        // ------- Get Device Info ------- //
        worklistServices.viewDeviceInfo(id).success(function (response) {
            if (response.response.success) {
                $scope.device_info = response.data.info;
                console.log($scope.device_info)
                $scope.device_info_popup = true;
                $scope.device_info_no_data = false;
            } else {
                $scope.device_info_popup = true;
                $scope.device_info_no_data = true;
            }
        });
    }
    $scope.deviceInfoPopupHide = function () {
        $scope.device_info_popup = false;
    }

}]);