app.controller('UserCtrl', ['$scope', '$http', '$location', '$route', '$timeout', 'appServices', 'userServices', function ($scope, $http, $location, $route, $timeout, appServices, userServices) {

    var request_url = appServices.getRequestUrl();

    var lu_role = appServices.getSessionDataByName("role");
    if (lu_role != null) {
        $scope.logged_user_role = lu_role;
    }

    // Initialized when Page load
    $scope.init = function () {
        $scope.getUserList();
        $scope.role_list = userServices.getUserRolesWithAll();
    };

    // Initialized when user add popup load
    $scope.initAddUser = function () {
        $scope.user = {};
        $scope.getUserRoles();
        $scope.getSupervisors = function (role) {
            if (role == 3) {
                $scope.getUserList(2);
            }
        };
    };

    // Initialized when user log load 
    $scope.initLog = function () {
        $scope.getUserLog();
        $scope.filterParams = {};
        $scope.userlog_page_count = 1;
        $scope.userlog_page_number = 1;

    };

    // Initialized when edit popup load
    $scope.prepareUserUpdate = function (id) {
        userServices.getSingleUser(id).success(function (response) {
            if (response.response.success) {

                var single_data = response.data;
                $scope.updUser = {};
                $scope.upd_image_prev = ''; // Reset previously loaded images
                $scope.updUser.id = single_data.ID;
                $scope.updUser.firstName = single_data.firstName;
                $scope.updUser.lastName = single_data.lastName;
                $scope.updUser.phone = single_data.phone;
                $scope.updUser.nic = single_data.nic;
                $scope.updUser.email = single_data.email;
                $scope.updUser.role = single_data.role;
                $scope.updUser.note = single_data.note;
                $scope.upd_role_name = userServices.mapUserRoles(single_data.role);

                if (single_data.role == 3) {
                    $scope.getSupervisorList();
                    $scope.updUser.supervisor_id = single_data.supervisorID;
                    $scope.updUser.supervisorName = single_data.supervisorName;
                }

                if (single_data.img_thumbnail != null && single_data.img_thumbnail != '') {
                    $scope.upd_image_prev = single_data.img_thumbnail;
                }
                $('#model-edit-user').addClass('in');
            }
        });
    };

    // Get Role List
    $scope.getUserRoles = function () {
        userServices.getUserRoles().success(function (response) {
            if (response.response.success) {
                $scope.role_list = response.data;
            }
        });
    };

    // Get User List
    $scope.getUserList = function (params) {
        userServices.getUserList(params).success(function (response) {
            if (response.response.success) {
                $scope.user_list_no_data = false;
                $scope.users_count = 0;
                angular.forEach(response.data.users, function (value) {
                    value.role = userServices.mapUserRoles(value.role);
                    if (value.role == 'Admin') {
                        $scope.users_count++;
                    }

                });
                $scope.user_list = response.data.users;
            } else {
                $scope.user_list_no_data = true;
            }
        });
    };

    // Get Supervisor List
    $scope.getSupervisorList = function () {
        userServices.getSupervisors().success(function (response) {
            if (response.response.success) {
                $scope.supervisor_list = response.data;
            }
        });
    };


    // ------- Add User ------- //
    $scope.addUser = function (user) {
        if ($scope.logged_user_role == 2) {
            user.supervisor_id = appServices.getSessionDataByName("id");
        }

        if ($scope.formAddUser.$invalid) {
            return;
        }

        appServices.buttonLoader(true, 'btn-add-user');
        $http({
            method: 'POST',
            url: request_url + '/User/add',
            data: $.param(user),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function (response) {
            appServices.buttonLoader(false, 'btn-add-user');
            if (response.data.response.success) {
                appServices.responseMessage(true, 'msg-add-user', true, 'User Added Successfully. Redirecting...');

                // clear the form and model
                $scope.user = {};
                user = {};
                $scope.formAddUser.$setUntouched();
                $scope.formAddUser.$setPristine();

                $timeout(function () {
                    $location.path('/settings/users');
                    appServices.responseMessage(false, 'msg-add-user');
                }, 2000);

            } else {
                appServices.responseMessage(true, 'msg-add-user', false, response.data.response.statusMsg);
            }
        }, function (response) {
            appServices.responseMessage(true, 'msg-add-user', false, 'Oops. Something went wrong with your connection.');
        });

    };

    // ------- Update User ------- //
    $scope.updateUser = function (user) {

        if ($scope.formUpdateUser.$invalid) {
            return;
        }

        appServices.buttonLoader(true, 'btn-update-user');
        $http({
            method: 'POST',
            url: request_url + '/User/update',
            data: $.param(user),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function (response) {
            appServices.buttonLoader(false, 'btn-update-user');
            if (response.data.response.success) {
                appServices.responseMessage(true, 'msg-update-user', true, 'User Updated Successfully. Redirecting...');

                $timeout(function () {
                    $route.reload();
                }, 2000);

            } else {
                appServices.responseMessage(true, 'msg-update-user', false, response.data.response.statusMsg);
            }
        }, function (response) {
            appServices.responseMessage(true, 'msg-update-user', false, 'Oops. Something went wrong with your connection.');
        });

    };

    // ------- Delete User ------- //
    $scope.deleteUser = function (id) {
        if (confirm('Are you sure you want to delete this User?')) {
            var params = {
                id: id
            };

            $http({
                method: 'POST',
                url: request_url + '/User/delete',
                data: $.param(params),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                if (response.data.response.success == false) {
                    if (response.data.response.statusCode == 501) {
                        alert('Some staff members are assigned to this supervisor. In order to delete this supervisor, delete or re-assign those staff members to another supervisor.');
                    } else {
                        $route.reload();
                    }
                } else {
                    $route.reload();
                }
            });
        }
    };

    // ------- Import Users ------- //
    $scope.importUsers = function (data) {

        appServices.buttonLoader(true, 'btn-import-users');

        userServices.importUser(data).then(function (response) {
            appServices.buttonLoader(false, 'btn-import-users');
            if (response.data.response.success) {
                appServices.responseMessage(true, 'msg-import-users', true, 'Import Successfull.');
                $timeout(function () {
                    appServices.responseMessage(false, 'msg-import-users', true, '');
                }, 3000);
            } else {
                if (response.data.response.statusCode == 510) {
                    appServices.responseMessage(true, 'msg-import-users', false, 'Succeeded with some issues');
                    $timeout(function () {
                        appServices.responseMessage(false, 'msg-import-users', true, '');
                    }, 3000);
                } else {
                    appServices.responseMessage(true, 'msg-import-users', false, response.data.response.statusMsg);
                    $timeout(function () {
                        appServices.responseMessage(false, 'msg-import-users', true, '');
                    }, 3000);
                }
            }

        }, function () {
            appServices.buttonLoader(false, 'btn-import-users');
            appServices.responseMessage(true, 'msg-import-users', false, 'Oops. Something went wrong with your connection.');
            $timeout(function () {
                appServices.responseMessage(false, 'msg-import-users', true, '');
            }, 3000);
        });
    };

    // Get User log
    $scope.getUserLog = function (params) {
        userServices.getUserLog(params).success(function (response) {
            if (response.response.success) {
                $scope.user_log_no_data = false;
                angular.forEach(response.data.activitylist, function (value) {
                    if ((value).hasOwnProperty('time')) {
                        var time_str = value.time;
                        var time = time_str.split(':');
                        var date = new Date();
                        date.setHours(time[0]);
                        date.setMinutes(time[1]);
                        value.formatted_time = moment(date).format('hh:mm a');;
                    }
                });
                $scope.user_log_list = response.data.activitylist;
                if ((response.data).hasOwnProperty('meta')) {
                    $scope.userlog_page_count = response.data.meta.numberOfPages;
                    $scope.userlog_page_number = response.data.meta.pageNumber;
                }
            } else {
                $scope.user_log_no_data = true;
            }
        });
    };

    // ------- Filter Schedule List ------- //
    $scope.filterUserLog = function (limit, pageNumber) {
        // if (limit == "All") {
        //     $scope.filterParams.limit = "All";
        // }
        if (limit != null && limit != undefined) {
            $scope.filterParams.limit = limit;
        }
        if ($scope.filter_userlog_by_date != null && $scope.filter_userlog_by_date != undefined && $scope.filter_userlog_by_date != '') {
            var date_object = JSON.parse($scope.filter_userlog_by_date);
            $scope.filterParams.s_date = date_object.start;
            $scope.filterParams.e_date = date_object.end;
        } else {
            delete $scope.filterParams.s_date;
            delete $scope.filterParams.e_date;
        }

        if (pageNumber != null && pageNumber != undefined) {
            $scope.filterParams.page_number = pageNumber;
        }

        $scope.getUserLog($scope.filterParams);
    };

    $scope.addUserPopupShow = function () {
        // clear the form and model
        $scope.user = {};
        user = {};
        appServices.responseMessage(false, 'msg-add-user');
        $location.path('/settings/users/add');
    };
    $scope.addUserPopupShowHide = function () {

        $scope.user = {};
        appServices.responseMessage(false, 'msg-add-user');
        $location.path('/settings/users');
    }

}]);