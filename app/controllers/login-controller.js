"use strict";
var REQUEST_URL = '/api';

jQuery(document).ready(function ($) {

    function display_response(visibility, type, message) {
        var container = $('.response-output');
        if (visibility) {
            if (type === 'success') {
                container.removeClass('error');
                container.addClass('success');
            } else {
                container.removeClass('success');
                container.addClass('error');
            }
            container.text(message);
            container.fadeIn();
        } else {
            container.hide();
        }
    }

    function display_loader(visibility) {
        var loader_object = $(".btn-ajax");
        if (visibility) {
            loader_object.addClass('spin-loader-mini');
        } else {
            loader_object.removeClass('spin-loader-mini');
        }
    }

    // Submit the form
    $("#frm-login").submit(function (e) {
        e.preventDefault();
        display_response(false); // Hide previously loaded response messages

        var nic = $('input[name=nic]').val();
        var password = $('input[name=password]').val();
        var username = $('#user-name').val();
        console.log(username);
        // Have to do field validation before submission
        //        if ( 'Some conditions' ) {
        //            Show errors
        //            return;
        //        }

        $.ajax({
            type: 'POST',
            url: REQUEST_URL + '/User/login',
            data: {
                nic: nic,
                password: password,
                username: username
            },
            beforeSend: function () {
                display_loader(true);
            },
            success: function (response) {
                //console.log(response);
                display_loader(false);
                var res = JSON.parse(response);
                if (res.response.success) {
                    // console.log(res);
                    if (res.data.role == 3) {
                        display_response(true, 'error', 'You are not allowed here.');
                    } else {
                        display_response(true, 'success', 'Login Successfull. Redirecting...');

                        // Sets session data into browser storage (Better than cookies ;-))
                        if (typeof (Storage) !== "undefined") {
                            localStorage.setItem("lu_id", res.data.ID);
                            localStorage.setItem("lu_fname", res.data.firstName);
                            localStorage.setItem("lu_lname", res.data.lastName);
                            localStorage.setItem("lu_role", res.data.role);
                            localStorage.setItem("lu_email", res.data.email);
                            localStorage.setItem("lu_nic", res.data.nic);
                            localStorage.setItem("lu_phone", res.data.phone);
                            localStorage.setItem("lu_avatar", res.data.meta_value);
                            localStorage.setItem("lu_company_uname", res.data.companyUsername);
                            localStorage.setItem("company_name", res.data.companyName);
                            localStorage.setItem("lu_debug_mode", res.data.debug);
                            localStorage.setItem("lu_skip_validation", res.data.skip_validation);
                            // localStorage.setItem("lu_company_logo", res.data.logo);
                        }
                        localStorage.setItem('logout-event', 'logout' + Math.random());

                        window.location.href = "/";
                    }
                } else {
                    display_response(true, 'error', res.response.statusMsg);
                }
            }
        });

    });

    window.addEventListener('storage', function (event) {
        if (event.key == 'logout-event') {
            console.log("logout");
        }
    });

    function setLoggedUserData(user) {
        console.log(user.f_name);
        if (typeof (Storage) !== "undefined") { // Checks if the browser supports LocalStorage
            if (user.ID != undefined) {
                localStorage.setItem("su_id", user.ID);
            }
            if (user.f_name != undefined) {
                localStorage.setItem("su_fname", user.f_name);
            }
            if (user.l_name != undefined) {
                localStorage.setItem("su_lname", user.l_name);
            }
            if (user.email != undefined) {
                localStorage.setItem("su_email", user.email);
            }
        }
    }

    // Search URL Parameters
    function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    /*
     * Password Reset
     * Step 1: Sending email to the server
     * Server will check the email and send confirmation link via that email
     */
    $("#frm-reset-password").submit(function (e) {
        e.preventDefault();
        display_response(false); // Hide previously loaded response messages 
        display_loader(true);
        var email = $('#user-reset-email').val();
        var username = $('#user-name').val();
        var post_data = {
            email: email,
            username: username
        };

        $.ajax({
            type: 'POST',
            url: REQUEST_URL + '/User/resetpasswordrequest',
            data: post_data,
            success: function (response) {
                console.log(response);
                display_loader(false);
                var res = JSON.parse(response);
                if (res.response.success) {
                    display_response(true, 'success', 'An email with a reset link has been sent to ' + email + '. Please click the link on email to confirm the password change request.');
                } else {
                    display_response(true, 'error', 'Sorry, We cannot verify that email. Please try again with a different email.');
                }
            }
        });
    });


    /*
     * Password Reset
     * Step 2 : User will click the link on email and visits /login.php?action=reset&id=x&token=xxxx...xxxx&type=x
     * Automatically sends this parameters to the server again and server will confirm that token is expired or not
     * If token expired user will directs again to type the email and receive a new confirmation link
     * If token not expired user can type a new password and change his/her password
     */
    checkResetLink();

    function checkResetLink() {
        var action = getUrlParameter('action');
        var id = getUrlParameter('id');
        var token = getUrlParameter('token');
        var username = $('#user-name').val();

        if (action === undefined || id === undefined || token === undefined) {
            return;
        }

        $.ajax({
            type: 'GET',
            url: REQUEST_URL + '/User/checkLink?id=' + id + '&token=' + token + '&username=' + username,
            success: function (response) {
                var res = JSON.parse(response);
                if (res.response.success) {
                    $('#frm-reset-password').detach();
                    $('#frm-change-password').show();
                } else {
                    display_response(true, 'error', 'Sorry, Your reset time has expired. Please enter the email and request a new reset link.');
                }
            },
            error: function () {
                display_response(true, 'error', 'Sorry, An error occured while connecting to the server. Please try again later.');
            }
        });
    }


    /*
     * Password reset
     * Step 3 : Submit new password after email confirmation
     */
    $("#frm-change-password").submit(function (e) {
        e.preventDefault();
        display_response(false); // Hide previously loaded response messages 
        display_loader(true);
        var new_password = $('#user-reset-password').val();
        var username = $('#user-name').val();
        console.log(username);

        var post_data = {
            id: getUrlParameter('id'),
            token: getUrlParameter('token'),
            password: new_password,
            username: username
        };

        $.ajax({
            type: 'POST',
            url: REQUEST_URL + '/User/resetpassword',
            data: post_data,
            success: function (response) {
                console.log(response);
                display_loader(false);
                var res = JSON.parse(response);
                if (res.response.success) {
                    display_response(true, 'success', 'Password change successfully. Please Wait...');

                    // Automatically send login request if successfull password change
                    $.ajax({
                        type: 'POST',
                        url: REQUEST_URL + '/User/login',
                        data: {
                            nic: res.data.nic,
                            password: new_password,
                            username: username
                        },
                        success: function (response_login) {
                            var res_login = JSON.parse(response_login);
                            console.log(res_login);
                            if (res_login.response.success) {
                                console.log(res_login.data);
                                // Sets session data into browser storage (Better than cookies ;-))
                                if (typeof (Storage) !== "undefined") {
                                    localStorage.setItem("lu_id", res_login.data.ID);
                                    localStorage.setItem("lu_fname", res_login.data.firstName);
                                    localStorage.setItem("lu_lname", res_login.data.lastName);
                                    localStorage.setItem("lu_role", res_login.data.role);
                                    localStorage.setItem("lu_email", res_login.data.email);
                                    localStorage.setItem("lu_nic", res_login.data.nic);
                                    localStorage.setItem("lu_phone", res_login.data.phone);
                                    localStorage.setItem("lu_avatar", res_login.data.meta_value);
                                    localStorage.setItem("lu_company_uname", res_login.data.companyUsername);
                                    localStorage.setItem("company_name", res_login.data.companyName);
                                    localStorage.setItem("lu_debug_mode", res_login.data.debug);
                                    // localStorage.setItem("lu_company_logo", res_login.data.logo);
                                }
                                localStorage.setItem('logout-event', 'logout' + Math.random());
                                window.location.replace("/");
                            } else {
                                window.location.replace("/login/" + username);
                            }
                        }
                    });
                } else {
                    console.log(res);
                    display_response(true, 'error', res.response.statusMsg);
                }
            },
            error: function () {
                display_response(true, 'error', 'Sorry, An error occured while connecting to the server. Please try again later.');
            }
        });

    });

    setCompanyLogo();

    function setCompanyLogo() {
        var username = $('#user-name').val();

        if (username === undefined) {
            return;
        }

        $.ajax({
            type: 'GET',
            url: REQUEST_URL + '/Admin/company?username=' + username,
            success: function (response) {

                var res = JSON.parse(response);
                if (res.response.success) {
                    console.log(res.data.logo);
                    if (res.data.logo != null) {
                        $("#company-logo-img").attr("src", res.data.logo);
                        localStorage.setItem("lu_company_logo", res.data.logo);
                    } else {
                        $("#company-logo-img").attr("src", "/assets/images/logo-moni.png");
                    }
                    setTimeout(function () {
                        $('.content-loader-2').hide();
                    }, 1000);
                    if (res.data.logo == undefined || res.data.logo == null) {
                        localStorage.setItem("lu_company_logo", null);
                    }

                } else {
                    window.location.href = "/404.php";

                }
            },
            error: function () {
                window.location.href = "/404.php";

            }
        });
    }

});