app.controller('FeedbackCtrl', ['$scope', '$rootScope', '$http', '$location', '$route', '$timeout', '$routeParams', 'appServices', 'checkpointServices', 'feedbackServices', function ($scope, $rootScope, $http, $location, $route, $timeout, $routeParams, appServices, checkpointServices, feedbackServices) {
    var request_url = appServices.getRequestUrl();
    var lu_role = appServices.getSessionDataByName("role");
    if (lu_role != null) {
        $scope.logged_user_role = lu_role;
    }
    var lu_debug = appServices.getSessionDataByName("debug");
    if (lu_debug != null) {
        $scope.logged_debug_mode = lu_debug;
    }
    
    // Initialized when Page load
    $scope.init = function () {
        $scope.feedback_page_count = 1;
        $scope.feedback_page_number = 1;
        $scope.getFeedbackList();
        
        $scope.feedback_reaction_list = feedbackServices.getFeedbackReaction();
        
        $scope.filterParams = {};
    };
    
    $scope.initFeedback = function () {
        $scope.getSingleFeedback();
        setTimeout( ()=>{
            $scope.markFeedbackRead();
        }, 6000);
    };
    // ------- Get Work List ------- //
    $scope.getFeedbackList = function (params) {
        feedbackServices.getFeedbackList(params).success(function (response) {
            if (response.response.success) {
                $scope.feedback_list_no_data = false;
//                angular.forEach(response.data.worklist, function (value) {
//                    value.server_time = appServices.timeConvert(value.server_time);
//                });
                $scope.feedback_list = response.data.feedbacks;
                if ((response.data).hasOwnProperty('meta')) {
                    $scope.feedback_page_count = response.data.meta.numberOfPages;
                    $scope.feedback_page_number = response.data.meta.pageNumber;
                }
            } else {
                $scope.feedback_list_no_data = true;
            }
        });
    };
    
    // ------- Filter Feedback List ------- //
    $scope.filterFeedbackList = function (reaction, pageNumber) {
        if (reaction == 'All') {
            delete $scope.filterParams.reaction;
        }

        if (reaction != null && reaction != undefined && reaction != 'all') {
            $scope.filterParams.reaction = reaction;
        }
        if (pageNumber != null && pageNumber != undefined) {
            $scope.filterParams.page_number = pageNumber;
        }

        $scope.getFeedbackList($scope.filterParams);
    };
    
    $scope.getSingleFeedback = function (id) {
        if(id === ''){
            $scope.id = id;
        }else{
            $scope.id = $routeParams.id;
        }
        feedbackServices.getSingleFeedback($scope.id).success(function (response) {
            if (response.response.success) {                
                $scope.feedback_single = response.data;
                if ($scope.feedback_single.fd_image_ori != null && $scope.feedback_single.fd_image_ori != 'null') {
                    $('.review-zoom-image').zoom({
                        url: $scope.feedback_single.fd_image_ori
                    });
                }

                if ($scope.feedback_single.img_original != null && $scope.feedback_single.img_original != 'null') {
                    $('.review-sample-zoom').zoom({
                        url: $scope.feedback_single.img_original
                    });
                }
            }else {
                console.log('Not Found');
            }
        });
    };
    
    // ------- Mark Feedback Read ------- //
    $scope.markFeedbackRead = function (id) {
        if(id === ''){
            $scope.id = id;
        }else{
            $scope.id = $routeParams.id;
        }
        var params = {
            id: $scope.id
        };
        $http({
            method: 'POST',
            url: request_url + '/Customer/read',
            data: $.param(params),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function () {
            $rootScope.initNotifications();
        });
    };
    
}]);