app.controller('LocationCtrl', ['$scope', '$http', '$location', '$route', '$timeout', 'appServices', 'locationServices', '$routeParams', function ($scope, $http, $location, $route, $timeout, appServices, locationServices, $routeParams) {

    var request_url = appServices.getRequestUrl();

    var lu_role = appServices.getSessionDataByName("role");
    if (lu_role != null) {
        $scope.logged_user_role = lu_role;
    }


    // Initialized when Location view load
    $scope.init = function () {
        $scope.getLocationList();
    };

    // Initialized when Single Location view load
    $scope.initView = function () {
        $scope.getLocationList();
        $scope.getSingleLocation();
    };

    // Initialized when Location edit
    $scope.initEdit = function () {
        $scope.getLocationList();

        if ($routeParams.id != undefined) {
            locationServices.getSingleLocation($routeParams.id).success(function (response) {
                if (response.response.success) {
                    console.log(response.data);
                    var single_data = response.data;
                    $scope.loc = {};
                    $scope.loc.id = single_data.ID;
                    $scope.loc.name = single_data.name;
                    $scope.loc.parentId = single_data.parentid;
                    $scope.loc.parentName = single_data.parentName;
                    $scope.loc.description = single_data.description;

                    if (single_data.img_thumbnail != null) {
                        $scope.image_prev = single_data.img_thumbnail;
                    }

                }
            });
        }
    };

    // Initialized when Location Add
    $scope.initAdd = function () {
        $scope.getLocationList();
        $scope.loc = {};
        $scope.loc.parentId = 0;

        if ($routeParams.pname != undefined) {
            $scope.loc.parentName = $routeParams.pname;
            $('#settings-loc-parent-wrapper').show();
        }
    };

    // ------- Get Location List ------- //
    $scope.getLocationList = function () {
        locationServices.getLocationList().success(function (response) {
            if (response.response.success) {
                $scope.location_list_no_data = false;
                $scope.location_list = response.data;

                // Highlight the active item
                if ($routeParams.id != undefined || $routeParams.parent) {
                    if ($routeParams.id != undefined) {
                        var url_id = $routeParams.id;
                    } else if ($routeParams.parent != undefined) {
                        var url_id = $routeParams.parent;
                    }
                    angular.forEach($scope.location_list, function (value) {
                        if (value.ID == url_id) {
                            value['active'] = 'active';
                        } else {
                            angular.forEach(value.childs, function (sub_value) {
                                if (sub_value.ID == url_id) {
                                    sub_value['active'] = 'active';
                                    value['active'] = 'has-active open';
                                }
                            });
                        }
                    });
                }

            } else {
                $scope.location_list_no_data = true;
            }
        });
    };

    // ------- Get Single Location ------- //
    $scope.getSingleLocation = function () {
        if ($routeParams.id != undefined) {
            locationServices.getSingleLocation($routeParams.id).success(function (response) {
                if (response.response.success) {
                    $scope.location_single = response.data;
                    console.log($scope.location_single);
                }
            });
        }
    };

    // ------- Add Location ------- //
    $scope.addLocation = function (location) {

        if ($routeParams.parent != undefined) {
            location.parentId = $routeParams.parent;
        }
        console.log(location);

        if ($scope.formAddLocation.$invalid) {
            return;
        }

        appServices.buttonLoader(true, 'btn-add-location');
        $http({
            method: 'POST',
            url: request_url + '/Location/add',
            data: $.param(location),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function (response) {
            appServices.buttonLoader(false, 'btn-add-location');
            console.log(response.data);
            if (response.data.response.success) {
                appServices.responseMessage(true, 'msg-add-location', true, 'Location Added Successfully. Redirecting...');

                $timeout(function () {
                    $route.reload();
                    appServices.responseMessage(false, 'msg-add-location');
                }, 2000);

            } else {
                appServices.responseMessage(true, 'msg-add-location', false, response.data.response.statusMsg);
            }
        }, function (response) {
            appServices.responseMessage(true, 'msg-add-location', false, 'Oops. Something went wrong with your connection.');
        });

    };

    // ------- Update Location ------- //
    $scope.updateLocation = function (location) {

        console.log(location);

        if ($scope.formUpdateLocation.$invalid) {
            return;
        }

        appServices.buttonLoader(true, 'btn-update-location');
        $http({
            method: 'POST',
            url: request_url + '/Location/update',
            data: $.param(location),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function (response) {
            appServices.buttonLoader(false, 'btn-update-location');
            console.log(response);
            if (response.data.response.success) {
                appServices.responseMessage(true, 'msg-update-location', true, 'Location Updated Successfully. Redirecting...');

                $timeout(function () {
                    appServices.responseMessage(false, 'msg-update-location');
                    $location.path('/settings/locations/' + $routeParams.id);
                }, 2000);

            } else {
                appServices.responseMessage(true, 'msg-update-location', false, response.data.response.statusMsg);
            }
        }, function (response) {
            appServices.responseMessage(true, 'msg-update-location', false, 'Oops. Something went wrong with your connection.');
        });

    };

    // ------- Delete Location ------- //
    $scope.deleteLocation = function (id) {
        if (confirm('Are you sure you want to delete this location?')) {
            var params = {
                id: id,
                confirm: 0
            };

            appServices.buttonLoader(true, 'btn-delete-location');
            $http({
                method: 'POST',
                url: request_url + '/Location/delete',
                data: $.param(params),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                appServices.buttonLoader(false, 'btn-delete-location');
                if (response.data.response.success) {

                    if (response.data.response.statusCode == 201) {
                        if (confirm('This will delete all checkpoints that allocated to this Location. Continue?')) {
                            params.confirm = 1;

                            appServices.buttonLoader(true, 'btn-delete-location');
                            $http({
                                method: 'POST',
                                url: request_url + '/Location/delete',
                                data: $.param(params),
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            }).then(function (response) {
                                appServices.buttonLoader(false, 'btn-delete-location');
                                console.log(response.data);
                                if (response.data.response.success) {
                                    $location.path('/settings/locations');
                                }
                            });
                        }
                    } else {
                        $location.path('/settings/locations');
                    }
                } else {
                    $location.path('/settings/locations');
                }
            });
        }
    };

    // ------- Import Locations ------- //
    $scope.importLocations = function (data) {

        console.log(data);
        appServices.buttonLoader(true, 'btn-import-location');

        locationServices.importLocations(data).then(function (response) {
            console.log(response);
            appServices.buttonLoader(false, 'btn-import-location');
            if (response.data.response.success) {
                appServices.responseMessage(true, 'msg-import-location', true, 'Import Successfull.');
                $timeout(function () {
                    appServices.responseMessage(false, 'msg-import-location', true, '');
                }, 3000);
            } else {
                if (response.data.response.statusCode == 510) {
                    appServices.responseMessage(true, 'msg-import-location', false, 'Succeeded with some issues');
                    $timeout(function () {
                        appServices.responseMessage(false, 'msg-import-location', true, '');
                    }, 3000);
                } else {
                    appServices.responseMessage(true, 'msg-import-location', false, response.data.response.statusMsg);
                    $timeout(function () {
                        appServices.responseMessage(false, 'msg-import-location', true, '');
                    }, 3000);
                }
            }

        }, function () {
            appServices.buttonLoader(false, 'btn-import-location');
            appServices.responseMessage(true, 'msg-import-location', false, 'Oops. Something went wrong with your connection.');
            $timeout(function () {
                appServices.responseMessage(false, 'msg-import-location', true, '');
            }, 3000);
        });
    };

}]);