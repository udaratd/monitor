app.controller('CheckpointCtrl', ['$scope', '$http', '$location', '$timeout', '$route', 'appServices', 'locationServices', 'checkpointServices', '$routeParams', function ($scope, $http, $location, $timeout, $route, appServices, locationServices, checkpointServices, $routeParams) {

    var request_url = appServices.getRequestUrl();

    // Initialized when checkpoint view load
    $scope.init = function () {
        $scope.getLocationList();
        $scope.getCheckpointList();
        $scope.filterParams = {};
    };

    // Initialized when add checkpoint popup load
    $scope.initAddCheckpoint = function () {
        $scope.checkpoint = {};
        $scope.getLocationList();
    };

    // Initialized when checkpoint single
    $scope.initCheckpointSingle = function () {
        //$scope.getSingleCheckpoint();
    };

    // Initialized when edit popup load
    $scope.prepareCheckpointUpdate = function (checkpoint) {
        console.log(checkpoint);

        $scope.updCheckpoint = {};
        $scope.upd_image_prev = ''; // Reset previously loaded images
        $scope.updCheckpoint.id = checkpoint.checkPID;
        $scope.updCheckpoint.checkName = checkpoint.checkPName;
        $scope.updCheckpoint.locationid = checkpoint.Loc_child_ID;
        $scope.updCheckpoint.description = checkpoint.checkPDescription;

        if (checkpoint.img_thumbnail != null && checkpoint.img_thumbnail != '') {
            $scope.upd_image_prev = checkpoint.img_thumbnail;
        }

        //        $('#model-checkpoint-view').removeClass('in');
        $('#model-edit-checkpoint').addClass('in');
    };

    // ------- Get Location List ------- //
    $scope.getLocationList = function () {
        locationServices.getLocationList().success(function (response) {
            if (response.response.success) {

                // Modified
                var list = [];
                angular.forEach(response.data, function (value) {
                    if (value.childs != null || value.childs != undefined) {
                        list.push(value);
                    }
                });
                $scope.location_list = locationServices.prepareLocationList(list);
                // End modified

                //$scope.location_list = locationServices.prepareLocationList(response.data);
                return $scope.location_list;
            }
        });
    };

    // ------- Get Checkpoint List ------- //
    $scope.getCheckpointList = function (params) {


        checkpointServices.getCheckpointList(params).success(function (response) {
            if (response.response.success) {
                $scope.checkpoint_list_no_data = false;
                $scope.checkpoint_list = response.data.checkpoints;
                if ((response.data).hasOwnProperty('meta')) {
                    $scope.checkpoint_page_count = response.data.meta.numberOfPages;
                    $scope.checkpoint_page_number = response.data.meta.pageNumber;
                }
                //return $scope.checkpoint_list;
            } else {
                $scope.checkpoint_list_no_data = true;
            }
        });
    };

    // ------- Filter Checkpoint List ------- //
    $scope.filterCheckpointList = function (locationid, page_number) {

        if (locationid != null && locationid != undefined) {
            $scope.filterParams.locationid = locationid;
            // delete cc.filterParams.page_number;
        }
        if (page_number != null && page_number != undefined) {
            $scope.filterParams.page_number = page_number;
        }
        $scope.getCheckpointList($scope.filterParams);
    };

    // ------- Get Single Checkpoint ------- //
    $scope.getSingleCheckpoint = function (id) {
        checkpointServices.getSingleCheckpoint(id).success(function (response) {
            if (response.response.success) {
                console.log(response.data);
                $scope.checkpoint_single = response.data;
            }
        });
    };

    $scope.getSingleCheckpoint = function (id, event) {
        var parent_element = $(event.target).parents('.hover-box');
        parent_element.addClass('box-loader');
        checkpointServices.getSingleCheckpoint(id).success(function (response) {
            if (response.response.success) {
                parent_element.removeClass('box-loader');
                console.log(response.data);
                $scope.checkpoint_single = response.data;
                $('#model-checkpoint-view').addClass('in');
            }
        });
    };

    // ------- Add Checkpoint ------- //
    $scope.addCheckpoint = function (checkpoint) {
        console.log(checkpoint);

        if ($scope.formAddCheckpoint.$invalid) {
            return;
        }

        appServices.buttonLoader(true, 'btn-add-checkpoint');
        $http({
            method: 'POST',
            url: request_url + '/checkpoint/add',
            data: $.param(checkpoint),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function (response) {
            appServices.buttonLoader(false, 'btn-add-checkpoint');
            console.log(response);
            if (response.data.response.success) {
                appServices.responseMessage(true, 'msg-add-checkpoint', true, 'Checkpoint Added Successfully. Redirecting...');

                $timeout(function () {
                    window.location.href = "/checkpoints";
                }, 2000);

            } else {
                appServices.responseMessage(true, 'msg-add-checkpoint', false, response.data.response.statusMsg);
            }
        }, function (response) {
            appServices.responseMessage(true, 'msg-add-checkpoint', false, 'Oops. Something went wrong with your connection.');
        });

    };
    
    // ------- Add Bulk Checkpoints ------- //
    $scope.addBulkCheckpoints = function (checkpoint) {

        if ($scope.formAddBulkCheckpoints.$invalid) {
            return;
        }

        appServices.buttonLoader(true, 'btn-add-bulk-checkpoints');
        checkpointServices.addBulkCheckpoints( checkpoint ).then( function ( response ) {
            appServices.buttonLoader( false, 'btn-add-bulk-checkpoints' );
            if( response.data.response.success ){
                appServices.responseMessage(true, 'msg-add-bulk-checkpoints', true, 'Checkpoints added successfully. Refreshing...');
                location.reload();
            }else{
                appServices.responseMessage( true, 'msg-add-bulk-checkpoints', false, response.data.response.statusMsg );
            }
        }, function ( response ) {
            appServices.buttonLoader( false, 'btn-add-bulk-checkpoints' );
            appServices.responseMessage( true, 'msg-add-bulk-checkpoints', false, response.data.response.statusMsg );
        });  

    };

    // ------- Update Checkpoint ------- //
    $scope.updateCheckpoint = function (checkpoint) {
        console.log(checkpoint);

        if ($scope.formUpdateCheckpoint.$invalid) {
            return;
        }

        appServices.buttonLoader(true, 'btn-update-checkpoint');
        $http({
            method: 'POST',
            url: request_url + '/checkpoint/update',
            data: $.param(checkpoint),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function (response) {
            appServices.buttonLoader(false, 'btn-update-checkpoint');
            console.log(response);
            if (response.data.response.success) {
                appServices.responseMessage(true, 'msg-update-checkpoint', true, 'Checkpoint Updated Successfully. Redirecting...');

                $timeout(function () {
                    window.location.href = "/checkpoints";
                }, 2000);

            } else {
                appServices.responseMessage(true, 'msg-update-checkpoint', false, response.data.response.statusMsg);
            }
        }, function (response) {
            appServices.responseMessage(true, 'msg-update-checkpoint', false, 'Oops. Something went wrong with your connection.');
        });

    };

    // ------- Delete Checkpoint ------- //
    $scope.deleteCheckpoint = function (id) {
        if (confirm('Are you sure you want to delete this checkpoint?')) {

            var params = {
                id: id,
                confirm: 0
            };

            appServices.buttonLoader(true, 'btn-delete-checkpoint');
            $http({
                method: 'POST',
                url: request_url + '/checkpoint/delete',
                data: $.param(params),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                appServices.buttonLoader(false, 'btn-delete-checkpoint');

                if (response.data.response.success) {
                    if (response.data.response.statusCode == 201) {
                        if (confirm('This will delete all schedules and worklists allocated to this Checkpoint. Continue?')) {
                            params.confirm = 1;

                            appServices.buttonLoader(true, 'btn-delete-checkpoint');
                            $http({
                                method: 'POST',
                                url: request_url + '/checkpoint/delete',
                                data: $.param(params),
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            }).then(function (response) {
                                appServices.buttonLoader(false, 'btn-delete-checkpoint');
                                console.log(response);
                                if (response.data.response.success) {
                                    window.location.reload();
                                }
                            });
                        }
                    } else {
                        window.location.reload();
                    }
                } else {
                    window.location.reload();
                }

            });
        }
    };

    // ------- Import Checkpoints ------- //
    $scope.importCheckpoints = function (data) {

        appServices.buttonLoader(true, 'btn-import-checkpoints');

        checkpointServices.importCheckpoints(data).then(function (response) {
            console.log(response);
            appServices.buttonLoader(false, 'btn-import-checkpoints');
            if (response.data.response.success) {
                appServices.responseMessage(true, 'msg-import-checkpoints', true, 'Import Successfull.');
                $timeout(function () {
                    appServices.responseMessage(false, 'msg-import-checkpoints', true, '');
                }, 3000);
            } else {
                if (response.data.response.statusCode == 510) {
                    appServices.responseMessage(true, 'msg-import-checkpoints', false, 'Succeeded with some issues');
                    $timeout(function () {
                        appServices.responseMessage(false, 'msg-import-checkpoints', true, '');
                    }, 3000);
                } else {
                    appServices.responseMessage(true, 'msg-import-checkpoints', false, response.data.response.statusMsg);
                    $timeout(function () {
                        appServices.responseMessage(false, 'msg-import-checkpoints', true, '');
                    }, 3000);
                }
            }

        }, function () {
            appServices.buttonLoader(false, 'btn-import-checkpoints');
            appServices.responseMessage(true, 'msg-import-checkpoints', false, 'Oops. Something went wrong with your connection.');
            $timeout(function () {
                appServices.responseMessage(false, 'msg-import-checkpoints', true, '');
            }, 3000);
        });
    };


    $scope.showAddCheckpoint = function () {
        // clear the form and model
        $scope.checkpoint = {};
        checkpoint = {};
        appServices.responseMessage(false, 'msg-add-checkpoint');
        $location.path('/checkpoints/add');
    };
    $scope.hideAddCheckpoint = function () {
        $scope.checkpoint = {};
        appServices.responseMessage(false, 'msg-add-checkpoint');
        $location.path('/checkpoints');
    };
}]);