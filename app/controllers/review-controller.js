app.controller('ReviewCtrl', ['$scope', '$rootScope', '$http', '$location', '$timeout', '$interval', '$route', 'appServices', 'reviewServices', '$routeParams', function ($scope, $rootScope, $http, $location, $timeout, $interval, $route, appServices, reviewServices, $routeParams) {

    var request_url = appServices.getRequestUrl();
    $scope.storedReviewData = null;
    $scope.nextid = null;
    $scope.nextDataList = [];
    $scope.oldDataList = [];
    $scope.data_from_server = null;
    var next_item_id;
    var time;
    var array_limit = 5;
    $scope.initReviewCount = function () {
        $('.content-loader').show();
        $scope.getReviewWorkCount();
        appServices.setReviewSessionData("");
        appServices.setOldReviewSessionData("");

        $scope.getPendingList();

    };

    // Initialized when review single view load
    $scope.initReview = function () {
        $scope.setNextDataList();
        $scope.getSingleReviewFromLocal($routeParams.id);
        $scope.markWorkRead($routeParams.id);
        $('.app-loader').hide();
    };

    // ------- Get Review Work Count ------- //
    $scope.getReviewWorkCount = function () {
        // $scope.review_summary = appServices.getReviewCount();
        reviewServices.getReviewWorkCount().success(function (response) {
            if (response.response.success) {
                $scope.review_summary = appServices.getReviewCount();

            }
        });
    };

    // ------- Get Pending list ------- //
    $scope.getPendingList = function () {
        reviewServices.getPendingIds().success(function (response) {
            if (response.response.success) {
                $scope.pending_list = response.data;
                appServices.setReviewPendingListData($scope.pending_list);
                appServices.setPendingListData($scope.pending_list, false);
                $scope.setNextDataList();
            }
        });
    };

    // ------- Set Next Data list ------- //
    $scope.setNextDataList = function () {
        $scope.nextDataList = appServices.getReviewSessionData();
        $scope.pending_list = appServices.getPendingListData();
        if ($scope.nextDataList == "" || $scope.nextDataList == null || $scope.nextDataList == undefined || !(Array.isArray($scope.nextDataList))) {
            $scope.nextDataList = [];
        }
        if ($scope.pending_list == "" || $scope.pending_list == null || $scope.pending_list == undefined || !(Array.isArray($scope.pending_list))) {
            $scope.pending_list = [];
        }
        console.log($scope.nextDataList);
        // console.log("1.1");
        if ($scope.nextDataList.length < array_limit) {
            // console.log("1.2");
            if ($scope.pending_list.length != 0) {
                // console.log("1.3");
                // get Next Item id from Pending list
                next_item_id = $scope.pending_list[0];
                // get 1st item from pending_list
                $scope.getSingleReviewFromServer(next_item_id);

            }
        }
        if ($scope.nextDataList.length == 2) {
            $('.content-loader').hide();


        }
    };



    // ------- Get Single Review(Server)------- //

    $scope.getSingleReviewFromServer = function (id) {

        reviewServices.getSingleReview(id.workID).success(function (response) {
            if (response.response.success) {

                $scope.data_from_server = response.data;

                $scope.data_from_server.uploadFormattedDate = moment($scope.data_from_server.uploadServerDate).format('ddd, D MMM YYYY');
                $scope.data_from_server.uploadServerTime = appServices.timeConvert($scope.data_from_server.uploadServerTime);
                $scope.data_from_server.timeDiff = appServices.convertTimeDifference($scope.data_from_server.timeDiff);
                $scope.nextDataList.push($scope.data_from_server);
                // console.log($scope.nextDataList);
                // console.log("2");
                var checksuccess = appServices.setReviewSessionData($scope.nextDataList);
                if (checksuccess) {
                    console.log("update success ,remove pending id ");
                    // update Pending List
                    $scope.pending_list = appServices.getPendingListData();
                    // $scope.pending_list.shift();
                    // console.log($scope.pending_list);
                    appServices.setPendingListData($scope.pending_list, true);

                    $scope.setNextDataList();
                    appServices.setQuotaExceeded(false);
                } else {
                    appServices.setQuotaExceeded(true);
                }

            }
        });

    }

    // ------- Get Single Review(local) ------- //
    $scope.waitLoad = function (id) {

        console.log("waiting...")
        $('.review-content-loader').show();

        var pendingdata = appServices.getPendingListData();
        if (pendingdata == undefined || pendingdata == null || pendingdata == "" || !(Array.isArray(pendingdata))) {
            pendingdata = [];
        }
        var pendingitem = pendingdata.find(x => x.workID === id);
        if (pendingitem == undefined) {
            // get Directily from server when no data
            console.log("No Local data, Getting From Server")
            reviewServices.getSingleReview(id).success(function (response) {
                if (response.response.success) {
                    $scope.review_single = response.data;
                    $scope.review_single.uploadFormattedDate = moment($scope.review_single.uploadServerDate).format('ddd, D MMM YYYY');
                    $scope.review_single.uploadServerTime = appServices.timeConvert($scope.review_single.uploadServerTime);
                    $scope.review_single.timeDiff = appServices.convertTimeDifference($scope.review_single.timeDiff);
                    console.log($scope.review_single);
                    $('.review-content-loader').hide();

                    if ($scope.review_single.w_img_original_string != null && $scope.review_single.w_img_original_string != 'null') {
                        $('.review-zoom-image').zoom({
                            url: "data:image/jpeg;base64," + $scope.review_single.w_img_original_string
                        });
                    } else if ($scope.review_single.w_img_original != null && $scope.review_single.w_img_original != 'null') {
                        $('.review-zoom-image').zoom({
                            url: $scope.review_single.w_img_original
                        });
                    }

                    if ($scope.review_single.c_img_original_string != null && $scope.review_single.c_img_original_string != 'null') {
                        $('.review-sample-zoom').zoom({
                            url: "data:image/jpeg;base64," + $scope.review_single.c_img_original_string
                        });
                    } else if ($scope.review_single.c_img_original != null && $scope.review_single.c_img_original != 'null') {
                        $('.review-sample-zoom').zoom({
                            url: $scope.review_single.c_img_original
                        });
                    }
                }
            });


        } else {
            var count_time = 0;
            time = $interval(function () {
                if (count_time > 15) {
                    $interval.cancel(time);
                    time = undefined;
                    $('.review-content-loader').hide();
                }
                console.log(id);
                $scope.localDataList2 = appServices.getReviewSessionData();
                if ($scope.localDataList2 == undefined || $scope.localDataList2 == null || $scope.localDataList2 == "" || !(Array.isArray($scope.localDataList2))) {
                    $scope.localDataList2 = [];
                }
                $scope.review_single = $scope.localDataList2.find(x => x.workID === id);
                console.log("waiting...2")
                if ($scope.review_single != undefined) {
                    $interval.cancel(time);
                    time = undefined;
                    $('.review-content-loader').hide();
                    console.log("end waiting...");
                    console.log("wait", $scope.review_single);

                    if ($scope.review_single.w_img_original_string != null && $scope.review_single.w_img_original_string != 'null') {
                        $('.review-zoom-image').zoom({
                            url: "data:image/jpeg;base64," + $scope.review_single.w_img_original_string
                        });
                    } else if ($scope.review_single.w_img_original != null && $scope.review_single.w_img_original != 'null') {
                        $('.review-zoom-image').zoom({
                            url: $scope.review_single.w_img_original
                        });
                    }

                    if ($scope.review_single.c_img_original_string != null && $scope.review_single.c_img_original_string != 'null') {
                        $('.review-sample-zoom').zoom({
                            url: "data:image/jpeg;base64," + $scope.review_single.c_img_original_string
                        });
                    } else if ($scope.review_single.c_img_original != null && $scope.review_single.c_img_original != 'null') {
                        $('.review-sample-zoom').zoom({
                            url: $scope.review_single.c_img_original
                        });
                    }
                }
                count_time = count_time + 1;
            }, 1000);
        }
    }

    // ------- Get Single Review(local) ------- //
    $scope.getSingleReviewFromLocal = function (id) {
        $scope.localDataList = appServices.getReviewSessionData();
        console.log($scope.localDataList);
        if ($scope.localDataList == undefined || $scope.localDataList == null || $scope.localDataList == "" || !(Array.isArray($scope.localDataList))) {
            $scope.localDataList = [];
        }
        $scope.review_single = $scope.localDataList.find(x => x.workID === id);
        console.log("normal", $scope.review_single);
        if ($scope.review_single == undefined) {
            $scope.getOldReviewSessionData(id);
        } else {

            if ($scope.review_single.w_img_original_string != null && $scope.review_single.w_img_original_string != 'null') {
                $('.review-zoom-image').zoom({
                    url: "data:image/jpeg;base64," + $scope.review_single.w_img_original_string
                });
            } else if ($scope.review_single.w_img_original != null && $scope.review_single.w_img_original != 'null') {
                $('.review-zoom-image').zoom({
                    url: $scope.review_single.w_img_original
                });
            }

            if ($scope.review_single.c_img_original_string != null && $scope.review_single.c_img_original_string != 'null') {
                $('.review-sample-zoom').zoom({
                    url: "data:image/jpeg;base64," + $scope.review_single.c_img_original_string
                });
            } else if ($scope.review_single.c_img_original != null && $scope.review_single.c_img_original != 'null') {
                $('.review-sample-zoom').zoom({
                    url: $scope.review_single.c_img_original
                });
            }
        }
    }

    // ------- Set Single Review(local) ------- //
    $scope.setSingleReviewToLocal = function (id) {
        if (isNaN(id)) {
            $location.path('/work-list');
        } else {
            reviewServices.getSingleReview(id).success(function (response) {
                if (response.response.success) {
                    $scope.local_data = response.data;
                    appServices.setReviewSessionData($scope.local_data);
                }
            });
        }
    }

    // ------- Set Single Review to Old (local ) ------- //
    $scope.setOldReviewSessionData = function (data) {
        console.log(data);
        $scope.oldDataList = appServices.getOldReviewSessionData();
        if ($scope.oldDataList == undefined || $scope.oldDataList == null || $scope.oldDataList == "" || !(Array.isArray($scope.oldDataList))) {
            $scope.oldDataList = [];
        }

        if ($scope.oldDataList.length < array_limit) {
            $scope.oldDataList.push(data);
        } else {
            $scope.oldDataList.shift();
            $scope.oldDataList.push(data);
        }
        var checkoldasessiondata = appServices.setOldReviewSessionData($scope.oldDataList);
        while (!checkoldasessiondata) {
            $scope.oldDataList.shift();
            checkoldasessiondata = appServices.setOldReviewSessionData($scope.oldDataList);
        }

    }
    $scope.getOldReviewSessionData = function (id) {
        var oldDataList = appServices.getOldReviewSessionData();
        console.log(oldDataList);
        if (oldDataList == undefined || oldDataList == null || oldDataList == "" || !(Array.isArray(oldDataList))) {
            oldDataList = [];
        }
        $scope.review_single = oldDataList.find(x => x.workID === id);
        if ($scope.review_single == undefined) {
            $scope.waitLoad(id);
            console.log("no data in both arrays,get from server");
        }
    }

    // ------- Mark Work Read ------- //
    $scope.markWorkRead = function (id) {
        var params = {
            work_id: id
        };
        $http({
            method: 'POST',
            url: request_url + '/Notification/readingnotification',
            data: $.param(params),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function () {
            $rootScope.initNotifications();
        });
    };

    // ------- Update Review ------- //
    $scope.updateReview = function (status, $event) {

        var review = $scope.review_single;
        var params = {
            id: review.workID,
            status: status
        };

        if ((typeof $scope.reviewUpdComment === 'undefined' || $scope.reviewUpdComment === "") && status !== 3) {
            appServices.responseMessage(true, 'msg-add-comment', false, 'You need to add a comment before making this choice.');
            delete params.comment;
            return false;
        } else if ((typeof $scope.reviewUpdComment === 'undefined' || $scope.reviewUpdComment === "") && status === 3) {
            delete params.comment;
        } else if (typeof $scope.reviewUpdComment !== 'undefined' && $scope.reviewUpdComment !== "") {
            params.comment = $scope.reviewUpdComment;
            appServices.responseMessage(false, 'msg-add-comment');
        }

        var button_id = $event.currentTarget.id;
        appServices.buttonLoader(true, button_id);

        $http({
            method: 'POST',
            url: request_url + '/Review/update',
            data: $.param(params),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function (response) {

            if (response.data.response.success) {
                // console.log("success");
            } else {
                // appServices.buttonLoader(false, button_id, true, false);
            }
        }, function () {
            // appServices.buttonLoader(false, button_id, true, false);
        });

        // review List Update
        var reviewlist = appServices.getReviewPendingListData();
        if (reviewlist == undefined || reviewlist == null || reviewlist == "" || !(Array.isArray(reviewlist))) {
            reviewlist = [];
        }
        var reviewitem = reviewlist.find(x => x.workID === $routeParams.id);
        if (reviewitem != undefined) {
            const indexreview = reviewlist.indexOf(reviewitem);
            reviewlist.splice(indexreview, 1);
            appServices.setReviewPendingListData(reviewlist);
        }

        // data list update
        var datalist = appServices.getReviewSessionData();
        if (datalist == undefined || datalist == null || datalist == "" || !(Array.isArray(datalist))) {
            datalist = [];
        }
        if (datalist.length >= array_limit) {
            var dataitem = datalist.find(x => x.workID === $routeParams.id);
            if (dataitem != undefined) {
                const indexdata = datalist.indexOf(dataitem);
                var shiftdata = datalist.shift();
                $scope.setOldReviewSessionData(shiftdata);
                appServices.setReviewSessionData(datalist);
                // console.log(datalist);
            }
        } else if (appServices.getQuotaExceeded()) {
            var dataitem = datalist.find(x => x.workID === $routeParams.id);
            if (dataitem != undefined) {
                var shiftdata = datalist.shift();
                $scope.setOldReviewSessionData(shiftdata);
                appServices.setReviewSessionData(datalist);
                // console.log(datalist);
            }
        }
        $timeout(function () {
            appServices.buttonLoader(false, button_id, true, true);
            if (!isNaN(reviewlist)) {
                $location.path('/work-list');
            } else {
                $location.path('/review/' + reviewlist[0].workID);
            }
        }, 500);

    };

    $scope.goToByScroll = function (id) {
        $('.scroll-container').animate({
            scrollTop: $("#" + id).offset().top
        }, 'slow');
    };

}]);