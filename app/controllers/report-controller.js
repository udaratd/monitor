app.controller('ReportCtrl', ['$scope', '$location', 'appServices', 'reportServices', 'userServices', 'locationServices', function ($scope, $location, appServices, reportServices, userServices, locationServices) {

    $scope.comparisonRep = false;
    $scope.reworkRep = false;
    $scope.initDashboard = function () {
        $scope.getDashboardReport();
    };

    $scope.initExecutive = function () {
        $scope.getExecutiveReport();
        $scope.viewDateRange();
        $scope.filterParams = {};
    };

    $scope.initComparison = function () {
        $scope.comparisonRep = true;
        $scope.filterParams = {};
        $scope.getStaffList();
        $scope.viewDateRange();
    };

    $scope.initRework = function () {
        $scope.reworkRep = true;
        $scope.filterParams = {};
        $scope.viewDateRange();
        $scope.getReworkReport($scope.filterParams);
        $scope.getStaffList();
        $scope.getLocationList();
    };

    $scope.initRepCustomer = function () {
        $scope.getCustomerReport();
        $scope.viewDateRange();
        $scope.filterParams = {};
    };

    // ------- Get Checkpoint List ------- //
    $scope.getCheckpointList = function () {
        reportServices.getCheckpointList().success(function (response) {
            if (response.response.success) {
                $scope.checkpoint_list = response.data;
            }
        });
    };

    // ------- Get Location List ------- //
    $scope.getLocationList = function () {
        locationServices.getLocationList().success(function (response) {
            if (response.response.success) {
                var list = [];
                angular.forEach(response.data, function (value) {
                    if (value.childs != null || value.childs != undefined) {
                        list.push(value);
                    }
                });
                $scope.location_list = locationServices.prepareLocationList(list);

                //                if($scope.reworkRep){
                //                    $scope.filter_rework_report_by_location = $scope.location_list[0].ID;
                //                    $scope.filterParams.location_id = $scope.location_list[0].ID;
                //                    $scope.getReworkReport($scope.filterParams);
                //                }
                return $scope.location_list;
            }
        });
    };

    // ------- Get Staff List and Report data ------- //
    $scope.getStaffList = function () {
        userServices.getStaffList().success(function (response) {
            if (response.response.success) {
                $scope.staff_list = response.data;

                if ($scope.comparisonRep) {
                    // $scope.filter_comp_report_by_staff_1 = $scope.staff_list[0].ID;
                    // $scope.filter_comp_report_by_staff_2 = $scope.staff_list[1].ID;
                    $scope.filterParams.staff_1 = $scope.staff_list[0].ID;
                    $scope.filterParams.staff_2 = $scope.staff_list[1].ID;


                    $scope.getComparisonReport($scope.filterParams);
                }
                return $scope.staff_list;
            }
        });
    };

    // Get Executive report
    $scope.getExecutiveReport = function (params) {
        reportServices.getExecutiveReport(params).success(function (response) {
            if (response.response.success) {

                var res = response.data;

                // Completed Tasks Circle
                if (res.completedTask != null && res.completedTask != undefined) {
                    if (res.completedTask.percentage != null) {
                        res.completedTask.percentage = appServices.percentageConvert(res.completedTask.percentage);
                    }
                }

                // On Time Circle
                if (res.ontime != null && res.ontime != undefined) {
                    if (res.ontime.onTimeTasksPercentage != null) {
                        res.ontime.onTimeTasksPercentage = appServices.percentageConvert(res.ontime.onTimeTasksPercentage);
                    }
                    if (res.ontime.percentage != null) {
                        res.ontime.percentage = appServices.percentageConvert(res.ontime.percentage);
                    }
                }

                // Rework Rate Circle
                if (res.reworkRate != null && res.reworkRate != undefined) {
                    if (res.reworkRate.reworkPercentage != null) {
                        res.reworkRate.reworkPercentage = appServices.percentageConvert(res.reworkRate.reworkPercentage);
                    }
                    if (res.reworkRate.percentage != null) {
                        res.reworkRate.percentage = appServices.percentageConvert(res.reworkRate.percentage);
                    }
                }

                // Flagged Rate Circle
                if (res.flaggedRate != null && res.flaggedRate != undefined) {
                    if (res.flaggedRate.flaggedPercentage != null) {
                        res.flaggedRate.flaggedPercentage = appServices.percentageConvert(res.flaggedRate.flaggedPercentage);
                    }
                    if (res.flaggedRate.percentage != null) {
                        res.flaggedRate.percentage = appServices.percentageConvert(res.flaggedRate.percentage);
                    }
                }

                // Late Submitted Checkpoints
                if (res.lateSubmitedCheckpoint != null && res.lateSubmitedCheckpoint != undefined) {
                    angular.forEach(res.lateSubmitedCheckpoint, function (value) {
                        value.timeDiff = appServices.convertTimeDifference(value.timeDiff);
                    });
                }

                $scope.executive = res;
            }
        });
    };

    // ------- Filter Executive Report ------- //
    $scope.filterExecutiveReport = function (date_period) {
        if (date_period != null && date_period != undefined && date_period != '') {
            //$scope.filterParams.datePeriod = date_period;
            var date_object = JSON.parse(date_period);
            $scope.filterParams.s_date = date_object.start;
            $scope.filterParams.e_date = date_object.end;
        }
        $scope.getExecutiveReport($scope.filterParams);
    };

    // ------- Filter Comparison Report ------- //
    $scope.filterComparisonReport = function (date_period, staff_id_1, staff_id_2) {
        if (date_period != null && date_period != undefined && date_period != '') {
            var date_object = JSON.parse(date_period);
            $scope.filterParams.s_date = date_object.start;
            $scope.filterParams.e_date = date_object.end;
        }
        if (staff_id_1 != null && staff_id_1 != undefined) {
            $scope.filterParams.staff_1 = staff_id_1;
        }
        if (staff_id_2 != null && staff_id_2 != undefined) {
            $scope.filterParams.staff_2 = staff_id_2;
        }
        $scope.getComparisonReport($scope.filterParams);


    };

    // ------- Filter Rework Report ------- //
    $scope.filterReworkReport = function (date_period, staff_id, location_id) {
        if (date_period != null && date_period != undefined && date_period != '') {
            var date_object = JSON.parse(date_period);
            $scope.filterParams.s_date = date_object.start;
            $scope.filterParams.e_date = date_object.end;
        }
        if (staff_id != null && staff_id != undefined) {
            $scope.filterParams.staff_id = staff_id;
        }
        if (location_id != null && location_id != undefined) {
            $scope.filterParams.location_id = location_id;
        }
        $scope.getReworkReport($scope.filterParams);
    };

    // ------- Filter Customer Report ------- //
    $scope.filterCustomerReport = function (date_period) {
        if (date_period != null && date_period != undefined && date_period != '') {
            //$scope.filterParams.datePeriod = date_period;
            var date_object = JSON.parse(date_period);
            $scope.filterParams.s_date = date_object.start;
            $scope.filterParams.e_date = date_object.end;
        }
        $scope.getCustomerReport($scope.filterParams);
    };

    // ------- View date range according to the filter change ------- //
    $scope.viewDateRange = function (range) {
        var s_date = moment().startOf('month');
        var e_date = moment().startOf('day').toDate();
        if (range != undefined && range != '') {
            var date_object = JSON.parse(range);
            s_date = date_object.start;
            e_date = date_object.end;
        }
        $scope.date_range_view = moment(s_date).format('MMM D, YYYY') + ' - ' + moment(e_date).format('MMM D, YYYY');
    };

    // Get Comparison report
    $scope.getComparisonReport = function (params) {
        reportServices.getComparisonReport(params).success(function (response) {

            $scope.chart_rework_no_data = true;
            $scope.chart_ontime_no_data = true;
            if (!response.response.success) {
                return;
            }

            if (response.data.user[0] !== null && response.data.user[0] !== undefined) {
                $scope.compUsers = response.data.user;
            }

            if (response.data.completedCheckpoints[0] !== null && response.data.completedCheckpoints[0] !== undefined) {
                $scope.chartCompletedCheckpoints(response.data);
            }

            if (response.data.rework !== null && response.data.rework !== undefined) {
                if (response.data.rework.staff_1 != 0 || response.data.rework.staff_2 != 0) {
                    $scope.chartReworks(response.data);
                    $scope.chart_rework_no_data = false;
                }
            }

            if (response.data.ontime !== null && response.data.ontime !== undefined) {
                if (response.data.ontime.staff_1 != 0 || response.data.ontime.staff_2 != 0) {
                    $scope.chartOntime(response.data);
                    $scope.chart_ontime_no_data = false;
                }
            }
        });
    };

    // Get Rework report
    $scope.getReworkReport = function (params) {
        reportServices.getReworkReport(params).success(function (response) {

            $scope.chart_data_availability = true;
            if (!response.response.success) {
                $scope.chart_data_availability = false;
                return;
            }
            if (response.data.process_checkpoints[0] !== null && response.data.process_checkpoints[0] !== undefined) {
                $scope.chartReworkedCheckpoints(response.data.process_checkpoints);
            }
            if (response.data.process_staffs !== null && response.data.process_staffs !== undefined) {
                $scope.chartReworkedStaff(response.data.process_staffs);
            }
        });
    };

    // Print Rework Report
    $scope.printReworkReport = function () {
        //        $scope.filterParams.print = '';
        //        params = $.param($scope.filterParams);
        //        var url = window.location.protocol+'//'+window.location.hostname+'/api/Report/process?'+params;
        //        window.open(url, '_blank');
    };

    // Get Customer report
    $scope.getCustomerReport = function (params) {
        reportServices.getCustomerReport(params).success(function (response) {
            if (response.response.success) {
                $scope.rep_cus_checkpoint_data = response.data;
            }
        });
    };

    // Get Dashboard report
    $scope.getDashboardReport = function () {
        reportServices.getDashboardReport().success(function (response) {
            if (response.response.success) {

                var res = response.data;

                // Completed Tasks Circle
                if (res.completedTask != null && res.completedTask != undefined) {
                    if (res.completedTask.percentage != null) {
                        res.completedTask.percentage = appServices.percentageConvert(res.completedTask.percentage);
                    }
                }

                // On Time Circle
                if (res.ontime != null && res.ontime != undefined) {
                    if (res.ontime.onTimeTasksPercentage != null) {
                        res.ontime.onTimeTasksPercentage = appServices.percentageConvert(res.ontime.onTimeTasksPercentage);
                    }
                    if (res.ontime.percentage != null) {
                        res.ontime.percentage = appServices.percentageConvert(res.ontime.percentage);
                    }
                }

                // Rework Rate Circle
                if (res.reworkRate != null && res.reworkRate != undefined) {
                    if (res.reworkRate.reworkPercentage != null) {
                        res.reworkRate.reworkPercentage = appServices.percentageConvert(res.reworkRate.reworkPercentage);
                    }
                    if (res.reworkRate.percentage != null) {
                        res.reworkRate.percentage = appServices.percentageConvert(res.reworkRate.percentage);
                    }
                }

                // Reworked Checkpoints
                if (res.reworkedCheckpoint != null && res.reworkedCheckpoint != undefined) {
                    angular.forEach(res.reworkedCheckpoint, function (value) {
                        value.uploadServerDate = moment(value.uploadServerDate).format('ddd, D MMM YYYY');
                    });
                }

                $scope.dashboard = res;
            }
        });
    };

    // Process Completed Checkpoints Chart
    $scope.chartCompletedCheckpoints = function (data) {

        var date_list = [];
        var staff_member_1 = [];
        var staff_member_2 = [];

        angular.forEach(data.completedCheckpoints, function (value) {
            date_list.push(moment(value.date).format('MMM D'));
            staff_member_1.push(value.staff_1);
            staff_member_2.push(value.staff_2);
        });

        Highcharts.chart('chart-completed-checkpoints', {
            title: false,
            xAxis: {
                title: {
                    text: 'Days'
                },
                categories: date_list
            },
            yAxis: {
                title: {
                    text: 'Checkpoints'
                }
            },
            series: [{
                name: data.user[0].staff_1.firstName,
                data: staff_member_1,
                color: '#7fa5d5'
            }, {
                name: data.user[1].staff_2.firstName,
                data: staff_member_2,
                color: '#389c37'
            }]
        });
    };

    // Process Reworks Chart
    $scope.chartReworks = function (data) {

        var user_data = data.user;
        var rework_data = data.rework;

        Highcharts.chart('chart-reworks', {
            chart: {
                type: 'bar'
            },
            title: false,
            xAxis: {
                title: {
                    text: 'Staff'
                },
                categories: [user_data[0].staff_1.firstName, user_data[1].staff_2.firstName]
            },
            yAxis: {
                title: {
                    text: 'Rework Amount'
                },
                min: 0
            },
            series: [{
                showInLegend: false,
                name: 'Rework Amount',
                data: [{
                        y: parseInt(rework_data.staff_1),
                        name: user_data[0].staff_1.firstName,
                        color: '#69a3e4'
                    },
                    {
                        y: parseInt(rework_data.staff_2),
                        name: user_data[1].staff_2.firstName,
                        color: '#389c37'
                    }
                ]
            }]
        });
    };

    // OnTime Completion Chart
    $scope.chartOntime = function (data) {

        var user_data = data.user;
        var ontime_data = data.ontime;

        Highcharts.chart('chart-ontime', {
            chart: {
                type: 'bar'
            },
            title: false,
            xAxis: {
                title: {
                    text: 'Staff'
                },
                categories: [user_data[0].staff_1.firstName, user_data[1].staff_2.firstName]
            },
            yAxis: {
                title: {
                    text: 'Rate %'
                },
                min: 0
            },
            series: [{
                showInLegend: false,
                name: 'Rate',
                data: [{
                        y: parseInt(ontime_data.staff_1),
                        name: user_data[0].staff_1.firstName,
                        color: '#69a3e4'
                    },
                    {
                        y: parseInt(ontime_data.staff_2),
                        name: user_data[1].staff_2.firstName,
                        color: '#389c37'
                    }
                ]
            }]
        });
    };

    // Process Reworked Checkpoints Chart
    $scope.chartReworkedCheckpoints = function (data) {

        var date_list = [];
        var checkpoint_list = [];
        var series = [];

        // Creates checkpoint list
        angular.forEach(data[0].checkpoints, function (val, key) {
            var chk_item = [];
            chk_item.name = key;
            chk_item.data = [];
            checkpoint_list.push(chk_item);
        });

        // Creates date list
        angular.forEach(data, function (value) {
            date_list.push(moment(value.date).format('MMM D'));

            var index = 0;
            angular.forEach(value.checkpoints, function (val, key) {
                checkpoint_list[index].data.push(parseInt(val));
                index++;
            });
        });

        // Inserting data into chart
        angular.forEach(checkpoint_list, function (value) {
            series.push(value);
        });

        Highcharts.chart('chart-rework-checkpoints', {
            title: false,
            xAxis: {
                title: {
                    text: 'Day'
                },
                categories: date_list
            },
            yAxis: {
                title: {
                    text: 'Rework Rate'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            series: series
        });
    };

    // Process Reworked Staff Chart
    $scope.chartReworkedStaff = function (data) {

        var name_list = [];
        var value_list = [];

        angular.forEach(data, function (value, key) {
            var value_item = {};
            name_list.push(key);
            value_item.y = parseInt(value);
            value_item.color = generateColorCode();
            value_list.push(value_item);
        });

        Highcharts.chart('chart-rework-staff', {
            chart: {
                type: 'column'
            },
            title: false,
            yAxis: {
                title: {
                    text: 'Rework Rate'
                }
            },
            xAxis: {
                title: {
                    text: 'Date Period'
                },
                categories: name_list
            },
            plotOptions: {
                series: {
                    pointPadding: 0,
                    pointWidth: 25,
                    groupPadding: 0,
                    states: {
                        hover: {
                            enabled: false
                        }
                    }
                }
            },
            series: [{
                type: 'column',
                showInLegend: false,
                name: 'Rate',
                data: value_list
            }]
        });
    };

    // Change views
    $scope.changeView = function (path) {
        $location.path('report/' + path);
    };

    function generateColorCode() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

}]);