// ------- App Services ------- //
app.service('appServices', function ($http) {

    var REQUEST_URL = '/api';
    var SESSION_DATA;
    var Review_count;

    this.getRequestUrl = function () {
        return REQUEST_URL;
    };

    this.setSessionData = function (data) {
        SESSION_DATA = data;
    };

    this.getSessionData = function () {
        return SESSION_DATA;
    };

    this.setReviewCount = function (data) {
        Review_count = data;
    };
    
    this.getReviewCount = function () {
        return Review_count;
    };

    /*
     * Outputs session single data 
     * This data will remove in the logout (main-controller.js)
     * @param name string
     */
    this.getSessionDataByName = function (name) {
        if (name == undefined || name == '') {
            return;
        }
        if (name === 'id') {
            return localStorage.getItem("lu_id");
        }
        if (name === 'role') {
            return localStorage.getItem("lu_role");
        }
        if (name === 'first_name') {
            return localStorage.getItem("lu_fname");
        }
        if (name === 'last_name') {
            return localStorage.getItem("lu_lname");
        }
        if (name === 'email') {
            return localStorage.getItem("lu_email");
        }
        if (name === 'phone') {
            return localStorage.getItem("lu_phone");
        }
        if (name === 'nic') {
            return localStorage.getItem("lu_nic");
        }
        if (name === 'avatar') {
            return localStorage.getItem("lu_avatar");
        }
        if (name === 'company_logo') {
            return localStorage.getItem("lu_company_logo");
        }
        if (name === 'company_name') {
            return localStorage.getItem("company_name");
        }
        if (name === 'company_uname') {
            return localStorage.getItem("lu_company_uname");
        }
        if (name === 'debug') {
            return localStorage.getItem("lu_debug_mode");
        }
        if (name === 'skip_validation') {
            return localStorage.getItem("lu_skip_validation");
        }

    };

    this.setSessionDataByName = function (attribute, data) {
        if (attribute === 'first_name') {
            return localStorage.setItem("lu_fname", data);
        }
        if (attribute === 'last_name') {
            return localStorage.setItem("lu_lname", data);
        }
        if (attribute === 'email') {
            return localStorage.setItem("lu_email", data);
        }
        if (attribute === 'phone') {
            return localStorage.setItem("lu_phone", data);
        }
        if (attribute === 'avatar') {
            return localStorage.setItem("lu_avatar", data);
        }
        if (attribute === 'nic') {
            return localStorage.setItem("lu_nic", data);
        }
        if (attribute === 'company_logo') {
            return localStorage.setItem("lu_company_logo", data);
        }
        if (attribute === 'company_name') {
            return localStorage.setItem("company_name", data);
        }
        if (attribute === 'company_uname') {
            return localStorage.setItem("lu_company_uname", data);
        }
        if (attribute === 'debug') {
            return localStorage.getsetItemItem("lu_debug_mode", data);
        }

    };


    // next Data
    this.setReviewSessionData = function (data) {
        // if ("nextdata" in localStorage) {
        //     localStorage.removeItem("nextdata")
        // }
        try {
            localStorage.setItem('nextdata', JSON.stringify(data));
            return true;
        } catch (e) {
            if (e.code == 22) {
                console.log("quota exceeded");

            }
            return false;
        }

    };
    this.getReviewSessionData = function () {
        return JSON.parse(localStorage.getItem('nextdata'));
    };

    // old Data
    this.setOldReviewSessionData = function (data) {
        // if ("oldreviewdata" in localStorage) {
        //     localStorage.removeItem("oldreviewdata")
        // }
        try {
            localStorage.setItem('oldreviewdata', JSON.stringify(data));
            return true;
        } catch (e) {
            if (e.code == 22) {
                console.log("quota exceeded")
            }
            return false;
        }

    };
    this.getOldReviewSessionData = function () {
        return JSON.parse(localStorage.getItem('oldreviewdata'));
    };

    // pending data
    this.setPendingListData = function (data, splice) {
        if ("pendinglist" in localStorage) {
            localStorage.removeItem("pendinglist");
        }
        if (splice) {
            data.shift();
            localStorage.setItem("pendinglist", JSON.stringify(data));
        } else {
            localStorage.setItem("pendinglist", JSON.stringify(data));
        }

    };
    this.getPendingListData = function () {
        return JSON.parse(localStorage.getItem("pendinglist"));
    };

    // pending to review
    this.setReviewPendingListData = function (data) {
        if ("reviewpending" in localStorage) {
            localStorage.removeItem("reviewpending");
        }
        localStorage.setItem("reviewpending", JSON.stringify(data));
    };
    this.getReviewPendingListData = function () {
        return JSON.parse(localStorage.getItem("reviewpending"));
    };

    // quota exceeded
    this.setQuotaExceeded = function (state) {
        if ("quotaExceeded" in localStorage) {
            localStorage.removeItem("quotaExceeded");
        }
        localStorage.setItem("quotaExceeded", state);
    };
    this.getQuotaExceeded = function () {
        return JSON.parse(localStorage.getItem("quotaExceeded"));
    };


    this.resetDataLocal = function () {
        var pending = [];
        var review = [];
        localStorage.setItem("reviewpending", JSON.stringify(pending));
        localStorage.setItem("pendinglist", JSON.stringify(review));
    };


    // User Logout //
    this.userLogout = function () {
        return $http.get(REQUEST_URL + '/User/logout');
    };

    this.toUrlParams = function (obj) {
        if (obj !== null) {
            var str = [];
            for (var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            return str.join("&");
            return str;
        }
    };

    /*
     * Renders corresponding output messages Response message
     * @param visibility boolean Whether to display or hide the error message. Default is false
     * @param object_id string Error Message Object
     * @param type boolean Whether the message is success or false. Success = true. Error = false
     * @param message string Message to display
     */
    this.responseMessage = function (visibility, object_id, type, message) {
        var container = $('#' + object_id);
        if (visibility) {
            if (type) {
                container.removeClass('error');
                container.addClass('success');
            } else {
                container.removeClass('success');
                container.addClass('error');
            }
            container.text(message);
            container.fadeIn();
        } else {
            container.hide();
        }
    };

    /*
     * Display the loading circle inside the button until ajax reponse comes
     * @param visibility boolean Whether to display or hide the loader. Default is false
     * @param object_id string Button Object
     * @param after boolean Whether to display success or fail icon after request
     */
    this.buttonLoader = function (visibility, object_id, after, after_status) {
        var loader_object = $('#' + object_id);
        if (visibility) {
            loader_object.addClass('spin-loader-mini');
        } else {
            loader_object.removeClass('spin-loader-mini');
            if (after) {
                if (after_status) {
                    loader_object.removeClass('ajax-fail');
                    loader_object.addClass('ajax-success');
                } else {
                    loader_object.removeClass('ajax-success');
                    loader_object.addClass('ajax-fail');
                }
            }
        }
    };

    /*
     * Display the popups
     * @param visibility boolean Whether to display or hide the popup. Default is false
     * @param object_id string popup Object
     */
    this.controllPopup = function (visibility, object_id) {
        var popup_object = $('#' + object_id);
        if (visibility) {
            popup_object.addClass('in');
        } else {
            popup_object.removeClass('in');
        }
    };

    /*
     * Display the popups
     * @param visibility boolean Whether to display or hide the popup. Default is false
     * @param object_id string popup Object
     */
    this.clearPopupImages = function (container) {
        $("#" + container + " .uploader-wrapper img").remove();
        $("#" + container + " .uploader-wrapper .remove-img-icon").remove();
    };


    /*
     * Convert a Time difference into a user friendly view (ex: 03:20:35 -> 03 Hours 20 Minutes)
     * @param time_dif string Time difference
     */
    this.convertTimeDifference = function (time_dif) {
        var output = '';
        if (time_dif != '' && time_dif != undefined && time_dif != null) {
            var arr = time_dif.split(":");
            if (arr[0] !== '00') {
                if (arr[0] === '01') {
                    output += arr[0] + ' Hour';
                } else {
                    output += arr[0] + ' Hours';
                }
            }
            if (arr[1] !== '00') {
                output += ' ' + arr[1] + ' Minutes';
            }
        } else {
            output += 'Ontime';
        }
        return output.replace('-', '');
    };

    this.timeConvert = function (time) {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice(1); // Remove full string match value
            time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    };

    this.percentageConvert = function (percentage) {
        if (percentage != null && percentage != undefined) {
            percentage = Math.round(Math.abs(percentage));
            return percentage;
        }
    };

});

// ------- User Services ------- //
app.service('userServices', function ($http, appServices) {

    // Get User Roles //
    this.getUserRoles = function () {
        return $http.get(appServices.getRequestUrl() + '/User/roles');
    };

    // Get User Roles with 'All' //
    this.getUserRolesWithAll = function () {
        var list = [{
                id: 'All',
                label: 'All'
            },
            {
                id: 1,
                label: 'Admin'
            },
            {
                id: 2,
                label: 'Supervisor'
            },
            {
                id: 3,
                label: 'Staff'
            }
        ];
        return list;
    };

    // Get Supervisor List //
    this.getSupervisors = function () {
        return $http.get(appServices.getRequestUrl() + '/User/listsupervisor');
    };

    // Get Staff List //
    this.getStaffList = function () {
        return $http.get(appServices.getRequestUrl() + '/schedule/staff');
    };

    // Get Single User //
    this.getSingleUser = function (id) {
        return $http.get(appServices.getRequestUrl() + '/User/single?id=' + id);
    };

    // Get User List //
    this.getUserList = function (params) {
        value = '';
        role = 'All';
        e_limit = 20;
        limit = 'all';

        if (params != null || params != undefined) {
            role = params;
        }

        return $http.get(appServices.getRequestUrl() + '/User/search?value=' + value + '&role=' + role + '&e_limit=' + e_limit + '&limit=' + limit);
    };

    // Map user role id with label
    this.mapUserRoles = function (id) {
        var output = 'No Role';
        if (id != null) {
            if (id == 1) {
                output = 'Admin';
            } else if (id == 2) {
                output = 'Supervisor';
            } else if (id == 3) {
                output = 'Staff';
            }
        }
        return output;
    };

    // Import Users from file //
    this.importUser = function (data) {
        return $http.post(appServices.getRequestUrl() + '/User/import', $.param(data), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });
    };

    // Get User log //
    this.getUserLog = function (params) {
        // var search = '';
        var s_date = moment().startOf('month').format('YYYY-MM-DD');
        var e_date = moment().format('YYYY-MM-DD');

        var e_limit = 20;
        var limit = '';
        var page_number = 1;
        if (params != null || params != undefined) {

            if (params.hasOwnProperty('s_date')) {
                s_date = params.s_date;
            }
            if (params.hasOwnProperty('e_date')) {
                e_date = params.e_date;
            }
            if (params.hasOwnProperty('e_limit')) {
                e_limit = params.e_limit;
            }
            if (params.hasOwnProperty('limit')) {
                limit = params.limit;
            }
            if (params.hasOwnProperty('page_number')) {
                page_number = params.page_number;
            }
        }
        var url = appServices.getRequestUrl() + '/Logactivity/list?s_date=' + s_date + '&e_date=' + e_date + '&e_limit=' + e_limit + '&limit=' + limit + '&page_number=' + page_number;
        return $http.get(url);
    };

});

// ------- Location Services ------- //
app.service('locationServices', function ($http, appServices) {

    // Get Location List //
    this.getLocationList = function () {
        return $http.get(appServices.getRequestUrl() + '/Location/all');
    };

    // Get Second Level Location List //
    this.getSecondLevelLocationList = function (id) {
        return $http.get(appServices.getRequestUrl() + '/Location/child?id=' + id);
    };

    // Get Checkpoint list by location //
    this.getCheckpointListByLocationId = function (id) {
        return $http.get(appServices.getRequestUrl() + '/Location/checkpoints?id=' + id);
    };

    // Prepare Location list to show (ex: Floor 1 > Room 2)
    this.prepareLocationList = function (data) {
        var prepared_list = [];
        angular.forEach(data, function (value) {
            if (value.hasOwnProperty('childs')) {
                angular.forEach(value.childs, function (value_child) {
                    value_child.name = value.name + ' > ' + value_child.name;
                    prepared_list.push(value_child);
                });
            } else {
                prepared_list.push(value);
            }
        });

        return prepared_list;
    };

    // Get Single Location //
    this.getSingleLocation = function (id) {
        return $http.get(appServices.getRequestUrl() + '/Location/single?id=' + id);
    };

    // Import Locations from file //
    this.importLocations = function (data) {
        return $http.post(appServices.getRequestUrl() + '/Location/import', $.param(data), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });
    };

});

// ------- Checkpoint Services ------- //
app.service('checkpointServices', function ($http, appServices) {

    // Get Schedule Checkpoint List //
    this.getScheduleCheckpointList = function () {
        return $http.get(appServices.getRequestUrl() + '/schedule/scheduleCheckpoints');
    };

    // Get Checkpoint List //
    this.getCheckpointList = function (params) {
        name = '';
        locationid = 'All';
        // e_limit = 20;
        e_limit = 15;
        limit = '';
        page_number = 1;
        // if (params != null || params != undefined) {
        //     locationid = params;
        // }

        if (params != null && params != undefined) {
            if (params.hasOwnProperty('locationid')) {
                locationid = params.locationid;
            }
            if (params.hasOwnProperty('e_limit')) {
                e_limit = params.e_limit;
            }
            if (params.hasOwnProperty('limit')) {
                limit = params.limit;
            }
            if (params.hasOwnProperty('page_number')) {
                page_number = params.page_number;
            }
        }

        return $http.get(appServices.getRequestUrl() + '/checkpoint/search?name=' + name + '&locationid=' + locationid + '&e_limit=' + e_limit + '&limit=' + limit + '&page_number=' + page_number);
    };

    // Get Single Checkpoint //
    this.getSingleCheckpoint = function (id) {
        return $http.get(appServices.getRequestUrl() + '/checkpoint/single?id=' + id);
    };

    // Import Checkpoints from file //
    this.importCheckpoints = function (data) {
        return $http.post(appServices.getRequestUrl() + '/Checkpoint/import', $.param(data), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });
    };
    
    // Add bulk checkpoints //
    this.addBulkCheckpoints = function ( data ) {
        return $http.post( appServices.getRequestUrl() + '/Checkpoint/add_bulk', $.param( data ), { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } });
    };

});

// ------- Schedule Services ------- //
app.service('scheduleServices', function ($http, appServices) {

    // List Repeat types //
    this.listRepeatTypes = function () {
        var list = [{
                id: 2,
                label: 'Weekly'
            },
            {
                id: 1,
                label: 'Daily'
            },
            {
                id: 3,
                label: 'One Day'
            }
        ];
        return list;
    };

    // Get Schedule List //
    this.getScheduleList = function (params) {
        name = '';
        checkName = 'All';
        parent = '';
        child = '';
        day_count = '';
        e_limit = 20;
        limit = 'all';

        if (params != null || params != undefined) {
            if (params.hasOwnProperty('name')) {
                name = params.name;
            }
            if (params.hasOwnProperty('checkName')) {
                checkName = params.checkName;
            }
            if (params.hasOwnProperty('parent')) {
                parent = params.parent;
            }
            if (params.hasOwnProperty('child')) {
                child = params.child;
            }
            if (params.hasOwnProperty('day_count')) {
                day_count = params.day_count;
            }
        }

        var url = appServices.getRequestUrl() + '/schedule/listSearch?name=' + name + '&checkName=' + checkName + '&parent=' + parent + '&child=' + child + '&day_count=' + day_count + '&e_limit=' + e_limit + '&limit=' + limit;
        return $http.get(url);
    };

});

// ------- Worklist Services ------- //
app.service('worklistServices', function ($http, appServices) {

    // Get WorkList //
    this.getWorkList = function (params) {
        var staff_id = 'All';
        var location_id = 'All Location';
        var status = 'All Action';
        var search = '';
        var s_date = moment().startOf('month').format('YYYY-MM-DD');
        var e_date = moment().format('YYYY-MM-DD');

        var e_limit = 20;
        var limit = '';
        var page_number = 1;

        if (params != null || params != undefined) {
            if (params.hasOwnProperty('staff_id')) {
                staff_id = params.staff_id;
            }
            if (params.hasOwnProperty('location_id')) {
                location_id = params.location_id;
            }
            if (params.hasOwnProperty('status')) {
                status = params.status;
            }
            if (params.hasOwnProperty('s_date')) {
                s_date = params.s_date;
            }
            if (params.hasOwnProperty('e_date')) {
                e_date = params.e_date;
            }
            if (params.hasOwnProperty('e_limit')) {
                e_limit = params.e_limit;
            }
            if (params.hasOwnProperty('limit')) {
                limit = params.limit;
            }
            if (params.hasOwnProperty('page_number')) {
                page_number = params.page_number;
            }
        }

        var url = appServices.getRequestUrl() + '/work/all?staff_id=' + staff_id + '&location_id=' + location_id + '&status=' + status + '&search=' + search + '&s_date=' + s_date + '&e_date=' + e_date + '&e_limit=' + e_limit + '&limit=' + limit + '&page_number=' + page_number;
        return $http.get(url);

    };

    // Get Work Statuses //
    this.getWorkStatusesWithAll = function () {
        var list = [{
                id: 'All Action',
                label: 'All Actions'
            },
            {
                id: 'Approved',
                label: 'Approved'
            },
            {
                id: 'Pending',
                label: 'Pending'
            },
            {
                id: 'Flagged',
                label: 'Flagged'
            },
            {
                id: 'Rework',
                label: 'Rework'
            }
        ];
        return list;
    };

    // get Device Infomations
    this.viewDeviceInfo = function (id) {
        return $http.get(appServices.getRequestUrl() + '/Work/deviceinfo?id=' + id);
    }

});

// ------- Review Services ------- //
app.service('reviewServices', function ($http, appServices) {

    // Get Review Work count //
    this.getReviewWorkCount = function () {
        return $http.get(appServices.getRequestUrl() + '/Review/workCount');
    };

    // Get Single Review Work //
    this.getSingleReview = function (id) {
        return $http.get(appServices.getRequestUrl() + '/Work/single?id=' + id);
    };

    // Get Single Review Work //
    this.getPendingIds = function () {
        return $http.get(appServices.getRequestUrl() + '/Work/pendingids');
    };

});

// ------- Report Services ------- //
app.service('reportServices', function ($http, appServices) {

    // Get Dashboard Report //
    this.getDashboardReport = function () {
        return $http.get(appServices.getRequestUrl() + '/Dashboard/dashboard');
    };

    // Get Checkpoint List (simplified list) //
    this.getCheckpointList = function () {
        return $http.get(appServices.getRequestUrl() + '/Report/checkpoints');
    };

    // Get Executive report //
    this.getExecutiveReport = function (params) {
        var s_date = moment().startOf('month').format('YYYY-MM-DD');
        var e_date = moment().format('YYYY-MM-DD');
        if (params != null || params != undefined) {
            if (params.hasOwnProperty('s_date')) {
                s_date = params.s_date;
            }
            if (params.hasOwnProperty('e_date')) {
                e_date = params.e_date;
            }
        }
        return $http.get(appServices.getRequestUrl() + '/Report/executive?s_date=' + s_date + '&e_date=' + e_date);
    };

    // Get Comparison report //
    this.getComparisonReport = function (params) {
        var s_date = moment().startOf('month').format('YYYY-MM-DD');
        var e_date = moment().format('YYYY-MM-DD');
        var staff_1;
        var staff_2;
        if (params != null || params != undefined) {
            if (params.hasOwnProperty('s_date')) {
                s_date = params.s_date;
            }
            if (params.hasOwnProperty('e_date')) {
                e_date = params.e_date;
            }
            if (params.hasOwnProperty('staff_1')) {
                staff_1 = params.staff_1;
            }
            if (params.hasOwnProperty('staff_2')) {
                staff_2 = params.staff_2;
            }
        }
        return $http.get(appServices.getRequestUrl() + '/Report/comparison?staff_1=' + staff_1 + '&staff_2=' + staff_2 + '&s_date=' + s_date + '&e_date=' + e_date);
    };

    // Get Rework report //
    this.getReworkReport = function (params) {
        var s_date = moment().startOf('month').format('YYYY-MM-DD');
        var e_date = moment().format('YYYY-MM-DD');
        var staff_id = '';
        var location_id = '';
        if (params != null || params != undefined) {
            if (params.hasOwnProperty('s_date')) {
                s_date = params.s_date;
            }
            if (params.hasOwnProperty('e_date')) {
                e_date = params.e_date;
            }
            if (params.hasOwnProperty('staff_id')) {
                staff_id = params.staff_id;
            }
            if (params.hasOwnProperty('location_id')) {
                location_id = params.location_id;
            }
        }

        var url = appServices.getRequestUrl() + '/Report/process?staff_id=' + staff_id + '&location_id=' + location_id + '&s_date=' + s_date + '&e_date=' + e_date;
        return $http.get(url);
    };

    // Get Customer report //
    this.getCustomerReport = function (params) {
        var s_date = moment().startOf('month').format('YYYY-MM-DD');
        var e_date = moment().format('YYYY-MM-DD');
        if (params != null || params != undefined) {
            if (params.hasOwnProperty('s_date')) {
                s_date = params.s_date;
            }
            if (params.hasOwnProperty('e_date')) {
                e_date = params.e_date;
            }
        }
        return $http.get(appServices.getRequestUrl() + '/Report/customer?s_date=' + s_date + '&e_date=' + e_date);
    };

});

// ------- Notification Services ------- //
app.service('notificationServices', function ($http, appServices) {

    // Get Notification Count //
    this.getNotificationCount = function () {
        return $http.get(appServices.getRequestUrl() + '/Notification/count');
    };

    // Get All Notifications //
    this.getAllNotifications = function () {
        return $http.get(appServices.getRequestUrl() + '/Notification/allnotification');
    };

});

// ------- Company Services ------- //
app.service('companyServices', function ($http, appServices) {

    // Get Company Details //
    this.getCompanyDetails = function () {
        return $http.get(appServices.getRequestUrl() + '/Company/single');
    };

    // Update company details //
    this.updateCompany = function (data) {
        return $http.post(appServices.getRequestUrl() + '/Company/update', $.param(data), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });
    };

});

// ------- Feedback Services ------- //
app.service('feedbackServices', function ($http, appServices) {
    
    // Get Feedback reaction with 'All' //
    this.getFeedbackReaction = function () {
        var list = [           
            {
                id: 0,
                label: 'All'
            },
            {
                id: 1,
                label: 'Bad'
            },
            {
                id: 2,
                label: 'Good'
            },
            {
                id: 3,
                label: 'Great'
            }
        ];
        return list;
    };
    
    // Get FeedbackList //
    this.getFeedbackList = function (params) {
        var reaction = '';
        var search = '';

        var e_limit = 20;
        var limit = '';
        var page_number = 1;

        if (params != null || params != undefined) {
            if (params.hasOwnProperty('reaction')) {
                reaction = params.reaction;
            }
            if (params.hasOwnProperty('e_limit')) {
                e_limit = params.e_limit;
            }
            if (params.hasOwnProperty('limit')) {
                limit = params.limit;
            }
            if (params.hasOwnProperty('page_number')) {
                page_number = params.page_number;
            }
        }

        var url = appServices.getRequestUrl() + '/Customer/all?reaction=' + reaction + '&search=' + search + '&e_limit=' + e_limit + '&limit=' + limit + '&page_number=' + page_number;
        return $http.get(url);

    };
    
    // Get Single Feedback //
    this.getSingleFeedback = function (id) {
        return $http.get(appServices.getRequestUrl() + '/Customer/single?id=' + id);
    };
    
    // Get Unread Feedback count //
    this.getUnreadFeedbackCount = function () {
        return $http.get(appServices.getRequestUrl() + '/Customer/unread_count');
    };
});

// ------- App Factories ------- //
app.factory('appFactories', function ($http, $rootScope, appServices) {

    return {
        checkSession: function () {
            return $http.get(appServices.getRequestUrl() + '/User/validateSession').then(function (response) {
                if (!response.data.response.success) {
                    var uname = appServices.getSessionDataByName("company_uname");
                    if (uname != null) {
                        window.location.href = "/login/" + uname;
                    } else {
                        window.location.href = "/404.php";
                    }
                } else {
                    appServices.setSessionData(response.data.data);
                    $('.app-loader').hide();
                }
            });
        },
        getNotifications: function () {
            $rootScope.initNotifications();
        }
    };

});