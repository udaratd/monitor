// Page loading bar
app.directive('routeLoader', function () {
    return {
        restrict: 'EA',
        link: function (scope, element) {
            function changeProgress(percentage) {
                element.css('width', percentage);
            }

            function showElement() {
                changeProgress('0');
                $('.loading-bar').css('opacity', '1');
            }

            function hideElement() {
                $('.loading-bar').css('opacity', '0');
                setTimeout(function () {
                    element.css('width', 0);
                }, 500);
            }

            scope.$on('$routeChangeStart', function () {
                showElement();
                changeProgress('70%');
            });
            scope.$on('$routeChangeSuccess', function () {
                changeProgress('100%');
                setTimeout(hideElement, 500);
            });
            scope.$on('$routeChangeError', function () {
                changeProgress('100%');
                setTimeout(hideElement, 500);
            });
        }
    };
});

// Menu navigation active menu link
app.directive('activeLink', ['$location', function (location) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs, controller) {
            var clazz = attrs.activeLink;
            var path = attrs.href;
            scope.location = location;
            scope.$watch('location.path()', function (newPath) {
                if (newPath.includes(path)) {
                    element.parent().addClass(clazz);
                } else {
                    element.parent().removeClass(clazz);
                }
            });
        }
    };
}]);

// Execute / Open a Model
app.directive('modelExecute', [function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.click(function () {
                var modelId = attr.modelExecute;
                $('#model-' + modelId).addClass('in');
            });
        }
    };
}]);

// Dismiss / Close a Model
app.directive('modelDismiss', [function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.click(function () {
                var modelId = attr.modelDismiss;
                $('#model-' + modelId).removeClass('in');
            });
        }
    };
}]);

// Showing loader until image load
app.directive('appLoadingImage', [function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.after('<div class="content-loader"></div>');
            element.bind("load", function () {
                element.next('.content-loader').remove();
            });
        }
    };
}]);

// Content Drawer Execute
app.directive('drawerExecute', [function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.click(function () {
                var drawerId = attr.drawerExecute;
                $('#' + drawerId).addClass('open');
            });
        }
    };
}]);

// Settings header
app.directive('activeSettingsHeader', ['$location', function ($location) {
    return {
        restrict: 'A',
        link: function (scope, element) {
            scope.location = $location;
            scope.$watch('location.path()', function (currentPath) {
                if (currentPath === element[0].attributes['href'].nodeValue) {
                    element.parent().addClass('active');
                } else {
                    element.parent().removeClass('active');
                }
            });
        }
    };
}]);

// Jquery Ui Date RangePicker
app.directive('jqDateRangePicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            var startDate = moment().startOf('month');
            var endDate = moment().startOf('day').toDate();
            element.daterangepicker({
                datepickerOptions: {
                    numberOfMonths: 2,
                },
                dateFormat: 'M dd',
                initialText: moment(startDate).format('MMM DD') + ' - ' + moment(endDate).format('MMM DD'),
                clear: function( event, data ) {
                    var default_val = {
                        start: moment(startDate).format('YYYY-MM-DD'),
                        end: moment(endDate).format('YYYY-MM-DD')
                    };
                    ngModelCtrl.$setViewValue( JSON.stringify( default_val ) );
                }
            });
        }
    };
});

// Jquery Ui DatePicker
app.directive('jqDatePicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            $(function () {
                var minDate, maxDate = '';
                if (attrs.minDate != undefined) {
                    minDate = attrs.minDate;
                }
                if (attrs.maxDate != undefined) {
                    maxDate = attrs.maxDate;
                }
                element.datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: minDate,
                    maxDate: maxDate,
                    onSelect: function (date) {
                        scope.$apply(function () {
                            ngModelCtrl.$setViewValue(date);
                        });
                    }
                });
            });
        }
    };
});

// App Time Picker
app.directive('appTimePicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            callBackFunction: '&callbackFn',
            startHour: '=',
            startMin: '=',
            timePeriod: '='
        },
        link: function (scope, element, attrs, ngModelCtrl) {
            $(function () {
                random_num = Math.random().toString(36).substr(2, 9);
                element.wrap('<div class="time-selection"></div>');
                element.after('<div class="select-wrapper"><select class="form-control" id="time-period-' + random_num + '"></select></div>');
                element.after('<div class="select-wrapper"><select class="form-control" id="time-min-' + random_num + '"></select></div>');
                element.after('<div class="select-wrapper"><select class="form-control" id="time-hour-' + random_num + '"></select></div>');

                var i, j, time_hours, time_min, final_time;
                var time_arr = ['01', '00'];
                var time_period = 'am';
                var is24 = false;

                scope.$watch('startHour', function (new_val) {
                    setHoursOptions(scope.startHour);
                });
                scope.$watch('startMin', function (new_val) {
                    setMinsOptions(scope.startMin);
                });
                scope.$watch('timePeriod', function (new_val) {
                    settimePeriodOptions(scope.timePeriod);
                });

                console.log(scope.startHour);
                setHoursOptions(scope.startHour);
                setMinsOptions(scope.startMin);
                settimePeriodOptions(scope.timePeriod);




                function convertToTwoDigits(n) {
                    n = parseInt(n);
                    var ret = n > 9 ? "" + n : "0" + n;
                    return ret;
                }

                function setHoursOptions(hstart) {

                    $('#time-hour-' + random_num).empty();
                    console.log(hstart)
                    if (hstart == undefined) {
                        hstart = 1;
                    }

                    for (i = hstart; i <= 12; i++) {
                        $('#time-hour-' + random_num).append('<option value="' + convertToTwoDigits(i) + '">' + convertToTwoDigits(i) + '</option>');
                    }
                }


                function setMinsOptions(mstart) {
                    $('#time-min-' + random_num).empty();
                    console.log(mstart)
                    if (mstart == undefined) {
                        mstart = 0;
                    }

                    for (j = mstart; j < 60; j++) {
                        $('#time-min-' + random_num).append('<option value="' + convertToTwoDigits(j) + '">' + convertToTwoDigits(j) + '</option>');
                    }
                }

                function settimePeriodOptions(tstart) {
                    $('#time-period-' + random_num).empty();
                    console.log(tstart);
                    if (tstart == undefined) {
                        tstart = '{"am":1,"pm":1}';
                    }
                    // var jsonTP = JSON.parse(tstart);
                    if (tstart.am == 1) {
                        $('#time-period-' + random_num).append('<option value="am">AM</option>');
                    }
                    if (tstart.pm == 1) {
                        $('#time-period-' + random_num).append('<option value="pm">PM</option>');
                    }
                }


                $(document).on('change', '#time-hour-' + random_num, function () {
                    time_hours = $(this).val();
                    time_arr[0] = time_hours;
                    if (time_period == 'pm') {
                        is24 = true;
                        // time_arr[0] = +time_arr[0] + 12;
                        if (time_arr[0] != 12) {
                            time_arr[0] = convertToTwoDigits(+time_arr[0] + 12); // Additional '+' means to convert sting to number
                        } else {
                            time_arr[0] = convertToTwoDigits(+time_arr[0]); // Additional '+' means to convert sting to number
                        }
                    } else {
                        is24 = false;
                        if (time_arr[0] != 12) {
                            time_arr[0] = convertToTwoDigits(time_hours); // Additional '+' means to convert sting to number
                        } else {
                            time_arr[0] = convertToTwoDigits(time_hours - 12); // Additional '+' means to convert sting to number
                        }

                    }
                    final_time = time_arr[0] + ':' + time_arr[1];
                    ngModelCtrl.$setViewValue(final_time);
                    console.log(time_arr);
                    scope.callBackFunction({
                        filter_array: time_arr
                    });

                });
                $(document).on('change', '#time-min-' + random_num, function () {
                    time_min = $(this).val();
                    time_arr[1] = time_min;
                    final_time = time_arr[0] + ':' + time_arr[1];
                    ngModelCtrl.$setViewValue(final_time);
                    console.log(time_arr);
                    scope.callBackFunction({
                        filter_array: time_arr
                    });
                });
                $(document).on('change', '#time-period-' + random_num, function () {
                    time_period = $(this).val();
                    if (time_period == 'pm') {
                        is24 = true;
                        if (time_arr[0] != 12) {
                            time_arr[0] = +time_arr[0] + 12; // Additional '+' means to convert sting to number
                        } else {
                            time_arr[0] = +time_arr[0]; // Additional '+' means to convert sting to number
                        }

                        final_time = time_arr[0] + ':' + time_arr[1];
                        ngModelCtrl.$setViewValue(final_time);

                    } else {
                        is24 = false;
                        console.log(time_arr[0]);
                        time_arr[0] = convertToTwoDigits(+time_arr[0] - 12); // Additional '+' means to convert sting to number
                        final_time = time_arr[0] + ':' + time_arr[1];
                        ngModelCtrl.$setViewValue(final_time);
                    }
                    console.log(time_arr);
                    scope.callBackFunction({
                        filter_array: time_arr
                    });
                });
                ngModelCtrl.$setViewValue(final_time);
                // console.log(final_time);
            });
        }
    };
});

// Image Zoom
app.directive('imageZoom', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.zoom();
        }
    };
});

// Jquery Ui Draggable / Droppable
app.directive('dragDrop', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (attr.dragDrop === 'drag') {
                element.draggable();
            } else if (attr.dragDrop === 'drop') {
                element.droppable();
            }
        }
    };
});

/*
 * Converts file data into base 64 encoded string
 */
app.directive("appFileModel", function () {
    return {
        require: 'ngModel',
        scope: {
            callBackFunction: '&callbackFn'
        },
        link: function (scope, element, attributes, ngModel) {

            // Set default values
            ngModel.$render = function () {
                if (ngModel.$modelValue != undefined) {
                    element.parent().find('.prev-thumb').remove();
                    element.after('<img src="' + ngModel.$modelValue + '" alt="Preview" class="prev-thumb">');
                    element.before('<a class="remove-img-icon"><i class="fa fa-times" aria-hidden="true"></i></a>');
                    element.parent().find('.remove-img-icon').bind("click", function (e) {
                        element.parent().find('.prev-thumb').remove();
                        element.parent().find('.remove-img-icon').remove();
                        ngModel.$setViewValue("");
                        element.val("");

                    });
                }
            };

            element.bind("change", function (changeEvent) {
                var file_data = changeEvent.target.files[0];
                var reader = new FileReader();
                reader.onloadend = function (loadEvent) {
                    scope.$apply(function () {
                        var image_data = loadEvent.target.result;
                        var base64_string = image_data.split(";base64,").pop();

                        var stringLength = image_data.length - 'data:image/png;base64,'.length;

                        var sizeInBytes = 4 * Math.ceil((stringLength / 3)) * 0.5624896334383812;
                        var sizeInKb = sizeInBytes / 1000;
                        if (sizeInKb > 2000) {
                            element.parent().find('.prev-thumb').remove();
                            element.parent().find('.remove-img-icon').remove();
                            element.after('<p class="prev-thumb file-upload-error-message">Maximum file size is 2MB</p>');
                            ngModel.$setViewValue("");
                            return;
                        }

                        if (attributes.uploadMode == 'full') {
                            ngModel.$setViewValue(image_data);
                        } else {
                            ngModel.$setViewValue(base64_string);
                        }

                        if (!attributes.preview) {
                            element.parent().find('.prev-thumb').remove(); // Remove previously loaded images
                            element.parent().find('.remove-img-icon').remove();
                            var prev_path = '/assets/images/default-image-document.png';
                            if (file_data.type) { // Ex: "image/jpeg", "application/x-zip-compressed"
                                var type_prefix = (file_data.type).substr(0, (file_data.type).indexOf('/')); // Ex: "image", "application"
                                if (type_prefix == 'image') {
                                    prev_path = image_data;
                                }
                            }
                            element.after('<img src="' + prev_path + '" alt="Preview" class="prev-thumb">');
                            element.before('<a class="remove-img-icon"><i class="fa fa-times" aria-hidden="true"></i></a>');
                            element.parent().find('.remove-img-icon').bind("click", function (e) {
                                element.parent().find('.prev-thumb').remove();
                                element.parent().find('.remove-img-icon').remove();
                                ngModel.$setViewValue("");
                                element.val("");
                                // element.trigger("change");
                            });
                        }
                        scope.callBackFunction();
                    });
                };
                if (changeEvent.target.files[0] !== undefined) {
                    reader.readAsDataURL(changeEvent.target.files[0]);
                }
            });
        }
    };
});

app.directive('contentLoader', ['$http', function ($http) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };

            scope.$watch(scope.isLoading, function (v) {
                if (v) {
                    elm.show();
                    $('body').addClass('scroll-lock');
                } else {
                    elm.hide();
                    $('body').removeClass('scroll-lock');
                }
            });
        }
    };
}]);

app.directive('hcChart', function () {
    return {
        restrict: 'E',
        template: '<div></div>',
        scope: {
            options: '='
        },
        link: function (scope, element) {
            if (scope.options) {
                Highcharts.chart(element[0], scope.options);
            }
        }
    };
});

// Pagination
app.directive('listPagination', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            callBackFunction: '&callbackFn'
        },
        template: '<div class="pagination">' +
            '<div class="paginate-group">' +
            '<a href="#" class="disabled nav-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>' +
            '<span>{{page_number}} of {{page_count}}</span>' +
            '<a href="#" class="disabled nav-next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>' +
            '</div>' +
            '</div>',
        link: function (scope, element, attr) {
            attr.$observe('pageNumber', function (new_val) {
                scope.page_number = parseInt(new_val);
                setNavigation(scope.page_count, scope.page_number);
            });
            attr.$observe('pageCount', function (new_val) {
                scope.page_count = parseInt(new_val);
                setNavigation(scope.page_count, scope.page_number);
            });

            element.on('click', 'a.nav-next', function () {
                scope.callBackFunction({
                    page_number: (scope.page_number) + 1
                });
            });

            element.on('click', 'a.nav-prev', function () {
                scope.callBackFunction({
                    page_number: (scope.page_number) - 1
                });
            });

            function setNavigation(page_count, page_number) {
                if (page_number < page_count) {
                    element.find('.nav-next').removeClass('disabled');
                } else {
                    element.find('.nav-next').addClass('disabled');
                }
                if (page_number != 1) {
                    element.find('.nav-prev').removeClass('disabled');
                } else {
                    element.find('.nav-prev').addClass('disabled');
                }
            }
        }
    };
});