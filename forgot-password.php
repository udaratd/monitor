<?php include 'header.php'; ?>

<div class="vertical-center full-height">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-8 col-md-offset-0 col-sm-offset-2">

                <div class="panel-login">
                    <div class="panel-header company-logo">
                        <a href="#"><img src="assets/images/logo-hotel-x.png" alt="Company Logo" /></a>
                        <div class="app-title">Physical Monitoring System</div>
                    </div>

                    <div class="panel-content">
                        <h2 class="text-center">Reset your password</h2>
                        <form action="/" method="POST">
                            <div class="form-group">
                                <label for="user-reset-email">Email</label>
                                <input type="text" name="reset_email" class="form-control" autofocus="autofocus" id="user-reset-email" required >
                            </div>

                            <input type="submit" name="submit" value="Reset Password" class="btn btn-primary btn-block">
                        </form>

                        <div class="row m-t-20 text-light">
                            <div class="col-sm-5">
                                <a href="login.php">Go back to login</a>
                            </div>
                            <div class="col-sm-7 text-right">
                                Need Access ? <a href="#">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-center m-t-10 f-s-13">
                    &copy; <?php echo date('Y'); ?> Moni Inc. All Rights Reserved. 
                </div>

            </div>
        </div>
    </div>
</div>
    
<?php include 'footer.php'; 