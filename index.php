<?php
//Https Redirection on AWS
if ($_SERVER['HTTP_HOST'] != 'localhost' and $_SERVER['HTTP_X_FORWARDED_PROTO'] != "https") {
    $location = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $location);
    exit;
}
define('ROOT_PATH', $_SERVER["DOCUMENT_ROOT"]);
define('REQUEST_URL', '/api');
define('VERSION_NUMBER', '1.0.4');
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" ng-app="moniApp" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" ng-app="moniApp" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" ng-app="moniApp" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" ng-app="moniApp" class="no-js">
<!--<![endif]-->

<head>
    <base href="/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="shortcut icon" type="image/png" href="assets/images/favicon.png" />
    <title>Moni | Physical Monitoring System</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=768,height=1024 initial-scale=1.0">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="assets/libs/bootstrap-grid.css" rel="stylesheet" type="text/css">
    <link href="assets/libs/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/normalize.css?version=<?php echo VERSION_NUMBER; ?>" rel="stylesheet" type="text/css" media="all">
    <link href="assets/css/main.css?version=<?php echo VERSION_NUMBER; ?>" rel="stylesheet" type="text/css" media="all">

    <script src="assets/libs/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="assets/libs/jquery-ui/moment.js" type="text/javascript"></script>
    <script src="assets/libs/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/libs/jquery-ui/daterange-picker.js" type="text/javascript"></script>
    <script src="assets/libs/image-zoom/jquery.zoom.min.js" type="text/javascript"></script>
    <script src="https://code.angularjs.org/1.5.5/angular.min.js" type="text/javascript"></script>
    <!--    <script src="https://code.angularjs.org/1.5.5/angular-animate.min.js" type="text/javascript"></script>-->
    <script src="https://code.angularjs.org/1.5.5/angular-route.min.js" type="text/javascript"></script>

    <script src="assets/libs/charts/highcharts.js" type="text/javascript"></script>

    <script src="app/app.js?version=<?php echo VERSION_NUMBER; ?>" type="text/javascript"></script>
    <script src="app/services/services.js?version=<?php echo VERSION_NUMBER; ?>" type="text/javascript"></script>
    <script src="app/directives/directives.js?version=<?php echo VERSION_NUMBER; ?>" type="text/javascript"></script>

    <script src="app/controllers/main-controller.js?version=<?php echo VERSION_NUMBER; ?>" type="text/javascript"></script>
    <script src="app/controllers/user-controller.js?version=<?php echo VERSION_NUMBER; ?>" type="text/javascript"></script>
    <script src="app/controllers/checkpoint-controller.js?version=<?php echo VERSION_NUMBER; ?>" type="text/javascript"></script>
    <script src="app/controllers/schedule-controller.js?version=<?php echo VERSION_NUMBER; ?>" type="text/javascript"></script>
    <script src="app/controllers/worklist-controller.js?version=<?php echo VERSION_NUMBER; ?>" type="text/javascript"></script>
    <script src="app/controllers/review-controller.js?version=<?php echo VERSION_NUMBER; ?>" type="text/javascript"></script>
    <script src="app/controllers/location-controller.js?version=<?php echo VERSION_NUMBER; ?>" type="text/javascript"></script>
    <script src="app/controllers/feedback-controller.js?version=<?php echo VERSION_NUMBER; ?>" type="text/javascript"></script>
    <script src="app/controllers/report-controller.js?version=<?php echo VERSION_NUMBER; ?>" type="text/javascript"></script>

    <script src="assets/js/main.js?version=<?php echo VERSION_NUMBER; ?>" type="text/javascript"></script>

</head>

<body>
    <div class="app-loader"></div>
    <div class="moni-app">

        <div ng-controller="MainCtrl" ng-init="init()">
            <?php
        include_once ROOT_PATH.'/app/shared/app-toolbar.php';
        include_once ROOT_PATH.'/app/shared/app-sidebar.php';
        ?>
        </div>

        <?php
        include_once ROOT_PATH.'/app/shared/popups.php';
        include_once ROOT_PATH.'/app/shared/page-loader.php';
        ?>

        <div ng-view class="content-viewer container-fluid"></div>

        <?php include_once ROOT_PATH.'/app/shared/app-footer.php'; ?>

    </div>

</body>

</html>
