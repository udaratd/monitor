<?php 
define('VERSION_NUMBER', '0.8'); 
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" ng-app="myApp" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" ng-app="myApp" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" ng-app="myApp" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <link rel="shortcut icon" type="image/png" href="/assets/images/favicon.png" />
    <title>Physical Monitoring System</title>
    
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="/assets/libs/bootstrap-grid.css" rel="stylesheet" type="text/css"> 
    <link href="/assets/css/normalize.css?version=<?php echo VERSION_NUMBER; ?>" rel="stylesheet" type="text/css"> 
    <link href="/assets/css/main.css?version=<?php echo VERSION_NUMBER; ?>" rel="stylesheet" type="text/css"> 
    <link href="assets/css/main.css?version=<?php echo VERSION_NUMBER; ?>" rel="stylesheet" type="text/css"> 
</head>

<body class="bg-gradient">