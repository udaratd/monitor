"use strict";
var REQUEST_URL = '/api';

$(document).ready(function () {

    // Flip box
    function flipBox() {
        $(document).on('click', '.flip-maker', function (e) {
            e.preventDefault();
            $(this).parents('.flip-container').toggleClass('flip');
        });
    }

    flipBox();

    function display_response(visibility, type, message) {
        var container = $('.response-output');
        if (visibility) {
            if (type === 'success') {
                container.removeClass('error');
                container.addClass('success');
            } else {
                container.removeClass('success');
                container.addClass('error');
            }
            container.text(message);
            container.fadeIn();
        } else {
            container.hide();
        }
    }

    function display_loader(visibility) {
        var loader_object = $(".btn-ajax");
        if (visibility) {
            loader_object.addClass('spin-loader-mini');
        } else {
            loader_object.removeClass('spin-loader-mini');
        }
    }

    // User Login
    $("#frm-login").submit(function (e) {
        e.preventDefault();
        display_response(false); // Hide previously loaded response messages

        var email = $('#user-email').val();
        var password = $('#user-password').val();

        var post_data = {
            email: email,
            password: password
        };

        $.ajax({
            type: 'POST',
            url: REQUEST_URL + '/Admin/login',
            data: post_data,
            beforeSend: function () {
                display_loader(true);
            },
            success: function (response) {

                display_loader(false);
                var res = JSON.parse(response);
                if (res.response.success) {
                    console.log(response);
                    display_response(true, 'success', 'Login Successfull. Please Wait...');
                    setLoggedUserData(res.data);
                    window.location.replace("/admin");

                } else {
                    display_response(true, 'error', 'Invalid credentials. Please try again.');
                }
            }
        });

    });

    function setLoggedUserData(user) {
        console.log(user.f_name);
        if (typeof (Storage) !== "undefined") { // Checks if the browser supports LocalStorage
            if (user.ID != undefined) {
                localStorage.setItem("su_id", user.ID);
            }
            if (user.f_name != undefined) {
                localStorage.setItem("su_fname", user.f_name);
            }
            if (user.l_name != undefined) {
                localStorage.setItem("su_lname", user.l_name);
            }
            if (user.email != undefined) {
                localStorage.setItem("su_email", user.email);
            }
        }
    }

    // Search URL Parameters
    function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    /*
     * Password Reset
     * Step 1: Sending email to the server
     * Server will check the email and send confirmation link via that email
     */
    $("#frm-reset-password").submit(function (e) {
        e.preventDefault();
        display_response(false); // Hide previously loaded response messages 
        display_loader(true);
        var email = $('#user-reset-email').val();
        var post_data = {
            email: email
        };

        $.ajax({
            type: 'POST',
            url: REQUEST_URL + '/Admin/resetpasswordrequest',
            data: post_data,
            success: function (response) {
                console.log(response);
                display_loader(false);
                var res = JSON.parse(response);
                if (res.response.success) {
                    display_response(true, 'success', 'An email with a reset link has been sent to ' + email + '. Please click the link on email to confirm the password change request.');
                } else {
                    display_response(true, 'error', 'Sorry, We cannot verify that email. Please try again with a different email.');
                }
            }
        });
    });


    /*
     * Password Reset
     * Step 2 : User will click the link on email and visits /login.php?action=reset&id=x&token=xxxx...xxxx&type=x
     * Automatically sends this parameters to the server again and server will confirm that token is expired or not
     * If token expired user will directs again to type the email and receive a new confirmation link
     * If token not expired user can type a new password and change his/her password
     */
    checkResetLink();

    function checkResetLink() {
        var action = getUrlParameter('action');
        var id = getUrlParameter('id');
        var token = getUrlParameter('token');

        if (action === undefined || id === undefined || token === undefined) {
            return;
        }

        $.ajax({
            type: 'GET',
            url: REQUEST_URL + '/Admin/checkLink?id=' + id + '&token=' + token,
            success: function (response) {
                var res = JSON.parse(response);
                if (res.response.success) {
                    $('#frm-reset-password').detach();
                    $('#frm-change-password').show();
                } else {
                    display_response(true, 'error', 'Sorry, Your reset time has expired. Please enter the email and request a new reset link.');
                }
            },
            error: function () {
                display_response(true, 'error', 'Sorry, An error occured while connecting to the server. Please try again later.');
            }
        });
    }


    /*
     * Password reset
     * Step 3 : Submit new password after email confirmation
     */
    $("#frm-change-password").submit(function (e) {
        e.preventDefault();
        display_response(false); // Hide previously loaded response messages 
        display_loader(true);
        var new_password = $('#user-reset-password').val();
        var post_data = {
            id: getUrlParameter('id'),
            token: getUrlParameter('token'),
            password: new_password
        };

        $.ajax({
            type: 'POST',
            url: REQUEST_URL + '/Admin/resetpassword',
            data: post_data,
            success: function (response) {
                console.log(response);
                display_loader(false);
                var res = JSON.parse(response);
                if (res.response.success) {
                    display_response(true, 'success', 'Password change successfully. Please Wait...');

                    // Automatically send login request if successfull password change
                    $.ajax({
                        type: 'POST',
                        url: REQUEST_URL + '/Admin/login',
                        data: {
                            email: res.data.email,
                            password: new_password
                        },
                        success: function (response_login) {
                            var res_login = JSON.parse(response_login);
                            if (res_login.response.success) {
                                setLoggedUserData(res_login.data);
                                window.location.replace("/admin");
                            } else {
                                window.location.replace("/admin/login.php");
                            }
                        }
                    });
                } else {
                    display_response(true, 'error', response.data.response.statusMsg);
                }
            },
            error: function () {
                display_response(true, 'error', 'Sorry, An error occured while connecting to the server. Please try again later.');
            }
        });

    });



});