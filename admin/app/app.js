var app = angular.module('moniAppAdmin', ['ngRoute']);

app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider

        .when('/', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                }
            },
            templateUrl: 'app/views/companies/company-list.php',
            controller: 'CompanyCtrl'
        })
        .when('/company', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                }
            },
            templateUrl: 'app/views/companies/company-list.php',
            controller: 'CompanyCtrl'
        })
        .when('/company/add', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                }
            },
            templateUrl: 'app/views/companies/company-add.php',
            controller: 'CompanyCtrl'
        })
        .when('/company/update/:id', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                }
            },
            templateUrl: 'app/views/companies/company-update.php',
            controller: 'CompanyCtrl'
        })
        .when('/user/editprofile', {
            resolve: {
                validateSession: function (appFactories) {
                    return appFactories.checkSession();
                }
            },
            templateUrl: 'app/views/user/edit-profile.php',
            controller: 'MainCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
    $locationProvider.html5Mode(true);
}]);