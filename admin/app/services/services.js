// ------- App Services ------- //
app.service('appServices', function ($http, $location, $timeout) {

    var REQUEST_URL = '/api';
    var SESSION_DATA;

    this.getRequestUrl = function () {
        return REQUEST_URL;
    };

    this.setSessionData = function (data) {
        SESSION_DATA = data;
    };

    /*
     * Outputs session single data 
     * This data will remove in the login
     * @param name string
     */
    this.getSessionDataByName = function (name) {
        if (name == undefined || name == '') {
            return;
        }
        if (name === 'id') {
            return localStorage.getItem("su_id");
        }
        if (name === 'first_name') {
            return localStorage.getItem("su_fname");
        }
        if (name === 'last_name') {
            return localStorage.getItem("su_lname");
        }
        if (name === 'email') {
            return localStorage.getItem("su_email");
        }
        if (name === 'skip_validation') {
            return localStorage.getItem("lu_skip_validation");
        }
    };

    this.setSessionDataByName = function (attribute, data) {
        if (attribute === 'first_name') {
            return localStorage.setItem("su_fname", data);
        }
        if (attribute === 'last_name') {
            return localStorage.setItem("su_lname", data);
        }
        if (attribute === 'email') {
            return localStorage.setItem("su_email", data);
        }
    };

    this.removeSessionData = function () {
        localStorage.removeItem("su_id");
        localStorage.removeItem("su_fname");
        localStorage.removeItem("su_lname");
        localStorage.removeItem("su_email");
    };

    // User Logout //
    this.userLogout = function () {
        return $http.get(REQUEST_URL + '/Admin/logout');
    };

    this.toUrlParams = function (obj) {
        if (obj !== null) {
            var str = [];
            for (var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            return str.join("&");
            return str;
        }
    };

    /*
     * Renders corresponding output messages Response message
     * @param visibility boolean Whether to display or hide the error message. Default is false
     * @param object_id string Error Message Object
     * @param type boolean Whether the message is success or false. Success = true. Error = false
     * @param message string Message to display
     */
    this.responseMessage = function (visibility, object_id, type, message) {
        var container = $('#' + object_id);
        if (visibility) {
            if (type) {
                container.removeClass('error');
                container.addClass('success');
            } else {
                container.removeClass('success');
                container.addClass('error');
            }
            container.text(message);
            container.fadeIn();
        } else {
            container.hide();
        }
    };

    /*
     * Display the loading circle inside the button until ajax reponse comes
     * @param visibility boolean Whether to display or hide the loader. Default is false
     * @param object_id string Button Object
     * @param after boolean Whether to display success or fail icon after request
     */
    this.buttonLoader = function (visibility, object_id, after, after_status) {
        var loader_object = $('#' + object_id);
        if (visibility) {
            loader_object.addClass('spin-loader-mini');
        } else {
            loader_object.removeClass('spin-loader-mini');
            if (after) {
                if (after_status) {
                    loader_object.removeClass('ajax-fail');
                    loader_object.addClass('ajax-success');
                } else {
                    loader_object.removeClass('ajax-success');
                    loader_object.addClass('ajax-fail');
                }
            }
        }
    };

    /*
     * Change the view using route
     * @param path string Path to change
     * @param params json Params to add to the url
     * @param timeout int Time to execute this redirection (in miliseconds)
     */
    this.changeView = function (path, params, timeout) {
        if (path == undefined) {
            return;
        }
        if (timeout != undefined && timeout != null) {
            if (params != undefined && params != null) {
                $timeout(function () {
                    $location.path(path).search(params);
                }, timeout);
            } else {
                $timeout(function () {
                    $location.path(path).search({});
                }, timeout);
            }
        } else {
            if (params != undefined && params != null) {
                $location.path(path).search(params);
            } else {
                $location.path(path).search({});
            }
        }
    };

    /*
     * Get Notification type name by id
     * 1-Don't send notifications, 2-Email, 3-SMS
     */
    this.getNotificationTypeNameById = function (id) {
        if (id == undefined || id == null) {
            return;
        }
        var arr = [{
                id: 1,
                name: 'Don\'t send notifications'
            },
            {
                id: 2,
                name: 'Email'
            },
            {
                id: 3,
                name: 'SMS'
            }
        ];
        var output = '';
        $.each(arr, function (key, value) {
            if (id == value.id) {
                output = value.name;
            }
        });
        return output;
    };


    // Update User //
    this.updateUser = function (data) {
        return $http.post(this.getRequestUrl() + '/Admin/updateSuper', $.param(data), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });
    };

});

// ------- Company Services ------- //
app.service('companyServices', function ($http, appServices) {
    var tempData;
    // Get Company List //
    this.getCompanyList = function () {
        return $http.get(appServices.getRequestUrl() + '/Admin/all');
    };

    // Add Company //
    this.addCompany = function (data) {
        return $http.post(appServices.getRequestUrl() + '/Admin/add', $.param(data), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });
    };

    // Add User //
    this.addUser = function (data) {
        return $http.post(appServices.getRequestUrl() + '/Admin/addFirstUser', $.param(data), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });
    };

    // Get Single Company //
    this.getSingleCompany = function (id) {
        return $http.get(appServices.getRequestUrl() + '/Admin/single?id=' + id);
    };

    // Update Company //
    this.updateCompany = function (data) {
        return $http.post(appServices.getRequestUrl() + '/Admin/update', $.param(data), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });
    };


    // Update Debug Mode //
    this.updateDebugMode = function (data) {
        return $http.post(appServices.getRequestUrl() + '/Admin/debugenable', $.param(data), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });
    };

    // Set temp data //
    this.setTempData = function (data) {
        this.tempData = data;
    };
    // Set temp data /
    this.getTempData = function () {
        return this.tempData;
    };

    // Set temp data /
    this.removeTempData = function () {
        this.tempData = undefined;
    };



});


// ------- App Factories ------- //
app.factory('appFactories', function ($http, appServices) {

    return {
        checkSession: function () {
            return $http.get(appServices.getRequestUrl() + '/Admin/validateSession').then(function (response) {
                // console.log(response.data);
                if (response.data.response != undefined) {
                    if (!response.data.response.success) {
                        window.location.href = "/admin/login.php";
                    } else {
                        $('.app-loader').hide(); // Validation Successfull. Removing obstacles to see the app. Load the App.

                    }
                } else {
                    window.location.href = "/admin/login.php";
                }
            });
        }
    };

});