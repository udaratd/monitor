app.controller('CompanyCtrl', ['$scope', '$location', '$routeParams', 'appServices', 'companyServices', function ($scope, $location, $routeParams, appServices, companyServices) {

    var cc = this;
    cc.user_add_ui = 1;
    cc.company_user_data = {};

    cc.init = function () {
        // Server Error uncomment later
        cc.getCompanyList();
        cc.user_add_ui = 1;
        cc.setCompanyUserData();

    };

    cc.initUpdate = function () {
        console.log($routeParams.id);
        if ($routeParams.id != undefined) {
            cc.getSingleCompany($routeParams.id);
        }
    };

    // ------- Get Single Company  ------- //
    cc.getSingleCompany = function (id) {
        $('.app-loader').show();
        companyServices.getSingleCompany(id).success(function (response) {
            $('.app-loader').hide();
            if (response.response.success) {
                console.log(response.data);
                cc.upd_company = response.data;
                cc.upd_company.id = response.data.ID;
                cc.upd_company.company_name = response.data.name;
                if (response.data.active == 1) {
                    cc.upd_company.checked = true;
                } else if (response.data.active == 2) {
                    cc.upd_company.checked = false;
                } else {
                    console.log("invaild active value")
                }
                if (response.data.skip_validation == 1) {
                    cc.upd_company.skip_checked = true;
                } else if (response.data.active == 2) {
                    cc.upd_company.skip_checked = false;
                } else {
                    console.log("invaild active value")
                }
            }
        }.bind(cc));
    };

    // ------- Get Company List ------- //
    cc.getCompanyList = function () {
        $('.app-loader').show();
        companyServices.getCompanyList().success(function (response) {
            $('.app-loader').hide();
            if (response.response.success) {
                cc.company_list = response.data;
                angular.forEach(response.data, function (value, key) {
                    if (value.debug == 1) {
                        cc.company_list[key].checked = false;
                    } else if (value.debug == 2) {
                        cc.company_list[key].checked = true;
                    } else {
                        console.log("invaild Debug value")
                    }

                });
                cc.company_list_no_data = false;
            } else {
                cc.company_list_no_data = true;
            }
        }.bind(cc));

        // cc.user_add_ui = 1;
    };
    // ------- Add Company  ------- //
    cc.addCompany = function (company) {        
        $('.app-loader-2').show();
        cc.company_user_data.username = company.username;
        cc.company_user_data.firstName = company.f_name;
        cc.company_user_data.lastName = company.l_name;
        cc.company_user_data.email = company.email;
        cc.company_user_data.phone = company.phone;
        if(company.skip_checked){
            cc.company_user_data.skip_validation = 1;
        }else{
            cc.company_user_data.skip_validation = 2;
        } 
        companyServices.addCompany(company).success(function (response) {
            $('.app-loader-2').hide();
            if (response.hasOwnProperty('response')) {
                if (response.response.success) {
                    console.log(response.data);
                    appServices.responseMessage(true, 'msg-add-company', true, 'Company Added Successfully. Redirecting...');
                    cc.user_add_ui = 2;
                } else {
                    console.log(response.response);
                    appServices.responseMessage(true, 'msg-add-company', false, response.response.statusMsg);
                }
            }
        }.bind(cc));
    }

    // ------- Add User  ------- //
    cc.addUser = function (user) {
        appServices.buttonLoader(true, 'btn-add-user');
        companyServices.addUser(user).success(function (response) {
            appServices.buttonLoader(false, 'btn-add-user');
            if (response.hasOwnProperty('response')) {
                if (response.response.success) {
                    console.log(response.data);
                    appServices.responseMessage(true, 'msg-add-user', true, 'Userd Added Successfully. Redirecting...');
                    cc.user_add_ui = 3;
                } else {
                    console.log(response.response);
                    appServices.responseMessage(true, 'msg-add-user', false, response.response.statusMsg);
                }
            }
        }.bind(cc));

    }

    // ------- Update Company  ------- //
    cc.updateCompany = function (company) {
        appServices.buttonLoader(true, 'btn-update-company');
        if (company.checked) {
            company.active = 1;
        } else {
            company.active = 2;
        }
        
        if (company.skip_checked) {
            company.skip_validation = 1;
        } else {
            company.skip_validation = 2;
        }

        companyServices.updateCompany(company).success(function (response) {
            appServices.buttonLoader(false, 'btn-update-company');
            if (response.hasOwnProperty('response')) {
                if (response.response.success) {
                    console.log(response.data);
                    appServices.responseMessage(true, 'msg-update-company', true, 'Company Updated Successfully. Redirecting...');
                    $location.path("/company");
                } else {
                    appServices.responseMessage(true, 'msg-update-company', false, response.response.statusMsg);
                    console.log(response.response);
                }
            }
        }.bind(cc));
    }

    cc.setUpdateData = function (id) {
        $location.path("/company/update/" + id);
    }

    cc.completeAddCompany = function () {
        $location.path("/company");
        // cc.user_add_ui = 1;
    }

    cc.addIncompleteUser = function (current_company) {
        current_company.user_state = true;
        companyServices.setTempData(current_company);
        $location.path("/company/add")
        cc.user_add_ui = 2;
    }

    cc.setCompanyUserData = function () {
        if (companyServices.getTempData() != undefined) {
            console.log(companyServices.getTempData());
            cc.companyUserData = companyServices.getTempData();
            cc.company_user_data.username = cc.companyUserData.username;
            cc.company_user_data.firstName = cc.companyUserData.f_name;
            cc.company_user_data.lastName = cc.companyUserData.l_name;
            cc.company_user_data.email = cc.companyUserData.email;
            cc.company_user_data.phone = cc.companyUserData.phone;
            cc.user_add_ui = 2;
            companyServices.removeTempData();
        }


    }
    cc.updateDebugMode = function (id, status, elem) {

        $(elem.currentTarget).parent().addClass("disabled");
        cc.Status_data = {};
        cc.Status_data.id = id;
        if (status) {
            cc.Status_data.debug = 1;

        } else {
            cc.Status_data.debug = 2;
        }
        companyServices.updateDebugMode(cc.Status_data).then(function (response) {
            $(elem.currentTarget).parent().removeClass("disabled");
            // if (response.data.response.success) {
            //     appServices.responseMessage(true, 'msg-update-schedule-status', true, 'Schedule Status Updated successfully..');
            //     // $route.reload();
            // } else {
            //     appServices.responseMessage(true, 'msg-update-schedule-status', false, response.data.response.statusMsg);
            // }
        }, function () {
            $(elem.currentTarget).parent().removeClass("disabled");
            // appServices.responseMessage(true, 'msg-update-schedule-status', false, 'Oops. Something went wrong with your connection.');
        });
    }



}]);