app.controller('MainCtrl', ['$scope', '$location', 'appServices', function ($scope, $location, appServices) {

    var mc = this;
    mc.current_user = {}

    mc.init = function () {
        mc.setLoggedData();
    };


    // Set logged user data into model
    mc.setLoggedData = function () {
        mc.super_user_fname = appServices.getSessionDataByName('first_name');
        mc.super_user_lname = appServices.getSessionDataByName('last_name');
        mc.super_user_email = appServices.getSessionDataByName('email');

        mc.current_user.f_name = mc.super_user_fname;
        mc.current_user.l_name = mc.super_user_lname;
        mc.current_user.email = mc.super_user_email;

    };

    // ------- Update User  ------- //
    mc.updateUser = function (user) {
        appServices.updateUser(user).success(function (response) {
            if (response.hasOwnProperty('response')) {
                if (response.response.success) {
                    appServices.responseMessage(true, 'msg-update-super-user', true, 'User Updated Successfully. Redirecting...');
                    $location.path("/");
                } else {
                    appServices.responseMessage(true, 'msg-update-super-user', false, response.response.statusMsg);
                    console.log(response.response);
                }
            }
        }.bind(mc));
    }

    // User Logout
    mc.userLogout = function () {
        $('.link-logout').addClass('spin-loader-mini');
        appServices.userLogout().success(function (response) {
            if (response.response.success) {
                appServices.removeSessionData();
                window.location.replace("/admin/login.php");
                $('.link-logout').removeClass('spin-loader-mini');
            }
        });
    };

}]);