<div class="panel" ng-controller="CompanyCtrl as cc" ng-init="cc.init()">


    <div class="content-wrapper p-t-15">
        <div class="page-toolbar m-t-0">

            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="form-group">
                        <div class="search-wrapper">
                            <input type="search" name="term" class="search-control  form-control" ng-model="filter_companies.name"
                                placeholder="Search by Company Name">
                            <span class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
                <!--
                <div class="col-md-5 col-sm-5">
                    <div class="form-group group-inline">
                        <label for="user-job-role">Job Role</label>
                        <div class="select-wrapper">
                            <select class="form-control" id="user-job-role" ng-options="role.id as role.label for role in role_list" ng-model="user.role" ng-change="getUserList(user.role)">
                                <option value="" selected="selected">--Select Role--</option>
                            </select>
                        </div>
                    </div>
                </div> -->

                <div class="col-md-9 col-sm-3 text-right">
                    <a href="/admin/company/add" class="btn btn-primary">Add Company</a>
                </div>
            </div>

        </div>

        <!-- <div class="table-responsive" ng-if="!user_list_no_data"> -->
        <div class="table-responsive" ng-if="!cc.company_list_no_data">
            <table class="table table-bordered table-sortable table-condensed table-striped ">
                <thead>
                    <tr>
                        <th>Company Name</th>
                        <th>Contact Name</th>
                        <th>Contact Number</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Status</th>
                        <th class="w-160">Debug Mode</th>
                        <th class="w-160">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="company in cc.company_list | filter:filter_companies">
                        <td>{{company.name}}</td>
                        <td>{{company.f_name}} {{company.l_name}}</td>
                        <td><a href="tel:{{company.phone}}" ng-if="company.phone !== null">{{company.phone}}</a><span
                                ng-if="company.phone === null">-</span></td>
                        <td>{{company.email}}</td>
                        <td>{{company.address}}</td>
                        <td ng-if="company.active==1">Active</td>
                        <td ng-if="company.active!=1">Inactive</td>
                        <td class="text-center">
                            <span class="checkbox inline-block tbl-checkbox">
                                <input type="checkbox" id="debug_checkbox{{company.ID}}" ng-checked="company.checked"
                                    ng-init="cc.debug_check[$index] = company.checked" ng-model="cc.debug_check[$index]"
                                    ng-click="cc.updateDebugMode(company.ID,company.checked,$event)">
                                <label for="debug_checkbox{{company.ID}}">&nbsp;</label>
                            </span>
                        </td>
                        <td>
                            <ul class="admin-action-group horizontal-action-group">
                                <li><a href="" ng-click="cc.setUpdateData(company.ID)"><i class="fa fa-pencil-square-o"
                                            aria-hidden="true"></i> View / Edit</a></li>
                                <li><a href="" ng-if="company.user!=1" ng-click="cc.addIncompleteUser(company)" class="text-muted btn-loading btn-text-loading"><i
                                            class="fa fa-plus-square" aria-hidden="true"></i> Add User</a></li>
                            </ul>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>

        <div class="text-center m-t-30 m-b-15" ng-if="cc.company_list_no_data">
            <h3>No Companies found</h3>
        </div>


    </div>

    <!-- <div class="content-loader" content-loader=""></div> -->

</div>