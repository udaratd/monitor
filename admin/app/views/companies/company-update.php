<div class="panel" ng-controller="CompanyCtrl as cc" ng-init="cc.initUpdate()">
    <div class="content-wrapper p-t-15">
        <div class="page-toolbar m-t-0">
            <div class="row">
                <div class="col-md-6 col-sm-12 ">
                    <h2 class="f-w-500 m-t-0 m-b-0">Update Company</h2>
                </div>
                <div class="col-md-6 col-sm-12 text-right">
                    <a href="/admin/company"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back to
                        Companies</a>
                </div>
            </div>

        </div>

        <form name="formUpdateCompany" novalidate ng-submit="cc.updateCompany(cc.upd_company)">
            <div class="row">
                <div class="col-md-6 col-sm-12">

                    <div class="form-group">
                        <label>Company Name *</label>
                        <input class="form-control" type="text" ng-model="cc.upd_company.company_name" placeholder="ex: domedia"
                            required>
                    </div>
                    <div class="row half-gap">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>First Name</label>
                                <input class="form-control" type="text" ng-model="cc.upd_company.f_name" placeholder="ex: Saman"
                                    required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input class="form-control" type="text" ng-model="cc.upd_company.l_name" placeholder="ex: Perera"
                                    required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>User Name *</label>
                        <input class="form-control" type="text" ng-model="cc.upd_company.username " placeholder="ex: Sam"
                            required>
                    </div>

                    <div class="form-group" ng-class="{'has-error': formUpdateCompany.$error.email}">
                        <label>Email *</label>
                        <input class="form-control" type="email" ng-model="cc.upd_company.email" placeholder="ex: kamal@gmail.com"
                            required>
                    </div>

                    <div class="form-group">
                        <label>Active</label>

                        <span class="checkbox inline-block">
                            <input type="checkbox" id="update_checkbox" ng-checked="cc.upd_company.checked" ng-model="cc.upd_company.checked">
                            <label for="update_checkbox">&nbsp;</label>
                        </span>
                        
                        <label>Overlap Schedule</label>
                        
                        <span class="checkbox inline-block">
                            <input type="checkbox" ng-checked="cc.upd_company.skip_checked" ng-model="cc.upd_company.skip_checked" id="skip-schedule-validation">
                            <label for="skip-schedule-validation">&nbsp;</label>
                        </span>
                    </div>


                </div>

                <div class="col-md-6 col-sm-12">


                    <div class="form-group">
                        <label>Contact Number </label>
                        <input class="form-control" type="text" ng-model="cc.upd_company.phone" placeholder="ex: 0777547894"
                            required>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <input class="form-control" type="text" ng-model="cc.upd_company.address " placeholder="ex: Malabe"
                            required>
                    </div>

                    <div class="form-group">
                        <label for="company-logo">Upload Company Logo</label>
                        <div class="droppable-area" style="height: 160px;">

                            <div class="uploader-wrapper">
                                <input type="file" ng-model="cc.upd_company.logo" app-file-model="" id="company-logo"
                                    accept="image/*">
                                <label for="company-logo" class="btn btn-dark m-b-0">Select Logo</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="row">
                    <div class="col-md-12 col-sm-12">
                        Change Status
                    </div>
                </div>
                -->
            <div class="row text-right">
                <div class="col-md-11 col-sm-9">
                    <div class="response-message-text" id="msg-update-company"></div>
                </div>
                <div class="col-md-1 col-sm-3">
                    <button type="submit" ng-disabled="formUpdateCompany.$invalid" class="btn btn-primary btn-loading"
                        id="btn-update-company"><span>Save</span></button>
                </div>
            </div>

        </form>


    </div>
</div>