
<div class="panel" ng-controller="CompanyCtrl as cc" ng-init="cc.init()">

    <div class="content-wrapper " ng-if="cc.user_add_ui==1">
        <div class="page-toolbar m-t-0">
            <div class="row ">
                <div class="col-md-4 col-sm-12 col-md-offset-4">
                    <div class="progressbar-container">
                    <ul class="progressbar">
                        <li class="active">Company<span class="test1"></span></li>
                        <li>User<span class="test1"></span></li>
                        <li>Complete<span class="test1"></span></li>
                    </ul>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12 ">
                    <h2 class="f-w-500 m-t-0 m-b-0">Add Company</h2>
                </div>
                <div class="col-md-6 col-sm-12 text-right">
                    <a href="/admin/company"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back to Companies</a>
                </div>
            </div>

        </div>

        <form name="formAddCompany" novalidate ng-submit="cc.addCompany(company)">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        
                        <div class="form-group">
                            <label>Company Name *</label>
                            <input class="form-control" type="text" ng-model="company.company_name"  required>
                        </div>
    
                        <div class="form-group">
                            <label>First Name</label>
                            <input class="form-control" type="text" ng-model="company.f_name"  required>
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <input class="form-control" type="text" ng-model="company.l_name"  required>
                        </div>
                        
                        <div class="form-group">
                            <label>User Name *</label>
                            <input class="form-control" type="text" ng-model="company.username "  required>
                        </div>

                        <div class="form-group" ng-class="{'has-error': formAddCompany.$error.email}">
                            <label>Email *</label>
                            <input class="form-control" type="email" ng-model="company.email"  required>
                        </div>
                        
                        <div class="form-group" ng-class="{'has-error': formAddCompany.$error.email}">
                            <label>Overlap Schedule</label>
                            <span class="checkbox inline-block">
                                <input type="checkbox" ng-model="company.skip_validation" id="skip-schedule-validation">
                                <label for="skip-schedule-validation">&nbsp;</label>
                            </span>
                        </div> 

                    </div>

                    <div class="col-md-6 col-sm-12">
                        
                        
                        <div class="form-group">
                            <label>Contact Number </label>
                            <input class="form-control" type="text" ng-model="company.phone"  required>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <input class="form-control" type="text" ng-model="company.address "  required>
                        </div>

                        
                        <div class="form-group">
                            <label for="company-logo">Upload Company Logo</label>
                            <div class="droppable-area" style="height: 183px;">

                                <div class="uploader-wrapper">
                                    <input type="file" ng-model="company.logo" app-file-model="" id="company-logo" accept="image/*">
                                    <label for="company-logo" class="btn btn-dark m-b-0">Select Logo</label>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                <div class="row text-right">
                    <div class="col-md-10 col-sm-9">
                        <div class="response-message-text" id="msg-add-company"></div>
                    </div>
                    <div class="col-md-2 col-sm-3">
                        <button type="submit" ng-disabled="formAddCompany.$invalid" class="btn btn-primary btn-loading" id="btn-add-company"><span>Proceed to Next</span></button>
                    </div>
                </div>
                
        </form>
        
    
    </div>

    <div class="content-wrapper p-t-15" ng-if="cc.user_add_ui==2">
        <div class="page-toolbar m-t-0">
            <div class="row ">
                <div class="col-md-4 col-sm-12 col-md-offset-4">
                    <div class="progressbar-container">
                    <ul class="progressbar">
                        <li class="active">Company<span class="test1"></span></li>
                        <li class="active">User<span class="test1"></span></li>
                        <li>Complete<span class="test1"></span></li>
                    </ul>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12 ">
                    <h2 class="f-w-500 m-t-0 m-b-0">Add User</h2>
                </div>

            </div>

        </div>

        <form name="formAddUser" novalidate ng-submit="cc.addUser(cc.company_user_data)">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        
    

                        <div class="form-group">
                                    <label for="user-first-name">First Name *</label>
                                    <input class="form-control" type="text" ng-model="cc.company_user_data.firstName"  id="user-first-name" required>
                        </div>

                        <div class="form-group">
                                    <label for="user-last-name">Last Name *</label>
                                    <input class="form-control" type="text" ng-model="cc.company_user_data.lastName"  id="user-last-name" required>
                        </div>


                        <div class="form-group">
                            <label>Phone</label>
                            <input class="form-control" type="text" ng-model="cc.company_user_data.phone "  required>
                        </div>
                        
                        
                        
                        
                    </div>

                    <div class="col-md-6 col-sm-12">

                    <div class="form-group">
                            <label>NIC *</label>
                            <input class="form-control" type="text" ng-model="cc.company_user_data.nic"  required>
                        </div>

                        
                        <div class="form-group" ng-class="{'has-error': formAddUser.$error.email}">
                            <label>Email *</label>
                            <input class="form-control" type="email" ng-model="cc.company_user_data.email"  required>
                        </div>
                        
                        <div class="form-group">
                            <label>Password </label>
                            <input class="form-control" type="password" ng-model="cc.company_user_data.password"   required>
                        </div>              

                    </div>
                </div>
                
                <div class="row text-right">
                    <div class="col-md-11 col-sm-9">
                        <div class="response-message-text" id="msg-add-user"></div>
                    </div>
                    <div class="col-md-1 col-sm-3">
                        <button type="submit" ng-disabled="formAddUser.$invalid" class="btn btn-primary btn-loading" id="btn-add-user"><span>Save</span></button>
                    </div>
                </div>
                
            </form>
        
    
    </div>

    <div class="content-wrapper p-t-15" ng-if="cc.user_add_ui==3">
        <div class="page-toolbar m-t-0">
            <!-- <div class="row ">
                <div class="col-md-4 col-sm-12 col-md-offset-4">
                    <div class="progressbar-container">
                    <ul class="progressbar">
                        <li class="active">Company<span class="test1"></span></li>
                        <li class="active">User<span class="test1"></span></li>
                        <li class="active">Complete<span class="test1"></span></li>
                    </ul>
                    </div>
                </div>

            </div> -->
            <div class="row">

                <div class="success-container">
                <div class="check_mark">
                    <div class="sa-icon sa-success animate">
                        <span class="sa-line sa-tip animateSuccessTip"></span>
                        <span class="sa-line sa-long animateSuccessLong"></span>
                        <div class="sa-placeholder"></div>
                        <div class="sa-fix"></div>
                    </div>
                </div>
                </div>
                <h2 class="f-w-500 m-b-0 success-heading text-center">Successfully Added a Company</h2>
                <!-- <div class="col-md-4 col-sm-12 col-md-offset-4">
                    <div class="complete-data-wrapper">
                        <table>
                            <tr>
                                <td>Company Name</td>
                                <td>ABC</td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td>we@gmail.lk</td>
                            </tr>
                            <tr>
                                <td>First Name :</td>
                                <td>Jhon</td>
                            </tr>
                            <tr>
                                <td>Last Name :</td>
                                <td>Doe</td>
                            </tr>
                            <tr>
                                <td>User Name :</td>
                                <td>J101</td>
                            </tr>
                        </table>
                    </div>
                </div> -->
                <div class="row text-center">
                    <div class="col-md-12 col-sm-12 m-t-15">
                        <button  class="btn btn-primary btn-loading" ng-click="cc.completeAddCompany()"><span>Done</span></button>
                    </div>
                </div>
                
            </div>

        </div>


        
    
    </div>
</div>