<div class="panel" ng-controller="MainCtrl as mc" ng-init="mc.init()">

<div class="content-wrapper p-t-15">
        <div class="page-toolbar m-t-0">
            <div class="row">
                <div class="col-md-6 col-sm-12 ">
                    <h2 class="f-w-500 m-t-0 m-b-0">Edit Profile</h2>
                </div>
                <!-- <div class="col-md-6 col-sm-12 text-right">
                    <a href="/admin/company"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back to Companies</a>
                </div> -->
            </div>

        </div>

        <form name="formUpdateUser" novalidate ng-submit="mc.updateUser(mc.current_user)">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        

                        <div class="row half-gap">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>First Name *</label>
                                    <input class="form-control" type="text" ng-model="mc.current_user.f_name" placeholder="ex: Saman" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Last Name *</label>
                                    <input class="form-control" type="text" ng-model="mc.current_user.l_name" placeholder="ex: Perera" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" ng-class="{'has-error': formUpdateUser.$error.email}">
                            <label>Email *</label>
                            <input class="form-control" type="email" ng-model="mc.current_user.email" placeholder="ex: kamal@gmail.com" required>
                        </div>
                        <div class="form-group">
                            <label>Old Password</label>
                            <input class="form-control" type="password" ng-model="mc.current_user.password"  required>
                        </div>
                        <div class="form-group">
                            <label>New Password</label>
                            <input class="form-control" type="password" ng-model="mc.current_user.new_password"  required>
                        </div>


                        
                    </div>

                    <!-- <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>Old Password</label>
                            <input class="form-control" type="password" ng-model="mc.current_user.password"  required>
                        </div>
                        <div class="form-group">
                            <label>New Password</label>
                            <input class="form-control" type="password" ng-model="mc.current_user.new_password"  required>
                        </div>
                        
                        
                    </div> -->
                </div>

                <div class="row text-right">
                    <div class="col-md-4 col-sm-9">
                        <div class="response-message-text" id="msg-update-super-user"></div>
                    </div>
                    <div class="col-md-2 col-sm-3">
                        <button type="submit" ng-disabled="formUpdateUser.$invalid" class="btn btn-primary btn-loading" id="btn-update-super-user"><span>Save</span></button>
                    </div>
                </div>
                
            </form>
        
    
    </div>
</div>