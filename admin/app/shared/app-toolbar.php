<div class="toolbar-top">
    <div class="container-fluid full-height">
        <div class="row full-height vertical-center">
            <div class="col-md-2 col-sm-2">
                <a href="/admin/" class="company-logo">
<!--                    <img src="/assets/images/moni-logo.png" alt="Company Logo">-->
                    <img alt="Company Logo" src="{{company_logo}}" ng-if="company_logo != null && company_logo != 'null'">
                    <img alt="Company Logo" src="/assets/images/logo-moni-inverted-2.png" ng-if="company_logo == null || company_logo == 'null'">
                </a>
            </div>
            
            <div class="col-md-8 col-sm-8 full-height ">
            <ul class="toolbar-nav pull-right">
                <!-- <li><a href="/admin/" active-link="active"><i class="fa fa-home toolbar-nav-list-i" aria-hidden="true"></i> <span>Dashboard</span></a></li> -->
                <li><a href="/admin/company" active-link="active"><i class="fa fa-list toolbar-nav-list-i" aria-hidden="true"></i> <span>Company</span></a></li>
            </ul>
            </div>

            <div class="col-md-2 col-sm-2">
                <ul class="toolbar-nav pull-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-user-circle f-s-30 m-r-10" ng-if="logged_user_avatar == null || logged_user_avatar == 'null'" aria-hidden="true"></i> 
                            <img alt="" src="{{logged_user_avatar}}" ng-if="logged_user_avatar != null && logged_user_avatar != 'null'" class="toolbar-icon-user">
                            <span class="user-name m-r-5">{{mc.super_user_fname}}</span> <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </a>
                        <ul class="dropdown-menu right">
                            <li><a href="/admin/user/editprofile" >Edit Profile</a></li>
                            <li><a href="" ng-click="mc.userLogout()" class="btn-loading" id="link-logout">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

