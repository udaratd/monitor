<?php include 'header.php'; ?>

<div class="vertical-center full-height">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-8 col-md-offset-0 col-sm-offset-2">

                <div class="panel-login">
                    <div class="panel-header company-logo">
                        <a href="#"><img src="assets/images/logo-moni.png" alt="Company Logo" /></a>
                        <div class="app-title">Physical Monitoring System | Admin</div>
                    </div>
                    <?php 
                    // Password Reset
                    if(isset($_GET['action']) && $_GET['action'] == 'reset'): ?>
                    <!-- reset password -->
                    <div class="panel-content">
                        <h2 class="text-center">Reset your password</h2>
                        <form action="/" method="POST" id="frm-reset-password">
                            <div class="form-group">
                                <label for="user-reset-email">Email</label>
                                <input type="text" name="reset_email" class="form-control" autofocus="autofocus" id="user-reset-email" required >
                            </div>

                            <input type="submit" name="submit" value="Reset Password" class="btn btn-primary btn-block">
                        </form>

                        <form action="" method="POST" id="frm-change-password" style="display:none">
                            <div class="form-group">
                                <label for="user-reset-password">Password</label>
                                <input type="password" name="reset_password" class="form-control" autofocus="autofocus" id="user-reset-password" required >
                            </div>

                            <input type="submit" name="submit" value="Reset Password" class="btn btn-primary btn-block">
                        </form>
                        <div class="response-output"></div>
                        <div class="row m-t-20 text-light">
                            <div class="col-sm-5">
                                <a href="admin/login.php">Go back to login</a>
                            </div>

                        </div>
                    </div>
                    <!-------->
                    <?php 
                    // Login
                    else: ?>
                    <!-- login -->
                    <div class="panel-content">
                        <form action="" method="POST" id="frm-login">
                            <div class="form-group">
                                <label for="user-email">Email</label>
                                <input type="text" name="email" class="form-control" autofocus="autofocus" id="user-email" required >
                            </div>
                            <div class="form-group">
                                <label for="user-password">Password</label>
                                <input type="password" name="password" class="form-control" id="user-password" required >
                            </div>

                            <!-- <div class="form-group m-t-20">
                                <span class="checkbox">
                                    <input type="checkbox" value="1" name="remember_me" id="user-remember">
                                    <label for="user-remember" class="text-light">Remember Me</label>
                                </span>
                            </div> -->
                            
                            <div class="response-output"></div>

                            <button type="submit" name="submit" class="btn btn-primary btn-block btn-ajax">Log in</button>
                        </form>

                        <div class="row m-t-20 text-light">
                            <div class="col-sm-5">
                                <a href="/admin/login.php?action=reset">Forgot Password ?</a>
                            </div>
                        </div>
                    </div>
                    <!-------->
                    <?php endif; ?>
                    
                </div>

                <div class="text-center m-t-10 f-s-13">
                    &copy; <?php echo date('Y'); ?> Moni Inc. All Rights Reserved. 
                </div>

            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; 


