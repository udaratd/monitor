"use strict";

$(document).ready(function () {

    // Search field
    function searchField() {
        $(document).on('input', '.search-wrapper .search-control', function () {
            var parent = $(this).parents('.search-wrapper');
            if (!this.value) {
                parent.removeClass('dirty');
            } else {
                parent.addClass('dirty');
            }
        });
    }

    // Search Dropdown / Search Filter
    function searchDropdown() {
        $(document).on('click', '.search-selection .output', function () {
            $(".search-selection.open").removeClass('open');
            var parent = $(this).parents('.search-selection');
            parent.toggleClass('open');
            parent.find('.search-filter').val("");
            parent.find('.search-filter').focus();
        });
        $(document).on('click', function (e) {
            var container = $(".search-selection.open");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.removeClass('open');
            }
        });
        $(document).on('click', '.search-selection .result-list li', function () {
            var label_text = $(this).find('span').text();
            var parent = $(this).parents('.search-selection');
            parent.removeClass('open');
            parent.find('.output').val(label_text);
        });
    }

    // Header Search
    function headerSearch() {
        $(document).on('click', '.search-toggle', function (e) {
            e.preventDefault();
            $('.toolbar-search').slideToggle('fast');
        });
    }

    // Dropdown
    function menuDropdown() {
        $(document).on('click', '.dropdown-toggle', function (e) {
            e.preventDefault();
            $(this).parents('.dropdown').toggleClass('open');
        });
        $(document).on('click', function (e) {
            var container = $(".dropdown");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.removeClass('open');;
            }
        });
    }

    // Sidebar
    function appSidebar() {
        $(document).on('click', '.sidebar-triggerer', function (e) {
            e.preventDefault();
            var target = $(this).attr('data-sidebar');
            $('#' + target).addClass('open');
        });
        $(document).on('click', function (e) {
            var container = $(".app-sidebar, .sidebar-triggerer");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.removeClass('open');
            }
        });
        $(document).on('click', '.sidebar-closer', function (e) {
            e.preventDefault();
            $(this).parents('.app-sidebar').removeClass('open');
        });
    }

    // Accordian
    function locationAccordian() {
        $(document).on('click', '.location-accordian li.has-children > .item-strip', function () {
            $(this).parents('li.has-children').toggleClass('open');
            $(this).parents('li.has-children').find('.accodian-submenu').slideToggle('fast');
        });
        $(document).on('click', '.location-accordian li .item-label, .location-accordian li .item-icon', function (e) {
            e.stopPropagation();
        });
    }

    // Tabs In pgae load
    function tabsInPage() {
        $(document).on('click', '.tabs-inpage li a', function (e) {
            e.preventDefault();
            var tab_id = $(this).attr('data-tab');
            $('.tabs-inpage li').removeClass('active');
            $('.tab-content').removeClass('active');

            $(this).parent().addClass('active');
            $("#" + tab_id).addClass('active');
        });
    }

    // Content Drawer
    function contentDrawer() {
        $(document).on('click', '.push-content-opener', function () {
            $(this).parents('.content-wrapper').addClass('open');
        });

        $(document).on('click', '.content-drawer', function () {
            $(this).parents('.push-content').removeClass('open');
        });
    }

    // Checkbox act as radio button
    function checkboxAsRadio() {
        $(document).on('change', '.as-radio', function () {
            var $box = $(this);
            if ($box.is(":checked")) {
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                $(group).prop("checked", false);
                $box.prop("checked", true);
            } else {
                $box.prop("checked", false);
            }
        });
    }

    // Model Events
    function modelEvents() {
        $(document).on('click', '.model-closer a', function () {
            $(this).parents('.model-wrapper').removeClass('in');
        });
    }

    // Setup next prev
    function stepNavigator() {
        $(document).on('click', '.btn-step', function () {
            var stepId = $(this).attr('data-step');
            var reverse = $(this).attr('data-step-reverse');
            var currentStepPane = $('.step-pane');
            var stepToLoad = $('div[data-step-id="' + stepId + '"]');
            var navToHighlight = $('li[data-step-nav="' + stepId + '"]');

            currentStepPane.removeClass('active');

            if (stepId !== 'final') {

                stepToLoad.addClass('active');
                navToHighlight.addClass('active');

                if (reverse === 'true') {
                    var nextId = parseInt(stepId) + 1;
                    var navToDim = $('li[data-step-nav="' + nextId + '"]');
                    navToDim.removeClass('active');
                }

            } else {
                $(this).parents('.model-wrapper').removeClass('in');
                $('div[data-step-id="1"]').addClass('active');
                $('.step-nav-item').removeClass('active');
                $('.step-nav-item[data-step-nav="1"]').addClass('active');
            }
        });
    }



    searchField();
    searchDropdown();
    headerSearch();
    menuDropdown();
    appSidebar();
    locationAccordian();
    tabsInPage();
    contentDrawer();
    checkboxAsRadio();
    modelEvents();
    stepNavigator();

});