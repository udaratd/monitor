<?php
/**
 * The base configurations of the DoFramework.
 *
 * This file has the following configurations: 
 * MySQL settings
 * Error Reporting Settings
 * Debug Settings
 * 
 * 
 */

/** My DQL Settings **/
//$lifetime=3600;
session_start();


//set error reporting :
error_reporting(E_ALL & ~E_NOTICE);
//session_destroy();
define('SERVER_TYPE', 'aws');
define('COMPANY_DB_NAME', 'moni_company');
if(SERVER_TYPE === 'local'){
    define('DB_HOST', 'localhost');
//    print_r($_SESSION);
//    print_r($_SERVER);
    if(!empty($_SESSION['DB_NAME'])){
        define('DB_NAME', $_SESSION['DB_NAME']);
    } 
    else {
        define('DB_NAME', COMPANY_DB_NAME);
    }
    
    define('DB_USER', '');
    define('DB_PASSWORD', '');
//    define('DB_TEST_NAME', 'dev_test');
} else {
    define('DB_HOST', 'monilive102155485.c6gap9gbjqau.ap-southeast-1.rds.amazonaws.com');
//    print_r($_SESSION);
    if(!empty($_SESSION['DB_NAME'])){
        define('DB_NAME', $_SESSION['DB_NAME']);
    } 
    else {
        define('DB_NAME', COMPANY_DB_NAME);
    }    
    define('DB_USER', 'root');
    define('DB_PASSWORD', 'PZYKHcqU');
}



define('STORAGE_TYPE', 'S3'); //LOCAL or S3

if(STORAGE_TYPE === 'S3'){
    //AWS S3 Settings
    
    if(!empty($_SESSION['S3_BUCKET'])){
        define('S3_BUCKET', $_SESSION['S3_BUCKET']);
    }
    define('S3_ACCESS_KEY', 'AKIAJV2RKIWJV3UKR7PA');
    define('S3_ACCESS_SECRET', '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');
	if(!empty($_SESSION['S3_BUCKET'])){
		define('S3_BUCKET_DOMAIN', 'https://s3-ap-southeast-1.amazonaws.com/'.S3_BUCKET.'/');
	}
    define('S3_BUCKET_DOMAIN_FEEDBACK', 'https://s3-ap-southeast-1.amazonaws.com/');//Dont remove this, This line and above line are same but Dont remove
    define('S3_BUCKET_SUPER', 'moni-company');
    define('S3_BUCKET_DOMAIN_SUPER', 'https://s3-ap-southeast-1.amazonaws.com/moni-company/');
    
} else {
    //local server domain with api folder
    define('LOCAL_SERVER_DOMAIN', $_SERVER["DOCUMENT_ROOT"].'/api/');   
}

/** Image Upload Directory Path**/
define('UPLOAD_USER_DIR', 'Images/UserImage/');
define('UPLOAD_CHECK_DIR', 'Images/CheckpointImage/');
define('UPLOAD_LOCATION_DIR', 'Images/Locations/');
define('UPLOAD_TEMP1_DIR', 'Images/Temp1/');
define('UPLOAD_WORK_DIR', 'Images/Worklist/');
define('UPLOAD_CHECK_QR_DIR', 'Images/QR/');
define('UPLOAD_COMPANY_DIR', 'Images/Company/');
define('UPLOAD_CUSTOMER_FEEDBACK_DIR', 'Images/Feedback/');

define('USER_SAMPLE', 'file/sample/user.csv');
define('CHECKPOINT_SAMPLE', 'file/sample/checkpoint.csv');
define('LOCATION_SAMPLE', 'file/sample/location.csv');

/**
 * Path for Sample document of user, location, checkpoint bulk import
 */
//define('SAMPLE_DOC', $_SERVER['HTTP_HOST'].'/api/Images/sample/');
define('SAMPLE_DOC', 'file/sample/');
define('REPORT_DOC', $_SERVER['HTTP_HOST'].'/api/Images/reports/');
define('COMPANY_LOGO', 'logo/');
define('DATABASE_TABLE', 'https://s3-ap-southeast-1.amazonaws.com/moni-company/file/database/dev_moni.sql');

/**
 * Firebase Server Key
 */
define('FIREBASE_API_KEY', 'AAAAZYskm-k:APA91bEc9gjZOSmg9BNoxH4OJN5v-fD8ztSBG0SsKPtdw-Cc-lINQaEXAJ_95HOwy9ErPytzSSXlt2VYqZ0u6DMp4xpD94z_T5gCKuUUdOWcP-dPsVw_7V32Sfff4Un0pWCXDiZUzRdCqg_YM3pdC7qWnvdcSxiLaQ');

/**
 * Session time out in seconds
 */
define('SESSION_TIME_OUT',86400);

/** Error Reporting Settings  **/
define('LOG_DIR', 'logs');
define('LOG_FILE', 'log.txt');

/** Debug Settings **/
define('LOG_LEVEL',0); //0=No Log 1= Log File Only 2=include in Json Output 

//**encription
define('ENCRYPTION_METHOD','RC4');
define('ENCRYPTION_KEY','s1kX6VhcAJ'); //do not change this you will loose data.

//SDKS And Modules
//Define requred Module sand SDKS, shoud match the folder name( Case Sensitive)
$GLOBALS['SDKS']=array(
    'Aws',
    'pdf',
    'qrcode'
); 

$GLOBALS['MODULES']=array(
    'Loging',
    'FileManager'
);
