<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class CompanyController extends Controller {
    
    public function Render() {
        switch ($this->getRequest()->getType()) {
            case 'GET':
                $this->getHandler();
                break;
            case 'POST':
                $this->postHandler();
                break;
            default:
                echo Response::badRequest();
                break;
        }
    }

    public function getHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'single':
                $response = new Response();
                
                $ID = $_SESSION['COMPANY_ID'];
                
                $company = new Company();
                $company->setId($ID);
                if($company->getId()){
                    echo $response->create(500, 'Company id is required', false);
                    return false;
                }
                
                $result = CompanyDAO::getCompany($company->getId());
                if($result){
                    $response->setData($result);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'Unsuccess', false);
                }
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function postHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'add':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                $company = new Company($data);
                
                if(empty($company->getName())){
                    echo $response->create(500, 'Please Set Name of the Company', false);
                    return false;
                }
                if(!empty($company->getImage())){
                    $imageValidate = ImageManager::checkImageSize($company->getImage());

                    if($imageValidate > 1000 ){
                        echo $response->create(500, 'Image too large. Maximum resolution 500x200 px', false);
                        return false;
                    }

                }
                if(!empty($company->getEmail())){
                    
                    if(!Validation::emailValidation($company->getEmail())){
                        echo $response->create(500, 'A valid E-mail is required', false);
                        return false;
                    }
                }
                
                $count = CompanyDAO::getCompanyCount();
                if($count['count'] == 1){
                    echo $response->create(500, 'Please use update logo to change company logo', false);
                    return false;
                }
                
                $result = CompanyDAO::addCompany($company);
                if($result){
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'Unsuccess', false);
                }
                break;
            case 'update':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                $ID = $_SESSION['COMPANY_ID'];
                
                $company = new Company($data);
                $company->setId($ID);
                
                if(empty($company->getId())){
                    echo $response->create(500, 'Please Set Company ID', false);
                    return false;
                }
                if(empty($company->getName()) && empty($company->getImage())){
                    echo $response->create(500, 'Please Set Update Values', false);
                    return false;
                }
//                if(!empty($company->getEmail())){
//                    
//                    if(!Validation::emailValidation($company->getEmail())){
//                        echo $response->create(500, 'A valid E-mail is required', false);
//                        return false;
//                    }
//                }
                if(!empty($company->getImage())){
                    
                    $base64 = Validation::validateBase64String($company->getImage());
                    if($base64){
                        $imageValidate = ImageManager::checkImageSize($company->getImage());

                        if($imageValidate > 1000 ){
                            echo $response->create(500, 'Image too large. Maximum resolution 500x200 px', false);
                            return false;
                        }
                    } else {
                        if(!filter_var($company->getImage(), FILTER_VALIDATE_URL)){ 
                            echo $response->create(500, 'Image too large. Maximum resolution 500x200 px', false);
                            return false;
                        }
                    }  
                }
                
                if(!CompanyDAO::getCompany($company->getId())){
                    echo $response->create(500, 'Company Not Exist', false);
                    return false;
                }
                
                $result = CompanyDAO::updateCompany($company);
                if($result){
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'Unsuccess', false);
                }
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function authenticate() {
        return true;
    }

}
