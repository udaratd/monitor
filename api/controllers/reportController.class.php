<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of reportController
 *
 * @author user
 */
class ReportController extends Controller {
    //put your code here ffff
    public function Render() {
        switch ($this->getRequest()->getType()) {
            case 'GET':
                $this->getHandler();
                break;
            case 'POST':
                $this->postHandler();
                break;
            case 'CLI':
                $this->cliHandler();
                break;
            case 'CGI_FCGI':
                $this->cgi_fcgiHandler();
                break;
            default:
                echo Response::badRequest();
                break;
        }
    }

    public function getHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'executive':
                $response = new Response();
                
                $empty = array();
                 
                $data = $this->getRequest()->getData();
                
                $print = false;
                if(isset($data['print'])){
                    $print = true;
                } else {
                    $print = false;
                }
                
                $completedTask = ReportController::completedTask($data);
                if($completedTask){
                    $output['completedTask'] = $completedTask;
                }
                
                $ontime = ReportController::ontime($data);
                if($ontime){
                    $output['ontime'] = $ontime;
                }
                
                $reworkRate = ReportController::reworkRate($data);
                if($reworkRate){
                    $output['reworkRate'] = $reworkRate;
                }
                
                $flaggedRate = ReportController::flaggedRate($data);
                if($flaggedRate){
                    $output['flaggedRate'] = $flaggedRate;
                }
                
                $reworkStaff = ReportController::reworkStaff($data);
                if($reworkStaff){
                    $output['reworkStaff'] = $reworkStaff;
                } else {
                    $output['reworkStaff'] = $empty;
                }
                
                $reworkCheckpoint = ReportController::reworkCheckpoint($data);
                if($reworkCheckpoint){
                    $output['reworkCheckpoint'] = $reworkCheckpoint;
                } else {
                    $output['reworkCheckpoint'] = $empty;
                }
                
                $lateSubmitedCheckpoint = ReportController::lateSubmitedCheckpoint($data);
                if($lateSubmitedCheckpoint){
                    $output['lateSubmitedCheckpoint'] = $lateSubmitedCheckpoint;
                } else {
                    $output['lateSubmitedCheckpoint'] = $empty;
                }
                
                $moreThanOneRework = ReportController::moreThanOneRework($data);
                if($moreThanOneRework){
                    $output['moreThanOneRework'] = $moreThanOneRework;
                } else {
                    $output['moreThanOneRework'] = $empty;
                }

                if($output){
                    if(!$print){
                        $response->setData($output);
                        echo $response->create(200, 'Success', true);
                    } else {
                        
                        ob_start();
                        $pdfExecutive = new PDFExecutive();
                        $pdfExecutive->printExecutive($output);
                        $html = ob_get_clean();
                        
                        $name = 'Executive';
                        ReportController::generatePrint($html,$name);
                    }
                } else {
                    echo $response->create(500, 'No Data', false);
                }
                break;
            case 'comparison':
                $response = new Response();           
                
                $data = $this->getRequest()->getData();
                
                $s_date = $data['s_date'];
                $e_date = $data['e_date'];
                
                $date_period = ReportController::dateDifference($s_date, $e_date);
                
                $staff_1 = $data['staff_1'];
                $staff_2 = $data['staff_2'];               
                
                $print = false;
                if(isset($data['print'])){
                    $print = true;
                } else {
                    $print = false;
                }
                
                if(empty($staff_1) || empty($staff_2)){
                    echo $response->create(500, 'Please Set Two Staff Members', false);
                    return false;
                }
                
                $user = ReportController::com_user($staff_1, $staff_2);
                if($user){
                    $output['user'] = $user;
                }
                
                $completedCheckpoints = ReportController::com_completedCheckpoint($date_period, $staff_1, $staff_2, $s_date, $e_date);
                if($completedCheckpoints){
                    $output['completedCheckpoints'] = $completedCheckpoints;
                }
                
                $rework = ReportController::com_rework($date_period, $staff_1, $staff_2, $s_date, $e_date);
                if($rework){
                    $output['rework'] = $rework;
                }
                
                $ontime = ReportController::com_ontime($date_period, $staff_1, $staff_2, $s_date, $e_date);
                if($ontime){
                    $output['ontime'] = $ontime;
                }
                
                if($output){
                    if(!$print){
                        $response->setData($output);
                        echo $response->create(200, 'Success', true);
                    } else {
                        
                        ob_start();
                        $pdfComparison = new PDFComparison();
                        $pdfComparison->printComparison($output);
                        $html = ob_get_clean();
                        
                        $name = 'Comparison';
                        ReportController::generatePrint($html,$name);
                    }
                } else {
                    echo $response->create(500, 'Unsuccess', false);
                }
                break;
            case 'process':
                $response = new Response();
                
                $output = array();
                
                $data = $this->getRequest()->getData();
                
                $s_date = $data['s_date'];
                $e_date = $data['e_date'];
                
                $date_period = ReportController::dateDifference($s_date, $e_date);
                
                $staff_id = $data['staff_id'];
                $location_id = $data['location_id'];              
                
                $print = false;
                if(isset($data['print'])){
                    $print = true;
                } else {
                    $print = false;
                }
                
                if(empty($location_id)){
                    $location_id = false;
                }
                if(empty($staff_id)){
                    $staff_id = false;
                }

                $process_checkpoints = ReportController::process_checkpoint($staff_id, $location_id, $date_period, $s_date, $e_date);
                if($process_checkpoints){
                    $output['process_checkpoints'] = $process_checkpoints;
                }
                
                $process_staffs = ReportController::process_staff($staff_id, $location_id, $date_period, $s_date, $e_date);
                if($process_staffs){
                    $output['process_staffs'] = $process_staffs;
                }
                
                if($output){
                    if(!$print){
                        $response->setData($output);
                        echo $response->create(200, 'Success', true);
                    } else {
                        
                        ob_start();
                        $pdfProcess = new PDFProcess();
                        $pdfProcess->printProcess($output);
                        $html = ob_get_clean();
                        
                        $name = 'Process';
                        ReportController::generatePrint($html,$name);
                    }                  
                } else {
                    echo $response->create(500, 'Unsuccess', false);
                }
                
                break;
            case 'customer':
                $response = new Response();
                
                $input = $this->getRequest()->getData();
                
                $s_date_1 = $input['s_date'];
                $e_date_1 = $input['e_date'];
                
                $date_period = ReportController::dateDifference($s_date_1, $e_date_1);
                
                $data = array();
                $empty = array();               
                
                $print = false;
                if(isset($input['print'])){
                    $print = true;
                } else {
                    $print = false;
                }
                
                $today = $e_date_1;

                $dateRange = DateAndTimeManager::forwardBackwardDateRange($today, false, $date_period);
                $length = count($dateRange);

                $s_date = $dateRange[0];
                $e_date = $dateRange[$length-1];
                
                $result = ReportDAO::customer($s_date, $e_date);
                
                $dateRangeCount = 0;
                if($result){
                    foreach ($dateRange as $date){//loop throught date range
                        $resultCount = 0;
                        $output_2 = array();
                        foreach ($result as $checkpoint){//loop throught work
                            if($date === $checkpoint['server_date']){//if work date and date range date are same put that data to $output_2 array                           
                                array_push($output_2, $checkpoint);                            
                            } else {
                                $resultCount++;//if there are no data in peticulare day just increment the $resultCount
                            }
                        }

                        $sortCheckpointID = array();
                        foreach ($output_2 as $key => $value) {//sort output_2 using checkpoint id
                            $sortCheckpointID[$key] = $value['checkpoint_id'];
                        }
                        array_multisort($sortCheckpointID,SORT_ASC,$output_2);//this will perform the sort

                        $uniqueCheckpointID_s = array_unique($sortCheckpointID,SORT_REGULAR);//remove duplicate checkpoint and return checkpointID_s array

                        $outputCheckpoint = array();
                        $outputCheckpointCount = 0;
                        foreach ($uniqueCheckpointID_s as $checkpointID){
                            $output_3 =array();
                            $finalData = array();
                            foreach ($output_2 as $singleCheckpoint){//seperate multiple same data of same checkpoint
                                if($checkpointID == $singleCheckpoint['checkpoint_id'])
                                array_push($output_3, $singleCheckpoint);
                            }

                            $sortCheckpointBYUploadTime = array();
                            foreach ($output_3 as $key => $value) {
                                $sortCheckpointBYUploadTime[$key] = $value['server_time'];
                            }
                            array_multisort($sortCheckpointBYUploadTime,SORT_DESC,$output_3);

                            $count = count($output_3);

                            $finalData['location'] = $output_3[0]['location'];  
                            $finalData['checkpointName'] = $output_3[0]['checkpointName'];  
                            $finalData['sample'] = $output_3[0]['sample_img_thumbnail'];
                            if($count > 1){
                                $finalData['before'] = $output_3[1]['work_img_thumbnail'];
                            } else {
                                $before = ReportDAO::get_approved_work($output_3[0]['server_date'], $output_3[0]['checkpoint_id']);

                                if($before){
                                    $finalData['before'] = $before;
                                } else {
                                    $finalData['before'] = null;
                                }
                            }
                            $finalData['after'] = $output_3[0]['work_img_thumbnail'];  

                            $outputCheckpoint[$outputCheckpointCount] = $finalData;
                            $outputCheckpointCount++;

                        }

                        if(count($result) == $resultCount){
                            $output_1['date'] = $date;
                            $output_1['checkpoints'] = $empty;
                            array_push($data, $output_1);
                        } else {
                            $output_1['date'] = $date;
                            $output_1['checkpoints'] = $outputCheckpoint;
                            array_push($data, $output_1);
                        }
                    }
                }
                
                if($data){
                    if(!$print){
                        $response->setData($data);
                        echo $response->create(200, 'Success', true);
                    } else {
                        
                        ob_start();
                        $pdfCustomer = new PDFCustomer();
                        $pdfCustomer->printCustomer($data);
                        $html = ob_get_clean();
                        
                        $name = 'Customer';
                        ReportController::generatePrint($html,$name);
                    }                   
                } else {
                    echo $response->create(500, 'No Data', false);
                }
                break;
            case 'checkpoints':
                $response = new Response();

                $checkpoints = CheckpointDAO::getCheckpoints();

                if($checkpoints){
                    $response->setData($checkpoints);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No Data', false);
                }
                break;
            case 'staff':
                $response = new Response();

                $supervisorID = $this->getRequest()->getData()['supervisor_id'];
                
                if(!empty($supervisorID)){
                
                    $staff = UserDAO::getStaffAccordingToSupervisor($supervisorID);

                    if($staff){
                        $response->setData($staff);
                        echo $response->create(200, 'Success', true);
                    } else {
                        echo $response->create(500, 'No Data', false);
                    }
                } else {
                    echo $response->create(500, 'Missing Supervisor ID', false);
                }
                break;
            case 'sample':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                $type = $data['type'];
                if($type == 1){//user
                    $link['link'] = 'http://'.SAMPLE_DOC.'user.csv';
                }elseif ($type == 2) {//locations
                    $link['link'] = 'http://'.SAMPLE_DOC.'location.csv';
                }elseif ($type == 3) {//checkpoints
                    $link['link'] = 'http://'.SAMPLE_DOC.'checkpoint.csv';
                } else {
                    $link = false;
                }
                
                if($link){
                    $response->setData($link);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'Unsuccess', false);
                }
                break;
            case 'addssh':
                
                $host = '67.222.9.229';
                $port = '22';
                $username = 'dev';
                $password = 'samansiri';
                print_r($_SERVER);
                $crontab = new CrontabManager($host, $port, $username, $password);
//                $crontab->crontab_file_exists();
                var_dump($crontab->crontab_file_exists());
                
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function postHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'add':
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }
    
    public function cliHandler() {
        
    }

    public function cgi_fcgiHandler() {
        switch ($this->getRequest()->getAction()) {
            
        }
    }
    
    public function authenticate() {
//        SessionManager::checkTimeoutSession();
        return true;
    }
    
    private static function completedTask($reportData){
        
        $output = array();
        
        $data = $reportData;
        
        $s_date = $data['s_date'];
        $e_date = $data['e_date'];
                
        $date_period = ReportController::dateDifference($s_date, $e_date);
        if(empty($date_period)) {
            $date_period = "7 days";
        } else {
            $date_period = $date_period." days";
        }       

        $data["s_date"] = $s_date;
        $data["e_date"] = $e_date;

        $report = new Report($data);
        $result_1 = ReportDAO::getCompletedTask($report);

        //compair with
        $s_date = date_create($s_date);
        $s_date = date_sub($s_date, date_interval_create_from_date_string("1 day"));
        $s_date = date_format($s_date, "Y-m-d");

        $today = $s_date;

        $date = date_create($today);
        $s_date = date_sub($date, date_interval_create_from_date_string($date_period));

        $s_date = date_format($s_date,"Y-m-d");
        $e_date = $today;

        $data["s_date"] = $s_date;
        $data["e_date"] = $e_date;

        $report = new Report($data);
        $result_2 = ReportDAO::getCompletedTask($report);

        $new_completeTask = $result_1['completedTaskCount'];
        $original_completeTask = $result_2['completedTaskCount'];


        if($new_completeTask>0){
            $output['completedTaskCount'] = $new_completeTask;
            if ($original_completeTask>0) {
                $percentage = (($original_completeTask - $new_completeTask)/$original_completeTask)*100;
                $output['percentage'] = $percentage;
                if($percentage<0){
                    $output['status'] = "Positive";
                } elseif ($percentage==0) {
                    $output['status'] = "neutral";
                } else {
                    $output['status'] = "Negative";
                }
            }
            return $output;
        }else{
            $output['completedTaskCount'] = 0;
            $output['percentage'] = 0;
            $output['status'] = "neutral";
            return $output;
        }
    }
    
    private static function ontime($reportData){
        
        $output = array();

        $data = $reportData;

        $s_date = $data['s_date'];
        $e_date = $data['e_date'];
                
        $date_period = ReportController::dateDifference($s_date, $e_date);
        if(empty($date_period)) {
            $date_period = "7 days";
        } else {
            $date_period = $date_period." days";
        }

        $data["s_date"] = $s_date;
        $data["e_date"] = $e_date;

        $report = new Report($data);
        $result_1 = ReportDAO::getOnTimeTask($report);

        //compair with
        $s_date = date_create($s_date);
        $s_date = date_sub($s_date, date_interval_create_from_date_string("1 day"));
        $s_date = date_format($s_date, "Y-m-d");

        $today = $s_date;

        $date = date_create($today);
        $s_date = date_sub($date, date_interval_create_from_date_string($date_period));

        $s_date = date_format($s_date,"Y-m-d");
        $e_date = $today;

        $data["s_date"] = $s_date;
        $data["e_date"] = $e_date;

        $report = new Report($data);
        $result_2 = ReportDAO::getOnTimeTask($report);

        $new_onTimeTask = $result_1['onTimeTasks'];
        $total_completedTask = $result_1['completedTaskCount'];
        $original_onTimeTask = $result_2['onTimeTasks'];

        if($new_onTimeTask>0){
            $onTimePercentage = ($new_onTimeTask/$total_completedTask)*100;
            $output['onTimeTasksPercentage'] = $onTimePercentage;
            if ($original_onTimeTask>0) {
                $percentage = (($original_onTimeTask - $new_onTimeTask)/$original_onTimeTask)*100;
                $output['percentage'] = $percentage;
                if($percentage<0){
                    $output['status'] = "Positive";
                } elseif ($percentage==0) {
                    $output['status'] = "neutral";
                } else {
                    $output['status'] = "Negative";
                }
            }
            return $output;
        }else{
            $output['onTimeTasksPercentage'] = 0;
            $output['percentage'] = 0;
            $output['status'] = "neutral";
            return $output;
        }
    }
    
    private static function reworkRate($reportData){
        
        $output = array();

        $data = $reportData;

        $s_date = $data['s_date'];
        $e_date = $data['e_date'];
                
        $date_period = ReportController::dateDifference($s_date, $e_date);
        if(empty($date_period)) {
            $date_period = "7 days";
        } else {
            $date_period = $date_period." days";
        }

        $data["s_date"] = $s_date;
        $data["e_date"] = $e_date;

        $report = new Report($data);
        $result_1 = ReportDAO::getReworkRate($report);


        //Compair with
        $s_date = date_create($s_date);
        $s_date = date_sub($s_date, date_interval_create_from_date_string("1 day"));
        $s_date = date_format($s_date, "Y-m-d");

        $today = $s_date;

        $date = date_create($today);
        $s_date = date_sub($date, date_interval_create_from_date_string($date_period));

        $s_date = date_format($s_date,"Y-m-d");
        $e_date = $today;

        $data["s_date"] = $s_date;
        $data["e_date"] = $e_date;

        $report = new Report($data);
        $result_2 = ReportDAO::getReworkRate($report);


        $new_reworkTask = $result_1['reworkTask'];
        $total_completedTask = $result_1['completedTaskCount'];
        $original_reworkTask = $result_2['reworkTask'];

        if($new_reworkTask>0){
            $reworkPercentage = ($new_reworkTask/$total_completedTask)*100;
            $output['reworkPercentage'] = $reworkPercentage;
            if ($original_reworkTask>0) {
                $percentage = (($original_reworkTask - $new_reworkTask)/$original_reworkTask)*100;
                $output['percentage'] = $percentage;
                if($percentage<0){
                    $output['status'] = "Positive";
                } elseif ($percentage==0) {
                    $output['status'] = "neutral";
                } else {
                    $output['status'] = "Negative";
                }
            }
            return $output;
        }else{
            $output['reworkPercentage'] = 0;
            $output['percentage'] = 0;
            $output['status'] = "neutral";
            return $output;
        }
    }
    
    private static function flaggedRate($reportData){
        
        $output = array();

        $data = $reportData;

        $s_date = $data['s_date'];
        $e_date = $data['e_date'];
                
        $date_period = ReportController::dateDifference($s_date, $e_date);
        if(empty($date_period)) {
            $date_period = "7 days";
        } else {
            $date_period = $date_period." days";
        }

        $data["s_date"] = $s_date;
        $data["e_date"] = $e_date;

        $report = new Report($data);
        $result_1 = ReportDAO::getFlaggedRate($report);

        //compair with
        $s_date = date_create($s_date);
        $s_date = date_sub($s_date, date_interval_create_from_date_string("1 day"));
        $s_date = date_format($s_date, "Y-m-d");

        $today = $s_date;

        $date = date_create($today);
        $s_date = date_sub($date, date_interval_create_from_date_string($date_period));

        $s_date = date_format($s_date,"Y-m-d");
        $e_date = $today;

        $data["s_date"] = $s_date;
        $data["e_date"] = $e_date;

        $report = new Report($data);
        $result_2 = ReportDAO::getFlaggedRate($report);

        $new_flaggedTask = $result_1['flaggedTask'];
        $total_completedTask = $result_1['completedTaskCount'];
        $original_flaggedTask = $result_2['flaggedTask'];

        if($new_flaggedTask>0){
            $flaggedPercentage = ($new_flaggedTask/$total_completedTask)*100;
            $output['flaggedPercentage'] = $flaggedPercentage;
            if ($original_flaggedTask>0) {
                $percentage = (($original_flaggedTask - $new_flaggedTask)/$original_flaggedTask)*100;
                $output['percentage'] = $percentage;
                if($percentage<0){
                    $output['status'] = "Positive";
                } elseif ($percentage==0) {
                    $output['status'] = "neutral";
                } else {
                    $output['status'] = "Negative";
                }
            }
            return $output;
        }else{
            $output['flaggedPercentage'] = 0;
            $output['percentage'] = 0;
            $output['status'] = "neutral";
            return $output;
        }
    }
    
    private static function reworkStaff($reportData){
                
        $data = $reportData;        

        $report = new Report($data);
        $result = ReportDAO::getReworkStaff($report);

        if($result){
            return $result;
        } else {
            return false;
        }
    }
    
    private static function reworkCheckpoint($reportData){
                
        $data = $reportData;

        $report = new Report($data);
        $result = ReportDAO::getReworkCheckpoint($report);
        
        if($result){
            return $result;
        } else {
            return false;
        }
    }
    
    private static function lateSubmitedCheckpoint($reportData){

        $data = $reportData;

        $report = new Report($data);
        $result = ReportDAO::getLateSubmitedCheckpoint($report);
        
        if($result){
            return $result;
        } else {
            return false;
        }
    }
    
    private static function moreThanOneRework($reportData){
        
        $data = $reportData;

        $report = new Report($data);
        $result = ReportDAO::get_Checkpoint_With_MoreThan_OneRework($report);
        
        if($result){
            return $result;
        } else {
            return false;
        }
    }
    
    private static function com_completedCheckpoint($date_period, $staff_1, $staff_2, $s_date_1, $e_date_1){
        $output = array();               
        $staff_one = array();                
        $staff_two = array();

        $today = $e_date_1;

        $dateRange = DateAndTimeManager::forwardBackwardDateRange($today, false, $date_period);
        $length = count($dateRange);

        $s_date = $dateRange[0];
        $e_date = $dateRange[$length-1];

        $completedCheckpoints_staff_1 = ReportDAO::getCompletedCheckpoint($staff_1, $s_date, $e_date);

        for($i=0;$i<$length;$i++){
            $currentDate = $dateRange[$i];
            $completedCheckpointCount = 0;
            foreach ($completedCheckpoints_staff_1 as $checkpoint) {
                if($currentDate === $checkpoint['server_date']){
                    $completedCheckpointCount++;
                }
            }

            $staff_one[$i]['date'] = $currentDate;
            $staff_one[$i]['count'] = $completedCheckpointCount;
        }              

        $completedCheckpoints_staff_2 = ReportDAO::getCompletedCheckpoint($staff_2, $s_date, $e_date);

        for($i=0;$i<$length;$i++){
            $currentDate = $dateRange[$i];
            $completedCheckpointCount = 0;
            foreach ($completedCheckpoints_staff_2 as $checkpoint) {
                if($currentDate === $checkpoint['server_date']){
                    $completedCheckpointCount++;
                }
            }

            $staff_two[$i]['date'] = $currentDate;
            $staff_two[$i]['count'] = $completedCheckpointCount;
        }

        for($k=0;$k<$length;$k++){
            if($staff_one[$k]['date'] == $staff_two[$k]['date']){
                $output[$k]['date'] = $staff_one[$k]['date'];
                $output[$k]['staff_1'] = $staff_one[$k]['count'];
                $output[$k]['staff_2'] = $staff_two[$k]['count'];
            }
        }
        
        if($output){
            return $output;
        } else {
            return false;
        }
    }
    
    private static function com_rework($date_period, $staff_1, $staff_2, $s_date_1, $e_date_1){
        $output = array();               

        $today = $e_date_1;

        $dateRange = DateAndTimeManager::forwardBackwardDateRange($today, false, $date_period);
        $length = count($dateRange);

        $s_date = $dateRange[0];
        $e_date = $dateRange[$length-1];

        $rework_staff_1_all = ReportDAO::getOnTimeCompletionRateAllWork($staff_1, $s_date, $e_date);
        $rework_staff_1 = ReportDAO::getReworkedCheckpointCount($staff_1, $s_date, $e_date);             

        $rework_staff_2_all = ReportDAO::getOnTimeCompletionRateAllWork($staff_2, $s_date, $e_date);
        $rework_staff_2 = ReportDAO::getReworkedCheckpointCount($staff_2, $s_date, $e_date);

        if(!empty($rework_staff_1_all['allTasks'])){
            $output['staff_1'] = ($rework_staff_1['reworkTask']/$rework_staff_1_all['allTasks'])*100;
        } else {
            $output['staff_1'] = 0;
        }
        
        if(!empty($rework_staff_2_all['allTasks'])){
            $output['staff_2'] = ($rework_staff_2['reworkTask']/$rework_staff_2_all['allTasks'])*100;
        } else {
            $output['staff_2'] = 0;
        }

        
        if($output){
            return $output;
        } else {
            return false;
        }
    }
    
    private static function com_ontime($date_period, $staff_1, $staff_2, $s_date_1, $e_date_1){
        $output = array();               

        $today = $e_date_1;

        $dateRange = DateAndTimeManager::forwardBackwardDateRange($today, false, $date_period);
        $length = count($dateRange);

        $s_date = $dateRange[0];
        $e_date = $dateRange[$length-1];

        $ontime_staff_1_all = ReportDAO::getOnTimeCompletionRateAllWork($staff_1, $s_date, $e_date);
        $ontime_staff_1 = ReportDAO::getOnTimeCompletionRate($staff_1, $s_date, $e_date);

        $ontime_staff_2_all = ReportDAO::getOnTimeCompletionRateAllWork($staff_2, $s_date, $e_date);
        $ontime_staff_2 = ReportDAO::getOnTimeCompletionRate($staff_2, $s_date, $e_date);

        if(!empty($ontime_staff_1_all['allTasks'])){
            $output['staff_1'] = ($ontime_staff_1['onTimeTasks']/$ontime_staff_1_all['allTasks'])*100;
        } else {
            $output['staff_1'] = 0;
        }
        
        if(!empty($ontime_staff_2_all['allTasks'])){
            $output['staff_2'] = ($ontime_staff_2['onTimeTasks']/$ontime_staff_2_all['allTasks'])*100;
        } else {
            $output['staff_2'] = 0;
        }

        
        if($output){
            return $output;
        } else {
            return false;
        }
    }
    
    private static function com_user($staff_1, $staff_2){
        $output = array();               

        $staff_one = UserDAO::getUser(false, $staff_1);             

        $staff_two = UserDAO::getUser(false, $staff_2);

        $output[0]['staff_1'] = $staff_one;
        $output[1]['staff_2'] = $staff_two;

        
        if($output){
            return $output;
        } else {
            return false;
        }
    }
    
    private static function process_checkpoint($staff_id, $location_id, $date_period, $s_date_1, $e_date_1){
        $output_1 = array();
        $output_2 = array();
        $data = array();
        $checkpointRange = array();

        $today = $e_date_1;

        $dateRange = DateAndTimeManager::forwardBackwardDateRange($today, false, $date_period);
        $length = count($dateRange);

        $s_date = $dateRange[0];
        $e_date = $dateRange[$length-1];
        
        $result_2 = ReportDAO::processWork($s_date, $e_date, $location_id, $staff_id);

        if($result_2){                    
            foreach ($result_2 as $re){
                array_push($checkpointRange, $re['checkpointName']);
            }

            $uniqueCheckpoints = array_unique($checkpointRange);
            foreach ($uniqueCheckpoints as $unique){
                $empty[$unique] = 0;
            }

            for($i=0;$i<$length;$i++){
                
                $work = array();
//                $checkpoints = array();
                $dates = array();
                $checkpointsName = array();
                
                foreach ($result_2 as $re){
                    if($dateRange[$i] == $re['server_date']){
//                            array_push($checkpoints, $re['checkpoint_id']);
                        array_push($checkpointsName, $re['checkpointName']);
                        $dates[0] = $dateRange[$i];
                    }
                }
                if(!empty($dates)){
//                        $checkpointID_s = array_count_values($checkpoints);

                    $checkpointNames = array_count_values($checkpointsName);

                    foreach ($uniqueCheckpoints as $name){
                        if(!array_key_exists($name, $checkpointNames)){
                            $checkpointNames[$name] = 0;
                        }
                    }

                    $work['date'] = $dates[0];
                    ksort($checkpointNames);
                    $work['checkpoints'] = $checkpointNames;
                } else {
                    
                    $checkpointNames = array_count_values($checkpointsName);

                    foreach ($uniqueCheckpoints as $name){
                        if(!array_key_exists($name, $checkpointNames)){
                            $checkpointNames[$name] = 0;
                        }
                    }
                    
                    $work['date'] = $dateRange[$i];
                    ksort($checkpointNames);
                    $work['checkpoints'] = $checkpointNames;
                }
                array_push($output_2, $work);                    
            }
            
            
            $result_1 = ReportDAO::processRework($s_date,$e_date,$location_id,$staff_id);
            
            if($result_1){                    
//                foreach ($result_1 as $re){
//                    array_push($checkpointRange, $re['checkpointName']);
//                }
//
//                $uniqueCheckpoints = array_unique($checkpointRange);
//                foreach ($uniqueCheckpoints as $unique){
//                    $empty[$unique] = 0;
//                }

                for($i=0;$i<$length;$i++){

                    $rework = array();
    //                $checkpoints = array();
                    $dates = array();
                    $checkpointsName = array();

                    foreach ($result_1 as $re){
                        if($dateRange[$i] == $re['server_date']){
    //                            array_push($checkpoints, $re['checkpoint_id']);
                            array_push($checkpointsName, $re['checkpointName']);
                            $dates[0] = $dateRange[$i];
                        }
                    }
                    if(!empty($dates)){
    //                        $checkpointID_s = array_count_values($checkpoints);

                        $checkpointNames = array_count_values($checkpointsName);

                        foreach ($uniqueCheckpoints as $name){
                            if(!array_key_exists($name, $checkpointNames)){
                                $checkpointNames[$name] = 0;
                            }
                        }

                        $rework['date'] = $dates[0];
                        ksort($checkpointNames);
                        $rework['checkpoints'] = $checkpointNames;
                    } else {

                        $checkpointNames = array_count_values($checkpointsName);

                        foreach ($uniqueCheckpoints as $name){
                            if(!array_key_exists($name, $checkpointNames)){
                                $checkpointNames[$name] = 0;
                            }
                        }
                        
                        $rework['date'] = $dateRange[$i];
                        ksort($checkpointNames);
                        $rework['checkpoints'] = $checkpointNames;
                    }
                    array_push($output_1, $rework);                    
                }

            }           
        }

        if($output_1 && $output_2){
            $dataCount=0;
            foreach ($output_2 as $value) {
                $data[$dataCount]['date'] = $value['date'];
                $count=0;
                foreach ($value['checkpoints'] as $key => $workCount) {                
                    $reworkCount = $output_1[$dataCount]['checkpoints'][$key];
                    if($workCount != 0){
                        $rate = ($reworkCount/$workCount)*100;
                    } else {
                        $rate = 0;
                    }
                    $schedule[$key] = $rate;
                    $count++;
                }
                $data[$dataCount]['checkpoints'] = $schedule;
                $dataCount++;
            }
        }

        return $data;


    }
    
    private static function process_staff($staff_id, $location_id, $date_period, $s_date_1, $e_date_1){
        $output = array();
        $staffRange = array();

        $today = $e_date_1;

        $dateRange = DateAndTimeManager::forwardBackwardDateRange($today, false, $date_period);
        $length = count($dateRange);

        $s_date = $dateRange[0];
        $e_date = $dateRange[$length-1];

        $totalWorks = ReportDAO::processWork($s_date, $e_date, $location_id, $staff_id);
        
        $totalRework = ReportDAO::processRework($s_date,$e_date,$location_id,$staff_id);

        if(!$totalWorks || !$totalRework){
            return false;
        }
        
        if($totalWorks){
            foreach ($totalWorks as $work){
             array_push($staffRange, $work['staffName']);
            }             
        }
        
        $uniqueStaffs = array_unique($staffRange);
        foreach ($uniqueStaffs as $staff){
            $workCount=0;
            foreach ($totalWorks as $work){
                if($staff === $work['staffName']){
                    $workCount++;
                }
                $workOutput[$staff]=$workCount;
            }
        }
        
        foreach ($uniqueStaffs as $staff){
            $reworkCount=0;
            foreach ($totalRework as $rework){
                if($staff === $rework['staffName']){
                    $reworkCount++;
                }
                $reworkOutput[$staff]=$reworkCount;
            }
        }
        
        if($workOutput){
            foreach ($workOutput as $key_work => $result_work){
                foreach ($reworkOutput as $key_rework => $result_rework){
                    if($key_work === $key_rework){
                        if($result_work != 0){
                            $rate = ($result_rework/$result_work)*100;
                        } else {
                            $rate = 0;
                        }
                    }
                }
                $output[$key_work] = $rate;
            }
        }
        
        return $output;
    }
    
    public static function generatePrint($html,$name){
        
        date_default_timezone_set('Asia/Colombo');
        $today = date("Y-m-d");
        $currentTime = date("H:i:s");
        $currentTime = str_replace(':', '-', $currentTime);
        $date = $today.'_'.$currentTime;
        
        $dompdf = new Dompdf\Dompdf();
//        $dompdf->set_option('defaultFont', 'Courier');
        $dompdf->set_option('isHtml5ParserEnabled', true);
        $dompdf->set_option('isRemoteEnabled', true);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        
        ////////////////////for send email//////////////////
        $output = $dompdf->output();
        $path = UPLOAD_LOCATION_DIR;
        $fileName = $name.$date.'.pdf';        
        $file = FileManager::serverSidegeneratedFileUpload($path, $fileName, $output);
              
        EmailManager::sendEmailwithAttachment('udarad@domedia.lk', 'senderName', 'test.udara1@gmail.com', 'recipientName', 'subject', 'message', $file);
        ////////////////////////////////////////////////////
        
//        $dompdf->stream($name.$date.'.pdf',array('Attachment'=>0));


    }
    
    private static function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' ){
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);

        $interval = date_diff($datetime1, $datetime2);

        return $interval->format($differenceFormat);
    
    }

}
