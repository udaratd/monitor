<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LocationController extends Controller {
    
    public function Render() {
        switch ($this->getRequest()->getType()) {
            case 'GET':
                $this->getHandler();
                break;
            case 'POST':
                $this->postHandler();
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }
    
    public function getHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'all':
                $response = new Response();
                $result = LocationDAO::getParentChild();
                
                if($result) {
                    $response->setData($result);
                    echo $response->create(200, 'Success', true);
                } else {
                    //DAO listLocation Error
                    echo $response->create(500, 'No Data', false);
                }
                break;
            case 'search':
                $response = new Response();
                
                $name = $this->getRequest()->getData()['name'];
                
                if(isset($name)) {
                    $result = LocationDAO::searchLocation($name);
                    if($result) {
                        $response->setData($result);
                        echo $response->create(200, 'Success', true);
                    } else {
                        //DAO searchLocation function Error OR Not this Kind of data in System
                        echo $response->create(500, 'No Data', false);
                    }
                } else {
                    //(name) parameter Not set or Empty
                    echo $response->create(500, 'Missing Input fields', false);
                }
                break;
            case 'single':
                $response = new Response();
                
                $ID = $this->getRequest()->getData()['id'];
                $variables = array('id'=>$ID);
                
                if(Validation::variableValidation($variables) === true) {
                    if(LocationDAO::locationExists($ID) !== false) {
                        $result = LocationDAO::getLocation($ID);
                        if($result) {
                            $response->setData($result);
                            echo $response->create(200, 'Success', true);
                        } else {
                            //DAO getLocation function Error
                            echo $response->create(500, 'No Data', false);
                        }
                    } else {
                        //Location Not Existed in the System
                        echo $response->create(500, 'No Data', false);
                    }
                } else {
                    //(id) parameter Not set Or Empty
                    echo $response->create(500, 'Missing Input fields', false);
                }
                break;
            case 'child':
                $response = new Response();
                
                $ID = $this->getRequest()->getData()['id'];
                $variables = array('id'=>$ID);
                
                if(Validation::variableValidation($variables) === true) {
                    if(LocationDAO::locationExists($ID) !== false) {
                        $result = LocationDAO::getChild($ID);
                        if($result) {
                            $response->setData($result);
                            echo $response->create(200, 'Success', true);
                        } else {
                            echo $response->create(500, 'No child', false);
                        }
                    } else {
                        echo $response->create(500, 'No Parent Location', false);
                    }
                } else {
                    //(id) parameter Not set Or Empty
                    echo $response->create(500, 'Missing Input fields', false);
                }
                break;
            case 'checkpoints'://task and schedule filter
                $response = new Response();
                
                $ID = $this->getRequest()->getData()['id'];
                $variables = array('id'=>$ID);
                
                if(Validation::variableValidation($variables) === true) {
                    if(LocationDAO::locationExists($ID) !== false) {
                        $result = CheckpointDAO::getCheckpointsAccordingToLocation($ID);
                        if($result) {
                            $response->setData($result);
                            echo $response->create(200, 'Success', true);
                        } else {
                            echo $response->create(500, 'No Checkpoints', false);
                        }
                    } else {
                        echo $response->create(500, 'Location Not Existed', false);
                    }
                } else {
                    //(id) parameter Not set Or Empty
                    echo $response->create(500, 'Missing Input fields', false);
                }
                break;
            case 'export':
                LocationDAO::exportLocation();
                break;
             case 'sample':
                
                $link = S3_BUCKET_DOMAIN_SUPER.LOCATION_SAMPLE;
                
                header("Location: $link");
                die();
                
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }
    
    public function postHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'add':
                $response = new Response();
                
                $name = $this->getRequest()->getData()['name'];
                $description = $this->getRequest()->getData()['description'];
                $image = $this->getRequest()->getData()['image'];
                $parentID = $this->getRequest()->getData()['parentId'];
                
                if($image === 'null'){                   
                    echo $response->create(500, 'Invalid Image', false);
                    return false;
                }
                
                if(!empty($image)){
                    $imageValidate = ImageManager::checkImageSize($image);
                    
                    if($imageValidate < 1024 ){
                        echo $response->create(500, 'Invalid Image,Image resolution should at least 1024x768', false);
                        return false;
                    }
                }
                
                if(LocationDAO::checkLocation($name, $parentID) === false) {
                    
                    if(!empty($name)) {
                        
                        if(empty($parentID)){
                            $parentID = 0;
                        }
                        
                        if(is_numeric($parentID)) {
                        
                            $location = new Location($name,$parentID);
                            
                            if(!empty($description)){
                                $location->setDescription($description);
                            }
                            if(!empty($image)){
                                $location->setImage($image);
                            }
 
                            if(LocationDAO::addLocation($location)) {
                                
                                $activity = new LogActivity();
                                $activity->setActivity('Added Location');
                                $activity->setBy($_SESSION['ID']);
                                $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                                $activity->setUserType($_SESSION['role']);

                                $logResult = LogActivityDAO::addLog($activity);
                                
                                echo $response->create(200, 'Success', true);
                            } else {
                                //DAO addLocation function Error
                                echo $response->create(500, 'Unsuccess', false);
                            }
                        } else {
                            echo $response->create(500, 'You have to set numeric parentID', false);
                        }   
                    }else {
                        //parameters NOT SET OR EMPTY
                        echo $response->create(500, 'Missing Input fields', false);
                    }
                } else {
                    echo $response->create(500, 'Location exists', false);
                }
                break;
            case 'update':
                $response = new Response();
                
                $ID = $this->getRequest()->getData()['id'];
                $name = $this->getRequest()->getData()['name'];
                $description = $this->getRequest()->getData()['description'];
                $image = $this->getRequest()->getData()['image'];
                $parentID = $this->getRequest()->getData()['parentId'];                            
                
                if($image === 'null'){                   
                    echo $response->create(500, 'Invalid Image', false);
                    return false;
                }
                
                if(!empty($image)){
                    $imageValidate = ImageManager::checkImageSize($image);

                    if($imageValidate < 1024 ){
                        echo $response->create(500, 'Invalid Image,Image resolution should at least 1024x768', false);
                        return false;
                    }
                }
                
                if(empty($ID)){
                    echo $response->create(500, 'Please Set Location ID', false);
                    return false;
                }

                $locationData = LocationDAO::locationExists($ID);                
                if($locationData !== false) {

                    $locationName = $locationData['name'];
                    if($locationName === 'default-parent' || $locationName === 'default-child'){
                        echo $response->create(500, 'Not authorize to update Default location data', false);
                        return false;
                    }
                    
                    $location = new Location($ID,$name,$parentID,$description,$image);

                    if(LocationDAO::updateLocation($location)) {
                        
                        $activity = new LogActivity();
                        $activity->setActivity('Updated Location');
                        $activity->setBy($_SESSION['ID']);
                        $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                        $activity->setUserType($_SESSION['role']);

                        $logResult = LogActivityDAO::addLog($activity);
                        
                        echo $response->create(200, 'Success', true);
                    } else {
                        echo $response->create(500, 'Unsuccess', false);
                    }
                } else {
                    echo $response->create(500, 'Location not Exists', false);
                }               
                break;
            case 'delete':
                $respone = new Response();
                
                $ID = $this->getRequest()->getData()['id'];
                $confirm = $this->getRequest()->getData()['confirm'];

                $delete_confirm = false;

                if($confirm == 0){

                    $checkpoints = CheckpointDAO::getCheckpointsAccordingToLocation($ID);

                    if(is_array($checkpoints)){
                        if(sizeof($checkpoints)>0){
                            $delete_confirm = false;
                        } else {
                            $delete_confirm = true;
                        }
                    } else {
                        $delete_confirm = true;
                    }
                } elseif($confirm == 1) {
                    $delete_confirm = true;
                } else {
                    echo $respone->create(500, 'Missing input fields', false);
                }

                if($delete_confirm === true){

                    if(empty($ID)){
                        echo $respone->create(500, 'Please Set Location ID', false);
                        return false;
                    }
                    

                    if(LocationDAO::locationExists($ID) !== false) {   
                        if(LocationDAO::deleteLocation($ID)) {
                            
                            $activity = new LogActivity();
                            $activity->setActivity('Delete Location');
                            $activity->setBy($_SESSION['ID']);
                            $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                            $activity->setUserType($_SESSION['role']);

                            $logResult = LogActivityDAO::addLog($activity);
                            
                            echo $respone->create(200, 'Success', true);
                        } else {
                            echo $respone->create(500, 'DAO deleteLocation Function Error', false);
                        }
                    } else {
                        echo $respone->create(500, 'Location Not Exists', false);
                    }

                } else {
                    echo $respone->create(201, 'This will delete all checkpoints that allocated to this Location', true);
                }
                break;
            case 'import':
                $response = new Response();
                
                $error = array();
                $data = $this->getRequest()->getData();
                
                $file = $data['location'];                
                
                $fileExtensionData = explode(";", $file,2);

                $replaceData = explode(",", $file,2);

                $fileExtension = strtolower(substr($fileExtensionData[0], strpos($fileExtensionData[0], "/")+1));

//                if($fileExtension === 'vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
//                    $extension = '.xlsx';
//                }
                if ($fileExtension === 'vnd.ms-excel') {
                    $extension = '.csv';
                } else {
                    echo $response->create(500, 'Only support CSV file format', false);
                    return false;
                }
                
                $fileContent = substr($fileExtensionData[1], strpos($fileExtensionData[1], ",")+1);
                
                $fileName = 'location';                

                $path = UPLOAD_LOCATION_DIR;
                $filePath = FileManager::fileUpload($path, $fileName.$extension, $fileContent, $replaceData[0]);
                
                $locations = fopen($filePath, 'r');
                
                $i=0;
                while ($row = fgetcsv($locations)){
                    if($i == 0){
                        if($row[0] !== 'name' || $row[1] !== 'description' || $row[2] !== 'parentID'){
                            echo $response->create(500, 'Only support CSV file format', false);
                            return false;
                        }                    
                    } else {
                        $success = LocationController::importByCSV($row[0], $row[1], $row[2]);
                        if($success === true){
                            
                        } else {
                            $row[3] = $success;
                            array_push($error, $row);
                        }
                    }
                    $i++;
                }
                fclose($locations);
                
                if(!empty($error)){
                    $errorFile = LocationController::exportLocation($error);
                    $response->setData($errorFile);
//                    
                    //code 510 for error list
                    if(count($error) == $i-1){
                        echo $response->create(500, 'Carefully check the data of selected file, Eg:- Parent location ID', false);
                    } else {
                        echo $response->create(510, 'Unsuccess', false);
                    }                    
                } else {
                    echo $response->create(200, 'Success', true);
                }                
                break;
            default :
                echo Response::badRequest();
        }
    }

    public function authenticate() {
//        SessionManager::checkTimeoutSession();
        switch ($this->getRequest()->getAction()) {
            case 'all':
                if(SessionManager::is_admin() || SessionManager::is_supervisor() || SessionManager::is_staff()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'single':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'add':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'update':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'delete':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'child':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'checkpoints':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'export':
                return true;
            case 'import':
                return true;
            default :
                if(SessionManager::validateSession()) {
                    return true;
                } else {
                    return false;
                }
                break;
        }
    }
    
    private static function importByCSV($l_name,$l_description,$l_parentID){
        
                
        $name = $l_name;
        $description = $l_description;
        $parentID = $l_parentID;

        if(LocationDAO::checkLocation($name, $parentID) === false) {
            
            if(!empty($parentID)){
                $checkParent = LocationDAO::checkParentLocation($parentID);
                if(!$checkParent){
                    return 'Invalid Parent Location ID';
                }
            }
            
            if(!empty($name)) {

                if(empty($parentID)){
                    $parentID = 0;
                }

                if(is_numeric($parentID)) {

                    $location = new Location($name,$parentID);

                    if(!empty($description)){
                        $location->setDescription($description);
                    }

                    if(LocationDAO::addLocation($location)) {
//                        echo $response->create(200, 'Success', true);
                        return true;
                    } else {
                        //DAO addLocation function Error
//                        echo $response->create(500, 'Unsuccess', false);
                        return 'Unsuccess';
                    }
                } else {
//                    echo $response->create(500, 'You have to set numeric parentID', false);
                    return 'You have to set numeric parentID';
                }   
            }else {
                //parameters NOT SET OR EMPTY
//                echo $response->create(500, 'Missing Input fields', false);
                return 'Missing Input fields';
            }
                
        } else {
//            echo $response->create(500, 'Location Already exists', false);
            return 'Location Already exists';
        }
    }
    
    private static function exportLocation($result){
        try {
            $data = array();
            foreach ($result as $one){
                $errors['name'] = $one[0];
                $errors['description'] = $one[1];
                $errors['l_parentID'] = $one[2];
                $errors['fail_reason'] = $one[3];
                
                array_push($data, $errors);
            }
            return $data;
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
}