<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mobileController
 *
 * @author user
 */
class MobileController extends Controller{
    //put your code here
    public function Render() {
        switch ($this->getRequest()->getType()) {
            case 'GET':
                $this->getHandler();
                break;
            case 'POST':
                $this->postHandler();
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function getHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'schedulesForUser'://this is for mobile app interfaces
                $response = new Response();
                
                $user_id = $_SESSION['ID'];
                $date = date("Y-m-d");
                $s_date = $date;
                $e_date = $date;
                
                if(isset($user_id) && !empty($user_id)) {
                    $todaySchedulesForOneStaffmember = MobileDAO::checkCompleteWork($user_id, $s_date, $e_date);
                    if(is_array($todaySchedulesForOneStaffmember)) {
                        $response->setData($todaySchedulesForOneStaffmember);
                        echo $response->create(200, 'Success', true);
                    } elseif(is_null($todaySchedulesForOneStaffmember)) {
                        echo $response->create(500, 'You dont have schedules for today', false);
                    } else {
                        //DAO listScheduleForUser function error
                        echo $response->create(500, 'Unsuccess', false);
                    }
                } else {
                    //(userID) parameter not set or empty
                    echo $response->create(500, 'Missing Input fielda', false);
                }
                break;
            case 'rework':
                $response = new Response();
                
                $staffId = $_SESSION['ID'];
              
                $reworks = WorkDAO::getTodayRework($staffId);
                
                if(is_array($reworks)){
                    $response->setData($reworks);
                    echo $response->create(200, 'Success', true);
//                    if(array_key_exists("data", $reworks)){
//                        
//                    } else {
//                        echo $response->create(200, 'Rework not found', true);
//                    }
                } else {
                    echo $response->create(500, 'Rework not found', false);
                }
                
                break;
            case 'notification':
                $response = new Response();
                
                $staffId = $_SESSION['ID'];
              
                $reworksAndFlagged = WorkDAO::getTodayReworkAndFlag($staffId);
                
                if(is_array($reworksAndFlagged)){
                    $response->setData($reworksAndFlagged);
                    echo $response->create(200, 'Success', true);
//                    if(array_key_exists("data", $reworksAndFlagged)){
//                        
//                    } else {
//                        echo $response->create(200, 'Rework or Flagged not found', true);
//                    }
                } else {
                    echo $response->create(500, 'Rework or Flagged not found', false);
                }
                break;
            case 'checkCompany':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                if(empty($data['username'])){
                    echo $response->create(500, 'Username is required', false);
                    return false;
                }
                $username = $data['username'];
                $result = AdminDAO::checkCompany($username);
                if($result){
                    $response->setData($result);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'Incorrect Username', false);
                }
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }
    
    public function postHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'add':
                $response = new Response();

                $data = $this->getRequest()->getData();
                
                $device_info = false;
                if(!empty($data['device_info'])){
                    $device_info = $data['device_info'];
                }
                
                date_default_timezone_set('Asia/Colombo');//set default time zone to sri lanka
                $currentDate = date('Y-m-d');
                $currentTime = date('H:i:s');

                $keys = array("checkpointID","deviceDate","deviceTime","userID","status","scheduleID","image");
                    
                for($i=0;$i<count($keys);$i++){
                    if(!array_key_exists($keys[$i], $data)){
                        echo $response->create(500, 'Input Data fields not match', false);
                        return false;
                    }
                }
                
                $ignore = array('stf_comment');
                
                $variableValidation = Validation::variableValidation($data,$ignore);
                if(!is_array($variableValidation)) {
                    
                    $work = new Work($data);
                    $work->setServerDate($currentDate);
                    $work->setServerTime($currentTime);
                    
                    if(!empty($work->getImage())){
                        $imageValidate = ImageManager::checkImageSize($work->getImage());

                        if($imageValidate < 1024 ){
                            echo $response->create(500, 'Invalid Image,Image resolution should at least 1024x768', false);
                            return false;
                        }
                    }
                    
                    if(empty($work->getRework())){
                        $work->setRework(0);
                    }
                    
                    $workDuplicateValidation = WorkDAO::workDuplicateValidation($work);
                    if($workDuplicateValidation){
                        echo $response->create(500, 'Already Submit This Work', false);
                        return false;
                    }

                    $workID = WorkDAO::addWorkList($work);
                    if($workID){
                        
                        if($device_info){
                            MobileDAO::addDeviceInfo($workID, $device_info);
                        }
                        
                        $scheduleID = null;
                        $singleCheckpointData = CheckpointDAO::getCheckpoint($work->getCheckpointID(),$scheduleID);
                        if($singleCheckpointData){
                            if(empty($singleCheckpointData['img_original'])){
                                $checkpointObj = new Checkpoint();
                                $checkpointObj->setId($work->getCheckpointID());
                                $checkpointObj->setChekImage($work->getImage());
                                
                                CheckpointDAO::imageUpload($checkpointObj);
                            }
                        }
                        
                        $activity = new LogActivity();
                        $activity->setActivity('Submitted Work');
                        $activity->setBy($_SESSION['ID']);
                        $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                        $activity->setUserType($_SESSION['role']);

                        $logResult = LogActivityDAO::addLog($activity);
                        
                        echo $response->create(200, 'Success', true);
                    } else {
                        //check DAO addWorkList function
                        echo $response->create(500, 'Unsuccess', false);
                    }
                } else {
                    $response->setData($variableValidation);
                    echo $response->create(500, 'These fields are required', false);
                }
                break;
            case 'QR':
                $response = new Response();
                
                $QR = true;
                
                date_default_timezone_set('Asia/Colombo');//set default time zone to sri lanka
                $currentDate = date('Y-m-d');
                $currentTime = date('H:i:s');
                
                $userID = $_SESSION['ID']; //$this->getRequest()->getData()['userID'];
                $checkpointID = $this->getRequest()->getData()['checkpointID'];
                $repeatTypeID = 3;
                $s_time = $this->getRequest()->getData()['s_time'];
                $e_time = $this->getRequest()->getData()['e_time'];
                $oneTimeDate = $currentDate;
                $deviceDate = $this->getRequest()->getData()['deviceDate'];
                $deviceTime = $this->getRequest()->getData()['deviceTime'];
                $image = $this->getRequest()->getData()['image'];
                $stf_comment = $this->getRequest()->getData()['stf_comment'];
                $workStatus = "Pending";
                
                $device_info = false;
                if(!empty($this->getRequest()->getData()['device_info'])){
                    $device_info = $this->getRequest()->getData()['device_info'];
                }
                
                if(!empty($image)){
                    $imageValidate = ImageManager::checkImageSize($image);

                    if($imageValidate < 1024 ){
                        echo $response->create(500, 'Invalid Image,Image resolution should at least 1024x768', false);
                        return false;
                    }
                }
                
                if(!empty($userID) && !empty($checkpointID) && !empty($s_time) && !empty($e_time) && !empty($deviceDate) && !empty($deviceTime) && !empty($image)) {
                    $schedule = new Schedule();
                    $schedule->setUser_id($userID);
                    $schedule->setCheckpoint_id($checkpointID);
                    $schedule->setRepeatType_id($repeatTypeID);
                    $schedule->setStartTimebyString($s_time);
                    
                    $convertTime = strtotime($e_time);
                    $futureDate = $convertTime+(60*5);
                    
                    $schedule->setEndTimebyString(date('H:i:s', $futureDate));
                    $schedule->setOneTimeDate($oneTimeDate);
                    
                    $scheduleID = ScheduleDAO::addOneDaySchedule($schedule,$QR);
                    
                    if(is_string($scheduleID)) {
                        $work = new Work();
                        $work->setCheckpointID($checkpointID);
                        $work->setDeviceDate($deviceDate);
                        $work->setDeviceTime($deviceTime);
                        $work->setServerDate($currentDate);
                        $work->setServerTime($currentTime);
                        $work->setUserID($userID);
                        $work->setStatus($workStatus);
                        $work->setScheduleID($scheduleID);
                        $work->setImage($image);
                        $work->setStf_comment($stf_comment);
                        
                        $workID = WorkDAO::addWorkList($work);                       
                        if($workID){
                            
                            if($device_info){
                                MobileDAO::addDeviceInfo($workID, $device_info);
                            }
                            
                            $scheduleID = null;
                            $singleCheckpointData = CheckpointDAO::getCheckpoint($work->getCheckpointID(),$scheduleID);
                            if($singleCheckpointData){
                                if(empty($singleCheckpointData['img_original'])){
                                    $checkpointObj = new Checkpoint();
                                    $checkpointObj->setId($work->getCheckpointID());
                                    $checkpointObj->setChekImage($work->getImage());

                                    CheckpointDAO::imageUpload($checkpointObj);
                                }
                            }
                            
                            $activity = new LogActivity();
                            $activity->setActivity('Submitted QR Work');
                            $activity->setBy($_SESSION['ID']);
                            $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                            $activity->setUserType($_SESSION['role']);

                            $logResult = LogActivityDAO::addLog($activity);
                            
                            echo $response->create(200, 'Success', true);
                        } else {
                            if(!empty($scheduleID)){
                                if(!ScheduleDAO::deleteSchedule($scheduleID)) {
                                    System::log(new Log('Please Delete this schedule '.$scheduleID, LOG_CRITICAL));
                                }
                            }
                            //check DAO addWorkList function
                            echo $response->create(500, 'Unsuccess', false);
                        }
                        
                    } else {
                        System::log(new Log('Can not create Schedule for QR work', LOG_CRITICAL));
                        echo $response->create(500, 'Can not create Schedule for QR work', false);
                        
                    }
                } else {
                    echo $response->create(500, '(userID,checkpointID,s_time,e_time,deviceDate,deviceTime,image) parameters Can not be Empty', false);
                }
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function authenticate() {
        switch ($this->getRequest()->getAction()) {
            case 'add':
                if(SessionManager::is_staff()) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'QR':
                if(SessionManager::is_staff()) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'schedulesForUser':
                if(SessionManager::is_staff()) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'rework':
                if(SessionManager::is_staff()) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'notification':
                if(SessionManager::is_staff()) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'checkCompany':
                return true;
            default :
                if(SessionManager::validateSession()) {
                    return true;
                } else {
                    return false;
                }
                break;
        }
    }
}
