<?php

class ShellController extends Controller{
    
    public function Render() {
        switch ($this->getRequest()->getType()) {
            case 'GET':
                $this->getHandler();
                break;
            case 'POST':
                $this->postHandler();
                break;
            case 'CLI':
                $this->cliHandler();
                break;
            case 'CGI_FCGI':
                $this->cgi_fcgiHandler();
                break;
            default:
                echo Response::badRequest();
                break;
        }
    }

    public function getHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'executive':
                $response = new Response();
                
                $empty = array();
                
                $completedTask = ShellController::completedTask();
                if($completedTask){
                    $output['completedTask'] = $completedTask;
                }
                
                $ontime = ShellController::ontime();
                if($ontime){
                    $output['ontime'] = $ontime;
                }
                
                $reworkRate = ShellController::reworkRate();
                if($reworkRate){
                    $output['reworkRate'] = $reworkRate;
                }
                
                $flaggedRate = ShellController::flaggedRate();
                if($flaggedRate){
                    $output['flaggedRate'] = $flaggedRate;
                }
                
                $reworkStaff = ShellController::reworkStaff();
                if($reworkStaff){
                    $output['reworkStaff'] = $reworkStaff;
                } else {
                    $output['reworkStaff'] = $empty;
                }
                
                $reworkCheckpoint = ShellController::reworkCheckpoint();
                if($reworkCheckpoint){
                    $output['reworkCheckpoint'] = $reworkCheckpoint;
                } else {
                    $output['reworkCheckpoint'] = $empty;
                }
                
                $lateSubmitedCheckpoint = ShellController::lateSubmitedCheckpoint();
                if($lateSubmitedCheckpoint){
                    $output['lateSubmitedCheckpoint'] = $lateSubmitedCheckpoint;
                } else {
                    $output['lateSubmitedCheckpoint'] = $empty;
                }
                
                $moreThanOneRework = ShellController::moreThanOneRework();
                if($moreThanOneRework){
                    $output['moreThanOneRework'] = $moreThanOneRework;
                } else {
                    $output['moreThanOneRework'] = $empty;
                }

                if($output){
                                            
                    ob_start();
                    $pdfExecutive = new PDFExecutive();
                    $pdfExecutive->printExecutive($output);
                    $html = ob_get_clean();

                    $name = 'Executive';
                    ShellController::generatePrint($html,$name);
                    
                } else {
                    echo $response->create(500, 'No Data', false);
                }
                break;
            default :
                $log=new Log("sorry");
                System::log($log);
                break;
        }
    }

    public function postHandler() {
        
    }

    public function cliHandler() {
        
    }
    
    public function cgi_fcgiHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'executive':
                $response = new Response();
                
                $empty = array();
                
                $completedTask = ShellController::completedTask();
                if($completedTask){
                    $output['completedTask'] = $completedTask;
                }
                
                $ontime = ShellController::ontime();
                if($ontime){
                    $output['ontime'] = $ontime;
                }
                
                $reworkRate = ShellController::reworkRate();
                if($reworkRate){
                    $output['reworkRate'] = $reworkRate;
                }
                
                $flaggedRate = ShellController::flaggedRate();
                if($flaggedRate){
                    $output['flaggedRate'] = $flaggedRate;
                }
                
                $reworkStaff = ShellController::reworkStaff();
                if($reworkStaff){
                    $output['reworkStaff'] = $reworkStaff;
                } else {
                    $output['reworkStaff'] = $empty;
                }
                
                $reworkCheckpoint = ShellController::reworkCheckpoint();
                if($reworkCheckpoint){
                    $output['reworkCheckpoint'] = $reworkCheckpoint;
                } else {
                    $output['reworkCheckpoint'] = $empty;
                }
                
                $lateSubmitedCheckpoint = ShellController::lateSubmitedCheckpoint();
                if($lateSubmitedCheckpoint){
                    $output['lateSubmitedCheckpoint'] = $lateSubmitedCheckpoint;
                } else {
                    $output['lateSubmitedCheckpoint'] = $empty;
                }
                
                $moreThanOneRework = ShellController::moreThanOneRework();
                if($moreThanOneRework){
                    $output['moreThanOneRework'] = $moreThanOneRework;
                } else {
                    $output['moreThanOneRework'] = $empty;
                }

                if($output){
       
                    ob_start();
                    $pdfExecutive = new PDFExecutive();
                    $pdfExecutive->printExecutive($output);
                    $html = ob_get_clean();

                    $name = 'Executive';
                    ShellController::generatePrint($html,$name);
                    
                } else {
                    echo $response->create(500, 'No Data', false);
                }
                break;
            default :
                $log=new Log("No Data");
                System::log($log);
                break;
        }
    }

    public function authenticate() {
        return true;
    }

    private static function completedTask(){
        
        $output = array();
        
        $data = array();
        
        $date = date('Y-m-d');
                    
        $s_date_c = date('Y-m-01', strtotime($date));
        $e_date_c = date('Y-m-t', strtotime($date));

        $s_date_p = date('Y-m-01', strtotime('last month'));
        $e_date_p = date('Y-m-t', strtotime('last month'));

        //get this month data
        $data["s_date"] = $s_date_c;
        $data["e_date"] = $e_date_c;

        $report = new Report($data);
        $result_1 = ReportDAO::getCompletedTask($report);

        //get last month data
        $data["s_date"] = $s_date_p;
        $data["e_date"] = $e_date_p;

        $report = new Report($data);
        $result_2 = ReportDAO::getCompletedTask($report);

        $new_completeTask = $result_1['completedTaskCount'];
        $original_completeTask = $result_2['completedTaskCount'];


        if($new_completeTask>0){
            $output['completedTaskCount'] = $new_completeTask;
            if ($original_completeTask>0) {
                $percentage = (($original_completeTask - $new_completeTask)/$original_completeTask)*100;
                $output['percentage'] = $percentage;
                if($percentage<0){
                    $output['status'] = "Positive";
                } elseif ($percentage==0) {
                    $output['status'] = "neutral";
                } else {
                    $output['status'] = "Negative";
                }
            }
            return $output;
        }else{
            $output['completedTaskCount'] = 0;
            $output['percentage'] = 0;
            $output['status'] = "neutral";
            return $output;
        }
    }

    private static function ontime(){
        
        $output = array();
        
        $data = array();
        
        $date = date('Y-m-d');
                    
        $s_date_c = date('Y-m-01', strtotime($date));
        $e_date_c = date('Y-m-t', strtotime($date));

        $s_date_p = date('Y-m-01', strtotime('last month'));
        $e_date_p = date('Y-m-t', strtotime('last month'));


        //get this month data
        $data["s_date"] = $s_date_c;
        $data["e_date"] = $e_date_c;

        $report = new Report($data);
        $result_1 = ReportDAO::getOnTimeTask($report);

        //get last month data
        $data["s_date"] = $s_date_p;
        $data["e_date"] = $e_date_p;

        $report = new Report($data);
        $result_2 = ReportDAO::getOnTimeTask($report);

        $new_onTimeTask = $result_1['onTimeTasks'];
        $total_completedTask = $result_1['completedTaskCount'];
        $original_onTimeTask = $result_2['onTimeTasks'];

        if($new_onTimeTask>0){
            $onTimePercentage = ($new_onTimeTask/$total_completedTask)*100;
            $output['onTimeTasksPercentage'] = $onTimePercentage;
            if ($original_onTimeTask>0) {
                $percentage = (($original_onTimeTask - $new_onTimeTask)/$original_onTimeTask)*100;
                $output['percentage'] = $percentage;
                if($percentage<0){
                    $output['status'] = "Positive";
                } elseif ($percentage==0) {
                    $output['status'] = "neutral";
                } else {
                    $output['status'] = "Negative";
                }
            }
            return $output;
        }else{
            $output['onTimeTasksPercentage'] = 0;
            $output['percentage'] = 0;
            $output['status'] = "neutral";
            return $output;
        }
    }
    
    private static function reworkRate(){
        
        $output = array();
        
        $data = array();
        
        $date = date('Y-m-d');
                    
        $s_date_c = date('Y-m-01', strtotime($date));
        $e_date_c = date('Y-m-t', strtotime($date));

        $s_date_p = date('Y-m-01', strtotime('last month'));
        $e_date_p = date('Y-m-t', strtotime('last month'));

        //get this month data
        $data["s_date"] = $s_date_c;
        $data["e_date"] = $e_date_c;

        $report = new Report($data);
        $result_1 = ReportDAO::getReworkRate($report);

        //get last month data
        $data["s_date"] = $s_date_p;
        $data["e_date"] = $e_date_p;

        $report = new Report($data);
        $result_2 = ReportDAO::getReworkRate($report);

        $new_reworkTask = $result_1['reworkTask'];
        $total_completedTask = $result_1['completedTaskCount'];
        $original_reworkTask = $result_2['reworkTask'];

        if($new_reworkTask>0){
            $reworkPercentage = ($new_reworkTask/$total_completedTask)*100;
            $output['reworkPercentage'] = $reworkPercentage;
            if ($original_reworkTask>0) {
                $percentage = (($original_reworkTask - $new_reworkTask)/$original_reworkTask)*100;
                $output['percentage'] = $percentage;
                if($percentage<0){
                    $output['status'] = "Positive";
                } elseif ($percentage==0) {
                    $output['status'] = "neutral";
                } else {
                    $output['status'] = "Negative";
                }
            }
            return $output;
        }else{
            $output['reworkPercentage'] = 0;
            $output['percentage'] = 0;
            $output['status'] = "neutral";
            return $output;
        }
    }
    
    private static function flaggedRate(){
        
        $output = array();
        
        $data = array();
        
        $date = date('Y-m-d');
                    
        $s_date_c = date('Y-m-01', strtotime($date));
        $e_date_c = date('Y-m-t', strtotime($date));

        $s_date_p = date('Y-m-01', strtotime('last month'));
        $e_date_p = date('Y-m-t', strtotime('last month'));

        //get this month data
        $data["s_date"] = $s_date_c;
        $data["e_date"] = $e_date_c;

        $report = new Report($data);
        $result_1 = ReportDAO::getFlaggedRate($report);

        //get last month data
        $data["s_date"] = $s_date_p;
        $data["e_date"] = $e_date_p;

        $report = new Report($data);
        $result_2 = ReportDAO::getFlaggedRate($report);

        $new_flaggedTask = $result_1['flaggedTask'];
        $total_completedTask = $result_1['completedTaskCount'];
        $original_flaggedTask = $result_2['flaggedTask'];

        if($new_flaggedTask>0){
            $flaggedPercentage = ($new_flaggedTask/$total_completedTask)*100;
            $output['flaggedPercentage'] = $flaggedPercentage;
            if ($original_flaggedTask>0) {
                $percentage = (($original_flaggedTask - $new_flaggedTask)/$original_flaggedTask)*100;
                $output['percentage'] = $percentage;
                if($percentage<0){
                    $output['status'] = "Positive";
                } elseif ($percentage==0) {
                    $output['status'] = "neutral";
                } else {
                    $output['status'] = "Negative";
                }
            }
            return $output;
        }else{
            $output['flaggedPercentage'] = 0;
            $output['percentage'] = 0;
            $output['status'] = "neutral";
            return $output;
        }
    }
    
    private static function reworkStaff(){
                
        $data = array();
        
        $date = date('Y-m-d');
                    
        $s_date_c = date('Y-m-01', strtotime($date));
        $e_date_c = date('Y-m-t', strtotime($date));

        $data["s_date"] = $s_date_c;
        $data["e_date"] = $e_date_c;

        $report = new Report($data);
        $result = ReportDAO::getReworkStaff($report);

        if($result){
            return $result;
        } else {
            return false;
        }
    }
    
    private static function reworkCheckpoint(){
                
        $data = array();
        
        $date = date('Y-m-d');
                    
        $s_date_c = date('Y-m-01', strtotime($date));
        $e_date_c = date('Y-m-t', strtotime($date));

        $data["s_date"] = $s_date_c;
        $data["e_date"] = $e_date_c;

        $report = new Report($data);
        $result = ReportDAO::getReworkCheckpoint($report);
        
        if($result){
            return $result;
        } else {
            return false;
        }
    }
    
    private static function lateSubmitedCheckpoint(){

        $data = array();
        
        $date = date('Y-m-d');
                    
        $s_date_c = date('Y-m-01', strtotime($date));
        $e_date_c = date('Y-m-t', strtotime($date));

        $data["s_date"] = $s_date_c;
        $data["e_date"] = $e_date_c;

        $report = new Report($data);
        $result = ReportDAO::getLateSubmitedCheckpoint($report);
        
        if($result){
            return $result;
        } else {
            return false;
        }
    }
    
    private static function moreThanOneRework(){
        
        $data = array();
        
        $date = date('Y-m-d');
                    
        $s_date_c = date('Y-m-01', strtotime($date));
        $e_date_c = date('Y-m-t', strtotime($date));

        $data["s_date"] = $s_date_c;
        $data["e_date"] = $e_date_c;

        $report = new Report($data);
        $result = ReportDAO::get_Checkpoint_With_MoreThan_OneRework($report);
        
        if($result){
            return $result;
        } else {
            return false;
        }
    }
    
    public static function generatePrint($html,$name){
        
        date_default_timezone_set('Asia/Colombo');
        $today = date("Y-m-d");
        $currentTime = date("H:i:s");
        $currentTime = str_replace(':', '-', $currentTime);
        $date = $today.'_'.$currentTime;
        
        $dompdf = new Dompdf\Dompdf();
//        $dompdf->set_option('defaultFont', 'Courier');
        $dompdf->set_option('isHtml5ParserEnabled', true);
        $dompdf->set_option('isRemoteEnabled', true);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        
        ////////////////////for send email//////////////////
        $output = $dompdf->output();
        $path = UPLOAD_LOCATION_DIR;
        $fileName = $name.$date.'.pdf';        
        $file = FileManager::serverSidegeneratedFileUpload($path, $fileName, $output);
        echo $file;      
//        ShellController::sendReport($file);
        ////////////////////////////////////////////////////
        
//        $dompdf->stream($name.$date.'.pdf',array('Attachment'=>0));


    }
    
    private static function sendReport($file){
        $details = CompanyDAO::getCompanyID();
        $ID = $details[0]['ID'];

        $company = new Company();
        $company->setId($ID);

        $businessDetails = CompanyDAO::getCompany($company->getId());
        
        if($businessDetails){
            
            $title = 'Hello '.$businessDetails['companyName'].',';
            $subject = 'Executive Report for Last Month';
            $email = $businessDetails['email'];
            
            ob_start();
            $emailTemplate = new EmailTemplates();
            $emailTemplate->header($title, $businessDetails);
//            $emailTemplate->addNewStaff($message, $businessDetails['companyName']);
            $emailTemplate->footer($businessDetails);
            $html = ob_get_clean();
            
            EmailManager::sendEmailwithAttachment($email, $businessDetails['companyName'], $email, 'Admin', $subject, $html, $file);
        }
    }
}

