<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LogactivityController extends Controller {
    
    public function Render() {
        switch ($this->getRequest()->getType()) {
            case 'GET':
                $this->getHandler();
                break;
            case 'POST':
                $this->postHandler();
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function getHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'list':
                $response = new Response();

                $s_date = $this->getRequest()->getData()['s_date'];
                $e_date = $this->getRequest()->getData()['e_date'];
                $pageNumber = intval($this->getRequest()->getData()['page_number']);
                $e_limit = $this->getRequest()->getData()['e_limit'];
                $chk_limit = $this->getRequest()->getData()['limit'];
                
                $activity = new LogActivity();
                $activity->setBusiness_id($_SESSION['COMPANY_ID']);

                if(empty($s_date) || empty($e_date)){
                    $timeZone = 'Asia/Colombo';
                    $today = date('Y-m-d');
                    $dateObj = new DateTime($today);
                    $dateObj->setTimezone(new DateTimeZone($timeZone));
                    $current = $dateObj->format('Y-m-d');
                    
                    $selectedWeek = DateAndTimeManager::getDateRange($current);        
                    $s_date = $selectedWeek[0];
                    $e_date = $selectedWeek[6];
                }

                if(empty($e_limit)){
                    $e_limit = 5;
                } else {
                    $e_limit = intval($e_limit);
                }

                if(empty($pageNumber)){
                    $pageNumber = 1;
                }

                $s_limit = 0;
                for($i=1;$i<$pageNumber;$i++){
                    $s_limit = $s_limit+$e_limit;
                }

                if( (($s_limit === 0) || (!empty($s_limit) && is_numeric($s_limit))) && !empty($e_limit) && is_numeric($e_limit)){
                    $s_limit = intval($s_limit);
                    $e_limit = intval($e_limit);
                }
                
                $limit = false;
                if(strtolower($chk_limit) === 'all'){
                    $limit = true;
                } else {
                    $limit = false;
                }
                
                $countSearch = LogActivityDAO::listLogCount($activity, $s_date, $e_date, $limit);

                if($countSearch){
                    $count = $countSearch['count'];
                } else {
                    echo $response->create(500, 'No data', false);
                    return false;
                }

                $numberOfPages = ceil($count/$e_limit);
                
                if(empty($numberOfPages)){
                    $numberOfPages = 1;
                }
                
                $meta['numberOfPages'] = $numberOfPages;
                $meta['pageNumber'] = $pageNumber;

                $result = LogActivityDAO::listLog($activity, $s_date, $e_date, $s_limit, $e_limit, $limit);
                if($result){
                    
                    $output['activitylist'] = $result;
                    $output['meta'] = $meta;
                    
                    $response->setData($output);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No Data', false);
                }
                break;
            default :
                break;
        }
    }

    public function postHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'add':
                break;
            default :
                break;
        }
    }

    public function authenticate() {
        switch ($this->getRequest()->getAction()) {
            case 'list':
                if(SessionManager::is_admin()) {
                    return true;
                } else {
                    return false;
                }
                break;
            default :
                return false;
        }
    }

}
