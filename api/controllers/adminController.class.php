<?php

class AdminController extends Controller{
    
    public function Render() {
        switch ($this->getRequest()->getType()) {
            case 'GET':
                $this->getHandler();
                break;
            case 'POST':
                $this->postHandler();
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function getHandler() {
        switch ($this->getRequest()->getAction()){
            case 'all':
                $response = new Response();
                
                $companyList = AdminDAO::listCompany();
                
                if($companyList){
                    $response->setData($companyList);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No data', false);
                }
                break;
            case 'single':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                $admin = new Admin($data);
                if(empty($admin->getId())){
                    echo $response->create(500, 'Company ID is required', false);
                    return false;
                }
                $company = AdminDAO::single($admin);
                
                if($company){
                    $response->setData($company);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No data', false);
                }
                break;
            case 'showDatabases':
                $response = new Response();
                
                $databases = AdminDAO::showDatabases();
                if($databases){
                    $response->setData($databases);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No data', false);
                }
                
                break;
            case 'logout':
                $response = new Response();
                
                SessionManager::loguot();
                if(count($_SESSION) == 0){
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'Unsuccess', false);
                }
                break;
            case 'validateSession':
                $response = new Response();
                
                if(SessionManager::validateSession()){
                    
                    if($_SESSION['role'] == 100){
                        $response->setData($_SESSION);
                        echo $response->create(200, 'Success', true);
                    } else {
                        SessionManager::loguot();
                        echo $response->create(500, 'Unsuccess', false);
                    }
                } else {
                    echo $response->create(500, 'Unsuccess', false);
                }
                break;
            case 'checkLink':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                if(empty($data['id'])){
                    echo $response->create(500, 'ID is required', false);
                    return false;
                }
                if(empty($data['token'])){
                    echo $response->create(500, 'Token is required', false);
                    return false;
                }
                
                $ID = $data['id'];
                $token = $data['token'];
                
                $resetData = AdminDAO::getResetData($ID);

                if($resetData){
                    $secret = hash('sha256', $resetData['secret']);

                    if($token === $secret){
                        $reset_d_t = date_create($resetData['d_t']);
                        $current_d_t = date_create(date('Y-m-d H:i:s'));
                        
                        if($current_d_t < $reset_d_t){
                            echo $response->create(500, 'Invalid Link', false);
                            return false;
                        }
                        $dateTimeDiff = date_diff($current_d_t, $reset_d_t, false);

                        $hours = $dateTimeDiff->format('%h');
                        
                        if(intval($hours) >= 1){
                            echo $response->create(500, 'Link expired', false);
                        } else {
                            echo $response->create(200, 'Success', true);
                        }
                    } else {
                        echo $response->create(500, 'Invalid Token', false);
                        return false;
                    }
                } else {
                    echo $response->create(500, 'Unsuccess', false);
                }
                break;
            case 'checkUser':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                if(empty($data['username'])){
                    echo $response->create(500, 'Company username is required', false);
                    return false;
                }
                
                $username = $data['username'];
                $company = AdminDAO::checkCompany($username);
                $database = $company['dbname'];
                
                $result = AdminDAO::checkNewlyAddedUserForNewlyRegisteredCompany($database);
                if($result){
                    $response->setData($result);
                    echo $response->create(200, 'Success', false);
                } else {
                    echo $response->create(500, 'No User Found', false);
                }
                break;
            case 'company':
                $response = new Response();
                
                session_unset();// Before Login to the System, Unset All session variables
                
                $data = $this->getRequest()->getData();
                if(empty($data['username'])){
                    echo $response->create(500, 'Company username is required', false);
                    return false;
                }
                $dbname = COMPANY_DB_NAME;
                $username = $data['username'];
                $company = AdminDAO::checkCompany($username,$dbname);
                
                if($company){
                    $response->setData($company);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No data', false);
                }
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function postHandler() {
        switch ($this->getRequest()->getAction()){
            case 'add':
                $response = new Response();
                try{
                    $message = array();

                    $data = $this->getRequest()->getData();
                    $admin = new Admin($data);

                    $today = date('Y-m-d');
                    $expiry_date = strtotime('+30 days',$today);
                    $admin->setExpiryDate($expiry_date);
                    $admin->setCheckFirstUser(0);
                    
                    if(empty($admin->getCom_name())){
                        echo $response->create(500, 'Company Name is Required', false);
                        return false;
                    }
                    if(empty($admin->getUsername())){
                        echo $response->create(500, 'Username is Required', false);
                        return false;
                    }
                    if(empty($admin->getEmail())){
                        echo $response->create(500, 'Email is Required', false);
                        return false;
                    }
                    if(!empty($admin->getPhone())){
                        if(!Validation::phoneValidation($admin->getPhone())){
                            echo $response->create(500, 'Valid phone is Required', false);
                            return false;
                        }
                    }
                    if($admin->getSkip_validation() || $admin->getSkip_validation() === 'true'){
                        $admin->setSkip_validation(1);
                    } else {
                        $admin->setSkip_validation(2);
                    }
                    if(!empty($admin->getLogo())){

                        if(!Validation::validateBase64String($admin->getLogo())){
                            echo $response->create(500, 'Valid image is required', false);
                            return false;
                        }

                        $imageValidate = ImageManager::checkImageSize($admin->getLogo());

                        if($imageValidate > 1000 ){
                            echo $response->create(500, 'Image too large. Maximum resolution 500x200 px', false);
                            return false;
                        }
                    }
                    if(empty($admin->getExpiryDate())){
                        echo $response->create(500, 'Please set next payment date', false);
                        return false;
                    }


    //                $name = Validation::removeSpecialCharactersFromString(str_replace('-', '', $admin->getCom_name()));
    //                $unique = strtolower($name).'_1';
    //                $uniqueID = AdminController::chechUniqueID($unique);                
    //                $admin->setDbName('moni_'.str_replace('-', '_', $uniqueID));
    //                $admin->setToken($uniqueID);

                    if (!Validation::checkStringForLettersAndNumbers($admin->getUsername())) {
                        echo $response->create(500, 'Can contain english letters and numbers', false);
                        return false;
                    }

                    $checkUsername = AdminDAO::checkCompany($admin->getUsername());
                    if($checkUsername){
                        echo $response->create(500, 'Already use this username', false);
                        return false;
                    }

                    if(!Validation::emailValidation($admin->getEmail())){
                        echo $response->create(500, 'Valid email is required', false);
                        return false;
                    }

                    $checkEmail = AdminDAO::checkEmail($admin);
                    if($checkEmail){
                        echo $response->create(500, 'Already use this email', false);
                        return false;
                    }

                    $admin->setDbName(strtolower($admin->getUsername()));
                    $admin->setBucketName(strtolower($admin->getUsername()));

//                    $teatArray = array('devmoni','devmoni1','devmoni2','devmoni3','devmoni4');
//                    $bucketSuccess = false;
//                    $bCount = 1;
//                    $newBucketName = $admin->getBucketName();
//                    do{
//                        $bucketSuccess = in_array($newBucketName, $teatArray);
//                        if($bucketSuccess){
//                            echo $newBucketName;
//                            echo "\r\n";
//                            $newBucketName = $admin->getBucketName().$bCount;
//                            
//                            $bCount++;
//                        } else {
//                            echo $newBucketName;
//                            return;
//                        }
//                    }while ($bucketSuccess);
                    
                    
                    if(!empty($admin->getBucketName())){

                        $allBucket = FileManager::listAllBuckets();

                        if($allBucket){
                            if(in_array($admin->getBucketName(), $allBucket)){
                                echo $response->create(500, 'Already use this username', false);
                                return false;
                            }
                        }
                        
                        
                        
                        $bucketSuccess = false;
                        $bCount = 1;
                        $newBucketName = $admin->getBucketName();
                        do{
                            $bucketSuccess = FileManager::createS3bucket($newBucketName);
                            if(!$bucketSuccess){
                                $newBucketName = $admin->getBucketName().$bCount;
                                $bCount++;
                            } else {
                                $admin->setBucketName($newBucketName);
                            }
                            
                        }while (!$bucketSuccess);


                        if($bucketSuccess){

                            array_push($message, 'Successfuly created s3 bucket');

                            $folderNames = array(UPLOAD_USER_DIR,UPLOAD_CHECK_DIR,UPLOAD_LOCATION_DIR,UPLOAD_WORK_DIR,UPLOAD_CHECK_QR_DIR,UPLOAD_CUSTOMER_FEEDBACK_DIR);

                            $folderSuccess = FileManager::createFolder_AWS_S3($admin->getBucketName(),$folderNames);

                            if($folderSuccess){
                                array_push($message, 'Successfuly created folder structure');
                            } else {

                                $listObjects = FileManager::list_1000_Objects($admin->getBucketName());
                                if($listObjects){

                                    $deleteObject = FileManager::deleteMultipleObjects($admin->getBucketName(), $listObjects);                    
                                    if($deleteObject){

                                        $deleteBucket = FileManager::deleteS3bucket($admin->getBucketName());                        
                                        if(!$deleteBucket){
                                            System::log(new Log('Check Folder structure is CORRECT in '.$admin->getBucketName().' Bucket, If it is not, Delete Bucket ', LOG_CRITICAL));
                                        }
                                    }
                                }

                                echo $response->create(500, 'Something went wrong, Folder structure not create correctly', false);
                                System::log(new Log('Something went wrong, Folder structure not create correctly', LOG_CRITICAL));
    //                            array_push($message, 'Something went wrong, Folder structure not create correctly');//ask from thilina
                            }
                        } else {
                            echo $response->create(500, 'Username Already exists', false);
                            return false;
                        }
                    }

                    $result = AdminDAO::addNewCompany($admin);
                    if($result){

                        $ID = $result;
                        $admin->setId($ID);
                        $companyDetails = AdminDAO::single($admin);

                        $database_name = $admin->getDbName();

                        $allDatabases = AdminDAO::showDatabases();
                        if($allDatabases){
                            if(in_array($database_name, $allDatabases)){
                                echo $response->create(500, 'Already use this username', false);
                                return false;
                            }
                        }

                        $createDatabase = AdminDAO::createDatabase($database_name);

                        if($createDatabase){
                            $createTables = AdminDAO::createTables($database_name);

                            if($createTables){

                                $title = 'Hello '.$companyDetails['name'].',';
                                $email = $companyDetails['email'];
                                $name = $companyDetails['name'];
                                $subject = 'Welcome to Moni';
                                
                                $httpHost = AdminController::createDomain();
                                
                                $message = "http://".$httpHost."/login/".$admin->getUsername();
                                
                                $sendEmail = AdminController::sendRegisterEmail($title,$message, $email, $name, $subject);
                                
                                $response->setData($companyDetails);
                                echo $response->create(200, 'Success', true);
                            } else {
                                echo $response->create(500, 'Can not create tables', false);
                            }
                        } else {
                            echo $response->create(500, 'Can not create database', false);
                        } 
                    } else {
                        echo $response->create(500, 'Unsuccess', false);
                    }
                } catch (Exception $ex){
                    echo $response->create(500, $ex->getMessage(), false);
                }
                break;
            case 'update':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                $admin = new Admin($data);
                
                if(empty($admin->getId())){
                    echo $response->create(500, 'ID is Required', false);
                    return false;
                }
                if(empty($admin->getCom_name())){
                    echo $response->create(500, 'Company Name is Required', false);
                    return false;
                }
                if(empty($admin->getUsername())){
                    echo $response->create(500, 'Username is Required', false);
                    return false;
                }
                if(empty($admin->getEmail())){
                    echo $response->create(500, 'Email is Required', false);
                    return false;
                }
                if(empty($admin->getActive())){
                    echo $response->create(500, 'Please mention active', false);
                    return false;
                }
                if(!empty($admin->getPhone())){
                    if(!Validation::phoneValidation($admin->getPhone())){
                        echo $response->create(500, 'Valid phone is Required', false);
                        return false;
                    }
                }
                if($admin->getSkip_validation() || $admin->getSkip_validation() === 'true'){
                    $admin->setSkip_validation(1);
                } else {
                    $admin->setSkip_validation(2);
                }

                if(!empty($admin->getLogo())){
                    
                    $base64 = Validation::validateBase64String($admin->getLogo());
                    if($base64){
                        $imageValidate = ImageManager::checkImageSize($admin->getLogo());
                        if($imageValidate > 1000 ){
                            echo $response->create(500, 'Image too large. Maximum resolution 500x200 px', false);
                            return false;
                        }
                    } else {
                        if(!Validation::checkULR($admin->getLogo())){ 
                            echo $response->create(500, 'Invalid Image', false);
                            return false;
                        }
                    }
                }
                if(!Validation::emailValidation($admin->getEmail())){
                    echo $response->create(500, 'Valid email is required', false);
                    return false;
                }
                if (!Validation::checkStringForLettersAndNumbers($admin->getUsername())) {
                    echo $response->create(500, 'Can contain english letters and numbers', false);
                    return false;
                }
                
                $checkUsername = AdminDAO::checkCompany($admin->getUsername());

                if($checkUsername){
                    if($checkUsername['ID'] != $admin->getId()){
                        echo $response->create(500, 'Already use this username', false);
                        return false;
                    }
                }
                
                $checkEmail = AdminDAO::checkEmail($admin);
                if($checkEmail['ID'] != $admin->getId()){
                    echo $response->create(500, 'Already use this email', false);
                    return false;
                }
                
                $result = AdminDAO::updateCompany($admin);
                if($result){
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'Unsuccess', false);
                }
                
                break;
            case 'addFirstUser':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                $user=new User($data);
                $user->setRole(1);
                
                if(empty($user->getFName()) || empty($user->getLName()) || empty($user->getEmail()) || empty($user->getNic()) || empty($user->getPassword()) || empty($user->getRole())){
                    echo $response->create(500, 'Missing input fields', false);
                    return false;
                }
                if(empty($data['username'])){
                    echo $response->create(500, 'Company username is required', false);
                    return false;
                }
                
                $username = $data['username'];
                $company = AdminDAO::checkCompany($username);
                
                $allBucket = FileManager::listAllBuckets();
                if($allBucket){
                    if(!in_array($company['bucket_name'], $allBucket)){
                        echo $response->create(500, 'Bucket not exist', false);
                        return false;
                    }
                }

                $allDatabases = AdminDAO::showDatabases();
                if($allDatabases){
                    if(!in_array($company['dbname'], $allDatabases)){
                        echo $response->create(500, 'Database not exist', false);
                        return false;
                    }
                }
                
                $database = $company['dbname'];
                
                if(!Validation::emailValidation($user->getEmail())){
                    echo $response->create(500, 'Valid email is required', false);
                    return false;
                }
                if(!Validation::nicValidation($user->getNic())){
                    echo $response->create(500, 'Valid nic is required', false);
                    return false;
                }
                if(!empty($user->getPhone())){
                    if(!Validation::phoneValidation($user->getPhone())){
                        echo $response->create(500, 'Valid phone number is required', false);
                        return false;
                    }
                }

                $result = UserDAO::addUser($user,$database);
                if($result){
                    $admin = new Admin();
                    $admin->setUsername($username);
                    $admin->setCheckFirstUser(1);
                    
                    $updateFirstUser = AdminDAO::updateCheckFirstUser($admin);
                    
                    $locationObj = new Location('default-parent',0);
                    $location_p_id = LocationDAO::addLocation($locationObj);
                    if($location_p_id){
                        $locationObj = new Location('default-child',$location_p_id);
                        $location_c_id = LocationDAO::addLocation($locationObj);
                    }
                    
                    echo $response->create(200, 'Success', true);

                } else {
                    echo $response->create(500, 'Unsuccess', false);
                }
                break;
            case 'login':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                $admin = new Admin($data);
                
                if(empty($admin->getEmail())){
                    echo $response->create(500, 'Email is required', false);
                    return false;
                }
                if(empty($admin->getPassword())){
                    echo $response->create(500, 'Password is required', false);
                    return false;
                }

                $result = AdminDAO::login($admin);
                if($result){
                    
                    SessionManager::createSessionVar('ID', $result['ID']);
                    SessionManager::createSessionVar('email', $result['email']);
                    SessionManager::createSessionVar('role', 100);
                    
                    $response->setData($result);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'Login credentials are invalid, Please try again.', false);
                }
                break;
            case 'updateSuper':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                $admin = new Admin($data);
                $admin->setId($_SESSION['ID']);
                
                if(empty($admin->getId())){
                    echo $response->create(500, 'User ID is required', false);
                    return false;
                }
                if(empty($admin->getFName())){
                    echo $response->create(500, 'First Name is required', false);
                    return false;
                }
                if(empty($admin->getLName())){
                    echo $response->create(500, 'Last Name is required', false);
                    return false;
                }               
                if(empty($admin->getEmail())){
                    echo $response->create(500, 'Email is required', false);
                    return false;
                }
                
                if(!empty($admin->getPassword()) && !empty($admin->getNewPassword())){


                    if(!Validation::validatePassword($admin->getNewPassword())){
                        echo $response->create(500, 'Password must contain at least 6 characters, including UPPER/lowercase and numbers', false);
                        return false;
                    }
                    
                    $userDetails = AdminDAO::getPassword($admin->getId());                    
                    $old_password = $userDetails['password'];

                    $sha_password = hash('sha256', $admin->getPassword());

                    if($old_password != $sha_password){
                        echo $response->create(500, 'Current password seems to be incorrect!!', false);
                        return false;
                    }
                       
                }
                
                
                if($admin->getEmail() != $_SESSION['email']){
                    
                    if(AdminDAO::checkUser($admin)){
                        echo $response->create(500, 'Already registered', false);
                        return false;
                    }
                }

                $result = AdminDAO::updateSuperAdmin($admin);

                if($result){
                    if(!empty($admin->getEmail())){
                        SessionManager::createSessionVar('email', $result['email']);
                    }
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'Unsuccess', false);
                }
                break;
            case 'resetpasswordrequest':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                if(empty($data['email'])){
                    echo $response->create(500, 'E-mail is required', false);
                    return false;
                }
                
                if(!Validation::emailValidation($data['email'])){
                    echo $response->create(500, 'A valid E-mail is required', false);
                    return false;
                }
                
                $admin = new Admin($data);
                $userDetails = AdminDAO::checkUser($admin);
                
                if($userDetails){
                    $user_id = $userDetails['ID'];
                    $email = $userDetails['email'];
                    $secret = rand(1000, 9999);
                    $name = $userDetails['f_name'].' '.$userDetails['l_name'];

                    $addResetDetails = AdminDAO::addResetPassword($user_id, $email, $secret);
                    if($addResetDetails){
                        $subject = 'Reset Password Instructions';
                        $ID = $addResetDetails;
                        $token = hash('sha256', $secret);

                        $httpHost = AdminController::createDomain();

                        $title = 'Hello '.$userDetails['f_name'].',';
                        $message = "http://".$httpHost."/admin/login.php?action=reset&id=".$ID."&token=".$token;
 
                        $sendEmail = AdminController::sendResetEmail($title,$message, $email, $name, $subject);

                        if($sendEmail){
                            echo $response->create(200, 'Success', true);
                        } else {
                            echo $response->create(500, 'Try Again', false);
                        }
                    } else {
                        echo $response->create(500, 'Try Again', false);
                    }
                } else {
                    echo $response->create(500, 'User not found', false);
                }
                
                break;
            case 'resetpassword':
                $response = new Response();
                
                $data = $this->getRequest()->getData();

                if(empty($data['id'])){
                    echo $response->create(500, 'ID is required', false);
                    return false;
                }
                if(empty($data['token'])){
                    echo $response->create(500, 'Token is required', false);
                    return false;
                }
                if(empty($data['password'])){
                    echo $response->create(500, 'password is required', false);
                    return false;
                }
                
                $ID = $data['id'];
                $token = $data['token'];
                $password = $data['password'];
                
                $resetData = AdminDAO::getResetData($ID);

                if($resetData){
                    $secret = hash('sha256', $resetData['secret']);

                    if($token === $secret){
                        $reset_d_t = date_create($resetData['d_t']);
                        $current_d_t = date_create(date('Y-m-d H:i:s'));
                        
                        if($current_d_t < $reset_d_t){
                            echo $response->create(500, 'Try Again Redirect', false);
                            return false;
                        }
                        $dateTimeDiff = date_diff($current_d_t, $reset_d_t, false);

                        $hours = $dateTimeDiff->format('%h');
                        
                        if(intval($hours) >= 1){
                            echo $response->create(500, 'Link is expired', false);
                            return false;
                        } else {
                            
                            if(!Validation::validatePassword($password)){
                                echo $response->create(500, 'Password must contain at least 6 characters, including UPPER/lowercase and numbers', false);
                                return false;
                            }
                            

                            $admin = new Admin($data);
                            $admin->setEmail($resetData['email']);
                            $userDetails = AdminDAO::checkUser($admin);
                            
                            if(!$userDetails){
                                echo $response->create(500, 'User Not found', false);
                                return false;
                            }

                            $user_id = $userDetails['ID'];

                            $admin->setId($user_id);
                            $admin->setPassword($password);

                            $resetSuccess = AdminDAO::resetPassword($admin);

                            if($resetSuccess){
                                $response->setData($resetData);
                                echo $response->create(200, 'Success', true);
                            } else {
                                echo $response->create(500, 'Try Again', false);
                            }

                        }
                    } else {
                        echo $response->create(500, 'Invalid Token', false);
                        return false;
                    }
                }
                
                break;
            case 'debugenable':
                $response = new Response();

                // 1 :- disable, 2 :- enable
                
                $data = $this->getRequest()->getData();
                
                $admin = new Admin($data);
                if(empty($admin->getId())){
                    echo $response->create(500, 'Company ID is required', false);
                    return false;
                }
                if($admin->getDebug() == 1 || $admin->getDebug() == 2){
                    
                } else {
                    $admin->setDebug(1);
                }

                $result = AdminDAO::debugEnableDisable($admin);
                if($result){
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'Unsuccess', false);
                }
                break;
            case 'add_new_table':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                $databaseTable = new DatabaseTable($data);
                if(empty($databaseTable->getDatabase_name())){
                    echo $response->create(500, 'Database name is requieded', false);
                    return false;
                }
                $databaseTable->setTable_name('customer_feedback');
                $databaseTable->setPrimary_key('id', true);
                $databaseTable->setInt_field('checkpoint_id');
                $databaseTable->setInt_field('reaction');
                $databaseTable->setLongText_field('feedback', true);
                $databaseTable->setVarchar_field('name', 1000, true);
                $databaseTable->setVarchar_field('phone', 45, true);
                $databaseTable->setVarchar_field('image', 1000, true);
                $databaseTable->setInt_field('feedback_read');
                
                $databases = AdminDAO::createNewTable($databaseTable);               
                if($databases){
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No data', false);
                }
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }
    
    public function authenticate() {
        switch ($this->getRequest()->getAction()) {
            case 'login':
                return true;
            case 'add':
                if(SessionManager::is_superAdmin()){
                    return true;
                } else {
                    return false;
                }
            case 'update':
                if(SessionManager::is_superAdmin()){
                    return true;
                } else {
                    return false;
                }
            case 'addFirstUser':
                if(SessionManager::is_superAdmin()){
                    return true;
                } else {
                    return false;
                }
            case 'all':
                if(SessionManager::is_superAdmin()){
                    return true;
                } else {
                    return false;
                }
            case 'single':
                if(SessionManager::is_superAdmin()){
                    return true;
                } else {
                    return false;
                }
            case 'updateSuper':
                if(SessionManager::is_superAdmin()){
                    return true;
                } else {
                    return false;
                }
            case 'debugenable':
                if(SessionManager::is_superAdmin()){
                    return true;
                } else {
                    return false;
                }
            case 'logout':
                return true;
            case 'resetpasswordrequest':
                return true;
            case 'checkLink':
                return true;
            case 'resetpassword':
                return true;
            case 'company':
                return true;
            case 'validateSession':
                return true;
            case 'add_new_table':
                return true;
            default :
                echo Response::badRequest();
                return false;
                break;
        }
    }

    private static function chechUniqueID($unique){

        $uniqueID = AdminDAO::checkCompany($unique);
        if($uniqueID){
            $count = intval(substr($unique, strpos($unique, "_")+1));
            $count++;
            $arr = explode("_", $unique,2);
            $unique_1 = $arr[0].'_'.$count;
            
            $result = AdminController::chechUniqueID($unique_1);
            if($result){
                return $result;
            }
        } else {
            return $unique;
        }
    }
    
    private static function createUniqueBucket($bucketName){

        do{
            
        }while (true);
    }
    
    private static function createDomain(){
        if(isset($_SERVER['HTTP_HOST'])){
            return $_SERVER['HTTP_HOST'];
        } else {
            return false;
        }
    }
    
    private static function sendResetEmail($title,$message,$recipientEmail,$recipientName,$subject){
        
        $businessDetails['companyName'] = 'Moni';
        $businessDetails['logo'] = 'https://s3-ap-southeast-1.amazonaws.com/moni-company/monilogo/logo-moni.png';
        $businessDetails['email'] = 'moni.doweby.net';//domain
        
        ob_start();
        $emailTemplate = new EmailTemplates();
        $emailTemplate->header($title, $businessDetails);
        $emailTemplate->resetLink($message);
        $emailTemplate->footer($businessDetails);
        $html = ob_get_clean();
        
        $result = EmailManager::sendEmail($businessDetails['email'], $businessDetails['companyName'], $recipientEmail, $recipientName, $subject, $html);
        
        return $result;
    }
    
    private static function sendRegisterEmail($title,$message,$recipientEmail,$recipientName,$subject){
        
        $businessDetails['companyName'] = 'Moni';
        $businessDetails['logo'] = 'https://s3-ap-southeast-1.amazonaws.com/moni-company/monilogo/logo-moni.png';
        $businessDetails['email'] = 'moni.doweby.net';//domain
        
        ob_start();
        $emailTemplate = new EmailTemplates();
        $emailTemplate->header($title, $businessDetails);
        $emailTemplate->registerNewCompany($message);
        $emailTemplate->footer($businessDetails);
        $html = ob_get_clean();
        
        $result = EmailManager::sendEmail($businessDetails['email'], $businessDetails['companyName'], $recipientEmail, $recipientName, $subject, $html);
        
        return $result;
    }
}

