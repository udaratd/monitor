<?php

class CustomerController extends Controller {
    
    public function Render() {
        switch ($this->getRequest()->getType()) {
            case 'GET':
                $this->getHandler();
                break;
            case 'POST':
                $this->postHandler();
                break;
            default:
                echo Response::badRequest();
                break;
        }
    }

    public function getHandler() {
        switch ($this->getRequest()->getAction()){
            case 'single':
                $response = new Response();

                $data = $this->getRequest()->getData();
                
                $customer = new Customer($data);
                if(empty($customer->getId())){
                    echo $response->create(500, 'ID is required', false);
                    return false;
                }
                
                $feedBack = CustomerDAO::single($customer);
                if($feedBack){
                    $response->setData($feedBack);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'Data not found', false);
                }
                break;
            case 'all':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                $customer = new Customer($data);
                if(empty($customer->getE_limit())){
                    $customer->setE_limit(20);
                } else {
                    $customer->setE_limit(intval($customer->getE_limit()));
                }
                
                if(empty($customer->getPageNumber())){
                    $customer->setPageNumber(1);
                } else {
                    $customer->setPageNumber(intval($customer->getPageNumber()));
                }
                
                $s_limit = 0;
                for($i=1;$i<$customer->getPageNumber();$i++){
                    $s_limit = $s_limit+$customer->getE_limit();
                }
                
                $customer->setS_limit(intval($s_limit));
//                if( (($customer->getS_limit() === 0) || (!empty($customer->getS_limit()) && is_numeric($customer->getS_limit()))) && !empty($customer->getE_limit()) && is_numeric($customer->getE_limit())){
//                    $customer->setS_limit(intval($s_limit));
//                }
//                $customer->setS_limit(intval(0));
                $count = CustomerDAO::allCount($customer);
                
                $numberOfPages = ceil($count['count']/$customer->getE_limit());
                    
                //if number of pages = 0 then set as 1
                if(empty($numberOfPages)){
                    $numberOfPages = 1;
                }

                $meta['numberOfPages'] = $numberOfPages;
                $meta['pageNumber'] = $customer->getPageNumber();

                $limit = false;
                if(strtolower($customer->getLimit()) === 'all'){
                    $limit = true;
                } else {
                    $limit = false;
                }
                
                $result = CustomerDAO::all($customer);
                if($result) {
                    $output['feedbacks'] = $result;
                    $output['meta'] = $meta;
                    $response->setData($output);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No Data', false);
                }
                break;
            case 'unread_count':
                $response = new Response();
                
                $customer = new Customer();
                $customer->setFeedback_read(0);
                
                $count = CustomerDAO::unreadCount($customer);
                if($count){
                    $response->setData($count);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'Data not found', false);
                }
                break;
            case 'single-page':
                $response = new Response();
                
                session_unset();// Before Login to the System, Unset All session variables

                $data = $this->getRequest()->getData();
                
                if(empty($data['username'])){
                    echo $response->create(500, 'Company username is required', false);
                    return false;
                }
                if(empty($data['checkpoint_id'])){
                    echo $response->create(500, 'Checkpoint ID is required', false);
                    return false;
                }
                
                $username = $data['username'];
                $checkpoint_id = $data['checkpoint_id'];
                
                if(empty($_SESSION['DB_NAME'])){
 
                    $admin = new Admin();
                    $admin->setUsername($username);
                    
                    if(empty($admin->getUsername())){
                        echo $response->create(500, 'Username is required', false);
                        return false;
                    }
                    
                    $findCompany = AdminDAO::getSingleByUsername($admin);//if anything happen please remove company database name from DAO class of this function
                    if($findCompany){                       
                        
                        $allBucket = FileManager::listAllBuckets();
                        if($allBucket){
                            if(!in_array($findCompany['bucket_name'], $allBucket)){
                                echo $response->create(500, 'Bucket not exist', false);
                                return false;
                            }
                        }
                        
                        $allDatabases = AdminDAO::showDatabases();//if anything happen please remove company database name from DAO class of this function
                        if($allDatabases){
                            if(!in_array($findCompany['dbname'], $allDatabases)){
                                echo $response->create(500, 'Database not exist', false);
                                return false;
                            }
                        }
                        
                        SessionManager::createSessionVar('DB_NAME', $findCompany['dbname']);
                        SessionManager::createSessionVar('S3_BUCKET', $findCompany['bucket_name']);
                        SessionManager::createSessionVar('COMPANY_ID', $findCompany['ID']);
                        SessionManager::createSessionVar('DEBUG_ENABLE_DISABLE', $findCompany['debug']);
                        
                        $customer = new Customer();
                        $customer->setCheckpoint_id($checkpoint_id);
                        
                        $checkpointDetails = CustomerDAO::getCheckpoint($customer);
                        if($checkpointDetails){
                            $temp['company'] = $findCompany;
                            $temp['checkpoint'] = $checkpointDetails;                           
                            $response->setData($temp);
                            echo $response->create(200, 'Success', true);
                        } else {
                            $response->create(500, 'Sorry, Checkpoint not found, Try again', 500);
                            return false;
                        }
                                                
                    } else {
                        echo $response->create(403, 'Company not found', false);
                        return false;
                    }
                } else {
                    echo $response->create(403, 'Incorrect company', false);
                    return false;
                }
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function postHandler() {
        switch ($this->getRequest()->getAction()){
            case 'add':
                $response = new Response();
                
                session_unset();// Before Login to the System, Unset All session variables
                
                $data = $this->getRequest()->getData();
                
                $customer = new Customer($data);
                $customer->setFeedback_read(0);
                if(empty($data['username'])){
                    echo $response->create(500, 'Company username is required', false);
                    return false;
                }
                if(empty($customer->getCheckpoint_id())){
                    echo $response->create(500, 'Checkpoint ID is required', false);
                    return false;
                }
                
                $username = $data['username'];
                
                if(empty($_SESSION['DB_NAME'])){
 
                    $admin = new Admin();
                    $admin->setUsername($username);
                    
                    if(empty($admin->getUsername())){
                        echo $response->create(500, 'Username is required', false);
                        return false;
                    }
                    
                    $findCompany = AdminDAO::getSingleByUsername($admin);//if anything happen please remove company database name from DAO class of this function
                    if($findCompany){                       
                        
                        $allBucket = FileManager::listAllBuckets();
                        if($allBucket){
                            if(!in_array($findCompany['bucket_name'], $allBucket)){
                                echo $response->create(500, 'Bucket not exist', false);
                                return false;
                            }
                        }
                        
                        $allDatabases = AdminDAO::showDatabases();//if anything happen please remove company database name from DAO class of this function
                        if($allDatabases){
                            if(!in_array($findCompany['dbname'], $allDatabases)){
                                echo $response->create(500, 'Database not exist', false);
                                return false;
                            }
                        }
                        
                        SessionManager::createSessionVar('DB_NAME', $findCompany['dbname']);
                        SessionManager::createSessionVar('S3_BUCKET', $findCompany['bucket_name']);
                        SessionManager::createSessionVar('COMPANY_ID', $findCompany['ID']);
                        SessionManager::createSessionVar('DEBUG_ENABLE_DISABLE', $findCompany['debug']);
                        
                        $id = CustomerDAO::add($customer);
                        if($id){
                            
                            echo $response->create(200, 'Success', true);
                        } else {
                            $response->create(500, 'Sorry, Checkpoint not found, Try again', 500);
                            return false;
                        }
                                                
                    } else {
                        echo $response->create(403, 'Company not found', false);//mobile app use this code
                        return false;
                    }
                } else {
                    echo $response->create(403, 'Incorrect company', false);//mobile app use this code
                    return false;
                }
                break;
            case 'read':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                $customer = new Customer($data);
                $customer->setFeedback_read(1);
                if(empty($customer->getId())){
                    echo $response->create(500, 'ID is required', false);
                    return false;
                }
                
                $feedBackRead = CustomerDAO::read($customer);
                if($feedBackRead){
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'Data not found', false);
                }
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function authenticate() {
        return true;
    }
}

