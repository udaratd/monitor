<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class NotificationController extends Controller{
    
    
    public function Render() {
        switch ($this->getRequest()->getType()) {
            case 'GET':
                $this->getHandler();
                break;
            case 'POST':
                $this->postHandler();
                break;
            default:
                echo Response::badRequest();
                break;
        }
    }
    
    public function getHandler(){
        switch ($this->getRequest()->getAction()) {
            case 'allnotification':
                $response = new Response();
                
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_role = $_SESSION['role'];
                
                if($loggedUser_role == 1){
                    $loggedUser_ID = false;
                }
                
                $date_period = $this->getRequest()->getData()['date_period'];
                
                if(empty($date_period)) {
                    $date_period = "7 days";
                } else {
                    $date_period = $date_period." days";
                }
                
                $today = date("Y-m-d");
                $date = date_create($today);
                $s_date = date_sub($date, date_interval_create_from_date_string($date_period));

                $s_date = date_format($s_date,"Y-m-d");
                $e_date = $today;

                $result = WorkDAO::getAllNotification($loggedUser_ID, $s_date, $e_date);
                if($result){
                    $response->setData($result);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No Data', false);
                }
                break;
            case 'unreadnotification':
                $response = new Response();
                
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_role = $_SESSION['role'];
                
                if($loggedUser_role == 1){
                    $loggedUser_ID = false;
                }
                
                $date_period = $this->getRequest()->getData()['date_period'];
                
                if(empty($date_period)) {
                    $date_period = "7 days";
                } else {
                    $date_period = $date_period." days";
                }
                
                $today = date("Y-m-d");
                $date = date_create($today);
                $s_date = date_sub($date, date_interval_create_from_date_string($date_period));

                $s_date = date_format($s_date,"Y-m-d");
                $e_date = $today;
                
                $result = WorkDAO::getUnreadNotification($loggedUser_ID, $s_date, $e_date);
                if($result){
                    $response->setData($result);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No Data', false);
                }
                break;
            case 'count':
                $response = new Response();
                
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_role = $_SESSION['role'];
                
                if($loggedUser_role == 1){
                    $loggedUser_ID = false;
                }
                
                $date_period = $this->getRequest()->getData()['date_period'];
                
                if(empty($date_period)) {
                    $date_period = "7 days";
                } else {
                    $date_period = $date_period." days";
                }
                
                $today = date("Y-m-d");
                $date = date_create($today);
                $s_date = date_sub($date, date_interval_create_from_date_string($date_period));

                $s_date = date_format($s_date,"Y-m-d");
                $e_date = $today;
                
                $result = WorkDAO::getUnreadNotificationCount($loggedUser_ID, $s_date, $e_date);
                if($result){
                    $response->setData($result);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No Data', false);
                }
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function postHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'readingnotification':
                $response = new Response();
                
                $workID = $this->getRequest()->getData()['work_id'];
                
                if(empty($workID)){
                    echo $response->create(500, 'Missing Input Fields', false);
                    return false;
                }
                
                $result = WorkDAO::readingNotification($workID);
                
                if($result){
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'Unsuccess', false);
                }
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }
    
    public function authenticate() {
//        SessionManager::checkTimeoutSession();
        switch ($this->getRequest()->getAction()){
            case 'allnotification':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'unreadnotification':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'readingnotification':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'count':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                } else {
                    return false;
                }
                break;
            default:
                echo Response::badRequest();
                break;
        }
    }

}