<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DashboardController extends Controller {
    
    public function Render() {
        switch ($this->getRequest()->getType()) {
            case 'GET':
                $this->getHandler();
                break;
            case 'POST':
                $this->postHandler();
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function getHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'dashboard':
                $response = new Response();
                
                $empty = array();
                
                $data = $this->getRequest()->getData();

                $completedTask = DashboardController::completedTask($data);
                if($completedTask){
                    $output['completedTask'] = $completedTask;
                }
                
                $ontime = DashboardController::ontime($data);
                if($ontime){
                    $output['ontime'] = $ontime;
                }
                
                $reworkRate = DashboardController::reworkRate($data);
                if($reworkRate){
                    $output['reworkRate'] = $reworkRate;
                }
                
                $reworkedCheckpoint = DashboardController::reworkedCheckpoint($data);
                if($reworkedCheckpoint){
                    $output['reworkedCheckpoint'] = $reworkedCheckpoint;
                } else {
                    $output['reworkedCheckpoint'] = $empty;
                }
                
                if($output){
                    $response->setData($output);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No Data', false);
                }
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }
    
    public function postHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'add':
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function authenticate() {
//        SessionManager::checkTimeoutSession();
        return true;
    }
    
    private static function completedTask($reportData){
        
        $output = array();
        
        $data = $reportData;
        
        $date_period = $data['datePeriod'];
                
        if(empty($date_period)) {
            $date_period = "7 days";
        } else {
            $date_period = $date_period." days";
        }

        //EXAMPLE
        //new = this month
        //original = last month

        //get new data
        $today = date("Y-m-d");
        $date = date_create($today);
        $s_date = date_sub($date, date_interval_create_from_date_string($date_period));

        $s_date = date_format($s_date,"Y-m-d");
        $e_date = $today;


        $data["s_date"] = $s_date;
        $data["e_date"] = $e_date;

        $report = new Report($data);
        $result_1 = ReportDAO::getCompletedTask($report);

        //get original data
        $s_date = date_create($s_date);
        $s_date = date_sub($s_date, date_interval_create_from_date_string("1 day"));
        $s_date = date_format($s_date, "Y-m-d");

        $today = $s_date;

        $date = date_create($today);
        $s_date = date_sub($date, date_interval_create_from_date_string($date_period));

        $s_date = date_format($s_date,"Y-m-d");
        $e_date = $today;

        $data["s_date"] = $s_date;
        $data["e_date"] = $e_date;

        $report = new Report($data);
        $result_2 = ReportDAO::getCompletedTask($report);

        $new_completeTask = $result_1['completedTaskCount'];
        $original_completeTask = $result_2['completedTaskCount'];


        if($new_completeTask>0){
            $output['completedTaskCount'] = $new_completeTask;
            if ($original_completeTask>0) {
                $percentage = (($original_completeTask - $new_completeTask)/$original_completeTask)*100;
                $output['percentage'] = $percentage;
                if($percentage<0){
                    $output['status'] = "Positive";
                } elseif ($percentage==0) {
                    $output['status'] = "neutral";
                } else {
                    $output['status'] = "Negative";
                }
            }
            return $output;
        }else{
            $output['completedTaskCount'] = 0;
            $output['percentage'] = 0;
            $output['status'] = "neutral";
            return $output;
        }
    }
    
    private static function ontime($reportData){
        
        $output = array();

        $data = $reportData;

        $date_period = $data['datePeriod'];

        if(empty($date_period)) {
            $date_period = "7 days";
        } else {
            $date_period = $date_period." days";
        }

        //EXAMPLE
        //new = this month
        //original = last month

        //get new data
        $today = date("Y-m-d");
        $date = date_create($today);
        $s_date = date_sub($date, date_interval_create_from_date_string($date_period));

        $s_date = date_format($s_date,"Y-m-d");
        $e_date = $today;

        $data["s_date"] = $s_date;
        $data["e_date"] = $e_date;

        $report = new Report($data);
        $result_1 = ReportDAO::getOnTimeTask($report);

        //get original data
        $s_date = date_create($s_date);
        $s_date = date_sub($s_date, date_interval_create_from_date_string("1 day"));
        $s_date = date_format($s_date, "Y-m-d");

        $today = $s_date;

        $date = date_create($today);
        $s_date = date_sub($date, date_interval_create_from_date_string($date_period));

        $s_date = date_format($s_date,"Y-m-d");
        $e_date = $today;

        $data["s_date"] = $s_date;
        $data["e_date"] = $e_date;

        $report = new Report($data);
        $result_2 = ReportDAO::getOnTimeTask($report);

        $new_onTimeTask = $result_1['onTimeTasks'];
        $total_completedTask = $result_1['completedTaskCount'];
        $original_onTimeTask = $result_2['onTimeTasks'];

        if($new_onTimeTask>0){
            $onTimePercentage = ($new_onTimeTask/$total_completedTask)*100;
            $output['onTimeTasksPercentage'] = $onTimePercentage;
            if ($original_onTimeTask>0) {
                $percentage = (($original_onTimeTask - $new_onTimeTask)/$original_onTimeTask)*100;
                $output['percentage'] = $percentage;
                if($percentage<0){
                    $output['status'] = "Positive";
                } elseif ($percentage==0) {
                    $output['status'] = "neutral";
                } else {
                    $output['status'] = "Negative";
                }
            }
            return $output;
        }else{
            $output['onTimeTasksPercentage'] = 0;
            $output['percentage'] = 0;
            $output['status'] = "neutral";
            return $output;
        }
    }
    
    private static function reworkRate($reportData){
        
        $output = array();

        $data = $reportData;

        $date_period = $data['datePeriod'];

        if(empty($date_period)) {
            $date_period = "7 days";
        } else {
            $date_period = $date_period." days";
        }

        $today = date("Y-m-d");
        $date = date_create($today);
        $s_date = date_sub($date, date_interval_create_from_date_string($date_period));

        $s_date = date_format($s_date,"Y-m-d");
        $e_date = $today;


        //EXAMPLE
        //new = this month
        //original = last month

        //get new data
        $data["s_date"] = $s_date;
        $data["e_date"] = $e_date;

        $report = new Report($data);
        $result_1 = ReportDAO::getReworkRate($report);


        //get original data
        $s_date = date_create($s_date);
        $s_date = date_sub($s_date, date_interval_create_from_date_string("1 day"));
        $s_date = date_format($s_date, "Y-m-d");

        $today = $s_date;

        $date = date_create($today);
        $s_date = date_sub($date, date_interval_create_from_date_string($date_period));

        $s_date = date_format($s_date,"Y-m-d");
        $e_date = $today;

        $data["s_date"] = $s_date;
        $data["e_date"] = $e_date;

        $report = new Report($data);
        $result_2 = ReportDAO::getReworkRate($report);


        $new_reworkTask = $result_1['reworkTask'];
        $total_completedTask = $result_1['completedTaskCount'];
        $original_reworkTask = $result_2['reworkTask'];

        if($new_reworkTask>0){
            $reworkPercentage = ($new_reworkTask/$total_completedTask)*100;
            $output['reworkPercentage'] = $reworkPercentage;
            if ($original_reworkTask>0) {
                $percentage = (($original_reworkTask - $new_reworkTask)/$original_reworkTask)*100;
                $output['percentage'] = $percentage;
                if($percentage<0){
                    $output['status'] = "Positive";
                } elseif ($percentage==0) {
                    $output['status'] = "neutral";
                } else {
                    $output['status'] = "Negative";
                }
            }
            return $output;
        }else{
            $output['reworkPercentage'] = 0;
            $output['percentage'] = 0;
            $output['status'] = "neutral";
            return $output;
        }
    }
    
    private static function reworkedCheckpoint($reportData){
        
        $data = $reportData;

        $date_period = $data['datePeriod'];

        if(empty($date_period)) {
            $date_period = "7 days";
        } else {
            $date_period = $date_period." days";
        }

        $today = date("Y-m-d");
        $date = date_create($today);
        $s_date = date_sub($date, date_interval_create_from_date_string($date_period));

        $s_date = date_format($s_date,"Y-m-d");
        $e_date = $today;

        $data["s_date"] = $s_date;
        $data["e_date"] = $e_date;

        $report = new Report($data);
        $result = WorkDAO::getReworkedWorkforDashBoard($report);

        if($result){
            return $result;
        } else {
            return false;
        }
    }
}