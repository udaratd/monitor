<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//code 510 :- schedule delete

class scheduleController extends Controller {
    
    public function Render() {
        switch ($this->getRequest()->getType()) {
            case 'GET':
                $this->getHandler();
                break;
            case 'POST':
                $this->postHandler();
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }
    
    public function getHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'listSearch':
                $response = new Response();
                
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_role = $_SESSION['role'];
                
                if($loggedUser_role == 1){
                    $loggedUser_ID = false;
                }
                
                $data = $this->getRequest()->getData();
                
                $userName = $data['name'];
                $checkpointName = $data['checkName'];
                $s_date = $data['s_date'];
                $e_date = $data['e_date'];
                $parent = $data['parent'];
                $child = $data['child'];
                $order = $data['order'];
                $dayCount = $data['day_count'];
//                $userName = $this->getRequest()->getData()['name'];
//                $checkpointName = $this->getRequest()->getData()['checkName'];
//                $s_date = $this->getRequest()->getData()['s_date'];
//                $e_date = $this->getRequest()->getData()['e_date'];
//                $parent = $this->getRequest()->getData()['parent'];
//                $child = $this->getRequest()->getData()['child'];
//                $order = $this->getRequest()->getData()['order'];
//                $dayCount = $this->getRequest()->getData()['day_count'];
                
                $pageNumber = $data['page_number'];
                $e_limit = $data['e_limit'];
                $schedule_limit = $data['limit'];
                
                if(empty($order) || $order === null){
                    $order = false;
                } else {
                    if($order === "asc"){
                        $order = true;
                    } else {
                        $order = false;
                    }
                }
                
                if(empty($s_date) || empty($e_date)) {
                    
                    $today = date("Y-m-d");
                    $date = date_create($today);
                    if(empty($dayCount)){
                        $e_date = date_add($date, date_interval_create_from_date_string("14 days"));
                    } else {
                        $days = $dayCount." days";
                        $e_date = date_add($date, date_interval_create_from_date_string($days));
                    }
                    
                    
                    $s_date = $today;
                    $e_date = date_format($e_date,"Y-m-d");
                }
                
                if(isset($userName) && isset($checkpointName)) {
                    
                    ///////////////////////////////////////////////////
                    if(empty($e_limit)){
                        $e_limit = 20;
                    } else {
                        $e_limit = intval($e_limit);
                    }

                    if(empty($pageNumber)){
                        $pageNumber = 1;
                    }

                    $s_limit = 0;
                    for($i=1;$i<$pageNumber;$i++){
                        $s_limit = $s_limit+$e_limit;
                    }

                    if( (($s_limit === 0) || (!empty($s_limit) && is_numeric($s_limit))) && !empty($e_limit) && is_numeric($e_limit)){
                        $s_limit = intval($s_limit);
                        $e_limit = intval($e_limit);
                    }

                    $count = ScheduleDAO::searchScheduleCount($loggedUser_ID,$userName,$order);

                    $numberOfPages = ceil($count['count']/$e_limit);
                    
                    //if number of pages = 0 then set as 1
                    if(empty($numberOfPages)){
                        $numberOfPages = 1;
                    }

                    $meta['numberOfPages'] = $numberOfPages;
                    $meta['pageNumber'] = $pageNumber;

                    $limit = false;
                    if(strtolower($schedule_limit) === 'all'){
                        $limit = true;
                    } else {
                        $limit = false;
                    }
                    ////////////////////////////////////////////////
                    
                    
                    $schedules = ScheduleDAO::searchSchedule($loggedUser_ID,$userName, $checkpointName, $s_date, $e_date, $parent, $child, $order, $s_limit, $e_limit, $limit);
                    if(is_array($schedules)) {
                        $output['schedules'] = $schedules;
                        $output['meta'] = $meta;
                        $response->setData($output);
                        echo $response->create(200, 'Success', true);
                    } else {
                        echo $response->create(500, 'No Data', false);
                    }
                } else {
                    //(name,checkName) parameters not Set
                    echo $response->create(500, 'Missing Input fielda', false);
                }
                break;
            case 'single':
                $response = new Response();
                $ID = $this->getRequest()->getData()['id'];
                if(isset($ID)) {
                    $schedule = ScheduleDAO::singleSchedule($ID);
                    if($schedule) {
                        $response->setData($schedule);
                        echo $response->create(200, 'Success', true);
                    } else {
                        //DAO singleSchedule function error OR Schedule not Existe
                        echo $response->create(500, 'No Data', false);
                    }
                } else {
                    //(id) parameter not set
                    echo $response->create(500, 'Missing Schedule ID', false);
                }
                break;
            case 'staff'://this case for create schedule and reschedule interfaces
                $response = new Response();
                
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_role = $_SESSION['role'];
                
                if($loggedUser_role == 1){
                    $loggedUser_ID = false;
                }

                $result = UserDAO::listStaff($loggedUser_ID);
                if($result) {
                    $response->setData($result);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No Data', false);
                }
                break;
            case 'scheduleCheckpoints'://this case for create schedule interface
                $response = new Response();
                
                $search = $this->getRequest()->getData()['search'];
                $status = $this->getRequest()->getData()['status'];
                
                $assign_unassign = null;
                
                if(isset($status) && !empty($status)){
                    if($status === 'Unassigned') {
                        $assign_unassign = false;//not assign
                    } elseif($status === 'Assigned') {
                        $assign_unassign = true;//already assign
                    } else {
                        $response->create(500, 'Invalid status', false);
                        return false;
                    }
                }
                
                $checkpoints = CheckpointDAO::checkpointsForSchedule($search, $assign_unassign);
                
//                $response->setData($checkpoints);
//                echo $response->create(200, 'Success', true);
                
                if($checkpoints){
                    $response->setData($checkpoints);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No Data', false);
                }
                
                break;
            case 'export':
                ScheduleDAO::exportSchedule();
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }
    
    public function postHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'createDaily':
                $response = new Response();
                
                $data = $this->getRequest()->getData();

                $type = $data['repeatType_id'];
                if($type != 1){
                    echo $response->create(500, 'Incorrect Repeat Type', false);
                    return false;
                } else {
                    $keys = array("user_id","checkpoint_id","repeatType_id","startTime","endTime");
                    
                    for($i=0;$i<count($keys);$i++){
                        if(!array_key_exists($keys[$i], $data)){
                            echo $response->create(500, 'Data not match with Schedule', false);
                            return false;
                        }
                    }
                }
                
                $ignore=array('description');
                if(Validation::variableValidation($data,$ignore)){
                
                    $schedule = new Schedule($data);
                    
                    $temp_time1 = strtotime($schedule->getStartTime());
                    $temp_time2 = strtotime($schedule->getEndTime());

                    $startTime = date("H:i:s",$temp_time1);               
                    $endTime = date("H:i:s",$temp_time2);

                    if($startTime > $endTime){
                        echo $response->create(500, 'Please Select Valid Time Range', false);
                        return false;
                    }
                    
                    $success = ScheduleDAO::addDailySchedule($schedule);
                    
                    if(is_string($success)) {
                        
                        $check_image = CheckpointDAO::getCheckImage($schedule->getCheckpoint_id());
                        
                        $title = "New Schedule";
                        $message = "You have new schedule today at ".date('h:i a', strtotime($schedule->getStartTime()));
                        $image = $check_image;
                        
                        $go = array("main"=>'go');
                        
                        $push = new Push($title, $message, $image);
                        $push->setData($go);
                        PushDAO::SendMultiplePush($push, $schedule->getUser_id());
                        
                        echo $response->create(200, 'Success', true);
                        
                        $activity = new LogActivity();
                        $activity->setActivity('Daily Schedule Created');
                        $activity->setBy($_SESSION['ID']);
                        $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                        $activity->setUserType($_SESSION['role']);

                        $logResult = LogActivityDAO::addLog($activity);
                        
                    } elseif(is_array($success)) {
                        $response->setData($success);
                        echo $response->create(500, 'User is not available', false);
                    } else {
                        echo $response->create(500, 'Please carefully check the Start Time and End Time of the Schedule, if you selected a date, check that date also', false);
                    }
                } else {
                    echo $response->create(500, 'Missing Input fields', false);
                }
                break;
            case 'createWeekly':
                $response = new Response();
                
                $data = $this->getRequest()->getData();

                $days = array("sunday","monday","tuesday","wednesday","thursday","friday","saturday");
                $count = Validation::arrayKeyExists($days, $data);
                if($count == 0)
                {                    
                    echo $response->create(500, 'this is not a Weekly Schedule', false);
                    return false;
                }
                
                $type = $data['repeatType_id'];
                if($type != 2){
                    echo $response->create(500, 'Incorrect Schedule Type', false);
                    return false;
                } else {
                    $keys = array("user_id","checkpoint_id","repeatType_id","startTime","endTime");
                    
                    for($i=0;$i<count($keys);$i++){
                        if(!array_key_exists($keys[$i], $data)){
                            echo $response->create(500, 'Data not match with Schedule Type', false);
                            return false;
                        }
                    }
                }
                $ignore=array('description');
                if(Validation::variableValidation($data,$ignore)){
                
                    $schedule = new Schedule($data);
                    
                    $temp_time1 = strtotime($schedule->getStartTime());
                    $temp_time2 = strtotime($schedule->getEndTime());

                    $startTime = date("H:i:s",$temp_time1);               
                    $endTime = date("H:i:s",$temp_time2);

                    if($startTime > $endTime){
                        echo $response->create(500, 'Please Select Valid Time Range', false);
                        return false;
                    }
                    
                    $success = ScheduleDAO::addWeeklySchedule($schedule);
                    if(is_string($success)) {
                        
                        $day = strtolower(date('l'));
                        if($data[$day] == 1){
                            
                            $check_image = CheckpointDAO::getCheckImage($schedule->getCheckpoint_id());
                        
                            $title = "New Schedule";
                            $message = "You have new schedule today at ".date('h:i a', strtotime($schedule->getStartTime()));
                            $image = $check_image;

                            $go = array("main"=>'go');
                            
                            $push = new Push($title, $message, $image);
                            $push->setData($go);
                            PushDAO::SendMultiplePush($push, $schedule->getUser_id());
                            
                        }
                        
                        echo $response->create(200, 'Success', true);
                        
                        $activity = new LogActivity();
                        $activity->setActivity('Weekly Schedule Created');
                        $activity->setBy($_SESSION['ID']);
                        $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                        $activity->setUserType($_SESSION['role']);

                        $logResult = LogActivityDAO::addLog($activity);
                        
                    } elseif(is_array($success)) {
                        $response->setData($success);
                        echo $response->create(500, 'User is not available', false);
                    } else {
                        echo $response->create(500, 'Please carefully check the Start Time and End Time of the Schedule, if you selected a date, check that date also', false);
                    }
                } else {
                    echo $response->create(500, ' Missing Input fields ', false);
                }
                break;
            case 'createOneDay':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                $type = $data['repeatType_id'];
                if($type != 3){
                    echo $response->create(500, 'Incorrect Schedule Type', false);
                    return false;
                } else {
                    $keys = array("user_id","checkpoint_id","repeatType_id","startTime","endTime","oneTimeDate");
                    
                    for($i=0;$i<count($keys);$i++){
                        if(!array_key_exists($keys[$i], $data)){
                            echo $response->create(500, 'Data not match with Schedule Type', false);
                            return false;
                        }
                    }
                }

//                $one = $this->getRequest()->getData()['oneTimeDate'];
//                if(empty($one)){
//                    echo $response->create(200, 'oneTimeDate not set', false);
//                    return false;
//                }
                
                $ignore=array('description');
                if(Validation::variableValidation($data,$ignore)){
                
                    $schedule = new Schedule($data);
                    
                    $today = date("Y-m-d");
                    $temp_date = strtotime($schedule->getOneTimeDate());
                    $date = date("Y-m-d",$temp_date);

                    if($today > $date){
                        echo $response->create(500, 'Invalid Date', false);
                        return false;
                    }

                    $temp_time1 = strtotime($schedule->getStartTime());
                    $temp_time2 = strtotime($schedule->getEndTime());

                    $startTime = date("H:i:s",$temp_time1);               
                    $endTime = date("H:i:s",$temp_time2);

                    if($startTime > $endTime){
                        echo $response->create(500, 'Please Select Valid Time Range', false);
                        return false;
                    }
                    
                    if($today == $date){
                        $timeZone = 'Asia/Colombo';
                        $dateObj = new DateTime();
                        $dateObj->setTimezone(new DateTimeZone($timeZone));

                        $currentTime = $dateObj->format('H:i:s');
                        
                        if($currentTime > $startTime){
                            echo $response->create(500, 'Please Select Valid Time', false);
                            return false;
                        }
                    }
                    
                    $success = ScheduleDAO::addOneDaySchedule($schedule);
                    if(is_string($success)) {
                        
                        if(date('Y-m-d') === $schedule->getOneTimeDate()){
                            $check_image = CheckpointDAO::getCheckImage($schedule->getCheckpoint_id());
                        
                            $title = "New Schedule";
                            $message = "You have new schedule today at ".date('h:i a', strtotime($schedule->getStartTime()));
                            $image = $check_image;

                            $go = array("main"=>'go');
                            
                            $push = new Push($title, $message, $image);
                            $push->setData($go);
                            PushDAO::SendMultiplePush($push, $schedule->getUser_id());
                        }
                        
                        echo $response->create(200, 'Success', true);
                        
                        $activity = new LogActivity();
                        $activity->setActivity('One Day Schedule Created');
                        $activity->setBy($_SESSION['ID']);
                        $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                        $activity->setUserType($_SESSION['role']);

                        $logResult = LogActivityDAO::addLog($activity);
                        
                    } elseif(is_array($success)) {
                        $response->setData($success);
                        echo $response->create(500, 'User is not available', false);
                    } else {
                        echo $response->create(500, 'Please carefully check the Start Time and End Time of the Schedule, if you selected a date, check that date also', false);
                    }
                } else {
                    echo $response->create(500, ' Missing Input fields ', false);
                }
                break;
            case 'reschedule':
                $response = new Response();

//                $schedule_s = json_decode(file_get_contents('php://input'), true);
                $oldUserID = $this->getRequest()->getData()['oldUser_id'];
                $newUserID = $this->getRequest()->getData()['newUser_id'];
                $sDate = $this->getRequest()->getData()['startDate'];
                $eDate = $this->getRequest()->getData()['endDate'];
                $status = $this->getRequest()->getData()['scheduleStatus'];
                
                if($sDate > $eDate){
                    echo $response->create(500, 'Invalid Date Range', false);
                    return false;
                }
                
                if(strlen($status)<=0){
                    echo $response->create(500, 'Please mention Permanent or Temporary', false);
                    return false;
                }
                
                if($status == 0){
                    if(empty($oldUserID) || empty($newUserID)){
                        echo $response->create(500, 'Missing Input Fields', false);
                        return false;
                    }
                } elseif ($status == 1) {
                    if(empty($oldUserID) || empty($newUserID) || empty($sDate) || empty($eDate) || empty($status)){
                        echo $response->create(500, 'Missing Input Fields', false);
                        return false;
                    }
                } else {
                    echo $response->create(500, 'Please mention Permanent or Temporary', false);
                    return false;
                }

                $schedule_s = ScheduleDAO::getSchedules($oldUserID,$sDate,$eDate);
                
                foreach ($schedule_s as $schedule){
                    
                    $reschedule = ScheduleDAO::getSchedule($schedule['ID'],$sDate,$eDate);
                    if(is_array($reschedule)){
                        echo $response->create(500, 'Already rescheduled', false);
                        return false;
                    }
                }
                
                $i=0;
                foreach ($schedule_s as $schedule){
                    $schedule_s[$i]['user_id'] = $newUserID;
                    $schedule_s[$i]['startDate'] = $sDate;
                    $schedule_s[$i]['endDate'] = $eDate;
                    $schedule_s[$i]['scheduleStatus'] = $status;
                    
                    $i++;
                }

                
                $invalidSchedules = ScheduleDAO::rescheduleValidation($schedule_s);
                if(is_array($invalidSchedules)) {
                    $response->setData($invalidSchedules);
                    echo $response->create(500, 'User is not available', false);
                    return false;
                }
                
                $success = array();
                
                foreach ($schedule_s as $data) {
                
                    $scheduleStatus = $data['scheduleStatus'];

                    if($scheduleStatus == 1) {
                        if(empty($data['startDate']) || empty($data['endDate'])) {
                            echo $response->create(500, '(startDate,endDate) Not Set OR Empty', false);
                            return false;
                        } else {
                            $today = date("Y-m-d");
                            $s_date = $data['startDate'];
                            $e_date = $data['endDate'];

                            if($s_date < $today || $e_date < $today) {
                                echo $response->create(500, 'Invalid start or end data', false);
                                return false;
                            }
                        }
                    }

                    $type = $data['repeatType_id'];

                    if($type == 1) {

                        $keys = array("ID","user_id","checkpoint_id","repeatType_id","startTime","endTime","scheduleStatus");

                        for($i=0;$i<count($keys);$i++){
                            if(!array_key_exists($keys[$i], $data)){
                                echo $response->create(500, 'Data not match with Repeat Type Daily', false);
                                return false;
                            }
                        }

                    }elseif ($type == 2) {

                        $days = array("sunday","monday","tuesday","wednesday","thursday","friday","saturday");
                        $count = Validation::arrayKeyExists($days, $data);
                        if($count == 0)
                        {                    
                            echo $response->create(500, 'this is not a Weekly Schedule', false);
                            return false;
                        }

                        $keys = array("ID","user_id","checkpoint_id","repeatType_id","startTime","endTime","scheduleStatus");

                        for($i=0;$i<count($keys);$i++){
                            if(!array_key_exists($keys[$i], $data)){
                                echo $response->create(500, 'Data not match with Repeat Type Weekly', false);
                                return false;
                            }
                        } 

                    } elseif ($type == 3) {

                        $keys = array("ID","user_id","checkpoint_id","repeatType_id","startTime","endTime","oneTimeDate","scheduleStatus");

                        for($i=0;$i<count($keys);$i++){
                            if(!array_key_exists($keys[$i], $data)){
                                echo $response->create(500, 'Data not match with Repeat Type One Day', false);
                                return false;
                            }
                        }

                    } else {
                        //future development
                    }


                    $newSchedule = new Schedule($data);
                    $success[] = ScheduleDAO::reSchedule($newSchedule);
                }

                if(in_array(false, $success,TRUE)) {
                    echo $response->create(500, 'DAO reSchedule function Error', false);
                    return false;
                } else {
                    
                    $activity = new LogActivity();
                    $activity->setActivity('Updated Schedule');
                    $activity->setBy($_SESSION['ID']);
                    $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                    $activity->setUserType($_SESSION['role']);

                    $logResult = LogActivityDAO::addLog($activity);
                    
                    echo $response->create(200, 'Success', true);
                    return true;
                }    
                break;               
            case 'delete':
                $response = new Response();
                
                $scheduleID = $this->getRequest()->getData()['ID'];
                $confirm = $this->getRequest()->getData()['confirm'];//1 :- approve, 2 :- not approve
                if(empty($scheduleID)){
                    echo $response->create(500, 'Schedule ID is required', false);
                    return false;
                }
                
                $works = WorkDAO::getWorkByScheduleID($scheduleID);
                if($works){                    
                    if(SessionManager::is_supervisor()){
                        echo $response->create(511, 'Some works are assigned to this schedule. In order to delete this schedule, contact system admin', false);
                        return false;
                    }
                    if(SessionManager::is_admin() && $confirm == 2){
                        echo $response->create(510, 'This will delete all works allocated to this Schedule. Continue?', false);
                        return false;
                    }
                }
                
                if(ScheduleDAO::deleteSchedule($scheduleID)) {
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'Something went wrong, Please try again', false);
                }
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function authenticate() {
//        SessionManager::checkTimeoutSession();
        switch ($this->getRequest()->getAction()) {
            case 'listSearch':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'single':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'staff':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'scheduleCheckpoints':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'createDaily':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'createWeekly':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'createOneDay':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'reschedule':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'delete':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'export':
                return true;
            default :
                if(SessionManager::validateSession()) {
                    return true;
                } else {
                    return false;
                }
                break;
        }
    }
}
