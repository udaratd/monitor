<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of reviewController
 *
 * @author user
 */
class ReviewController extends Controller {
    
    public function Render() {
        switch ($this->getRequest()->getType()) {
            case 'GET':
                $this->getHandler();
                break;
            case 'POST':
                $this->postHandler();
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function getHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'reviewActivity':
                $response = new Response();
                
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_role = $_SESSION['role'];
                
                if($loggedUser_role == 1){
                    $loggedUser_ID = false;
                }
                
                $pendingActivity = WorkDAO::getPendingWork($loggedUser_ID);
                
                if(is_array($pendingActivity)) {
                    if(count($pendingActivity) > 0) {
                        $response->setData($pendingActivity);
                        echo $response->create(200, 'Success', true);
                    } else {
                        echo $response->create(200, 'You dont have Pending Activity', true);
                    }
                } else {
                    //DAO getPendingWork function error
                    echo $response->create(500, 'Error', false);
                }
                
                break;
            case 'workCount':
                $response = new Response();
                
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_role = $_SESSION['role'];
                
                if($loggedUser_role == 1){
                    $loggedUser_ID = false;
                }
                
                $pendingActivity = WorkDAO::getPendingWorkCount($loggedUser_ID);
                
                if(is_array($pendingActivity)) {
                    if(count($pendingActivity) > 0) {
                        $response->setData($pendingActivity);
                        echo $response->create(200, 'Success', true);
                    } else {
                        echo $response->create(200, 'You dont have Pending Activity', true);
                    }
                } else {
                    //DAO getPendingWork function error
                    echo $response->create(500, 'Error', false);
                }
                
                break;
            default :
                Response::badRequest();
                break;
        }
    }
    
    public function postHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'update':
                $response = new Response();
                
                $status = null;
                $workID = $this->getRequest()->getData()['id'];
                $comment = $this->getRequest()->getData()['comment'];
                
                if(!isset($workID) || empty($workID)) {
                    echo $response->create(500, 'Work ID not Set', false);
                    return false;
                }
                
                switch ($this->getRequest()->getData()['status']) {
                    case 1://Rework
                        $status = "Rework";
                        break;
                    case 2://Flag
                        $status = "Flagged";
                        break;
                    case 3://Approve
                        $status = "Approved";
                        break;
                    default :
                        echo $response->create(500, 'Status not Set', false);
                        return false;
                        break;
                }
                
                if(WorkDAO::checkWork($workID)) {
                    if(WorkDAO::updateStatus($workID, $status, $comment)) {
                        
                        if($status === 'Rework' || $status === 'Flagged'){
                            
                            $workData = WorkDAO::singleWork($workID,false);

                            $userID = $workData['staffID'];

                            $title = $status." : ".$workData['loc_parentName'].' > '.$workData['loc_childName'];
                            $message = $workData['checkpointName'];
                            $image = $workData['w_img_thumbnail'];
                            
                            $workID = $workData['workID'];
                            $staffID = $workData['staffID'];
                            $scheduleID = $workData['scheduleID'];
                            $checkpointID = $workData['checkpointID'];
                            
                            $data = array();
                            if(!empty($checkpointID)){
                                $data['checkpoint_id'] = $checkpointID;
                            }
                            if(!empty($scheduleID)){
                                 $data['ID'] = $scheduleID;
                            }
                            if(!empty($workID)){
                                 $data['workID'] = $workID;
                            }
                            if(!empty($staffID)){
                                 $data['staffID'] = $staffID;
                            }
                            if(!empty($status)){
                                 $data['status'] = $status;
                            }
                            
                            $data['main'] = 'main';// Dont remove this keyword,keep this value in data array                          
                            
                            $push = new Push($title, $message, $image);
                            $push->setData($data);
                            PushDAO::SendMultiplePush($push, $userID);
                        }
                        
                        $activity = new LogActivity();
                        $activity->setActivity($status.' Work');
                        $activity->setBy($_SESSION['ID']);
                        $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                        $activity->setUserType($_SESSION['role']);

                        $logResult = LogActivityDAO::addLog($activity);
                        
                        echo $response->create(200, 'Success', true);
                    } else {
                        echo $response->create(500, 'Can not check this work', false);
                    }
                } else {
                    echo $response->create(500, 'Work not exists', false);
                }
                
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function authenticate() {
//        SessionManager::checkTimeoutSession();
        switch ($this->getRequest()->getAction()) {
            case 'reviewActivity':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'update':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'workCount':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                } else {
                    return false;
                }
                break;
            default :
                return false;
        }
    }
}
