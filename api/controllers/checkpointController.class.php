<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use chillerlan\QRCode\{QRCode, QROptions};

class checkpointController extends Controller {
    
    public function Render() {
        switch ($this->getRequest()->getType()) {
            case 'GET':
                $this->getHandler();
                break;
            case 'POST':
                $this->postHandler();
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }
    
    public function getHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'all':
                $response = new Response();
                
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_role = $_SESSION['role'];
                
                if($loggedUser_role == 1){
                    $loggedUser_ID = false;
                }
                
                $order = $this->getRequest()->getData()['order'];
                
                if(empty($order) || $order === null){
                    $order = false;
                } else {
                    if($order === "asc"){
                        $order = true;
                    } else {
                        $order = false;
                    }
                }
                
                $result = CheckpointDAO::listCkeckpoint($loggedUser_ID,$order);
                if($result){
                    $response->setData($result);
                    echo $response->create(200, 'Sucess', true);
                } else {
                    //DAO listCkeckpoint function Error
                    echo $response->create(500, 'No Data', false);
                }
                break;
            case 'search':
                $response = new Response();
                
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_role = $_SESSION['role'];
                
                if($loggedUser_role == 1){
                    $loggedUser_ID = false;
                }
                
                $data = $this->getRequest()->getData();
                
                $name = $data['name'];
                $locationid = $data['locationid'];
                $order = $data['order'];
//                $name = $this->getRequest()->getData()['name'];
//                $locationid = $this->getRequest()->getData()['locationid'];
//                $order = $this->getRequest()->getData()['order'];
                
                $pageNumber = $data['page_number'];
                $e_limit = $data['e_limit'];
                $chk_limit = $data['limit'];
                
                if(empty($order) || $order === null){                    
                    $order = false;
                } else {
                    
                    if($order === "asc"){                        
                        $order = true;
                    } else {
                        $order = false;
                    }
                }
                
                if(isset($name) && isset($locationid)) {
                    
                    if(empty($e_limit)){
                        $e_limit = 20;
                    } else {
                        $e_limit = intval($e_limit);
                    }

                    if(empty($pageNumber)){
                        $pageNumber = 1;
                    }

                    $s_limit = 0;
                    for($i=1;$i<$pageNumber;$i++){
                        $s_limit = $s_limit+$e_limit;
                    }

                    if( (($s_limit === 0) || (!empty($s_limit) && is_numeric($s_limit))) && !empty($e_limit) && is_numeric($e_limit)){
                        $s_limit = intval($s_limit);
                        $e_limit = intval($e_limit);
                    }

                    $countSearch = CheckpointDAO::searchCkeckpointCount($loggedUser_ID, $name, $locationid, $order);
//                    print_r($count);
                    if($countSearch){
                        $count = count($countSearch);
                    } else {
                        echo $response->create(500, 'No data', false);
                        return false;
                    }
//                    echo "\r\n";
//                    echo $e_limit;
//                    $numberOfPages = ceil($count['count']/$e_limit);
                    $numberOfPages = ceil($count/$e_limit);
                    
                    //if number of pages = 0 then set as 1
                    if(empty($numberOfPages)){
                        $numberOfPages = 1;
                    }

                    $meta['numberOfPages'] = $numberOfPages;
                    $meta['pageNumber'] = $pageNumber;

                    $limit = false;
                    if(strtolower($chk_limit) === 'all'){
                        $limit = true;
                    } else {
                        $limit = false;
                    }
                    
                    
                    $result = CheckpointDAO::searchCkeckpoint($loggedUser_ID,$name, $locationid, $order, $s_limit, $e_limit, $limit);
//                    print_r($result);
                    if($result) {
                        $output['checkpoints'] = $result;
                        $output['meta'] = $meta;
                        $response->setData($output);
                        echo $response->create(200, 'Success', true);
                    } else {
                        //DAO searchCheckpoint function Error or Not this kind of data in database
                        echo $response->create(500, 'No Data', false);
                    }
                } else {
                    echo $response->create(500, '(name,locationid) parameters not set', false);
                }
                break;
            case 'single':
                $response = new Response();
                $ID = $this->getRequest()->getData()['id'];
                $scheduleID = $this->getRequest()->getData()['scheduleID'];//only use for mobile application
                if(!empty($ID)) {
                    $result = CheckpointDAO::getCheckpoint($ID,$scheduleID);
                    if($result) {
                        $response->setData($result);
                        echo $response->create(200, 'Success', true);
                    } else {
                        echo $response->create(500, 'Invalid Checkpoint', false);
                    }
                } else {
                    //(id) parameter not det
                    echo $response->create(500, 'Missing Input fields', false);
                }
                break;
            case 'export':
                $response = new Response();
                CheckpointDAO::exportCheckpoint();
                break;
            case 'qr_export':
                $response = new Response();
                $ID_S = $this->getRequest()->getData()['ID'];
                if(empty($ID_S)){
                    $ID = array();
                } else {
                    $ID = explode(',', $ID_S);
                }
                
                CheckpointDAO::getCheckpointForQR($ID);
                break;
            case 'feed_qr_export':
                $response = new Response(); 
                $ID_S = $this->getRequest()->getData()['ID'];
                if(empty($ID_S)){
                    $ID = array();
                } else {
                    $ID = explode(',', $ID_S);
                }
                
                CheckpointDAO::getCheckpointForFeedbackQR($ID);
                break;
            case 'qr_test':
                $data = 'http://moni.domedia.lk/api/checkpoint/qr_test';
                echo '<img src="'.(new QRCode)->render($data).'" alt="QR Code" />';
                break;
            case 'sample':
                
                $link = S3_BUCKET_DOMAIN_SUPER.CHECKPOINT_SAMPLE;
                
                header("Location: $link");
                die();
                
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }
    
    public function postHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'add':
                $response = new Response();
                
                $checkName = $this->getRequest()->getData()['checkName'];
                $locationid = $this->getRequest()->getData()['locationid'];
                $description = $this->getRequest()->getData()['description'];
                $image = $this->getRequest()->getData()['image'];

                if(!empty($image)){
                    $imageValidate = ImageManager::checkImageSize($image);
                    
                    if($imageValidate < 1024 ){
                        echo $response->create(500, 'Invalid Image,Image resolution should at least 1024x768', false);
                        return false;
                    }
                }
                
                if(empty($checkName) || empty($locationid)){
                    echo $response->create(500, 'Missing Input fields', false);
                    return false;
                }
                


                $checkpoint = new Checkpoint($checkName, $locationid,$image);
                if(!empty($description)){
                    $checkpoint->setDescription($description);
                }

                if(CheckpointDAO::addCheckpoint($checkpoint)) {
                    
                    $activity = new LogActivity();
                    $activity->setActivity('Added Checkpoint');
                    $activity->setBy($_SESSION['ID']);
                    $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                    $activity->setUserType($_SESSION['role']);

                    $logResult = LogActivityDAO::addLog($activity);
                    
                    echo $response->create(200, 'Success', true);
                } else {
                    //DAO addCheckpoint function Error
                    echo $response->create(500, 'Unsuccess', false);
                }

                break;
            case 'update':
                $response = new Response();
                
                $ID = $this->getRequest()->getData()['id'];
                $checkpointName = $this->getRequest()->getData()['checkName'];
                $locationid = $this->getRequest()->getData()['locationid'];
                $description = $this->getRequest()->getData()['description'];
                $image = $this->getRequest()->getData()['image'];

                if(empty($checkpointName)&&empty($locationid)&&empty($description)&&empty($image)){
                    echo $response->create(500, 'Please set Data to Update', false);
                    return false;
                }
                
                if(!empty($image)){
                    $imageValidate = ImageManager::checkImageSize($image);
                    
                    if($imageValidate < 1024 ){
                        echo $response->create(500, 'Invalid Image,Image resolution should at least 1024x768', false);
                        return false;
                    }
                }
                
                if(empty($ID)){
                    echo $response->create(500, 'Please Set Checkpoint ID', false);
                    return false;
                }
                
                
                $checkpointExists = CheckpointDAO::checkpointExists($ID);
                if($checkpointExists === true) {

                    $checkpoint = new Checkpoint($ID,$checkpointName,$locationid,$image,$description);

                    if(CheckpointDAO::updateCheckpoint($checkpoint)) {
                        
                        $activity = new LogActivity();
                        $activity->setActivity('Updated Checkpoint');
                        $activity->setBy($_SESSION['ID']);
                        $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                        $activity->setUserType($_SESSION['role']);

                        $logResult = LogActivityDAO::addLog($activity);
                        
                        echo $response->create(200, 'Success', true);
                    } else {
                        //DAO updateCheckpoint function Error
                        echo $response->create(500, 'Unsuccess', false);
                    }
                } else {
                     echo $response->create(500, 'Checkpoint not Exists', false);
                }

                break;
            case 'delete':
                $response = new Response();
                
                $ID = $this->getRequest()->getData()['id'];
                $confirm = $this->getRequest()->getData()['confirm'];

                $delete_confirm = false;

                if($confirm == 0){

                    $schedule = ScheduleDAO::getScheduleAccordingToCheckpointID($ID);                   
                    
                    if(is_array($schedule)){
                        if(sizeof($schedule)>0){
                            $delete_confirm = false;
                        } else {
                            $delete_confirm = true;
                        }
                    } else {
                        $delete_confirm = true;
                    }
                } elseif($confirm == 1) {
                    $delete_confirm = true;
                } else {
                    echo $response->create(500, 'Missing input fields', false);
                }
                
                if($delete_confirm === true){
                    if(!empty($ID)) {
                        $checkpointExists = CheckpointDAO::checkpointExists($ID);

                        if($checkpointExists === true) {
                            if(CheckpointDAO::deleteCheckpoint($ID)) {
                                
                                $activity = new LogActivity();
                                $activity->setActivity('Deleted Checkpoint');
                                $activity->setBy($_SESSION['ID']);
                                $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                                $activity->setUserType($_SESSION['role']);

                                $logResult = LogActivityDAO::addLog($activity);
                                
                                echo $response->create(200, 'Success', true);
                            } else {
                                //DAO deleteCheckpoint function Error
                                echo $response->create(500, 'Unsuccess', false);
                            }
                        } else {
                            echo $response->create(500, 'Checkpoint not Exists', false);
                        }
                    } else {
                        //(id) Parameter NOT SET
                        echo $response->create(500, 'Missing Input fields', false);
                    }
                } else {
                    echo $response->create(201, 'This will delete all schedules and work that allocated to this Checkpoint', true);
                }
                break;
            case 'import':
                $response = new Response();
                
                $error = array();
                $data = $this->getRequest()->getData();
                
                $file = $data['checkpoint'];                
                
                $fileExtensionData = explode(";", $file,2);

                $replaceData = explode(",", $file,2);

                $fileExtension = strtolower(substr($fileExtensionData[0], strpos($fileExtensionData[0], "/")+1));

//                if($fileExtension === 'vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
//                    $extension = '.xlsx';
//                }
                if ($fileExtension === 'vnd.ms-excel') {
                    $extension = '.csv';
                } else {
                    echo $response->create(500, 'Only support CSV file format', false);
                    return false;
                }
                
                $fileContent = substr($fileExtensionData[1], strpos($fileExtensionData[1], ",")+1);
                
                $fileName = 'checkpoint';                

                $path = UPLOAD_CHECK_DIR;
                $filePath = FileManager::fileUpload($path, $fileName.$extension, $fileContent, $replaceData[0]);
                
                $checkpoints = fopen($filePath, 'r');
                
                $i=0;
                while ($row = fgetcsv($checkpoints)){
                    if($i == 0){
                        if($row[0] !== 'Checkpoint Name' || $row[1] !== 'Location ID' || $row[2] !== 'Description'){
                            echo $response->create(500, 'Only support CSV file format', false);
                            return false;
                        }                    
                    } else {
                        $success = checkpointController::importByCSV($row[0], $row[1], $row[2]);
                        if($success === true){
                            
                        } else {
                            $row[3] = $success;
                            array_push($error, $row);
                        }
                    }
                    $i++;
                }
                fclose($checkpoints);
                
                if(!empty($error)){
                    $errorFile = checkpointController::exportCheckpoint($error);
                    $response->setData($errorFile);
                    //code 510 for error list                    
                    if(count($errorFile) == $i-1){
                        echo $response->create(500, 'Please Carefully check data of the selected document', false);
                    } else {
                        echo $response->create(510, 'Unsuccess', false);
                    }
                } else {
                    echo $response->create(200, 'Success', true);
                }
                break;
            case 'add_bulk':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                if(!isset($data['count'])){
                    echo $response->create(500, 'Please send checkpoint count', false);
                    return false;
                }
                if(empty($data['count'])){
                    echo $response->create(500, 'How many checkpoints do you want to create?', false);
                    return false;
                }
                
                $count = intval($data['count']);
                if($count <= 0){
                    echo $response->create(500, 'How many checkpoints do you want to create?', false);
                    return false;
                }
                
                $locationID = 0;
                if(isset($data['locationid'])){
                    if(!empty($data['locationid'])){
                        $locationID = $data['locationid'];
                    }
                }
                
                if($locationID == 0){
                    $locationObj = new Location();
                    $locationObj->setName('default-child');
                    
                    $defaultLocationData = LocationDAO::getDefaultData($locationObj);
                    if($defaultLocationData){
                        $locationID = $defaultLocationData['ID'];
                    }
                }
                
                $checkpointObj = new Checkpoint();
                $checkpointObj->setChekPoinName('default');
                $checkpointObj->setLocationid($locationID);
                
                $addBulkCheckpoints = CheckpointDAO::addBulkCheckpoint($checkpointObj, $count);
                if($addBulkCheckpoints){
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'Something went wrong!, Please try again', false);
                }
                break;
            default :
                echo Response::badRequest(); 
                break;
        }
    }

    public function authenticate() {
        switch ($this->getRequest()->getAction()) {
            case 'all':
                return true;
                break;
            case 'single':
                if(SessionManager::is_admin() || SessionManager::is_supervisor() || SessionManager::is_staff()) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'search':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'add':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'update':
                if(SessionManager::is_admin() || SessionManager::is_supervisor() || SessionManager::is_staff()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'delete':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'add_bulk':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'export':
                return true;
            case 'qr_export':
                return true;
            case 'feed_qr_export':
                return true;
            case 'qr_test':
                return false;
            case 'import':
                return true;
            default :
                if(SessionManager::validateSession()) {
                    return true;
                } else {
                    return false;
                }                 
                break;
        }
    }
    
    private static function importByCSV($checkName,$locationid,$description){

        $image = null;
        
        if(empty($checkName) || empty($locationid)){
            return 'Missing Input Fields';
        }

        $checkpoint = new Checkpoint($checkName, $locationid,$image);
        if(!empty($description)){
            $checkpoint->setDescription($description);
        }

        if(CheckpointDAO::addCheckpoint($checkpoint)) {
            return true;
        } else {
            return 'Unsuccess';
        }

    }
    
    private static function exportCheckpoint($result){
        try {  
            $data = array();
            foreach ($result as $one){
                $errors['name'] = $one[0];
                $errors['location_id'] = $one[1];
                $errors['description'] = $one[2];
                $errors['fail_reason'] = $one[3];
                
                array_push($data, $errors);
            }
            return $data;
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
}