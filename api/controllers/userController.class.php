<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class UserController extends Controller{
    
    public function Render() {
        switch ($this->getRequest()->getType()) {
            case 'GET':
                $this->getHandler();
                break;
            case 'POST':
                $this->postHandler();
                break;
            default:
                echo Response::badRequest();
                break;
        }
    }
    
    public function getHandler(){
        switch ($this->getRequest()->getAction()) {
            case 'search':
                $response = new Response();
                /**
                 * this value can be user firstName, lastName, nic, phone 
                 */
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_role = $_SESSION['role'];
                
                if($loggedUser_role == 1){
                    $loggedUser_ID = false;
                }
                
                $data = $this->getRequest()->getData();
                
                $value = $data['value'];
                $jobRole = $data['role'];
//                $value = $this->getRequest()->getData()['value'];
//                $jobRole = $this->getRequest()->getData()['role'];
                
                $pageNumber = $data['page_number'];
                $e_limit = $data['e_limit'];
                $user_limit = $data['limit'];
                
                if(isset($value) && isset($jobRole)) {
                    
                    if(empty($e_limit)){
                        $e_limit = 20;
                    } else {
                        $e_limit = intval($e_limit);
                    }

                    if(empty($pageNumber)){
                        $pageNumber = 1;
                    }

                    $s_limit = 0;
                    for($i=1;$i<$pageNumber;$i++){
                        $s_limit = $s_limit+$e_limit;
                    }

                    if( (($s_limit === 0) || (!empty($s_limit) && is_numeric($s_limit))) && !empty($e_limit) && is_numeric($e_limit)){
                        $s_limit = intval($s_limit);
                        $e_limit = intval($e_limit);
                    }

                    $count = UserDAO::searchUserCount($loggedUser_ID,$value,$jobRole);

                    $numberOfPages = ceil($count['count']/$e_limit);
                    
                    //if number of pages = 0 then set as 1
                    if(empty($numberOfPages)){
                        $numberOfPages = 1;
                    }

                    $meta['numberOfPages'] = $numberOfPages;
                    $meta['pageNumber'] = $pageNumber;

                    $limit = false;
                    if(strtolower($user_limit) === 'all'){
                        $limit = true;
                    } else {
                        $limit = false;
                    }
                    
                    $result = UserDAO::searchUser($loggedUser_ID,$value,$jobRole,$s_limit,$e_limit,$limit);
                    if($result) {
                        $output['users'] = $result;
                        $output['meta'] = $meta;
                        $response->setData($output);
                        echo $response->create(200, 'Success', true);
                    } else {
                        //UserDAO searchUser function Error
                        echo $response->create(500, 'No Data', false);
                    }
                } else {
                    //(value,role) parameter NOT SET, parameter can be firstName,lastName,nic,phone
                    echo $response->create(500, 'Missing Input Data', false);
                }
                break;
            case 'single':
                $response = new Response();
                
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_role = $_SESSION['role'];
                
                if($loggedUser_role == 1){
                    $loggedUser_ID = false;
                }
                
                $ID = $this->getRequest()->getData()['id'];
                if(isset($ID)) {
                    $result = UserDAO::getUser($loggedUser_ID,$ID);
                    if(is_array($result) && count($result)>0) {
                        $response->setData($result);
                        echo $response->create(200, 'Success', true);
                    } else {
                        //UserDAO getUser function Error OR Not this kind of data in database
                        echo $response->create(500, 'No Data', false);
                    }
                } else {
                    //(jobRole) parameter NOT SET
                    echo $response->create(500, 'Missing Input Data', false);
                }
                break;
            case 'listsupervisor':
                $response = new Response();
                $result = UserDAO::getSupervisor();
                if($result) {
                    $response->setData($result);
                    echo $response->create(200, 'Success', true);
                } else {
                    //UserDAO getUser function Error OR Not this kind of data in database
                    echo $response->create(500, 'No data', false);
                }
                break;
            case 'singleStaffprofile':
                $response = new Response();
                
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_role = $_SESSION['role'];
                
                if($loggedUser_role == 1){
                    $loggedUser_ID = false;
                }
                
                $staffID = $this->getRequest()->getData()['id'];

                $staffProfile_checkpoints = UserDAO::singleUserWithCheckpoint($staffID,$loggedUser_ID,$loggedUser_role);
                
                if($staffProfile_checkpoints) {
                    $response->setData($staffProfile_checkpoints);
                    echo $response->create(200, 'Success', true);
                } else {
                    //check DAO singleUserWithCheckpoint function
                    echo $response->create(500, 'No Data', false);
                }
                break;
            case 'validateSession':
                $response = new Response();
                
                if(SessionManager::validateSession()){
                    if($_SESSION['role'] != 100){
                        $response->setData($_SESSION);
                        echo $response->create(200, 'Success', true);
                    } else {
                        SessionManager::loguot();
                        echo $response->create(500, 'Unsuccess', false);
                    }
                } else {
                    echo $response->create(500, 'Unsuccess', false);
                }
                break;
            case 'logout':
                $response = new Response();
                if(count($_SESSION)==0){
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(200, 'Unsuccess', true);
                }
                break;
            case 'roles':
                $response = new Response();
                $roles = UserDAO::getUserRole();
                if($roles){
                    $response->setData($roles);
                    echo $response->create(200, 'Success', true);
                } else {
                    $response->create(500, "Unsuccess", false);
                }
                break;
            case 'export':
                UserDAO::exportUser();
                break;
            case 'checkLink':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                if(empty($data['id'])){
                    echo $response->create(500, 'ID is required', false);
                    return false;
                }
                if(empty($data['token'])){
                    echo $response->create(500, 'Token is required', false);
                    return false;
                }
                if(empty($data['username'])){
                    echo $response->create(500, 'Company username is required', false);
                    return false;
                }
                
                $username = $data['username'];
                
                $admin = new Admin();
                $admin->setUsername($username);
                
                $findCompany = AdminDAO::getSingleByUsername($admin);
                if(!$findCompany){
                    echo $response->create(500, 'Company not found', false);
                    return false;
                } else {
                    $dbname = $findCompany['dbname'];
                }
                
                $ID = $data['id'];
                $token = $data['token'];
                
                $resetData = UserDAO::getResetData($ID,$dbname);

                if($resetData){
                    $secret = hash('sha256', $resetData['secret']);

                    if($token === $secret){
                        $reset_d_t = date_create($resetData['date_time']);
                        $current_d_t = date_create(date('Y-m-d H:i:s'));
                        
                        if($current_d_t < $reset_d_t){
                            echo $response->create(500, 'Invalid Link', false);
                            return false;
                        }
                        $dateTimeDiff = date_diff($current_d_t, $reset_d_t, false);

                        $hours = $dateTimeDiff->format('%h');
                        
                        if(intval($hours) >= 1){
                            echo $response->create(500, 'Link expired', false);
                        } else {
                            echo $response->create(200, 'Success', true);
                        }
                    } else {
                        echo $response->create(500, 'Invalid Token', false);
                        return false;
                    }
                } else {
                    echo $response->create(500, 'Unsuccess', false);
                }
                break;
            case 'sample':
                
                $link = S3_BUCKET_DOMAIN_SUPER.USER_SAMPLE;
                
                header("Location: $link");
                die();
                
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }
    
    public function postHandler(){
        switch ($this->getRequest()->getAction()) {
            case 'add':
                $response=new Response();
                
                $success = null;
                
                $fName = $this->getRequest()->getData()['firstName'];
                $lName = $this->getRequest()->getData()['lastName'];
                $email = $this->getRequest()->getData()['email'];
                $nic = $this->getRequest()->getData()['nic'];
                $phone = $this->getRequest()->getData()['phone'];
                $password = $this->getRequest()->getData()['password'];
                $role = $this->getRequest()->getData()['jobRole'];
                $image = $this->getRequest()->getData()['imageString'];
                $supervisorID = $this->getRequest()->getData()['supervisor_id'];
                $note = $this->getRequest()->getData()['note'];
                
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_role = $_SESSION['role'];
                
                if(!empty($image)){
                    $imageValidate = ImageManager::checkImageSize($image);
                    
                    if($imageValidate < 500 ){
                        echo $response->create(500, 'Invalid Image,Image resolution should at least 500x400', false);
                        return false;
                    }
                }
                
                if($loggedUser_role == 2 && $role == 1){
                    echo $response->create(500, 'Not Authorized to add Admin', false);
                    return false;
                }
                if($loggedUser_role == 2 && $role == 2){
                    echo $response->create(500, 'Not Authorized to add Supervisor', false);
                    return false;
                }
                
                if(empty($fName) || empty($lName) || empty($email) || empty($nic) || empty($password) || empty($role)){
                    echo $response->create(500, 'Missing input fields', false);
                    return false;
                }

                    
                if( Validation::emailValidation($email) && Validation::nicValidation($nic) ) {

                    if(UserDAO::checkUserNIC($nic)=== false) {

                        $id = UserDAO::checkUser($email);

                        if($id === false) {

                            $user=new User();
                            $user->setFName($fName);
                            $user->setLName($lName);
                            $user->setEmail($email);
                            $user->setNic($nic);
                            $user->setPhone($phone);
                            $user->setPassword($password);
                            $user->setRole($role);
                            $user->setImage($image);
                            $user->setNote($note);

                            if($loggedUser_role == 1 && $role == 3){//new add part $role
                                if(empty($supervisorID)){
                                    echo $response->create(500, 'Please Set Supervisor ID', false);
                                    return false;
                                } else {
                                    $user->setSupervisorID($supervisorID);
                                }
                            }
                            if($loggedUser_role == 2){
                                $user->setSupervisorID($loggedUser_ID);
                            }

                            if(!empty($phone)) {
                               if(Validation::phoneValidation($phone)) {
                                   $success = true;
                                   $user->setPhone($phone);
                               } else {
                                   $success = false;
                                   echo $response->create(500, 'Invalid Phone number', false);
                               }
                            }
                            
                            if($success === null || $success === true) {
                                if(UserDAO::addUser($user)){
                                    
                                    $activity = new LogActivity();
                                    $activity->setActivity('Added User');
                                    $activity->setBy($_SESSION['ID']);
                                    $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                                    $activity->setUserType($_SESSION['role']);

                                    $logResult = LogActivityDAO::addLog($activity);
                                    
                                    echo $response->create(200, 'Sucess', true);
                                }else{
                                    //UserDAO::addUser function Error
                                    echo $response->create(500, 'Unsuccess', false);
                                }
                            }

                        } else {
                            echo $response->create(500, 'Already registered', false);
                        }
                    } else {
                        echo $response->create(500, 'Already used this NIC number', false);
                    }
                } else {
                    echo $response->create(500, 'Email or NIC not valid', false);
                }

                break;        
            case 'login'://check expiry date and update automatically
                $response = new Response();
                
                session_unset();// Before Login to the System, Unset All session variables
                
                $nic = $this->getRequest()->getData()['nic'];
                $password = $this->getRequest()->getData()['password'];
                $token = $this->getRequest()->getData()['token'];
                $username = $this->getRequest()->getData()['username'];
                $companyName = null;
                if(empty($_SESSION['DB_NAME'])){
 
                    $admin = new Admin();
                    $admin->setUsername($username);
                    
                    if(empty($admin->getUsername())){
                        echo $response->create(500, 'Username is required', false);
                        return false;
                    }
                    
                    $findCompany = AdminDAO::getSingleByUsername($admin);//if anything happen please remove company database name from DAO class of this function

                    if($findCompany){
                        
                        if($findCompany['active'] == 2){
                            if(!empty($token) && $token !== null){
                                echo $response->create(500, 'Please Contact Administrator', false);
                            } else {
                                echo $response->create(500, 'Please Make the payment', false);
                            }
                            return false;
                        }
                        
                        $allBucket = FileManager::listAllBuckets();
                        if($allBucket){
                            if(!in_array($findCompany['bucket_name'], $allBucket)){
                                echo $response->create(500, 'Bucket not exist', false);
                                return false;
                            }
                        }
                        
                        $allDatabases = AdminDAO::showDatabases();//if anything happen please remove company database name from DAO class of this function
                        if($allDatabases){
                            if(!in_array($findCompany['dbname'], $allDatabases)){
                                echo $response->create(500, 'Database not exist', false);
                                return false;
                            }
                        }
                        
                        $companyName = $findCompany['name'];
                        
                        SessionManager::createSessionVar('COMPANY_NAME', $findCompany['username']);
                        SessionManager::createSessionVar('DB_NAME', $findCompany['dbname']);
                        SessionManager::createSessionVar('S3_BUCKET', $findCompany['bucket_name']);
                        SessionManager::createSessionVar('COMPANY_ID', $findCompany['ID']);
                        SessionManager::createSessionVar('DEBUG_ENABLE_DISABLE', $findCompany['debug']);
                        SessionManager::createSessionVar('SKIP_VALIDATION', $findCompany['skip_validation']);
                        
                    } else {
                        echo $response->create(403, 'Incorrect company 1', false);//mobile app use this code
                        return false;
                    }
                } else {
                    echo $response->create(403, 'Incorrect company 2', false);//mobile app use this code
                    return false;
                }

                if(!empty($nic)&&!empty($password)) {
                    $result = UserDAO::checkLogin($nic, $password);
                    if($result){
                        
                        
                        if(!empty($token) && $token !== null){
                            $userID = $result['ID'];
                            
                            $devices = MobileDAO::checkDevice($token);
                            
                            if($devices){
                                
                                if($devices['user_id'] != $userID){
                                    if(MobileDAO::deleteDeviceByToken($token)){
                                        MobileDAO::addDevice($userID, $token);
                                    } 
                                }
                                
                            } else {
                                
                                $userData = MobileDAO::checkUser($userID);
                                
                                if($userData){                                  
                                    if(MobileDAO::deleteDevice($userData['ID'])){
                                        MobileDAO::addDevice($userID, $token);
                                    }                                    
                                } else {
                                    MobileDAO::addDevice($userID, $token);
                                }   
                            }
                        }
                        
                        $result['companyUsername'] = $username;
                        $result['companyName'] = $companyName;
                        $result['debug'] = $findCompany['debug'];
                        $result['logo'] = $findCompany['logo'];
                        $result['skip_validation'] = $findCompany['skip_validation'];
                        $response->setData($result);
                        
//                        ini_set('session.gc_maxlifetime', 86400);
//                        ini_set('session.cookie_lifetime', 86400);
                        
                        SessionManager::createSession($result['ID'],$result['role'],$result['email']);
                        
                        $activity = new LogActivity();
                        $activity->setActivity('Logged in to system');
                        $activity->setBy($_SESSION['ID']);
                        $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                        $activity->setUserType($_SESSION['role']);

                        $logResult = LogActivityDAO::addLog($activity);                        
                        echo $response->create(200, 'Sucess', true);
                    } else {
                        echo $response->create(500, 'Incorrect NIC or Password', false);
                    }
                } else {
                    echo $response->create(500, 'Missing input fields', false);
                }
                break;
            case 'update':
                $response=new Response();
                
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_Role = $_SESSION['role'];
                
                $ID = $this->getRequest()->getData()['id'];
                $fName = $this->getRequest()->getData()['firstName'];
                $lName = $this->getRequest()->getData()['lastName'];
                $email = $this->getRequest()->getData()['email'];
                $nic = $this->getRequest()->getData()['nic'];
                $phone = $this->getRequest()->getData()['phone'];
                $password = $this->getRequest()->getData()['password'];
//                $role = $this->getRequest()->getData()['jobRole'];
                $image = $this->getRequest()->getData()['imageString'];
                $supervisorID = $this->getRequest()->getData()['supervisor_id'];
                $note = $this->getRequest()->getData()['note'];
                
                if(empty($fName)&&empty($lName)&&empty($email)&&empty($nic)&&empty($phone)&&empty($image)&&empty($lName)&&empty($password)){
                    echo $response->create(500, 'Please set Data to Update', false);
                    return false;
                }
                
                $success = null;
                $empty = null;
                
                if(!empty($image)){
                    $imageValidate = ImageManager::checkImageSize($image);

                    if($imageValidate < 500 ){
                        echo $response->create(500, 'Invalid Image,Image resolution should at least 500x400', false);
                        return false;
                    }
                }
                
                if(!empty($ID)) {
                    
                    $userExists = UserDAO::checkUserID($ID);
                    
                    if($userExists === true) {
                        
                        
                        if(!empty($email)){
                            
                            if(Validation::emailValidation($email)) {

                                $e_id = UserDAO::checkUser($email);//email can not be duplicated

                                if( ($e_id === false) || ($e_id === $ID) ) {

                                } else {
                                    echo $response->create(500, 'Already used this email', false);
                                    return false;
                                }
                            } else {
                                echo $response->create(500, 'Invalid Email', false);
                                return false;
                            }
                        }
                        if(!empty($nic)){
                            
                            if(Validation::nicValidation($nic)){
                                
                                $n_id = UserDAO::checkUserNIC($nic);//nic can not be duplicated
                                
                                if(($n_id === false) || ($n_id === $ID)){
                                    
                                } else {
                                    echo $response->create(500, 'Already used this nic', false);
                                    return false;
                                }
                            } else {
                                echo $response->create(500, 'Invalid NIC', false);
                                return false;
                            }
                        }
                            
                        if(!empty($phone)){
                            if(Validation::phoneValidation($phone)) {
                                
                            } else {
                                echo $response->create(500, 'Invalid Phone', false);
                                return false;
                            }
                        }  
                                
                        $user = new User();
                        $user->setId($ID);
                        $user->setFName($fName);
                        $user->setLName($lName);
                        $user->setEmail($email);
                        $user->setNic($nic);
                        $user->setPhone($phone);
                        $user->setPassword($password);
//                        $user->setRole($role);
                        $user->setImage($image);
                        $user->setSupervisorID($supervisorID);
                        $user->setNote($note);

                        if(UserDAO::updateUser($user,$loggedUser_ID,$loggedUser_Role)){
                            
                            $activity = new LogActivity();
                            $activity->setActivity('Updated user');
                            $activity->setBy($_SESSION['ID']);
                            $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                            $activity->setUserType($_SESSION['role']);

                            $logResult = LogActivityDAO::addLog($activity);
                            
                            echo $response->create(200, 'Sucess', true);
                        }else{
                            echo $response->create(500, 'Unsuccess', false);
                        }
                        
                    } else {
                        echo $response->create(500, 'User not Exists', false);
                    }
                } else {
                    echo $response->create(500, 'Please Set User ID', false);
                }
                
                
                break;
            case 'delete':
                $response = new Response();
                $id = $this->getRequest()->getData()['id'];

                if(!empty($id)) {
                    
                    $userData = UserDAO::getUser(false, $id);
                    $deletingUserRole = $userData['role'];
                    
                    $loggedUser_ID = $_SESSION['ID'];
                    $loggedUser_role = $_SESSION['role'];

                    if($loggedUser_role == 1 && $deletingUserRole == 1){
                        $admin = UserDAO::getUserAccordingToRole($deletingUserRole);
                        if(is_array($admin)){
                            if(sizeof($admin)>1){
                                
                            } else {
                                echo $response->create(500, 'Not Authorized to Delete Default Admin', false);
                                return false;
                            }
                        }
                    }
                    if($loggedUser_role == 1 && $deletingUserRole == 2){
                        $allocatedSupervisor = UserDAO::getAlocatedSupervisor($id);
                        if(is_array($allocatedSupervisor)){
                            echo $response->create(501, 'First update staff under this supervisor', false);
                            return false;
                        }                        
                    }
                    if($loggedUser_role == 2 && $deletingUserRole == 1){
                        echo $response->create(500, 'Not Authorized to Delete Admin', false);
                        return false;
                    }
                    if($loggedUser_role == 2 && $deletingUserRole == 2){
                        echo $response->create(500, 'Not Authorized to Delete Supervisor', false);
                        return false;
                    }

                    $userExists = UserDAO::checkUserID($id);
                    if($userExists === true) {
                        if(UserDAO::deleteUser($id)) {
                            
                            $activity = new LogActivity();
                            $activity->setActivity('Deleted User');
                            $activity->setBy($_SESSION['ID']);
                            $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                            $activity->setUserType($_SESSION['role']);

                            $logResult = LogActivityDAO::addLog($activity);
                            
                            echo $response->create(200, 'Success', true);
                        } else {
                            //UserDAO deleteUser function Error
                            echo $response->create(500, 'Unsuccess', false);
                        }
                    } else {
                        echo $response->create(500, 'User not Exists', false);
                    }
                } else {
                    echo $response->create(500, 'Missing user id', false);
                }
                break;
            case 'updateField':
                $response = new Response();
                
                $ID = null;
                $fieldName = null;
                $value = null;
                
                $data = $this->getRequest()->getData();

                if(Validation::variableValidation($data)) {
                    
                    $count = 0;
                    foreach ($data as $name => $val){

                        if($count == 0){
                            $ID = $val;
                        }
                        if($count == 1){
                            $fieldName = $name;
                            $value = $val;
                        }
                        $count++;
                    }
                    
                    $userExists = UserDAO::checkUserID($ID);
                    
                    if($userExists === true) {
                        if( ($ID !== null) && ($fieldName !== null) && ($value !== null) ){
                            if(UserDAO::updatField($ID, $fieldName, $value)) {
                                echo $response->create(200, 'Success', true);
                            } else {
                                //DAO updateField function Error
                                echo $response->create(500, 'Unsuccess', false);
                            }
                        } else {
                            //(id,field,value) parameters Not SET or Empty
                            echo $response->create(500, 'Missing input fields', false);
                        }
                    } else {
                        echo $response->create(500, 'No such User', false);
                    }
                } else {
                    //(id,field,value) parameters Not SET or Empty
                    echo $response->create(500, 'Missing input fields', false);
                }
                break;
            case 'changePassword':
                $response = new Response();
                
                $ID = $this->getRequest()->getData()['id'];
                $oldPass = $this->getRequest()->getData()['oldPass'];
                $newPass = $this->getRequest()->getData()['newPass'];
                
                if(empty($ID) || empty($oldPass) || empty($newPass)){
                    echo $response->create(500, 'Missing input fields', false);
                    return false;
                }
    
                $userExists = UserDAO::changePassword($ID);

                if($userExists === true) {
                    
                    $old_hash = hash('sha256', $oldPass);
                    if($userExists['password'] == $old_hash){
                    
                        if(UserDAO::changePassword($ID, $newPass)) {
                            echo $response->create(200, 'Success', true);
                        } else {
                            //DAO changePassword function Error
                            echo $response->create(500, 'Unsuccess', false);
                        }
                    } else {
                        echo $response->create(500, 'Current password seems to be incorrect!!', false);
                    }
                } else {
                    echo $response->create(500, 'No such User', false);
                }
                break;
            case 'logout':
                $response = new Response();
                if(count($_SESSION)==0){
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(200, 'Unsuccess', true);
                }
                break;
            case 'import':
                $response = new Response();
                
                $error = array();
                $data = $this->getRequest()->getData();
                $file = $data['user'];                

                $fileExtensionData = explode(";", $file,2);

                $replaceData = explode(",", $file,2);

                $fileExtension = strtolower(substr($fileExtensionData[0], strpos($fileExtensionData[0], "/")+1));

                // Set support to EXCEL Format
//                if($fileExtension === 'vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
//                    $extension = '.xlsx';
//                }
                if ($fileExtension === 'vnd.ms-excel') {
                    $extension = '.csv';
                } else {
                    echo $response->create(500, 'Only support CSV file format', false);
                    return false;
                }
                
                $fileContent = substr($fileExtensionData[1], strpos($fileExtensionData[1], ",")+1);
                
                $fileName = 'user';                

                $path = UPLOAD_USER_DIR;
                $filePath = FileManager::fileUpload($path, $fileName.$extension, $fileContent, $replaceData[0]);
                
                $user = fopen($filePath, 'r');
                $i=0;
                while ($row = fgetcsv($user)){
                    if($i == 0){
                        if($row[0] !== 'First Name' || $row[1] !== 'Last Name' || $row[2] !== 'Email' || $row['3'] !== 'NIC' || $row[4] !== 'Phone' || $row[5] !== 'Password' || $row[6] !== 'Job Role' || $row[7] !== 'Supervisor ID'){
                            echo $response->create(500, 'Only support CSV file format', false);
                            return false;
                        }                    
                    } else {
                        $success = UserController::importByCSV($row[0], $row[1], $row[2], $row[3], $row[4], $row[5], $row[6], $row[7]);
                        if($success === true){
                            
                        } else {
                            $row[8] = $success;
                            array_push($error, $row);
                        }
                    }
                    $i++;
                }
                fclose($user);
                
                if(!empty($error)){
                    $errorFile = UserController::exportUser($error);
                    $response->setData($errorFile);
                    //code 510 for error list
                    if(count($errorFile) == $i-1){
                        echo $response->create(500, 'Please Carefully check data of the selected document, Eg :- Job Role', false);
                    } else {
                        echo $response->create(510, 'Unsuccess', false);
                    }                   
                } else {
                    echo $response->create(200, 'Success', true);
                }
                
                break;
            case 'resetpasswordrequest':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                
                if(empty($data['email'])){
                    echo $response->create(500, 'E-mail is required', false);
                    return false;
                }
                if(empty($data['username'])){
                    echo $response->create(500, 'Company username is required', false);
                    return false;
                }
                
                if(!Validation::emailValidation($data['email'])){
                    echo $response->create(500, 'A valid E-mail is required', false);
                    return false;
                }
                $username = $data['username'];
                
                $admin = new Admin();
                $admin->setUsername($username);
                
                $findCompany = AdminDAO::getSingleByUsername($admin);
                if(!$findCompany){
                    echo $response->create(500, 'Company not found', false);
                    return false;
                } else {
                    $dbname = $findCompany['dbname'];
                }
                $email = $data['email'];
                
                $singleUser = UserDAO::getPasswordResetData($email,$dbname);
                
                if($singleUser){
                    $user_id = $singleUser['ID'];
                    $email = $singleUser['email'];
                    $secret = rand(1000, 9999);
                    $name = $singleUser['firstName'].' '.$singleUser['lastName'];

                    $addResetDetails = UserDAO::addResetPassword($user_id, $email, $secret, $dbname);
                    if($addResetDetails){
                        $subject = 'Reset Password Instructions';
                        $ID = $addResetDetails;
                        $token = hash('sha256', $secret);

                        $httpHost = UserController::createDomain();

                        $title = 'Hello '.$singleUser['firstName'].',';
                        $message = "http://".$httpHost."/login/".$username."?action=reset&id=".$ID."&token=".$token;

                        $sendEmail = UserController::sendResetEmail($title,$message, $email, $name, $subject, $findCompany);

                        if($sendEmail){
                            echo $response->create(200, 'Success', true);
                        } else {
                            echo $response->create(500, 'Try Again', false);
                        }
                    } else {
                        echo $response->create(500, 'Try Again', false);
                    }
                }
                break;
            case 'resetpassword':
                $response = new Response();
                
                $data = $this->getRequest()->getData();

                if(empty($data['id'])){
                    echo $response->create(500, 'ID is required', false);
                    return false;
                }
                if(empty($data['token'])){
                    echo $response->create(500, 'Token is required', false);
                    return false;
                }
                if(empty($data['password'])){
                    echo $response->create(500, 'password is required', false);
                    return false;
                }
                if(empty($data['username'])){
                    echo $response->create(500, 'Company username is required', false);
                    return false;
                }
                
                $ID = $data['id'];
                $token = $data['token'];
                $password = $data['password'];
                
                $username = $data['username'];
                
                $admin = new Admin();
                $admin->setUsername($username);
                
                $findCompany = AdminDAO::getSingleByUsername($admin);
                if(!$findCompany){
                    echo $response->create(500, 'Company not found', false);
                    return false;
                } else {
                    $dbname = $findCompany['dbname'];
                }
                
                $resetData = UserDAO::getResetData($ID,$dbname);

                if($resetData){
                    $secret = hash('sha256', $resetData['secret']);

                    if($token === $secret){
                        $reset_d_t = date_create($resetData['date_time']);
                        $current_d_t = date_create(date('Y-m-d H:i:s'));
                        
                        if($current_d_t < $reset_d_t){
                            echo $response->create(500, 'Try Again Redirect', false);
                            return false;
                        }
                        $dateTimeDiff = date_diff($current_d_t, $reset_d_t, false);

                        $hours = $dateTimeDiff->format('%h');
                        
                        if(intval($hours) >= 1){
                            echo $response->create(500, 'Link is expired', false);
                            return false;
                        } else {

                            $userDetails = UserDAO::checkUser($resetData['email'],$dbname);

                            if(!$userDetails){
                                echo $response->create(500, 'User not found', false);
                                return false;
                            }
//                            if(!Validation::validatePassword($password)){
//                                echo $response->create(500, 'Password must contain at least 6 characters, including UPPER/lowercase and numbers', false);
//                                return false;
//                            }
                
                            $user_id = $userDetails;

                            $resetSuccess = UserDAO::changePassword($user_id,$password,$dbname);

                            if($resetSuccess){
                                
                                $result = UserDAO::getUser(false,$user_id,$dbname);
                                
                                $response->setData($result);
                                echo $response->create(200, 'Success', true);
                            } else {
                                echo $response->create(500, 'Try Again', false);
                            }
                        }
                    } else {
                        echo $response->create(500, 'Invalid Token', false);
                        return false;
                    }
                }
                
                break;
            default:
                echo Response::badRequest();
                break;
        }
    }
    
    public function authenticate() {
        switch ($this->getRequest()->getAction()) {
            case 'login':
                return true;
            case 'changePassword':                
                if(SessionManager::is_admin() || SessionManager::is_supervisor() || SessionManager::is_staff()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'search':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'single':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'singleStaffprofile':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'add':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'update':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'updateField':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'delete':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                }else {
                    return false;
                }
                break;
            case 'logout':
                SessionManager::loguot();
                return true;               
            case 'validateSession':
                return true;                
            case 'roles':
                return true;               
            case 'export':
                return true;               
            case 'import':
                return true;               
            case 'resetpasswordrequest':
                return true;               
            case 'resetpassword':
                return true;               
            case 'checkLink':
                return true;               
            default :
                if(SessionManager::validateSession()) {
                    return true;
                } else {
                    return false;
                }
                break;
        }
    }
    
    private static function createDomain(){
        if(isset($_SERVER['HTTP_HOST'])){
            return $_SERVER['HTTP_HOST'];
        } else {
            return false;
        }
    }
    
    private static function importByCSV($fName,$lName,$email,$nic,$phone,$password,$role,$supervisorID){              

        $loggedUser_ID = $_SESSION['ID'];
        $loggedUser_role = $_SESSION['role'];


        if($loggedUser_role == 2 && $role === 1){
            return 'Not Authorized to add Admin';
        }
        if($loggedUser_role == 2 && $role === 2){
            return 'Not Authorized to add Supervisor';
        }
        if(empty($fName) || empty($lName) || empty($email) || empty($nic) || empty($password) || empty($role)){
            return 'Missing input fields';
        }
        if($role == 1 || $role == 2 || $role == 3){
            
        } else {
            return 'Please select valid user role';
        }

        if( Validation::emailValidation($email) && Validation::nicValidation($nic) ) {

            $nicValidation = UserDAO::checkUserNIC($nic);
            if($nicValidation === false) {

                $id = UserDAO::checkUser($email);

                if($id === false) {

                    $user=new User();
                    $user->setFName($fName);
                    $user->setLName($lName);
                    $user->setEmail($email);
                    $user->setNic($nic);
                    $user->setPhone($phone);
                    $user->setPassword($password);
                    $user->setRole($role);
                    
                    if($loggedUser_role == 1 && $role == 3){                        
                        if(empty($supervisorID)){
                            return 'Please Set Supervisor ID';
                        } else {
                            $checkSupervisor = UserDAO::checkSupervisor($supervisorID);
                            if($checkSupervisor){
                                if($checkSupervisor['role'] === 2){                                                                
                                    $user->setSupervisorID($supervisorID);
                                } else {                                
                                    return 'Invalid Supervisor ID';
                                }
                            } else {
                                return 'Invalid Supervisor ID';
                            }
                        }
                    }
                    if($loggedUser_role == 2){
                        $user->setSupervisorID($loggedUser_ID);
                    }

                    if(!empty($phone)) {
                       if(Validation::phoneValidation($phone)) {
                           $user->setPhone($phone);
                       } else {
                           return 'Invalid Phone number';                              
                       }
                    }
                    
                    if(UserDAO::addUser($user)){
                        return true;
                    }else{
                        return 'Unsuccess';
                    }
                } else {
                    return 'Already registered';
                }
            } else {
                return 'Already used this NIC number';
            }
        } else {
            return 'Email or NIC not valid';
        }
    }
    
    private static function exportUser($result){
        try {  
            
            $data = array();
            foreach ($result as $one){
                $errors['f_name'] = $one[0];
                $errors['l_name'] = $one[1];
                $errors['email'] = $one[2];
                $errors['nic'] = $one[3];
                $errors['phone'] = $one[4];
                $errors['password'] = $one[5];
                $errors['role'] = $one[6];
                $errors['supervisor_id'] = $one[7];
                $errors['fail_reason'] = $one[8];
                
                array_push($data, $errors);
            }
            return $data;
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    private static function sendResetEmail($title,$message,$recipientEmail,$recipientName,$subject,$findCompany){
        
//        $details = CompanyDAO::getCompanyID();
//        $ID = $details[0]['ID'];
//
//        $company = new Company();
//        $company->setId($ID);
//
//        $businessDetails = CompanyDAO::getCompany($company->getId());

        $businessDetails['companyName'] = $findCompany['name'];
        $businessDetails['logo'] = $findCompany['logo'];
        $businessDetails['email'] = $findCompany['email'];
        
        ob_start();
        $emailTemplate = new EmailTemplates();
        $emailTemplate->header($title, $businessDetails);
        $emailTemplate->resetLink($message);
        $emailTemplate->footer($businessDetails);
        $html = ob_get_clean();
        
        $result = EmailManager::sendEmail($businessDetails['email'], $businessDetails['name'], $recipientEmail, $recipientName, $subject, $html);
        
        return $result;
    }
}