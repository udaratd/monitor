<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class workController extends Controller {
    
    public function Render() {
        switch ($this->getRequest()->getType()) {
            case 'GET':
                $this->getHandler();
                break;
            case 'POST':
                $this->postHandler();
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function getHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'pendingids':
                $response = new Response();
                
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_role = $_SESSION['role'];
                
                if($loggedUser_role == 1){
                    $loggedUser_ID = false;
                }
                
                $workIDs = WorkDAO::getPendingWorkIDs($loggedUser_ID);
                if($workIDs) {
                    $response->setData($workIDs);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No such data', false);
                }
                
                break;
            case 'all':
                $response = new Response();
                
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_role = $_SESSION['role'];
                
                if($loggedUser_role == 1){
                    $loggedUser_ID = false;
                }
                
                $staffID = $this->getRequest()->getData()['staff_id'];
                $locationID = $this->getRequest()->getData()['location_id'];
                $status = $this->getRequest()->getData()['status'];
                $serchValue = $this->getRequest()->getData()['search'];
                $s_date = $this->getRequest()->getData()['s_date'];
                $e_date = $this->getRequest()->getData()['e_date'];
                $order = $this->getRequest()->getData()['order'];
                $pageNumber = intval($this->getRequest()->getData()['page_number']);
                $e_limit = $this->getRequest()->getData()['e_limit'];
                $chk_limit = $this->getRequest()->getData()['limit'];
                
                if(empty($order) || $order === null){
                    $order = false;
                } else {
                    if($order === "asc"){
                        $order = true;
                    } else {
                        $order = false;
                    }
                }
                
                /////////////////new////////////
                if(empty($e_limit)){
                    $e_limit = 20;
                } else {
                    $e_limit = intval($e_limit);
                }

                if(empty($pageNumber)){
                    $pageNumber = 1;
                }

                $s_limit = 0;
                for($i=1;$i<$pageNumber;$i++){
                    $s_limit = $s_limit+$e_limit;
                }

                if( (($s_limit === 0) || (!empty($s_limit) && is_numeric($s_limit))) && !empty($e_limit) && is_numeric($e_limit)){
                    $s_limit = intval($s_limit);
                    $e_limit = intval($e_limit);
                }
                ////////////////////new//////////////////
                
                if(empty($s_date) || empty($e_date)) {
                    
                    $today = date("Y-m-d");
                    $date = date_create($today);
                    $s_date = date_sub($date, date_interval_create_from_date_string("6 days"));
                    
                    
                    $s_date = date_format($s_date,"Y-m-d");
                    $e_date = $today;
                }
                
                $countSearch = WorkDAO::listOriginalWorkListCount($loggedUser_ID, $serchValue, $s_date, $e_date, $staffID, $locationID, $status, $order);
                    
                if($countSearch){
                    $count = count($countSearch);
                } else {
                    echo $response->create(500, 'No data', false);
                    return false;
                }

                $numberOfPages = ceil($count/$e_limit);
                
                if(empty($numberOfPages)){
                    $numberOfPages = 1;
                }
                
                $meta['numberOfPages'] = $numberOfPages;
                $meta['pageNumber'] = $pageNumber;

                $limit = false;
                if(strtolower($chk_limit) === 'all'){
                    $limit = true;
                } else {
                    $limit = false;
                }
                
                $result = WorkDAO::listOriginalWorkList($loggedUser_ID, $serchValue, $s_date, $e_date, $staffID, $locationID, $status, $order, $s_limit, $e_limit, $limit);
                if(is_array($result)) {
                    
                    $output['worklist'] = $result;
                    $output['meta'] = $meta;
                    
                    $response->setData($output);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No data', false);
                }
                break;
            case 'single':
                $response = new Response();
                
                $loggedUser_ID = $_SESSION['ID'];
                $loggedUser_role = $_SESSION['role'];
                
                if($loggedUser_role == 1){
                    $loggedUser_ID = false;
                }
                
                $workID = $this->getRequest()->getData()['id'];
                
                if(!isset($workID) || empty($workID)) {
                    echo $response->create(500, 'Work ID not Set', false);
                    return false;
                }
                
                $workData = WorkDAO::singleWork($workID,$loggedUser_ID);
                if($workData) {
                    //This output has a key name called scheduleStatus, scheduleStatus = 0 normal schedule, scheduleStatus = 2 QR schedule, scheduleStatus = 1 original schedule but reworked
                    $response->setData($workData);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No such data', false);
                }
                
                break;           
            case 'deviceinfo':
                $response = new Response();
                
                $data = $this->getRequest()->getData();
                if(empty($data['id'])){
                    echo $response->create(500, 'Work ID is required', false);
                    return false;
                }
                
                $workID = $data['id'];
                
                $deviceInfo = MobileDAO::getDeviceInfo($workID);
                if($deviceInfo) {
                    $info = json_decode($deviceInfo['info']);
                    $deviceInfo['info'] = $info;
                    $response->setData($deviceInfo);
                    echo $response->create(200, 'Success', true);
                } else {
                    echo $response->create(500, 'No such data', false);
                }
                
                break;           
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function postHandler() {
        switch ($this->getRequest()->getAction()) {
            case 'delete':
                $response = new Response();
                
                $workID = $this->getRequest()->getData()['id'];
                
                if(!isset($workID) || empty($workID)) {
                    echo $response->create(500, 'Work ID not Set', false);
                    return false;
                }
                
                if(WorkDAO::checkWork($workID)) {
                    if(WorkDAO::deleteWork($workID)) {
                        
                        $activity = new LogActivity();
                        $activity->setActivity('Deleted Work');
                        $activity->setBy($_SESSION['ID']);
                        $activity->setBusiness_id($_SESSION['COMPANY_ID']);
                        $activity->setUserType($_SESSION['role']);

                        $logResult = LogActivityDAO::addLog($activity);
                        
                        echo $response->create(200, 'Success', true);
                    } else {
                        echo $response->create(500, 'Unsuccess', false);
                    }
                } else {
                    echo $response->create(500, 'Work not exists', false);
                }
                
                break;
            default :
                echo Response::badRequest();
                break;
        }
    }

    public function authenticate() {
//        SessionManager::checkTimeoutSession();
        switch ($this->getRequest()->getAction()) {
            case 'all':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'single':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'delete':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                } else {
                    return false;
                }
                break;           
            case 'pendingids':
                if(SessionManager::is_admin() || SessionManager::is_supervisor()) {
                    return true;
                } else {
                    return false;
                }
                break;           
            case 'deviceinfo':
                if(SessionManager::is_admin()) {
                    return true;
                } else {
                    return false;
                }
                break;           
            default :
                if(SessionManager::validateSession()) {
                    return true;
                } else {
                    return false;
                }
                break;
        }
    }
}

