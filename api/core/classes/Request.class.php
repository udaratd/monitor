<?php
/**
 * Request Class - Represents Incoming Requests coming thought API. 
 * 
 * @author DoMedia Team<hello@domedia.lk>
 * @package ApiKit
 * @version 1.0
 * @copyright (c) 2017, DoMedia.lk
 */
class Request {
    private $controller;
    private $action;
    private $data;
    private $type;
    
    /**
     * Constructor 
     * @param String $controller Name of the Controller 
     * @param String $action Action of the Controller 
     * @param array $data Aditional Data related to Request
     * @since 1.0
     */
    function __construct($controller, $action,Array $data) {
        $this->controller = $controller;
        $this->action = $action;
        $this->data = $data;
        $this->type=REQUEST_TYPE;
    }
    
    
    function getType() {
        return $this->type;
    }

    function setType($type) {
        $this->type = $type;
    }

    function getController() {
        return $this->controller;
    }

    function getAction() {
        return $this->action;
    }

    function getData() {
        return $this->data;
    }

    function setController($controller) {
        $this->controller = $controller;
    }

    function setAction($action) {
        $this->action = $action;
    }

    function setData($data) {
        $this->data = $data;
    }

}
