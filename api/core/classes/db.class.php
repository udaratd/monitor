<?php
/**
 * PDO Database Class 
 * This class is a wrapper for PHP PDO
 * @package ApiKit
 * @author DoMedia <thilina@domedia.lk>
 * @version 1.0
 */
class database{
	private $host = DB_HOST;
	private $user = DB_USER;
	private $pass = DB_PASSWORD;
	private $dbname = DB_NAME;

	private $dbh; //database handler
	private $error;
	private $stmt; //statement (quary)

	/**
         * Constructor
         */
	public function __construct($db_name = null){
            
            if(!empty($db_name)){
                $this->dbname = $db_name;
            }

            // Set Database Source name
            $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
            // Set options
            $options = array(
                PDO::ATTR_PERSISTENT    => true,
                PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION
            );
            // Create a new PDO instanace
            try{
                $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
            }
                    // Catch any errors
            catch(PDOException $e){
                $this->error = $e->getMessage();
            }
	}

        public function createDatabase($database_name){
            try {

                $dbh_1 = new PDO("mysql:host=$this->host", $this->user, $this->pass,
                        array(
                            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                        )
                );

                $dbh_1->exec("CREATE DATABASE $database_name");

                
                $dbh_2 = new PDO("mysql:host=$this->host;dbname=$database_name", $this->user, $this->pass,
                        array(
                            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                        )
                );
                
                $sql = file_get_contents('http://moni.doweby.net/api/database/dev_moni.sql');

                    
                $dbh_2->exec($sql);
                

            } catch (PDOException $e) {
                die("DB ERROR: ". $e->getMessage());
            }
        }

                /**
         * Sets the Query 
         * @param String $query Valid SQL Query
         * @return void
         * @since 1.0
         */
	public function query($query){
		//set quary to variable $stmt
		$this->stmt = $this->dbh->prepare($query);
	}


        /**
         * Binds Data into the request (ex: bind('name','saman')
         * 
         * @param String $param Paramiter name you passed to sql quary 
         * @param String $value Actual value of the paramiter 
         * @param String $type (optional) Types :PARAM_INT,PARAM_BOOL,PARAM_NULL,PARAM_STR
         * @return void
         * @since 1.0
         */
	public function bind($param, $value, $type = null){
		if (is_null($type)) {
			switch (true) {
				case is_int($value):
					$type = PDO::PARAM_INT;
					break;
				case is_bool($value):
					$type = PDO::PARAM_BOOL;
					break;
				case is_null($value):
					$type = PDO::PARAM_NULL;
					break;
				default:
					$type = PDO::PARAM_STR;
			}
		}
		$this->stmt->bindValue($param, $value, $type);
	}

        /**
         * Executes previousy set quary and returns TRUE on sucess FALSE on Failure
         * @return bool TRUE on success or FALSE on failure.
         * @since 1.0
         */
	public function execute(){
		return $this->stmt->execute();
	}

        /**
         * Returns an array containing all of the result set rows
         * 
         * @return array  returns an array containing all of the remaining rows in the result set.
         * @since 1.0
         */
	public function resultset(){
		$this->execute();
		return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
	}

        
        /**
         * Fetches the next row from a result set
         * @return mixed Associative Array on success FALSE on Failure.
         * @since 1.0
         */
	public function single(){
		$this->execute();
		return $this->stmt->fetch(PDO::FETCH_ASSOC);
	}

        /**
         * Returns the number of rows affected by the last SQL statement 
         * @return int Returns the number of rows.
         * @since 1.0
         */
	public function rowCount(){
		return $this->stmt->rowCount();
	}

        /**
         * Returns the ID of the last inserted row or sequence value
         * @return string returns a string representing the row ID of the last row that was inserted into the database.
         * @since 1.0
         */
	public function lastInsertId(){
		return $this->dbh->lastInsertId();
	}

        /**
         * Initiates a transaction
         * @return bool Returns TRUE on success or FALSE on failure.
         * @since 1.0
         */
	public function beginTransaction(){
		return $this->dbh->beginTransaction();
	}

        /**
         * Commits/Ends a transaction
         * @return bool Returns TRUE on success or FALSE on failure.
         * @since 1.0
         */
	public function endTransaction(){
		return $this->dbh->commit();
	}

        /**
         * Rolls back a transaction
         * @return bool Returns TRUE on success or FALSE on failure.
         * @since 1.0
         */
	public function cancelTransaction(){
		return $this->dbh->rollBack();
	}

        /**
         * Dump an SQL prepared command
         * @return void 
         * @since 1.0
         */
	public function debugDumpParams(){
		return $this->stmt->debugDumpParams();
	}
        
        /**
         * Returns Errors 
         * @return String
         */
        function getError() {
            return $this->error;
        }

        function getDbname() {
            return $this->dbname;
        }

        function setDbname($dbname) {
            $this->dbname = $dbname;
        }

}

?>