<?php
/**
 * System Class
 *
 * @author Domedia
 */
class System {
    public static $log;
    public static function log(Log $log){
        System::$log=$log;
        $logD=new LogDAO();
        if($logD->addLog($log)){
            return true;
        }
        return false;
    }
 
}
