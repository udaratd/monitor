<?php
/**
 * Response - Handles Responses of the API
 * 
 * @author DoMedia Team <hello@domedia.lk>
 * @package ApiKit
 * @version 1.0
 * 
 */
class Response {
    private $json;
    private $outPutArr;
    private $statusCode;
    private $statusMsg;
    private $success;
    private $data;
    private $response;
    
    /**
     * Constructor 
     * @param array $arr Data Object that needs to be sent as a response
     */
    function __construct($arr=null) {
        if($arr){
            $this->data=$arr;
            
        }
        $this->outPutArr=array();
        $this->response=array();
        
    }
    
    /**
     * Create Response based on given data and return Json String
     * 
     * @param int $statusCode HTTP Response Code 
     * @param String $statusMsg Human Readable message of the response.
     * @param bool $success TRUE on Sucess FALSE on fail.
     * @return String JSON represantaion of the Response Object
     * @since 1.0
     */
    function create($statusCode,$statusMsg,$success){
        $this->statusCode=$statusCode;
        $this->statusMsg=$statusMsg;
        $this->success=$success;
        
        $this->response["statusCode"]=$this->statusCode;
        $this->response["statusMsg"]=$this->statusMsg;
        $this->response["success"]=$this->success;
        
        $this->outPutArr['response']=$this->response;
        if($this->data){
            $this->outPutArr['data']=$this->data;
        }
        
        //insert System Log Data
        if($this->getSystemLog()){
            $this->outPutArr['error']=$this->getSystemLog();
        }
        
        $this->json=  json_encode($this->outPutArr,JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        
        return $this->json;
        
        
        
    }
    function getSystemLog(){
        $log=System::$log;
        if($log){
            return $log->getArray();
        }else{
            return false;
        }
    }
    
    /**
     * Generated Common Bad Request Response
     * @return String JSon String of Bad Request
     * @since 1.0
     */
    public static function badRequest(){
        $response=new Response();
        return $response->create(400, 'Bad request', false);
    }
    
    
    function getJson() {
        return $this->json;
    }

    function getOutPutArr() {
        return $this->outPutArr;
    }

    function setJson($json) {
        $this->json = $json;
    }

    function setOutPutArr($outPutArr) {
        $this->outPutArr = $outPutArr;
    }
    function getStatusCode() {
        return $this->statusCode;
    }

    function getStatusMsg() {
        return $this->statusMsg;
    }

    function getSuccess() {
        return $this->success;
    }

    function getData() {
        return $this->data;
    }

    function getResponse() {
        return $this->response;
    }

    function setStatusCode($statusCode) {
        $this->statusCode = $statusCode;
    }

    function setStatusMsg($statusMsg) {
        $this->statusMsg = $statusMsg;
    }

    function setSuccess($success) {
        $this->success = $success;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setResponse($response) {
        $this->response = $response;
    }

}
