<?php
/**
 * Base Controller Class
 * @author DoMedia Team <hello@domedia.lk>
 * @version 1.0
 * @copyright (c) 2017, DoMedia.lk
 */
abstract class Controller{
    
    /* @var $request Request */
    private $request;
    abstract function Render();
    
    /**
     * Constructor (Superclass)
     * @param Request $request Request object
     */
    function __construct(Request $request) {
        $this->request=$request; 
    }
    
    /**
     * Get Request 
     * @return Request Returns Request object of the Controller
     * @since 1.0
     */
    function getRequest(){
        return $this->request;
    }
    
    /**
     * Default request handler 
     */
    function defaultHandler(){
        return Response::badRequest();
    }
    
    /**
     * Authenticates Requests
     * 
     * @return Bool Returns true on success and false on failure
     * @since 1.0.1
     */
    function authenticate(){
        if(SessionManager::validateSession()){
            return true;
        }else{
            return false;
        }
    }
}

