<?php
/**
 * Framework Functions 
 * @package ApiKit
 * @since 1.0
 */


/**
 * Outputs data inside <pre></pre> tags
 * 
 * @param String|Array $data
 * @since 1.0
 * @deprecated 1.0
 */
function preOut($data){
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}


/**
 * Converts Object to array
 * @param type $data
 * @return type
 */
function object_to_array($data)
{
    if (is_array($data) || is_object($data))
    {
        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = object_to_array($value);
        }
        return $result;
    }
    return $data;
}

/**
 * Health Check
 * @return Array Array Of errors or true on sucess
 */
function healthCheck(){
    $errors=array();
    $database = 'database';
    //check database connection. 
   if(class_exists($database)){
       $dbcheck=new database();
       if($dbcheck->getError()){
           array_push($errors, $dbcheck->getError());
       }
   }else{
       array_push($errors,"database Class not found");
   }

   return $errors;
    
}

/**
 * Includes Given SDK
 * @param type $sdk Name Of the SDK
 * @return boolean True on sucess False On fail
 */
function include_SDK($sdk){
    if(in_array($sdk, $GLOBALS['SDKS'])){
        include_once SDK_DIR.$sdk.'/autoloader.php';
        return true;
    }else{
        return false;
    }
}
