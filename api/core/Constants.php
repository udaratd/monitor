<?php
/**
 * This file defines all Constants of APIKIT framework
 */

//Moved here from Framework->init function
define("DS", DIRECTORY_SEPARATOR);
define("TAB", '&#9;');
define("CONTROLLER_PATH", "controllers");

// Define platform, controller, action,
$path=explode("/",parse_url($_SERVER['REQUEST_URI'])['path']);

define("CONTROLLER", isset($path[2]) ? $path[2] : null);

define("ACTION", isset($path[3]) ? $path[3]: null);

define("CURR_CONTROLLER_PATH", CONTROLLER_PATH.DS);
define("CLASS_DIR", "classes".DS);

if(php_sapi_name()=='cli'){
    define("REQUEST_TYPE", 'CLI');
}elseif (php_sapi_name() == 'cgi-fcgi') {
    define("REQUEST_TYPE", 'CGI_FCGI');
} else{
    define("REQUEST_TYPE", filter_input(INPUT_SERVER, 'REQUEST_METHOD'));
}

//end moved codes

define("MODULE_DIR", "core/Modules".DS);

define("SDK_DIR", "sdks".DS);

/**
 * Log Types 
 */
define('LOG_ERROR','ERROR');
define('LOG_WARN','WARNING');
define('LOG_NOTICES','NOTICE');
define('LOG_EXCEPTION','EXCEPTION');
define('LOG_CRITICAL','CRITICAL');//define new log type called critical
