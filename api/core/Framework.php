<?php
/**
 * Framework
 * 
 * @package ApiKit
 * @author Domedia Team<hello@domedia.lk>
 * @copyright (c) 2017, DoMedia.lk
 * @since 1.0
 * 
 */
class Framework {

    /**
     * Constructor 
     */
   public static function run() {
       self::init();
       self::core_autoload();
       self::before_autoload();
       self::autoload();
       self::check_Controller_Action();
       self::Load_SDKs();
       self::dispatch(); //send off to the destination

   }
   
   /**
    * Run Shell 
    */
    public static function run_shell() {
      //self::init();
       self::core_autoload();
       self::before_autoload();
       self::autoload();
       //self::check_Controller_Action();
       self::Load_SDKs();
       self::shell_dispatch(); //send off to the destination
       
       

   }
   
   /**
    * Intialize Framework Constants and starts Session
    * @since 1.0
    * @return void
    */
   private static function init() {
       //Start session
        $lifetime=3600;
//        session_start();
        setcookie(session_name(),session_id(),time()+$lifetime,'/');
       
//       $currentCookieParams = session_get_cookie_params(); 
//  
//
//        $rootDomain = $_SERVER['HTTP_HOST']; 
//
//        session_set_cookie_params( 
//            $currentCookieParams["lifetime"], 
//            $currentCookieParams["path"], 
//            $rootDomain, 
//            $currentCookieParams["secure"], 
//            $currentCookieParams["httponly"] 
//        ); 
//
//        session_name('mysessionname'); 
//        session_start(); 
//
//        setcookie($cookieName, $cookieValue, time() + 3600, '/', $rootDomain); 
    
   }

   /**
    * Core Autoload Function
    * Loads Framework core
    * @since 1.0
    * @return void
    */
   private static function core_autoload(){
       //Load Framework Core Classes
       spl_autoload_register('Framework::CoreAutoloader');
   }
   /**
    * Autoload Function
    * 
    * @since 1.0
    * @return void
    */
   private static function autoload() {
       
       //Load Framework Modules
       self::ModuleLoader();
       //Register PHP autoloader
       spl_autoload_register('Framework::Autoloader');
       
   }
   
   /**
    * Core Autoloader Function
    * 
    * @since 1.0
    * @param String $className
    */
   private static function CoreAutoloader($className) 
   {
        $Class_file = "core/classes/".$className.'.class.php';
        if(file_exists($Class_file)){
            include $Class_file;
        }
   }
   
   /**
    * Module Autoloader Function
    * 
    * @since 1.0
    * @param String $className
    */
   private static function ModuleLoader(){
        $directories = glob(MODULE_DIR.'*' , GLOB_ONLYDIR);
        foreach($directories as $dir)
        {
           $module=explode('/',$dir); 
           if(in_array(end($module),$GLOBALS['MODULES'])){
                $file = sprintf('%s/init.php', $dir);
                if(is_file($file)) 
                {
                    include_once $file;
                } 
           }

        }
       
   }
   
   /**
    * Autoloader Function
    * 
    * @since 1.0
    * @param String $className
    */
   private static function Autoloader($className) 
   {
        $Controller_file = CURR_CONTROLLER_PATH.$className.'.class.php';
        $Class_file = CLASS_DIR.$className.'.class.php';
        if(file_exists($Controller_file)){
            include $Controller_file;
        }else if(file_exists($Class_file)){
            include $Class_file;
        }else{
            //echo '<pre>Class File not Exisits :'.$className.'</pre><br>';
            //echo 'Path :'.$Class_file."<br>";
        }
   }

   /**
    * Before autoloading developer classes
    * $return Void
    */
private static function before_autoload(){
        //system health checks 
        $health_check=healthCheck();
        if(!empty($health_check)){
            //health check fail. 
            $r=new Response();
            $r->setData($health_check);
            echo $r->create(500, "Health Check Failed", false); 
            die();
            
        }
        
        //check SDKS
        $sdks=$GLOBALS['SDKS'];
        foreach ($sdks as $sdk) {
            if(!file_exists(SDK_DIR.$sdk.'/autoloader.php')){
                $r=new Response();
                $r->setData("SDK Not Found : ".$sdk);
                echo $r->create(500, "Health Check Failed", false); 
                die();
            }
        }
        
        //check Modules 
        
        $modules=$GLOBALS['MODULES'];
        foreach ($modules as $module) {
            if(!file_exists(MODULE_DIR.$module.'/init.php')){
                $r=new Response();
                $r->setData("Module Not Found : ".$module);
                echo $r->create(500, "Health Check Failed", false); 
                die();
            }
        }
        
}

   /**
    * Dispatch - Sends the request to the corresponding controller and handle request
    * 
    * @since 1.0
    * @return void
    */
    private static function dispatch(){
        $controller_class= strtolower(CONTROLLER)."Controller";
        $response=new Response();
        
       if(REQUEST_TYPE=="POST"){
           $requestData=$_POST;
       }else if(REQUEST_TYPE=='GET'){
           $requestData=$_GET;
       }else{
           echo $response->create(500, "Invalid Request, Not POST or GET", false);           
       }
        
        if(class_exists($controller_class)){
            $request=new Request(CONTROLLER, ACTION, $requestData);
            $controller = new $controller_class($request);
            if($controller->authenticate()){
                $controller->Render();
            }else{
                echo $response->create(401, "Not Authorized", false);
            }
        }else{
            echo $response->create(500, "Not Supported yet", false);
        }
        
    }
    
    /**
    * Shell Dispatch - Sends the request to the corresponding controller and handlerequest
    * 
    * @since 2.0
    * @return void
    */
    private static function shell_dispatch(){
        $controller_class= strtolower($_SERVER['argv'][1])."Controller";
        $response=new Response();

       
        $requestData=explode(',', $_SERVER['argv'][3]);
       
        
        if(class_exists($controller_class)){
            $request=new Request($_SERVER['argv'][1], $_SERVER['argv'][2], $requestData);
            $controller = new $controller_class($request);
            if($controller->authenticate()){
                $controller->Render();
            }else{
                echo $response->create(500, "Not Authorized", false);
            }
        }else{
            echo $response->create(500, "Not Supported yet", false);
            echo  $controller_class ." Class Not Eixists";
        }  
    }
    
    private static function check_Controller_Action() {
        if(empty(CONTROLLER) || empty(ACTION)) {
            $response = new Response();
            echo $response->create(500, "Invalid URL", false);
            die();
        }
    }
    
    /**
     * Load 3rd Party SDKs
     * 
     */
    private static function Load_SDKs(){
        $directories = glob(SDK_DIR.'*' , GLOB_ONLYDIR);
         foreach($directories as $dir)
        {
             $sdk=explode('/',$dir);
             if(in_array(end($sdk),$GLOBALS['SDKS'])){
                $file = sprintf('%s/autoloader.php', $dir);

                if(is_file($file)) 
                {
                    include_once $file;
                } 
             }

        }
    }

}