<?php

class EmailManager {

    public static function sendEmail($senderEmail,$senderName,$recipientEmail,$recipientName,$subject,$message){
        $mail = new Email();
        $mail->setTo($recipientEmail, $recipientName);
        $mail->setSubject($subject);
        $mail->setFrom('no-reply@'.$senderEmail, $senderName);
        $mail->addMailHeader('Reply-To','no-reply@'.$senderEmail,$senderName);
//        $mail->addGenericHeader('X-Mailer', 'PHP/'.phpversion());
        $mail->addGenericHeader('Content-Type', 'text/html; charset="utf-8"');
        $mail->setMessage($message);
        $mail->setWrap(100);

        $send = $mail->send();

        return ($send) ? true : false;
    }
    
    public static function sendEmailwithAttachment($senderEmail,$senderName,$recipientEmail,$recipientName,$subject,$message,$attachment){
        $mail = new Email();
        $mail->setTo($recipientEmail, $recipientName);
        $mail->setSubject($subject);
        $mail->setFrom('no-reply@'.$senderEmail, $senderName);
        $mail->addMailHeader('Reply-To','no-reply@'.$senderEmail,$senderName);
//        $mail->addGenericHeader('Content-Type', 'text/html; charset="utf-8"');
        $mail->addAttachment($attachment);
        $mail->setMessage($message);
        $mail->setWrap(100);

        $send = $mail->send();

        return ($send) ? true : false;
    }
}

