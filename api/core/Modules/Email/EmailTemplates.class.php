<?php

class EmailTemplates {
    
    function __construct() {
        
    }

    public function defaultTemplate($title,array $businessData, array $data = null, $message = null,$link=false){
        
        $booking = $data;
        ?>
        <html>
        <head>

            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Time Me</title>

        </head>
        <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
            <div id="wrapper" dir="ltr" style="background-color: #ffffff; margin: 0; padding: 70px 0 70px 0; -webkit-text-size-adjust: none !important; width: 100%;">
                <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                    <tr>
                        <td align="center" valign="top">
                            <div id="template_header_image">
                                    <p style="margin-top: 0;"><img src="<?php echo $businessData['logo'];?>" alt="Time Me" style="border: none; display: inline-block; font-size: 14px; font-weight: bold; height: auto; outline: none; text-decoration: none; text-transform: capitalize; vertical-align: middle; margin-right: 0; width: 150px; height: auto; max-height: 100px;"></p>						
                            </div>
                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container" style="box-shadow: 0 1px 4px rgba(0,0,0,0.1) !important; background-color: #ffffff; border: 1px solid #e5e5e5; border-radius: 3px !important;">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- Header -->
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header" style='background-color: #fff; border-radius: 3px 3px 0 0 !important; color: #3c3c3c; border-bottom: 1px solid #eee; font-weight: bold; line-height: 100%; vertical-align: middle; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; border-top: 5px solid #0088CC;'>
                                            <tr>
                                                <td id="header_wrapper" style="padding: 36px 48px; display: block;">
                                                    <h1 style='color: #3c3c3c; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 30px; font-weight: 300; line-height: 150%; margin: 0; text-align: center; text-shadow: none;'>

                                                        <?php echo $title;?>

                                                    </h1>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- End Header -->
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- Body -->
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
                                            <tr>
                                                <td valign="top" id="body_content" style="background-color: #ffffff; padding-bottom: 30px;">
                                                    <!-- Content -->
                                                    <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                    <?php
                                                            if($message !== null){
                                                    ?>
                                                            <tr>
                                                            <?php
                                                                    if($link === true){
                                                            ?>
                                                                    <td colspan="2" valign="top" style="padding: 48px 48px 0;">
                                                                            <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                                                                            <a href="<?php echo $message;?>"><?php echo $message;?></a>

                                                                            </div>
                                                                    </td>
                                                            <?php
                                                                    } else {
                                                            ?>
                                                                    <td colspan="2" valign="top" style="padding: 48px 48px 0;">
                                                                            <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                                                                            <?php echo $message;?>

                                                                            </div>
                                                                    </td>
                                                            <?php
                                                                    }
                                                            ?>	
                                                            </tr>

                                                            <?php
                                                                    } else {
                                                                            if($booking){
                                                                                    foreach ($booking as $key => $value) {                    
                                                            ?>
                                                                    <tr>

                                                                            <td valign="top" style="padding: 20px 48px 0;">
                                                                                    <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                                                                                    <strong><?php echo $key;?></strong>

                                                                                    </div>
                                                                            </td>

                                                                            <td valign="top" style="padding: 20px 48px 0;">
                                                                                    <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                                                                                    <?php echo $value;?>

                                                                                    </div>
                                                                            </td>

                                                                    </tr>
                                                            <?php                   
                                                                                    }
                                                                            }
                                                                    }
                                                            ?>
                                                    </table>
                                                    <!-- End Content -->
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- End Body -->
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- Footer -->
                                        <table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer" style="background-color: #f7f7f7;">
                                            <tr>
                                                <td valign="top" style="padding: 0; -webkit-border-radius: 6px;">
                                                    <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td colspan="2" valign="middle" id="credit" style="padding: 48px 48px 48px 48px; -webkit-border-radius: 6px; border: 0; color: #66b8e0; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;">
                                                                <p style="line-height: 165%; color: #3c3c3c; text-align: left; padding-top: 30px;">© Time Me<br>
                                                                    <?php echo $businessData['companyName'];?><br>
                                                                    Email :<?php echo $businessData['email'];?><br>
                                                                    Contact :<?php echo $businessData['phone'];?><br>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- End Footer -->
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>

        </body>
        </html>
        <?php
    }
    
    public function header($title,array $businessData){
        
        ?>
        <html>
        <head>

            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Time Me</title>

        </head>
        <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
            <div id="wrapper" dir="ltr" style="background-color: #ffffff; margin: 0; padding: 70px 0 70px 0; -webkit-text-size-adjust: none !important; width: 100%;">
                <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                    <tr>
                        <td align="center" valign="top">
                            <div id="template_header_image">
                                    <p style="margin-top: 0;">
                                        <?php
                                            if(!empty($businessData['logo'])){
                                        ?>
                                        <img src="<?php echo $businessData['logo'];?>" alt="<?php echo $businessData['companyName'];?>" style="border: none; display: inline-block; font-size: 14px; font-weight: bold; height: auto; outline: none; text-decoration: none; text-transform: capitalize; vertical-align: middle; margin-right: 0; width: 150px; max-width: 150px; height: auto; max-height: 100px;" width="150">
                                        <?php
                                            } else {
                                        ?>
                                    <p><?php echo $businessData['companyName'];?></p>
                                        <?php    
                                            }
                                        ?>
                                    </p>						
                            </div>
                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container" style="box-shadow: 0 1px 4px rgba(0,0,0,0.1) !important; background-color: #ffffff; border: 1px solid #e5e5e5; border-radius: 3px !important;">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- Header -->
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header" style='background-color: #fff; border-radius: 3px 3px 0 0 !important; color: #3c3c3c; border-bottom: 1px solid #eee; font-weight: bold; line-height: 100%; vertical-align: middle; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; border-top: 5px solid #0088CC;'>
                                            <tr>
                                                <td id="header_wrapper" style="padding: 36px 48px; display: block;">
                                                    <h1 style='color: #3c3c3c; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 30px; font-weight: 300; line-height: 150%; margin: 0; text-align: center; text-shadow: none;'>

                                                        <?php echo $title;?>

                                                    </h1>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- End Header -->
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- Body -->
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
                                            <tr>
                                                <td valign="top" id="body_content" style="background-color: #ffffff; padding-bottom: 30px;">
                                                    <!-- Content -->
                                                    <table border="0" cellpadding="20" cellspacing="0" width="100%">

        <?php
    }
    
    public function footer(array $businessData){
        
        ?>
                                         
                                                    </table>
                                                    <!-- End Content -->
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- End Body -->
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- Footer -->
                                        <table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer" style="background-color: #f7f7f7;">
                                            <tr>
                                                <td valign="top" style="padding: 0; -webkit-border-radius: 6px;">
                                                    <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td colspan="2" valign="middle" id="credit" style="padding: 48px 48px 48px 48px; -webkit-border-radius: 6px; border: 0; color: #66b8e0; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;">
                                                                <p style="line-height: 165%; color: #3c3c3c; text-align: left; padding-top: 30px;">© Moni |
                                                                    <?php echo $businessData['companyName'];?><br>
                                                                    Email - <?php echo $businessData['email'];?>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- End Footer -->
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>

        </body>
        </html>
        <?php
    }
    
    public function register($email){
        $httpHost = EmailTemplates::createDomain();
        ?>
        
        <tr>

            <td valign="top" style="padding: 20px 48px 0;">
                    <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                        <p>
                            TimeMe is your flexible booking software to organize all business aspects.<br>
                            The Username for your account is <?php echo $email;?><br>
                            To access TimeMe system <a href="<?php echo 'http://'.$httpHost.'/login.php';?>">click here.</a> Login to TimeMe and start using the system now.<br><br>
                            Best regards,<br>
                            TimeMe Team
                        </p>

                    </div>
            </td>

    </tr>
        
        <?php
    }
    
    public function resetLink($link){
        ?>
        
        <tr>

            <td valign="top" style="padding: 20px 48px 0;">
                    <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                        <p>
                            Someone has requested a link to change your password. You can do this through the link below.<br>
                            <a href="<?php echo $link;?>">Change my password</a><br><br>
                            If this wasn’t you, then you can safely ignore this email.<br>
                            Your password won't change until you access the link above and create a new one.<br><br>
                            Best regards,<br>
                            Moni Team
                        </p>

                    </div>
            </td>

    </tr>
        
        <?php
    }
    
    public function registerNewCompany($link){
        ?>
        
        <tr>

            <td valign="top" style="padding: 20px 48px 0;">
                    <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                        <p>
                            You can login to the system through the link below.<br>
                            LOGIN URL :- <a href="<?php echo $link;?>"></a><br><br>
                            Best regards,<br>
                            Moni Team
                        </p>

                    </div>
            </td>

    </tr>
        
        <?php
    }
    
    public function addNewStaff($link,$businessName){
        ?>
        
        <tr>

            <td valign="top" style="padding: 20px 48px 0;">
                    <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                        <p>
                            You are a staff member now. Your working hours and job description is now set. To reset your password
                            <a href="<?php echo $link;?>"> click here.</a><br><br>
                            You can start work in schedules after.<br><br>
                            Best regards,<br>
                            <?php echo $businessName;?>
                        </p>

                    </div>
            </td>

        </tr>
        
        <?php
    }
    
    public function booking_client(array $data,$businessName){
        $booking = $data;
        ?>
        <tr>

            <td valign="top" style="padding: 20px 48px 0;" colspan="2">
                    <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                        <p>
                            Your new appointment has been placed.<br> 
                            Here is your Booking details:<br>                            
                        </p>

                    </div>
            </td>

        </tr>
        <tr>

            <td valign="top" style="padding: 20px 48px 0;">
                    <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                        <p>
                            <strong><?php echo 'Company';?></strong>
                        </p>

                    </div>
            </td>
            <td valign="top" style="padding: 20px 48px 0;">
                    <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                        <p>
                            <?php echo $businessName;?>
                        </p>

                    </div>
            </td>

        </tr>
        <?php
        foreach ($booking as $key => $value) {                    
        ?>
        <tr>

            <td valign="top" style="padding: 20px 48px 0;">
                    <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                    <strong><?php echo $key;?></strong>

                    </div>
            </td>

            <td valign="top" style="padding: 20px 48px 0;">
                    <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                    <?php echo $value;?>

                    </div>
            </td>

        </tr>
        <?php                         
        }
        ?>
        <tr>

            <td valign="top" style="padding: 20px 48px 0;" colspan="2">
                <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                    <p>
                        <br><br>
                        You can change your appointment by contacting <?php echo $businessName;?>                            
                    </p>

                </div>
            </td>

        </tr>
        <?php
    }
    
    public function booking_staff(array $data,$businessName,$clientName){
        $booking = $data;
        ?>
        <tr>

            <td valign="top" style="padding: 20px 48px 0;" colspan="2">
                    <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                        <p>
                            A new appointment has received from <?php echo $clientName;?><br>
                            Your appointment details are as follows:                            
                        </p>

                    </div>
            </td>

        </tr>
        <?php
        foreach ($booking as $key => $value) {
            if($key == 'Venue' || $key == 'Service' || $key == 'Date' || $key == 'Time'){
        ?>
        <tr>

            <td valign="top" style="padding: 20px 48px 0;">
                    <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                    <strong><?php echo $key;?></strong>

                    </div>
            </td>

            <td valign="top" style="padding: 20px 48px 0; min-width: 200px ">
                    <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                    <?php echo $value;?>

                    </div>
            </td>

        </tr>
        <?php 
            }
        }
        ?>
        <tr>

            <td valign="top" style="padding: 20px 48px 0;" colspan="2">
                    <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                        <p>
                            <br><br>
                            Contact <?php echo $businessName;?> for any query.                            
                        </p>

                    </div>
            </td>

        </tr>
        <?php
    }
    
    public function booking_admin(array $data,$clientName){
        $booking = $data;
        ?>
        <tr>

            <td valign="top" style="padding: 20px 48px 0;">
                    <div id="body_content_inner" style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

                        <p>
                            <?php echo $clientName;?> has placed uta a booking on <?php echo $booking['Date'];?> with <?php echo $booking['Service Provider Name'];?> for <?php echo $booking['Service'];?>                            
                        </p>

                    </div>
            </td>

        </tr>
        <?php
    }
    
    private static function createDomain(){
        if(isset($_SERVER['HTTP_HOST'])){
            return $_SERVER['HTTP_HOST'];
        } else {
            return false;
        }
    }
   
}



