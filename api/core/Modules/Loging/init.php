<?php
spl_autoload_register('Log_Autoloader');
function Log_Autoloader($className){
        $directories = glob(MODULE_DIR.'*' , GLOB_ONLYDIR);
         foreach($directories as $dir)
        {
            $file = sprintf('%s/%s.class.php', $dir, $className);
            if(is_file($file)) 
            {
                include_once $file;
            } 

        }
       
}

