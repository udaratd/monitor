<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class LogDAO{
    private $filePath;
    private $file;
    
    function __construct() {
        $this->filePath=LOG_DIR.DS.LOG_FILE;
        $this->createLogFile(); 
    }
    
    public function addLog(Log $log){
        return $this->append($log->getFormattedString());
    }

    public function record($text){
        return $this->append($text);
    }
    
    
    
    /**
     * Checks Existance of log file in the file system
     * @return boolean True if file exists False if file is not in Log Dir
     * @since 1.0
     */
    public  function logFileExisits(){
        if(file_exists($this->filePath)){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Creates Log File if file is not exists 
     *
     */
    public function createLogFile(){
        
        $this->file=fopen($this->filePath, 'a');
        if($this->file){
            return true;
        }else{
            throw new Exception("Cannot create or open file");
        }
        
    }
    
    public function append($text){

        if(fwrite($this->file, PHP_EOL. $text)){
            return true;
        }else{
            return false;
        }
  
    }
    
    public function getLogFile(){
        return $this->logFile;
    }
}

