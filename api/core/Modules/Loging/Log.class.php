<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Log{
    private $time;
    private $type;
    private $text;
    private $phpfile;
    
    
    function __construct($text,$type=LOG_ERROR) {
        $this->time=date("Y-m-d H:i:s");
        $this->type=$type;
        $this->phpfile= debug_backtrace()[0]['file'];
        $this->text=$text;
    }
    
    /**
     * Return Log data as assoc array
     * @return Array Array with Log Data
     */
    public function getArray(){
        $arr=array();
        $arr['file']=$this->phpfile;
        $arr['type']=$this->type;
        $arr['msg']=$this->text;
        return $arr;
    }
    
    public function getFormattedString(){
        $string=$this->time.TAB.$this->type.TAB.$this->phpfile.TAB.$this->text;
        return $string;
    }
    function getTime() {
        return $this->time;
    }

    function getType() {
        return $this->type;
    }

    function getText() {
        return $this->text;
    }

    function getphpFile() {
        return $this->phpfile;
    }

    function setTime($time) {
        $this->time = $time;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setText($text) {
        $this->text = $text;
    }

    function setphpFile($file) {
        $this->phpfile = $file;
    }

}