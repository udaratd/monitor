<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class FileManager {
    
    private static $s3client;
    
    /**
     * Intialize File manager
     */
    public static function Intialize(){
        if(STORAGE_TYPE==='S3'){
            self::$s3client = Aws\S3\S3Client::factory(array(
               'version' => 'latest',
               'region'  => 'ap-southeast-1',
               'credentials' => [
                   'key'    => S3_ACCESS_KEY,
                   'secret' => S3_ACCESS_SECRET,
               ],
            ));
        
            self::$s3client->registerStreamWrapper();
        }
       
    }
    
    /**
     * Upload Images to server
     * @param type $path String path
     * @param type $fileName String file name
     * @param type $imageString String image
     * @return boolean|string|int return STRING image path on success INT on successfully deleted corrupted image FALSE on failure 
     */
    public static function imageUpload ($path, $fileName, $imageString) {
        
        $imageURL = false;
        
        $image = $imageString;
        $image = str_replace('data:image/jpg;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $data = base64_decode($image);
        
        if(STORAGE_TYPE === 'S3'){
            
            $context = stream_context_create(array(
                's3' => array(
                    'ACL' => 'public-read'
                )
            ));
            
            if(!empty($_SESSION['S3_BUCKET'])){
                $normalS3Bucket = S3_BUCKET;
                $fullPath_filename = 's3://'.$normalS3Bucket.'/'.$path.$fileName;
            } else {
                $superS3Bucket = S3_BUCKET_SUPER;
                $fullPath_filename = 's3://'.$superS3Bucket.'/'.$path.$fileName;
            }

            if(file_put_contents($fullPath_filename, $data, 0,$context)) {
                
                if(!empty($_SESSION['S3_BUCKET'])){
                    $normalS3BucketDomain = S3_BUCKET_DOMAIN;
                    $s3url = $normalS3BucketDomain.$path.$fileName;
                } else {
                    $superS3BucketDomain = S3_BUCKET_DOMAIN_SUPER;
                    $s3url = $superS3BucketDomain.$path.$fileName;
                }
                
                if(getimagesize($s3url)) {                    
                    $imageURL = $s3url;
                    return $imageURL;
                } else {
                    if(FileManager::deleteFile($s3url) === true) {
                        return 1;
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
            
        } else {
            $fullPath_filename = LOCAL_SERVER_DOMAIN.$path.$fileName;
            
            if(file_put_contents($fullPath_filename, $data)) {
                if(getimagesize($fullPath_filename)) {
                    $imageURL = $fullPath_filename;
                    return $imageURL;
                } else {
                    if(FileManager::deleteFile($pathWithImageName) === true) {
                        return 1;
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }
        
        
    }
    
    public static function feedBackimageUpload ($path, $fileName, $imageString) {
        
        $imageURL = false;
        
        $image = $imageString;
        $image = str_replace('data:image/jpg;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $data = base64_decode($image);
        
        if(STORAGE_TYPE === 'S3'){
            
            $context = stream_context_create(array(
                's3' => array(
                    'ACL' => 'public-read'
                )
            ));
            
            if(!empty($_SESSION['S3_BUCKET'])){
                $normalS3Bucket = $_SESSION['S3_BUCKET'];
                $fullPath_filename = 's3://'.$normalS3Bucket.'/'.$path.$fileName;
            }

            if(file_put_contents($fullPath_filename, $data, 0,$context)) {
                
                if(!empty($_SESSION['S3_BUCKET'])){
                    $normalS3BucketDomain = S3_BUCKET_DOMAIN_FEEDBACK.$_SESSION['S3_BUCKET'].'/';
                    $s3url = $normalS3BucketDomain.$path.$fileName;
                }
                
                if(getimagesize($s3url)) {                    
                    $imageURL = $s3url;
                    return $imageURL;
                } else {
                    return false;
                }
            } else {
                return false;
            }
            
        } else {
            $fullPath_filename = LOCAL_SERVER_DOMAIN.$path.$fileName;
            
            if(file_put_contents($fullPath_filename, $data)) {
                if(getimagesize($fullPath_filename)) {
                    $imageURL = $fullPath_filename;
                    return $imageURL;
                } else {
                    if(FileManager::deleteFile($pathWithImageName) === true) {
                        return 1;
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }
        
        
    }
    
    public static function fileUpload ($path, $fileName, $base64String, $replaceData, $fileType=null) {
        
        $fileURL = false;
        
        $fileContent = $base64String;
        $fileContent = str_replace($replaceData.',', '', $fileContent);
        $fileContent = str_replace(' ', '+', $fileContent);
        $data = base64_decode($fileContent);
        
        if(STORAGE_TYPE === 'S3'){
            
            $context = stream_context_create(array(
                's3' => array(
                    'ACL' => 'public-read'
                )
            ));
            
            $fullPath_filename = 's3://'.S3_BUCKET.'/'.$path.$fileName;

            if(file_put_contents($fullPath_filename, $data, 0,$context)) {
                $s3url = S3_BUCKET_DOMAIN.$path.$fileName;
                
                $fileURL = $s3url;
                return $fileURL;               
            } else {
                return false;
            }
            
        } else {
//            $fullPath_filename = LOCAL_SERVER_DOMAIN.$path.$fileName;
            $fullPath_filename = $path.$fileName;
            
            if(file_put_contents($fullPath_filename, $data)) {
                $fileURL = $fullPath_filename;
                return $fileURL;
            } else {
                return false;
            }
        }
        
        
    }
    
    public static function serverSidegeneratedFileUpload ($path, $fileName, $filedata) {
        
        $fileURL = false;
        
        
        if(STORAGE_TYPE === 'S3'){
            
            $context = stream_context_create(array(
                's3' => array(
                    'ACL' => 'public-read'
                )
            ));
            
            $fullPath_filename = 's3://'.S3_BUCKET.'/'.$path.$fileName;

            if(file_put_contents($fullPath_filename, $filedata, 0,$context)) {
                $s3url = S3_BUCKET_DOMAIN.$path.$fileName;
                
                $fileURL = $s3url;
                return $fileURL;               
            } else {
                return false;
            }
            
        } else {
//            $fullPath_filename = LOCAL_SERVER_DOMAIN.$path.$fileName;
            $fullPath_filename = $path.$fileName;
            echo $fullPath_filename;
            if(file_put_contents($fullPath_filename, $filedata)) {
                $fileURL = $fullPath_filename;
                return $fileURL;
            } else {
                return false;
            }
        }
        
        
    }
    
    /**
     * This function delete corrupted images
     * @param type $imagePath String image path
     * @return boolean True on success False on Failures
     */
    public static function deleteFile ($path) {
        
//        $s3 = new Aws\S3\S3Client($args); //PLEASE REMOVE THIS LINE
        
        if(STORAGE_TYPE === 'S3'){
            $s3= FileManager::getS3client();

            $path = explode('/',$path);//split using '/' and put to array

            $key_of_bucketName = array_search(S3_BUCKET, $path);
            
            $count = count($path)-$key_of_bucketName-1;            
            
            $path = array_slice($path,-$count,$count);//get last three element of array
            
            $path = implode("/", $path);//concat array values using '/' 
            
            $result=$s3->deleteObject([
                'Bucket'  => S3_BUCKET,
                'Key' => $path,
                ]);

            $response = $s3->doesObjectExist(S3_BUCKET, $path);
            
            if($response === false){
                return true;
            } else {
                return false;
            }   
        } else {
            if(unlink($path)) {
                return true;
            } else {
                return false;
            }
        }

    }
    
    /**
     * copy files to destination
     * 
     * @param type $source String source file name with path
     * @param type $destination String destination path
     * @return boolean True on Success False on Fail
     */
    public static function copyFile ($source,$destination) {
        if(copy($source, $destination)) {
            return true;
        } else {
            return false;
        }
    }
    
    public static function fileExist($path){
        if(STORAGE_TYPE === 'S3'){
            $s3= FileManager::getS3client();
            
            $path = explode('/',$path);//split using '/' and put to array

            $key_of_bucketName = array_search(S3_BUCKET, $path);
            
            $count = count($path)-$key_of_bucketName-1;            
            
            $path = array_slice($path,-$count,$count);//get last three element of array
            
            $path = implode("/", $path);//concat array values using '/'
            
            $response = $s3->doesObjectExist(S3_BUCKET, $path);
            
            if($response === true){
                return true;
            } else {
                return false;
            } 
        } else {
            if(file_exists($path)){
                return true;
            } else {
                return false;
            }
        }
    }

    public static function getS3client() {
        return self::$s3client;
    }
       
    public static function getFileSize($filename){
        if(STORAGE_TYPE === 'S3'){
            
        } else {
            
        }
    }
    
    public static function createS3bucket($bucketName){
        try {
            $s3 = FileManager::getS3client();

            $response = $s3->createBucket([
               'Bucket' => $bucketName, 
            ]);
            
            $metaData = $response->get('@metadata');
            $statusCode = $metaData['statusCode'];
            
            if($statusCode == 200){
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex){
//            echo $ex->getMessage();
            return false;
        }
    }
    
    public static function deleteS3bucket($bucketName){
        try {
            $s3 = FileManager::getS3client();
            
            $response = $s3->deleteBucket([
               'Bucket' => $bucketName, 
            ]);
            
            $metaData = $response->get('@metadata');
            $statusCode = $metaData['statusCode'];

            //status code 200 - OK
            //status code 204 - No Content Bucket Remove Success
            
            if($statusCode == 204){
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex){
            echo $ex->getMessage();
        }
    }
    
    public static function listAllObjects($bucketName){
        $objectKeys = array();
        try {
            $s3 = FileManager::getS3client();

            $objects = $s3->getPaginator('ListObjects',[
               'Bucket' => $bucketName, 
            ]);
            
            if($objects){
                foreach ($objects as $object){
                    array_push($objectKeys, $object['Key']);
                }
            }

            if($objectKeys){
                return $objectKeys;
            } else {
                return false;
            }
        } catch (Exception $ex){
            echo $ex->getMessage();
        }
    }
    
    public static function list_1000_Objects($bucketName){
        $objectKeys = array();
        try {
            $s3 = FileManager::getS3client();

            $objects = $s3->listObjects([
               'Bucket' => $bucketName, 
            ]);
            
            if($objects['Contents']){
                foreach ($objects['Contents'] as $object){
                    array_push($objectKeys, $object['Key']);
                }
            }

            if($objectKeys){
                return $objectKeys;
            } else {
                return false;
            }
        } catch (Exception $ex){
            echo $ex->getMessage();
        }
    }
    
    public static function deleteSingleObject($bucketName,$objectKey){
        try {
            $s3 = FileManager::getS3client();

            $deleteObjects = $s3->deleteObject([
               'Bucket' => $bucketName,
               'Key' => $objectKey,
            ]);
            
            $metaData = $deleteObjects->get('@metadata');
            $statusCode = $metaData['statusCode'];

            if($statusCode == 200){
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex){
            echo $ex->getMessage();
        }
    }
    
    public static function deleteMultipleObjects($bucketName,array $objectKey){
        try {
            $s3 = FileManager::getS3client();

            $deleteObjects = $s3->deleteObjects([
               'Bucket' => $bucketName,
               'Delete' => [
                   'Objects' => array_map(function ($key){
                               return ['Key' => $key];
                   }, $objectKey)
               ],
            ]);
      
            $metaData = $deleteObjects->get('@metadata');
            $statusCode = $metaData['statusCode'];

            if($statusCode == 200){
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex){
            echo $ex->getMessage();
        }
    }

    public static function createFolder_AWS_S3($s3BucketName,array $folderNames){
        $s3 = FileManager::getS3client();
        
        $success = array();
        if($folderNames){
            
            foreach ($folderNames as $folder){
                
                $response = $s3->putObject([
                    'Bucket'     => $s3BucketName,
                    'Key'        => $folder.'verify.txt',
                ]);
                
                $metaData = $response->get('@metadata');
                $statusCode = $metaData['statusCode'];
                
                if($statusCode == 200){
                    array_push($success, $statusCode);
                }
            }
        }
        
        $folderCount = count($folderNames);
        $successCount = count($success);
        
        if($folderCount == $successCount){
            return true;
        } else {
            return false;
        }
        
    }
    
    public static function listAllBuckets(){
        $buckets = array();
        try {
            $s3 = FileManager::getS3client();

            $listBucket = $s3->listBuckets();
            
            if($listBucket['Buckets']){

                foreach ($listBucket['Buckets'] as $bucket){
                    array_push($buckets, $bucket['Name']);
                }
            }

            if($buckets){
                return $buckets;
            } else {
                return false;
            }
        } catch (Exception $ex){
            echo $ex->getMessage();
        }
    }
    
    /**
     * get the file size of base 64 string
     * 
     * @param type $base64 string base 64 data
     * @return boolean return array of file size in bytes,kb,mb, False on failure
     */
    public static function getBase64Size($base64){ //return memory size in B, KB, MB
        try{
            $size_in_bytes = (int) (strlen(rtrim($base64, '=')) * 3 / 4);
            $size_in_kb    = $size_in_bytes / 1024;
            $size_in_mb    = $size_in_kb / 1024;

            $data['size_in_bytes'] = $size_in_bytes;
            $data['size_in_kb'] = $size_in_kb;
            $data['size_in_mb'] = $size_in_mb;

            return $data;

        }catch(Exception $e){
            return false;
        }
    }
    
    /**
     * 
     * @param type $width
     * @param type $height
     * @param type $imageObj
     * @param type $imageFileType
     * @return type
     */
    function imageResize($width,$height,$imageObj,$imageFileType){

        $ratio = $width / $height;
        if( $ratio > 1) {
            $resized_width = 1500; //suppose 500 is max width or height
            $resized_height = 1500/$ratio;
        }
        else {
            $resized_width = 1500*$ratio;
            $resized_height = 1500;
        }

        if ($imageFileType == 'png') {
            $image = imagecreatefrompng($imageObj);
        } else {
            $image = imagecreatefromjpeg($imageObj);
        }

        $resized_image = imagecreatetruecolor($resized_width, $resized_height);
        imagecopyresampled($resized_image, $image, 0, 0, 0, 0, $resized_width, $resized_height, $width, $height);
        ob_start(); // Let's start output buffering.
            imagejpeg($resized_image); //This will normally output the image, but because of ob_start(), it won't.
            $contents = ob_get_contents(); //Instead, output above is saved to $contents
        ob_end_clean();

        return base64_encode($contents);
    }
}

