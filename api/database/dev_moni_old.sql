-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 28, 2018 at 03:23 AM
-- Server version: 10.0.36-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dev_moni`
--

-- --------------------------------------------------------

--
-- Table structure for table `checkpoint`
--

CREATE TABLE `checkpoint` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `locationid` int(11) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `ID` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `image` varchar(500) DEFAULT NULL,
  `email` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE `device` (
  `ID` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(700) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parentid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `repeat_type`
--

CREATE TABLE `repeat_type` (
  `ID` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reset_password`
--

CREATE TABLE `reset_password` (
  `ID` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(500) NOT NULL,
  `secret` int(11) NOT NULL,
  `date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staff_supervisor`
--

CREATE TABLE `staff_supervisor` (
  `ID` int(11) NOT NULL,
  `supervisor_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `taskandschedule`
--

CREATE TABLE `taskandschedule` (
  `ID` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `checkpoint_id` int(11) NOT NULL,
  `repeatType_id` int(11) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `oneTimeDate` date DEFAULT NULL,
  `sunday` int(11) DEFAULT NULL,
  `monday` int(11) DEFAULT NULL,
  `tuesday` int(11) DEFAULT NULL,
  `wednesday` int(11) DEFAULT NULL,
  `thursday` int(11) DEFAULT NULL,
  `friday` int(11) DEFAULT NULL,
  `saturday` int(11) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `scheduleStatus` int(11) NOT NULL,
  `sid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_schedule`
--

CREATE TABLE `temp_schedule` (
  `ID` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `checkpoint_id` int(11) NOT NULL,
  `repeatType_id` int(11) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `oneTimeDate` date DEFAULT NULL,
  `sunday` int(11) DEFAULT NULL,
  `monday` int(11) DEFAULT NULL,
  `tuesday` int(11) DEFAULT NULL,
  `wednesday` int(11) DEFAULT NULL,
  `thursday` int(11) DEFAULT NULL,
  `friday` int(11) DEFAULT NULL,
  `saturday` int(11) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `scheduleStatus` int(11) NOT NULL,
  `sid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `phone` varchar(25) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_meta`
--

CREATE TABLE `user_meta` (
  `ID` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_nic`
--

CREATE TABLE `user_nic` (
  `ID` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `nic` varchar(35) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `work_list`
--

CREATE TABLE `work_list` (
  `ID` int(11) NOT NULL,
  `checkpoint_id` int(11) NOT NULL,
  `device_date` date NOT NULL,
  `device_time` time NOT NULL,
  `server_date` date NOT NULL,
  `server_time` time NOT NULL,
  `user_id` int(11) NOT NULL,
  `work_status` enum('Approved','Pending','Flagged','Rework') NOT NULL,
  `rework` int(11) NOT NULL,
  `original_work_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) NOT NULL,
  `image` varchar(500) DEFAULT NULL,
  `notification` int(11) NOT NULL,
  `comment` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `checkpoint`
--
ALTER TABLE `checkpoint`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `check_loc_id` (`locationid`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_device` (`user_id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `repeat_type`
--
ALTER TABLE `repeat_type`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `reset_password`
--
ALTER TABLE `reset_password`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `staff_supervisor`
--
ALTER TABLE `staff_supervisor`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_staff_supervisor` (`supervisor_id`),
  ADD KEY `user_staff_user` (`user_id`);

--
-- Indexes for table `taskandschedule`
--
ALTER TABLE `taskandschedule`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `sdule_tsk_re_type` (`repeatType_id`),
  ADD KEY `sdule_tsk_chek` (`checkpoint_id`),
  ADD KEY `sdule_tsk_user` (`user_id`);

--
-- Indexes for table `temp_schedule`
--
ALTER TABLE `temp_schedule`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_meta`
--
ALTER TABLE `user_meta`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_id_idx` (`user_id`);

--
-- Indexes for table `user_nic`
--
ALTER TABLE `user_nic`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_id_idx` (`user_id`);

--
-- Indexes for table `work_list`
--
ALTER TABLE `work_list`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `work_checkpoint` (`checkpoint_id`),
  ADD KEY `work_user` (`user_id`),
  ADD KEY `work_schedule` (`schedule_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `checkpoint`
--
ALTER TABLE `checkpoint`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `device`
--
ALTER TABLE `device`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT for table `repeat_type`
--
ALTER TABLE `repeat_type`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `reset_password`
--
ALTER TABLE `reset_password`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `staff_supervisor`
--
ALTER TABLE `staff_supervisor`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `taskandschedule`
--
ALTER TABLE `taskandschedule`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=196;

--
-- AUTO_INCREMENT for table `temp_schedule`
--
ALTER TABLE `temp_schedule`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=234;

--
-- AUTO_INCREMENT for table `user_meta`
--
ALTER TABLE `user_meta`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT for table `user_nic`
--
ALTER TABLE `user_nic`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;

--
-- AUTO_INCREMENT for table `work_list`
--
ALTER TABLE `work_list`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=412;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `checkpoint`
--
ALTER TABLE `checkpoint`
  ADD CONSTRAINT `check_loc_id` FOREIGN KEY (`locationid`) REFERENCES `location` (`ID`) ON DELETE CASCADE;

--
-- Constraints for table `device`
--
ALTER TABLE `device`
  ADD CONSTRAINT `user_device` FOREIGN KEY (`user_id`) REFERENCES `user` (`ID`) ON DELETE CASCADE;

--
-- Constraints for table `staff_supervisor`
--
ALTER TABLE `staff_supervisor`
  ADD CONSTRAINT `user_staff_supervisor` FOREIGN KEY (`supervisor_id`) REFERENCES `user` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_staff_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`ID`) ON DELETE CASCADE;

--
-- Constraints for table `taskandschedule`
--
ALTER TABLE `taskandschedule`
  ADD CONSTRAINT `sdule_tsk_chek` FOREIGN KEY (`checkpoint_id`) REFERENCES `checkpoint` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sdule_tsk_re_type` FOREIGN KEY (`repeatType_id`) REFERENCES `repeat_type` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sdule_tsk_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`ID`) ON DELETE CASCADE;

--
-- Constraints for table `user_meta`
--
ALTER TABLE `user_meta`
  ADD CONSTRAINT `meta_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `user_nic`
--
ALTER TABLE `user_nic`
  ADD CONSTRAINT `nic_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `work_list`
--
ALTER TABLE `work_list`
  ADD CONSTRAINT `work_checkpoint` FOREIGN KEY (`checkpoint_id`) REFERENCES `checkpoint` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `work_schedule` FOREIGN KEY (`schedule_id`) REFERENCES `taskandschedule` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `work_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`ID`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
