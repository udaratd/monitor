
<?php

class PDFHTML {
    
    public function htmlGenerator(array $checkpoints,$oldway){

        ?>

        <table width="100%" class="qr-table" border="0" cellspacing="0">
        
        <?php
        
        $i=0;
        foreach ($checkpoints as $checkpoint){
            
        $ID = $checkpoint['ID'];
        $name = $checkpoint['name'];
        $image = $checkpoint['qr'];
        $location = $checkpoint['location'];
//        $path = parse_url($image);
        $moduleDivider = $i%4;
        
        if($moduleDivider == 0){
        
        ?>
            <tr>
        <?php  
        }    
        ?>   
   
            <td valign="top">
                <?php 
                    if($oldway){
                ?>
                <div><img src="data:image/png;base64,<?php echo $image;?>" width="130" height="130" alt="" align="center"><br></div>
                <?php 
                    } else {
                ?>
                <div><img src="<?php echo $image;?>" width="130" height="130" alt="" align="center"><br></div>
                <?php                       
                    }
                ?>
                <div class="qr-desc">
                    <strong class="qr-title"><?php echo $name.' ('.$ID.')';?></strong>
                    <span><?php echo $location;?></span>
                </div>
            </td>
        
        <?php
        
        if($moduleDivider == 3){
        ?>
            </tr>            
        <?php
        }                
            $i++;
        }
        ?>

        </table>

        <style>
            .qr-table{
                border: 1px solid #444;
                border-collapse: collapse;
            }
            .qr-table td{
                border: 1px solid #444;
                text-align: center;
                padding: 10px;
            }
            .qr-desc{
                font-size: 11px; 
                margin-top: 5px;
            }
            .qr-title{
                margin-bottom: 5px;
                display: block;
            }
        </style>

        <?php
    }
}

?>


