<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author doweby
 */
class User {
    private $id;
    private $fName;
    private $lName;
    private $email;
    private $nic;
    private $phone;
    private $password;
    private $role;
    private $image;
    private $supervisorID;
    private $json;
    private $outPutArray;
    private $note;

//    private $q = array();
            
    function __construct($data = null) {

        if($data) {
            if(isset($data['id'])) {
                $this->id = $data['id'];
            }
            if(isset($data['firstName'])) {
                $this->fName = $data['firstName'];
            }
            if(isset($data['lastName'])) {
                $this->lName = $data['lastName'];
            }
            if(isset($data['email'])) {
                $this->email = $data['email'];
            }
            if(isset($data['nic'])) {
                $this->nic = $data['nic'];
            }
            if(isset($data['phone'])) {
                $this->phone = $data['phone'];
            }
            if(isset($data['password'])) {
                $this->password = $data['password'];
            }
            if(isset($data['jobRole'])) {
                $this->role = $data['jobRole'];
            }
            if(isset($data['imageString'])) {
                $this->image = $data['imageString'];
            }
            if(isset($data['supervisor_id'])) {
                $this->supervisorID = $data['supervisor_id'];
            }
            if(isset($data['note'])){
                $this->note = $data['note'];
            }
           
        }
        
    }          

    
//    public static function ValidateSession(){
//        if (session_status() === PHP_SESSION_NONE){session_start();} //start session if not already started
//        if(isset($_SESSION['email'])){
//            return true;
//        }else{
//            return false;
//        }
//    }
//    public static function InitiateSession($email){
//        if (session_status() === PHP_SESSION_NONE){session_start();} //start session if not already started
//        $_SESSION['email']=$email;
//        
//    }
//    
//    public static function logOut(){
//        unset($_SESSION['email']);
//    }
    
            
    function getId() {
        return $this->id;
    }

    function getFName() {
        return $this->fName;
    }

    function getLName() {
        return $this->lName;
    }

    function getEmail() {
        return $this->email;
    }

    function getPassword() {
        return $this->password;
    }
    
    function getPasswordHash() {
        return hash('sha256', $this->password);
    }

    function getRole() {
        return $this->role;
    }

    function getNic() {
        return $this->nic;
    }

    function getPhone() {
        return $this->phone;
    }

    function getImage() {
        return $this->image;
    }
    
    function getSupervisorID() {
        return $this->supervisorID;
    }

    function getJson() {
        return $this->json;
    }

    function getOutPutArray() {
        return $this->outPutArray;
    }

    function getNote() {
        return $this->note;
    }
    
    function setId($id) {
        $this->id = $id;
    }

    function setFName($fName) {
        $this->fName = $fName;
    }

    function setLName($lName) {
        $this->lName = $lName;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setRole($role) {
        $this->role = $role;
    }

    function setNic($nic) {
        $this->nic = $nic;
    }

    function setPhone($phone) {
        $this->phone = $phone;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setSupervisorID($supervisorID) {
        $this->supervisorID = $supervisorID;
    }

    function setNote($note) {
        $this->note = $note;
    }
    
    /**
     * This function check image availability and image string validation
     * @return boolean return True on if image available False on not available 
     */
    function hasImage(){
        if(empty($this->image)||$this->image==null){//validate image String
            return false;
        }else{
            return true;
        }
    }
}
