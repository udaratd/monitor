<?php

class DatabaseTable {
    
    private $database_name;
    private $table_name;
    private $primary_key;
    private $fields = array();
            
    function __construct($data = null) {
        if($data){
            if(isset($data['database_name'])){
                $this->database_name = $data['database_name'];
            }
        }
    }
    
    function getDatabase_name() {
        return $this->database_name;
    }
    
    function getTable_name() {
        return $this->table_name;
    }

    function getPrimary_key() {
        return $this->primary_key;
    }
    
    function getFields() {
        return $this->fields;
    }

    function setDatabase_name($database_name) {
        $this->database_name = $database_name;
    }
    
    function setTable_name($table_name) {
        $this->table_name = $table_name;
    }

    function setPrimary_key($primary_key, $autoincrement = true) {
        if(empty($this->primary_key)){
            if($primary_key){
                $this->primary_key = $primary_key;
                array_push($this->fields, array('field' => $primary_key, 'auto_increment' => $autoincrement, 'type' => 'p'));
            }
        }
    }

    function setVarchar_field($varchar_field, $length = 255, $allowNull = false) {
        if($varchar_field){
            array_push($this->fields, array('field' => $varchar_field, 'length' => $length, 'allow_null' => $allowNull, 'type' => 'v'));
        }
    }

    function setInt_field($int_field, $allowNull = false) {
        if($int_field){
            array_push($this->fields, array('field' => $int_field, 'allow_null' => $allowNull, 'type' => 'i'));
        }
    }

    function setDate_field($date_field, $allowNull = false) {
        if($date_field){
            array_push($this->fields, array('field' => $date_field, 'allow_null' => $allowNull, 'type' => 'd'));
        }
    }

    function setDate_time_field($date_time_field, $allowNull = false) {
        if($date_time_field){
            array_push($this->fields, array('field' => $date_time_field, 'allow_null' => $allowNull, 'type' => 'dt'));
        }
    }

    function setText_field($text_field, $allowNull = false) {
        if($text_field){
            array_push($this->fields, array('field' => $text_field, 'allow_null' => $allowNull, 'type' => 't'));
        }
    }
    
    function setLongText_field($longtext_field, $allowNull = false) {
        if($longtext_field){
            array_push($this->fields, array('field' => $longtext_field, 'allow_null' => $allowNull, 'type' => 'lt'));
        }
    }

    function setTimestamp_field($timestamp_field, $allowNull = false) {
        if($timestamp_field){
            array_push($this->fields, array('field' => $timestamp_field, 'allow_null' => $allowNull, 'type' => 'ts'));
        }
    }

    function setBoolean_field($boolean_field) {
        if($boolean_field){
            array_push($this->fields, array('field' => $boolean_field, 'type' => 'b'));
        }
    }
}