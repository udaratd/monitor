<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Location {
    private $id;
    private $name;
    private $description;
    private $image;
    private $parentID;
    private $q = array();
            
    function __construct() {
        $numOfAgv = func_get_args();
        switch (func_num_args()) {
            case 2:
                self::__construct1($numOfAgv[0],$numOfAgv[1]);
                break;
            case 5:
                self::__construct2($numOfAgv[0],$numOfAgv[1],$numOfAgv[2],$numOfAgv[3],$numOfAgv[4]);
                break;
            default :
                break;
        }
    }

    function __construct1($name, $parentID) {
        $this->name = $name;
        $this->parentID = $parentID;
    }
    
    function __construct2($id, $name, $parentID, $description, $image) {
        $this->id = $id;
        $this->name = $name;
        $this->parentID = $parentID;
        $this->description = $description;
        $this->image = $image;
        
        if($this->name !== ""){
            $this->q[] = "name = :name";
        }
        if($this->description !== ""){
            $this->q[] = "description = :description";
        }
        if($this->image !== ""){
            $this->q[] = "image = :image";
        }
        if($this->parentID !== ""){
            $this->q[] = "parentid = :pid";
        }   
    }
    
    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getDescription() {
        return $this->description;
    }

    function getImage() {
        return $this->image;
    }

    function getParentID() {
        return $this->parentID;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setParentID($parentID) {
        $this->parentID = $parentID;
    }
    
    function getQ() {
        return $this->q;
    }

    function setQ($q) {
        $this->q = $q;
    }
    
    function hasImage(){
        if(empty($this->image)||$this->image==null){//validate image String
            return false;
        }else{
            return true;
        }
    }
}

