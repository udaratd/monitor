<?php

class LogActivityDAO {
    
    public static function addLog(LogActivity $activity){
        
        $timeZone = 'Asia/Colombo';

        $selectedDate = $activity->getDate();
        $selectedTime = $activity->getTime();        
        $dateAndTime = $selectedDate.' '.$selectedTime;
        
        $dateObj = new DateTime($dateAndTime);
        $dateObj->setTimezone(new DateTimeZone($timeZone));
        $date = $dateObj->format('Y-m-d');
        $time = $dateObj->format('H:i:s');

        $database = new database($_SESSION['DB_NAME']);       
        try {
            $query = "INSERT INTO log_activity(activity, by_whom_id, date, time, business_id, user_type) VALUES(:activity, :by_whom, :date, :time, :business_id, :user_type)";
            $database->query($query);
            $database->bind(':activity', $activity->getActivity());
            $database->bind(':by_whom', $activity->getBy());
            $database->bind(':date', $date);
            $database->bind(':time', $time);
            $database->bind(':business_id', $activity->getBusiness_id());
            $database->bind(':user_type', $activity->getUserType());
            
            if($database->execute()){
                return true;
            } else {
                return false;
            }            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function listLog (LogActivity $activity, $s_date, $e_date, $s_limit, $e_limit, $all = false){

        $database = new database();       
        try {            
            $query = "SELECT
                        ID,
                        activity,
                        by_whom_id,
                        date,
                        time,
                        user_type
                      FROM 
                        log_activity 
                      WHERE business_id = :business_id"; 
            if(!$all){
                $query = $query." AND (date BETWEEN :s_date AND :e_date)";
            }
                
            $query = $query." ORDER BY date DESC, time DESC";
            

            if( ($s_limit == 0 || !empty($s_limit)) && !empty($e_limit)){
                $query = $query. " LIMIT :s_limit,:e_limit";                
            }

            $database->query($query);
            $database->bind(':business_id', $activity->getBusiness_id());
            
            if(!$all){
                $database->bind(':s_date', $s_date);
                $database->bind(':e_date', $e_date);
            }

            if( ($s_limit == 0 || !empty($s_limit)) && !empty($e_limit)){
                $database->bind(':s_limit', $s_limit);          
                $database->bind(':e_limit', $e_limit);          
            }

            
            $activities = $database->resultset();
            if($activities){
                $count=0;
                foreach ($activities as $single){
                    
                    $query_1 = "SELECT CONCAT(firstName,' ',lastName) as name FROM user WHERE ID = '".$single['by_whom_id']."' ";
                    $database->query($query_1);
                    $staff = $database->single();
                    $activities[$count]['by_whome_name'] = $staff['name'];
                    
                    if($single['user_type'] == 1) {                       
                        $activities[$count]['role'] = 'Admin';                        
                    }elseif ($single['user_type'] == 2) {                       
                        $activities[$count]['role'] = 'Supervisor';                        
                    }elseif ($single['user_type'] == 3) {                        
                        $activities[$count]['role'] = 'Staff';                        
                    }
                    $count++;
                }
                return $activities;
            } else {
                return false;
            }            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function listLogCount (LogActivity $activity,$s_date,$e_date,$all = false){

        $database = new database();       
        try {            
            $query = "SELECT
                        COUNT(*) as count
                      FROM 
                        log_activity 
                      WHERE business_id = :business_id"; 
            if(!$all){
                $query = $query." AND (date BETWEEN :s_date AND :e_date)";
            }
                
            $query = $query." ORDER BY date DESC, time DESC";
            
            $database->query($query);
            $database->bind(':business_id', $activity->getBusiness_id());
            
            if(!$all){
                $database->bind(':s_date', $s_date);
                $database->bind(':e_date', $e_date);
            }
            
            $activities = $database->single();
            if($activities){               
                return $activities;
            } else {
                return false;
            }            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
}

