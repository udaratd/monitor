<?php

class Admin {
    
    private $id;
    private $com_name;
    private $com_address;
    private $dbName;
    private $username;
    private $fName;
    private $lName;
    private $contactName;
    private $email;
    private $phone;
    private $logo;
    private $active;//1 :- active, 2 :- inactive
    private $password;
    private $newPassword;
    private $bucketName;
    private $expiryDate;//for check company payment
    private $checkFirstUser;// 0 :- no user, 1 :- has user
    private $debug;
    private $skip_validation;
            
    function __construct($data = null) {
        if($data){
            if(isset($data['id'])){
                $this->id = $data['id'];
            }
            if(isset($data['company_name'])){
                $this->com_name = $data['company_name'];
            }
            if(isset($data['address'])){
                $this->com_address = $data['address'];
            }
            if(isset($data['dbname'])){
                $this->dbName = $data['dbname'];
            }
            if(isset($data['username'])){
                $this->username = $data['username'];
            }
            if(isset($data['f_name'])){
                $this->fName = $data['f_name'];
            }
            if(isset($data['l_name'])){
                $this->lName = $data['l_name'];
            }
            if(isset($data['contact'])){
                $this->contactName = $data['contact'];
            }
            if(isset($data['email'])){
                $this->email = $data['email'];
            }
            if(isset($data['phone'])){
                $this->phone = $data['phone'];
            }
            if(isset($data['logo'])){
                $this->logo = $data['logo'];
            }
            if(isset($data['active'])){
                $this->active = $data['active'];
            }
            if(isset($data['password'])){
                $this->password = $data['password'];
            }
            if(isset($data['new_password'])){
                $this->newPassword = $data['new_password'];
            }
            if(isset($data['bucket_name'])){
                $this->bucketName = $data['bucket_name'];
            }
            if(isset($data['expiry_date'])){
                $this->expiryDate = $data['expiry_date'];
            }
            if(isset($data['user'])){
                $this->checkFirstUser = $data['user'];
            }
            if(isset($data['debug'])){
                $this->debug = $data['debug'];
            }
            if(isset($data['skip_validation'])){
                $this->skip_validation = $data['skip_validation'];
            }
        }
    }

    function getId() {
        return $this->id;
    }

    function getCom_name() {
        return $this->com_name;
    }

    function getCom_address() {
        return $this->com_address;
    }

    function getDbName() {
        return $this->dbName;
    }

    function getUsername() {
        return $this->username;
    }
    
    function getFName() {
        return $this->fName;
    }

    function getLName() {
        return $this->lName;
    }
    
    function getContactName() {
        return $this->contactName;
    }

    function getEmail() {
        return $this->email;
    }

    function getPhone() {
        return $this->phone;
    }

    function getLogo() {
        return $this->logo;
    }
    
    function getActive() {
        return $this->active;
    }
    
    function getPassword() {
        return $this->password;
    }
    
    function getPasswordHash() {
        return hash('sha256', $this->password);
    }
    
    function getNewPassword() {
        return $this->newPassword;
    }

    function getNewPasswordHash() {
        return hash('sha256', $this->newPassword);
    }
     
    function getBucketName() {
        return $this->bucketName;
    }
    
    function getExpiryDate() {
        return $this->expiryDate;
    }

    function getCheckFirstUser() {
        return $this->checkFirstUser;
    }
    
    function getDebug() {
        return $this->debug;
    }
    
    function getSkip_validation() {
        return $this->skip_validation;
    }
    
    function setId($id) {
        $this->id = $id;
    }

    function setCom_name($com_name) {
        $this->com_name = $com_name;
    }

    function setCom_address($com_address) {
        $this->com_address = $com_address;
    }
    
    function setDbName($dbName) {
        $this->dbName = $dbName;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setFName($fName) {
        $this->fName = $fName;
    }

    function setLName($lName) {
        $this->lName = $lName;
    }
    
    function setContactName($contactName) {
        $this->contactName = $contactName;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setPhone($phone) {
        $this->phone = $phone;
    }

    function setLogo($logo) {
        $this->logo = $logo;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setNewPassword($newPassword) {
        $this->newPassword = $newPassword;
    }

    function setBucketName($bucketName) {
        $this->bucketName = $bucketName;
    }

    function setExpiryDate($expiryDate) {
        $this->expiryDate = $expiryDate;
    }

    function setCheckFirstUser($checkFirstUser) {
        $this->checkFirstUser = $checkFirstUser;
    }

    function setDebug($debug) {
        $this->debug = $debug;
    }
    
    function setSkip_validation($skip_validation) {
        $this->skip_validation = $skip_validation;
    }
}

