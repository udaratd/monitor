<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DateAndTimeManager {
    
    function __construct() {
        
    }

    public static function timeConvert($timeString) {
        $temp = strtotime($timeString);
        $time = date("H:i:s",$temp);
        return $time;
    }
    
    public static function dateConvert($dateString) {
        $temp = strtotime($dateString);
        $date = date("Y-m-d",$temp);
        return $date;
    }
    
    public static function createDate($date,$every_days_or_week,$how_meny_times){
        
        $day_by_day = array();
        $today = date_create($date);
        
        for($i=0;$i<$how_meny_times;$i++){

            $days = $every_days_or_week;
            $temp = date_add($today, date_interval_create_from_date_string($days));

            $next_date = date_format($temp,"Y-m-d");

            $day_by_day[$i] = $next_date;
            
            $today = date_create($next_date);
            
        }
        array_unshift($day_by_day, $date);//get the originel date at the top
        return $day_by_day;
    }
    
//    public static function getDateRange($date){
//        $data = array();
//        
//        $year = substr($date, 0, 4);
//        $week = date('W', strtotime($date));
//        
//        $from = date("Y-m-d", strtotime("{$year}-W{$week}+1")); //Returns the date of sunday in week
//        $to = date("Y-m-d", strtotime("{$year}-W{$week}-6")); //Returns the date of saturday in week
//        
//        $datetime1 = date_create($from);
//        $datetime2 = date_create($to);
//        $interval = date_diff($datetime1, $datetime2);
//        $size = $interval->format('%a');//%d to %a
//        
//        $data[0]=$from;
//        for($i=0;$i<$size;$i++){
//            $count = "+".$i+1;
//            $data[$i+1] = date('Y-m-d',strtotime($count." days", strtotime($from)));
//        }
//
//        return $data;
//    }
    
    public static function getDateRange($date){
        $data = array();

        ////// START :- old code for get week start and end dates for selected day, Code deprecated date - 2018-12-20 /////////
        
//        $year = substr($date, 0, 4);
//        $week = date('W', strtotime($date));
//       
//        //get day name
//        $day = date('l', strtotime($date));
//        
//        $from = date("Y-m-d", strtotime("{$year}-W{$week}+1")); //Returns the date of sunday in week
//        $to = date("Y-m-d", strtotime("{$year}-W{$week}-6")); //Returns the date of saturday in week
//       
//        //if given day is Sunday add 7 days for both $from and $to
//        if($day === 'Sunday'){
//            $from = date('Y-m-d', strtotime('7 days', strtotime($from)));
//            $to = date('Y-m-d', strtotime('7 days', strtotime($to)));
//        }
        
        ////// END :- old code for get week start and end dates for selected day /////////
        
        ////// START :- new updated code for get week start and end dates for selected day, Code Add Date - 2018-12-20 /////////
        $tm = strtotime($date);
        $w = date("w", $tm);
        
        $from = date("Y-m-d", $tm - (86400 * $w) );
        $to = date("Y-m-d", $tm + 86400 * (6 - $w) );
        ////// END :- new updated code for get week start and end dates for selected day /////////
        
        $datetime1 = date_create($from);
        $datetime2 = date_create($to);
        $interval = date_diff($datetime1, $datetime2);
        $size = $interval->format('%a');
        
        $data[0]=$from;
        for($i=0;$i<$size;$i++){
            $count = "+".$i+1;
            $data[$i+1] = date('Y-m-d',strtotime($count." days", strtotime($from)));
        }

        return $data;
    }
    
    public static function forwardBackwardDateRange($date=null,$forward=true,$howlong=null){
        $dateRange = array();

        $today = null;
        $long = null;
        
        $from = null;
        $to = null;
        
        if(empty($date)){
            $today = date_create(date('Y-m-d'));
        } else {
            $today = date_create($date);
        }
        
        if(empty($howlong)){
            $long = '7 Days';
        } else {
            $long = $howlong.' Days';
        }

        if($forward === true){
            $from = $today;
            $from = date_format($from, 'Y-m-d');

            $to = date_add($today, date_interval_create_from_date_string($long));
            $to = date_format($to, 'Y-m-d');

            $datetime1 = date_create($from);
            $datetime2 = date_create($to);
            $interval = date_diff($datetime1, $datetime2);
            $size = $interval->format('%a');
            
            $dateRange[0]=$from;
            for($i=0;$i<$size;$i++){
                $count = "+".$i+1;
                $dateRange[$i+1] = date('Y-m-d',strtotime($count." days", strtotime($from)));
            }
            
        } else {
            $to = $today;
            $to = date_format($to, 'Y-m-d');

            $from = date_sub($today, date_interval_create_from_date_string($long));
            $from = date_format($from, 'Y-m-d');
            
            $datetime1 = date_create($from);
            $datetime2 = date_create($to);
            $interval = date_diff($datetime1, $datetime2);
            $size = $interval->format('%a');
            
            $dateRange[0]=$from;
            for($i=0;$i<$size;$i++){
                $count = "+".$i+1;
                $dateRange[$i+1] = date('Y-m-d',strtotime($count." days", strtotime($from)));
            }
            
        }
        
        return $dateRange;
    }
}