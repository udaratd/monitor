<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Report {
    
    private $id;
    private $s_date;
    private $e_date;
    private $staffID;
    private $supervisorID;
    private $checkpointID;
    private $locationID;
            
    function __construct($data = null) {
        if($data) {
            if(!empty($data['id'])) {
                $this->id = $data['id'];
            }
            if(!empty($data['s_date'])) {
                $this->s_date = $data['s_date'];
            }
            if(!empty($data['e_date'])) {
                $this->e_date = $data['e_date'];
            }
            if(!empty($data['staffID'])) {
                $this->staffID = $data['staffID'];
            }
            if(!empty($data['supervisorID'])) {
                $this->supervisorID = $data['supervisorID'];
            }
            if(!empty($data['checkpointID'])) {
                $this->checkpointID = $data['checkpointID'];
            }
            if(!empty($data['locationID'])) {
                $this->locationID = $data['locationID'];
            }
        }
    }
    
    function getId() {
        return $this->id;
    }

    function getS_date() {
        return $this->s_date;
    }

    function getE_date() {
        return $this->e_date;
    }

    function getStaffID() {
        return $this->staffID;
    }

    function getSupervisorID() {
        return $this->supervisorID;
    }

    function getCheckpointID() {
        return $this->checkpointID;
    }

    function getLocationID() {
        return $this->locationID;
    }

    function setId($id) {
        $this->id = $id;
    }
    
    function setS_date($s_date) {
        $this->s_date = $s_date;
    }

    function setE_date($e_date) {
        $this->e_date = $e_date;
    }
    
    function setStaffID($staffID) {
        $this->staffID = $staffID;
    }

    function setSupervisorID($supervisorID) {
        $this->supervisorID = $supervisorID;
    }

    function setCheckpointID($checkpointID) {
        $this->checkpointID = $checkpointID;
    }

    function setLocationID($locationID) {
        $this->locationID = $locationID;
    }

}
