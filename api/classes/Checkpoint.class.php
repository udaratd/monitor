<?php
/**
 * Description of Checkpoint
 * 
 * @author doweby
 */
class Checkpoint{
    private $id;
    private $chekPoinName;
    private $locationid;
    private $description;
    private $chekImage;
    private $limit;
    private $s_limit;
    private $e_limit;
    private $pageNumber;
    private $q = array();
    
    function __construct() {
        $numOfAgv = func_get_args();
        switch (func_num_args()) {
            case 3:
                self::__construct1($numOfAgv[0],$numOfAgv[1],$numOfAgv[2]);
                break;
            case 5:
                self::__construct2($numOfAgv[0],$numOfAgv[1],$numOfAgv[2],$numOfAgv[3],$numOfAgv[4]);
            default :
                break;
        }
    }
    function __construct1($chekPoinName, $locationid, $chekImage) {
        $this->chekPoinName = $chekPoinName;
        $this->locationid = $locationid;
        $this->chekImage = $chekImage;
    }
    function __construct2($id,$chekPoinName, $locationid, $image, $description) {
        $this->id = $id;
        $this->chekPoinName = $chekPoinName;
        $this->locationid = $locationid;
        $this->chekImage = $image;
        $this->description = $description;
        
        if(!empty($this->chekPoinName)){
            $this->q[] = "name = :name";
        }
        if(!empty($this->locationid)){
            $this->q[] = "locationid = :locationid";
        }
        if(!empty($this->description)){
            $this->q[] = "description = :description";
        }
        if(!empty($this->chekImage)){
            $this->q[] = "image = :image";
        }
    }
    
    function getId() {
        return $this->id;
    }

    function getChekPoinName() {
        return $this->chekPoinName;
    }

    function getLocationid() {
        return $this->locationid;
    }

    function getDescription() {
        return $this->description;
    }

    function getChekImage() {
        return $this->chekImage;
    }
    
    function getLimit() {
        return $this->limit;
    }

    function getS_limit() {
        return $this->s_limit;
    }

    function getE_limit() {
        return $this->e_limit;
    }

    function getPageNumber() {
        return $this->pageNumber;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setChekPoinName($chekPoinName) {
        $this->chekPoinName = $chekPoinName;
    }

    function setLocationid($locationid) {
        $this->locationid = $locationid;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setChekImage($chekImage) {
        $this->chekImage = $chekImage;
    }

    function setLimit($limit) {
        $this->limit = $limit;
    }

    function setS_limit($s_limit) {
        $this->s_limit = $s_limit;
    }

    function setE_limit($e_limit) {
        $this->e_limit = $e_limit;
    }

    function setPageNumber($pageNumber) {
        $this->pageNumber = $pageNumber;
    }
    
    function getQ() {
        return $this->q;
    }

    function setQ($q) {
        $this->q = $q;
    }

            /**
     * This function check image availability and image string validation
     * @return boolean return True on if image available False on not available 
     */
    function hasImage(){
        if(empty($this->chekImage)||$this->chekImage==null){//validate image String
            return false;
        }else{
            return true;
        }
    }

}

