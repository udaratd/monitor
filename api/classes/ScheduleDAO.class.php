<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class ScheduleDAO {
    
    
    public static function addDailySchedule(Schedule $schedule){
        $success = false;
        if($_SESSION['SKIP_VALIDATION'] == 1){
            $success = true;
        } else {
            $success = ScheduleDAO::checkSchedule($schedule);//(USER OVERLAP VALIDATION) IF SOMETHING HAPPENS, REMOVE BELOW LINE AND ACTIVE THIS LINE
        }
        
        $database = new database();
        $database->beginTransaction();
        try {
            
            if($success !== true) {
                if($success === false) {
                    throw new Exception("Check user function not working");
                } else {
                    return $success;
                }
            }
            
            $query = "INSERT INTO taskandschedule(user_id,checkpoint_id,repeatType_id,description,startTime,endTime,scheduleStatus) "
                    . "VALUES(:user_id,:check_id,:repeat_id,:description,:s_time,:e_time,:scheduleStatus)";
            
            $database->query($query);
            
            $database->bind(':user_id', $schedule->getUser_id());
            $database->bind(':check_id', $schedule->getCheckpoint_id());
            $database->bind(':repeat_id', $schedule->getRepeatType_id());
            $database->bind(':description', $schedule->getDescription());
            $database->bind(':s_time', $schedule->getStartTime());//24 hours
            $database->bind(':e_time', $schedule->getEndTime());//24 hours
            $database->bind(':scheduleStatus', 0);


            $result = $database->execute();
            
            if($result) {
                $ID = $database->lastInsertId();
                $database->endTransaction();
                return $ID;
            } else {
                return false;
            }
               
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            $database->cancelTransaction();
            return false;
        }
    }
    
    public static function addWeeklySchedule(Schedule $schedule){
        $success = false;
        if($_SESSION['SKIP_VALIDATION'] == 1){
            $success = true;
        } else {
            $success = ScheduleDAO::checkSchedule($schedule);//(USER OVERLAP VALIDATION) IF SOMETHING HAPPENS, REMOVE BELOW LINE AND ACTIVE THIS LINE
        }
        
        $database = new database();
        $database->beginTransaction();
        try {
            
            if($success !== true) {
                if($success === false) {
                    throw new Exception("Check user function not working");
                } else {
                    return $success;
                }
            }
            
            $query = "INSERT INTO taskandschedule(user_id,checkpoint_id,repeatType_id,description,startTime,endTime,sunday,monday,tuesday,wednesday,thursday,friday,saturday,scheduleStatus) "
                    . "VALUES(:user_id,:check_id,:repeat_id,:description,:s_time,:e_time,:sunday,:monday,:tuesday,:wednesday,:thursday,:friday,:saturday,:scheduleStatus)";
            
            $database->query($query);
            
            $database->bind(':user_id', $schedule->getUser_id());
            $database->bind(':check_id', $schedule->getCheckpoint_id());
            $database->bind(':repeat_id', $schedule->getRepeatType_id());
            $database->bind(':description', $schedule->getDescription());
            $database->bind(':s_time', $schedule->getStartTime());//24 hours
            $database->bind(':e_time', $schedule->getEndTime());//24 hours
            $database->bind(':sunday', $schedule->getSunday());
            $database->bind(':monday', $schedule->getMonday());
            $database->bind(':tuesday', $schedule->getTuesday());
            $database->bind(':wednesday', $schedule->getWednesday());
            $database->bind(':thursday', $schedule->getThursday());
            $database->bind(':friday', $schedule->getFriday());
            $database->bind(':saturday', $schedule->getSaturday());
            $database->bind(':scheduleStatus', 0);
            
              
            $result = $database->execute();
            
            if($result) {
                $ID = $database->lastInsertId();
                $database->endTransaction();
                return $ID;
            } else {
                throw new Exception("Can not create Weekly Schedule");
            }
               
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            $database->cancelTransaction();
            return false;
        }
    }
    
    public static function addOneDaySchedule(Schedule $schedule,$QR = false){
        $success = false;
        if($_SESSION['SKIP_VALIDATION'] == 1){
            $success = true;
        } else {
            $success = ScheduleDAO::checkSchedule($schedule);//(USER OVERLAP VALIDATION) IF SOMETHING HAPPENS, REMOVE BELOW LINE AND ACTIVE THIS LINE
        }
        
        $database = new database();
        $database->beginTransaction();
        try {
            
            $scheduleStatus = 2;//QR Schedule
            
            if($QR === false) {
                $scheduleStatus = 0;//Original Schedule
                if($success !== true) {
                    if($success === false) {
                        throw new Exception("Check user function not working");
                    } else {
                        return $success;
                    }
                }
            }

//            $date1 = date("Y-m-d");
//            $temp = strtotime($schedule->getOneTimeDate());
//            $date2 = date("Y-m-d",$temp);
//            
//            if($date1 > $date2){
//                throw new Exception("Invalid Date");
//            }
            
            $query = "INSERT INTO taskandschedule(user_id,checkpoint_id,repeatType_id,description,startTime,endTime,oneTimeDate,scheduleStatus) "
                    . "VALUES(:user_id,:check_id,:repeat_id,:description,:s_time,:e_time,:date,:scheduleStatus)";
            
            $database->query($query);
            
            $database->bind(':user_id', $schedule->getUser_id());
            $database->bind(':check_id', $schedule->getCheckpoint_id());
            $database->bind(':repeat_id', $schedule->getRepeatType_id());
            $database->bind(':description', $schedule->getDescription());
            $database->bind(':s_time', $schedule->getStartTime());//24 hours
            $database->bind(':e_time', $schedule->getEndTime());//24 hours
            $database->bind(':date', $schedule->getOneTimeDate());
            $database->bind(':scheduleStatus', $scheduleStatus);

            $result = $database->execute();
            
            if($result) {
                $ID = $database->lastInsertId();
                $database->endTransaction();
                return $ID;
            } else {
                throw new Exception("Can not create One Day Schedule");
            }
               
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            $database->cancelTransaction();
            return false;
        }
    }
    
    /**
     * 
     * @param Schedule $schedule
     * @return Shedule Array() an array with schedule objects if success false in fail
     */
    public static function checkUser(Schedule $new_schedule) {
//        $reschedule_IDs = ScheduleDAO::getTemporaryScheduleIDs();
        
        $database = new database();
        $database->beginTransaction();
        try {
            $query = "SELECT * "
                    . "FROM taskandschedule "
                    . "WHERE ( "
                        . "( "
                            . "( (startTime BETWEEN :s_time AND :e_time) OR (endTime BETWEEN :s_time AND :e_time) ) AND user_id = :user_id AND scheduleStatus = 0 "
                        . ") "
                    . "OR "
                        . "( "
                            . "( (startTime BETWEEN :s_time AND :e_time) OR (endTime BETWEEN :s_time AND :e_time) ) AND user_id = :user_id AND scheduleStatus = 1 AND endDate > CURDATE()"
                        . ")"
                    . ")";
            
//            if(is_array($reschedule_IDs)) {
//                if(sizeof($reschedule_IDs)>0){
//                    $query = $query." AND ( ID != ". implode(" AND ID != ", $reschedule_IDs).")";
//                }
//            }
            
            $database->query($query);
            
            $database->bind(':user_id', $new_schedule->getUser_id());
            $database->bind(':s_time', $new_schedule->getStartTime());
            $database->bind(':e_time', $new_schedule->getEndTime());
            
            $success = $database->execute();
            
            $result = $database->resultset();

            if($success) {
                $schedules = array();
                foreach ($result as $schedule_array){
                    $schedule = new Schedule($schedule_array);
                    array_push($schedules, $schedule);
                }
                return $schedules;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            $database->cancelTransaction();
            return false;
        }
    }
    
    public static function checkSchedule(Schedule $new_schedule) {
        $current_schedule_s = ScheduleDAO::checkUser($new_schedule);
        
        try {
            
            $success = null;
            $error = array();
            
            if ($current_schedule_s) {
//                echo 'current Schedule is ';
//                print_r($current_schedule_s);
//                echo '<br>';
//                echo 'New Shedule is : ';
//                print_r($new_schedule);

                foreach ($current_schedule_s as $current_schedule) {

                    $ID = $current_schedule->getId();
                    $current_schedule_sDate = $current_schedule->getStartDate();
                    $current_schedule_eDate = $current_schedule->getEndDate();
                    $new_schedule_sDate = $new_schedule->getStartDate();
                    $new_schedule_eDate = $new_schedule->getEndDate();
                    
                    switch ($current_schedule->getRepeatType_id()) {
                        case 1:  
                            
                            if(!empty($current_schedule_sDate) && !empty($current_schedule_eDate)){
                                if( ( ($new_schedule_sDate >= $current_schedule_sDate) &&  ($new_schedule_sDate <= $current_schedule_eDate) ) || ( ($new_schedule_eDate >= $current_schedule_sDate) &&  ($new_schedule_eDate <= $current_schedule_eDate) ) ){
                                    $error["daily"] = $ID;
                                }
                            } else {
                                $error["daily"] = $ID;
                            }
                            
                            break;
                        
                        case 2 :
                            
                            if(!empty($current_schedule_sDate) && !empty($current_schedule_eDate)){//THIS IF PART PREVENT user has a reschedule and assing new reschedule for that user in previous reschedule time period
                                if( ( ($new_schedule_sDate >= $current_schedule_sDate) &&  ($new_schedule_sDate <= $current_schedule_eDate) ) || ( ($new_schedule_eDate >= $current_schedule_sDate) &&  ($new_schedule_eDate <= $current_schedule_eDate) ) ){
                                    
                                    if($new_schedule->getRepeatType_id() == 2){
                                        if($current_schedule->getSunday() == 1 && $new_schedule->getSunday() == 1) {//edited, $new_schedule part
                                            $error["sunday"] = $ID;
                                        }
                                        if($current_schedule->getMonday() == 1 && $new_schedule->getMonday() == 1) {
                                            $error["monday"] = $ID;
                                        }
                                        if($current_schedule->getTuesday() == 1 && $new_schedule->getTuesday() == 1) {
                                            $error["tuesday"] = $ID;
                                        }
                                        if($current_schedule->getWednesday() == 1 && $new_schedule->getWednesday() == 1) {
                                            $error["wednesday"] = $ID;
                                        }
                                        if($current_schedule->getThursday() == 1 && $new_schedule->getThursday() == 1) {
                                            $error["thursday"] = $ID;
                                        }
                                        if($current_schedule->getFriday() == 1 && $new_schedule->getFriday() == 1) {
                                            $error["friday"] = $ID;
                                        }
                                        if($current_schedule->getSaturday() == 1 && $new_schedule->getSaturday() == 1) {
                                            $error["saturday"] = $ID;
                                        }
                                    } else {
                                        if($current_schedule->getSunday() == 1) {//edited, $new_schedule part
                                            $error["sunday"] = $ID;
                                        }
                                        if($current_schedule->getMonday() == 1) {
                                            $error["monday"] = $ID;
                                        }
                                        if($current_schedule->getTuesday() == 1) {
                                            $error["tuesday"] = $ID;
                                        }
                                        if($current_schedule->getWednesday() == 1) {
                                            $error["wednesday"] = $ID;
                                        }
                                        if($current_schedule->getThursday() == 1) {
                                            $error["thursday"] = $ID;
                                        }
                                        if($current_schedule->getFriday() == 1) {
                                            $error["friday"] = $ID;
                                        }
                                        if($current_schedule->getSaturday() == 1) {
                                            $error["saturday"] = $ID;
                                        }
                                    }
                                }
                            } else {
                                if($new_schedule->getRepeatType_id() == 2){// This part for crea
                                    if($current_schedule->getSunday() == 1 && $new_schedule->getSunday() == 1) {//edited, $new_schedule part
                                        $error["sunday"] = $ID;
                                    }
                                    if($current_schedule->getMonday() == 1 && $new_schedule->getMonday() == 1) {
                                        $error["monday"] = $ID;
                                    }
                                    if($current_schedule->getTuesday() == 1 && $new_schedule->getTuesday() == 1) {
                                        $error["tuesday"] = $ID;
                                    }
                                    if($current_schedule->getWednesday() == 1 && $new_schedule->getWednesday() == 1) {
                                        $error["wednesday"] = $ID;
                                    }
                                    if($current_schedule->getThursday() == 1 && $new_schedule->getThursday() == 1) {
                                        $error["thursday"] = $ID;
                                    }
                                    if($current_schedule->getFriday() == 1 && $new_schedule->getFriday() == 1) {
                                        $error["friday"] = $ID;
                                    }
                                    if($current_schedule->getSaturday() == 1 && $new_schedule->getSaturday() == 1) {
                                        $error["saturday"] = $ID;
                                    }
                                } else {
                                    if($current_schedule->getSunday() == 1) {//edited, $new_schedule part
                                        $error["sunday"] = $ID;
                                    }
                                    if($current_schedule->getMonday() == 1) {
                                        $error["monday"] = $ID;
                                    }
                                    if($current_schedule->getTuesday() == 1) {
                                        $error["tuesday"] = $ID;
                                    }
                                    if($current_schedule->getWednesday() == 1) {
                                        $error["wednesday"] = $ID;
                                    }
                                    if($current_schedule->getThursday() == 1) {
                                        $error["thursday"] = $ID;
                                    }
                                    if($current_schedule->getFriday() == 1) {
                                        $error["friday"] = $ID;
                                    }
                                    if($current_schedule->getSaturday() == 1) {
                                        $error["saturday"] = $ID;
                                    }
                                }
                            }
                            break;
                            
                        case 3:

                            if($new_schedule->getRepeatType_id() == 3){
                                
                                date_default_timezone_set('Asia/Colombo');
                            
                                $today = date("Y-m-d");
                                $currentTime = date("H:i:s");

                                if($today > $new_schedule->getOneTimeDate() && !empty($new_schedule->getOneTimeDate())) {
    //                                throw new Exception("Your Schedule date is Invalid");//not working this line
                                    $error["InvalidDate"] = 'Schedule date is already pass';
                                }

                                if ($today == $new_schedule->getOneTimeDate() && $currentTime > $new_schedule->getStartTime() && !empty($new_schedule->getOneTimeDate())) {
    //                                throw new Exception("Invalid Schedule Start Time");//not working this line
                                    $error["InvalidTime"] = 'Schedule time is already pass';
                                }

                                $currentSchedule_oneTimeDate = $current_schedule->getOneTimeDate();
                                $newSchedule_oneTimeDate = $new_schedule->getOneTimeDate();

                                if($currentSchedule_oneTimeDate < $newSchedule_oneTimeDate){
                                    $success = true;//OK
                                }
                                if ($currentSchedule_oneTimeDate == $newSchedule_oneTimeDate) {

                                    $currentSchedule_startTime = $current_schedule->getStartTime();
                                    $currentSchedule_endTime = $current_schedule->getEndTime();

                                    $newSchedule_startTime = $new_schedule->getStartTime();
                                    $newSchedule_endTime = $new_schedule->getEndTime();

                                    if( ($currentSchedule_startTime <= $newSchedule_startTime) && ($newSchedule_startTime > $currentSchedule_endTime) || ($currentSchedule_startTime < $newSchedule_endTime) && ($newSchedule_endTime >= $currentSchedule_endTime) ) {
                                        $error["oneDay"] = $ID;
                                    }
                                }
                                if($currentSchedule_oneTimeDate > $newSchedule_oneTimeDate) {
                                    $success = true;//OK
                                }
                                
                            } elseif($new_schedule->getRepeatType_id() == 2) {
                                if($new_schedule->getSunday() == 1) {//edited, $new_schedule part
                                    $error["sunday"] = $ID;
                                }
                                if($new_schedule->getMonday() == 1) {
                                    $error["monday"] = $ID;
                                }
                                if($new_schedule->getTuesday() == 1) {
                                    $error["tuesday"] = $ID;
                                }
                                if($new_schedule->getWednesday() == 1) {
                                    $error["wednesday"] = $ID;
                                }
                                if($new_schedule->getThursday() == 1) {
                                    $error["thursday"] = $ID;
                                }
                                if($new_schedule->getFriday() == 1) {
                                    $error["friday"] = $ID;
                                }
                                if($new_schedule->getSaturday() == 1) {
                                    $error["saturday"] = $ID;
                                }
                            } else {
                                $error["daily"] = $ID;
                            }
                            
                            
                            break;
                            
                        default:
                            return true;
                            break;
                    }
                }
                
                if(count($error) == 0) {
                    return true;
                } else {
                    return $error;
                }
                
            } else {
                return true;//allow assign an user
            }                 
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            echo $exc->getMessage();
            return false;
        }
    }
    
    public static function updateFields(Schedule $schedule) {
        $fields = array();

        if(!empty($schedule->getUser_id())) {
            $fields[] = "user_id = :user_id";
        }
        if(!empty($schedule->getCheckpoint_id())) {
            $fields[] = "checkpoint_id = :check_id";
        }
        if(!empty($schedule->getRepeatType_id())) {
            $fields[] = "repeatType = :repeat_id";
        }
        if(!empty($schedule->getDescription())) {
            $fields[] = "description = :description";
        }
        if(!empty($schedule->getStartTime())) {
            $fields[] = "startTime = :s_time";
        }
        if(!empty($schedule->getEndTime())) {
            $fields[] = "endTime = :e_time";
        }
        if(!empty($schedule->getOneTimeDate())) {
            $fields[] = "oneTimeDate = :oneTimeDate";
        }
        if(!empty($schedule->getSunday())) {
            $fields[] = "sunday = :sunday";
        }
        if(!empty($schedule->getMonday())) {
            $fields[] = "monday = :monday";
        }
        if(!empty($schedule->getTuesday())) {
            $fields[] = "tuesday = :tuesday";
        }
        if(!empty($schedule->getWednesday())) {
            $fields[] = "wednesday = :wednesday";
        }
        if(!empty($schedule->getThursday())) {
            $fields[] = "thursday = :thursday";
        }
        if(!empty($schedule->getFriday())) {
            $fields[] = "friday = :friday";
        }
        if(!empty($schedule->getSaturday())) {
            $fields[] = "saturday = :saturday";
        }
        if(!empty($schedule->getStartDate())) {
            $fields[] = "startDate = :startDate";
        }
        if(!empty($schedule->getEndDate())) {
            $fields[] = "endDate = :endDate";
        }    
        return $fields;
    }
    
    public static function insertFields(Schedule $schedule) {
        $data = array();
        $fields = array();
        $value = array();
        if(!empty($schedule->getUser_id())) {
            $fields[] = "user_id";
            $value[] = ":user_id";
        }
        if(!empty($schedule->getCheckpoint_id())) {
            $fields[] = "checkpoint_id";
            $value[] = ":check_id";
        }
        if(!empty($schedule->getRepeatType_id())) {
            $fields[] = "repeatType_id";
            $value[] = ":repeat_id";
        }
        if(!empty($schedule->getDescription())) {
            $fields[] = "description";
            $value[] = ":description";
        }
        if(!empty($schedule->getStartTime())) {
            $fields[] = "startTime";
             $value[] = ":s_time";
        }
        if(!empty($schedule->getEndTime())) {
            $fields[] = "endTime";
            $value[] = ":e_time";
        }
        if(!empty($schedule->getOneTimeDate())) {
            $fields[] = "oneTimeDate";
            $value[] = ":oneTimeDate";
        }
        if(!empty($schedule->getSunday())) {
            $fields[] = "sunday";
            $value[] = ":sunday";
        }
        if(!empty($schedule->getMonday())) {
            $fields[] = "monday";
            $value[] = ":monday";
        }
        if(!empty($schedule->getTuesday())) {
            $fields[] = "tuesday";
            $value[] = ":tuesday";
        }
        if(!empty($schedule->getWednesday())) {
            $fields[] = "wednesday";
            $value[] = ":wednesday";
        }
        if(!empty($schedule->getThursday())) {
            $fields[] = "thursday";
            $value[] = ":thursday";
        }
        if(!empty($schedule->getFriday())) {
            $fields[] = "friday";
            $value[] = ":friday";
        }
        if(!empty($schedule->getSaturday())) {
            $fields[] = "saturday";
            $value[] = ":saturday";
        }
        if(!empty($schedule->getStartDate())) {
            $fields[] = "startDate";
            $value[] = ":startDate";
        }
        if(!empty($schedule->getEndDate())) {
            $fields[] = "endDate";
            $value[] = ":endDate";
        }
        if(!empty($schedule->getScheduleStatus())) {
            $fields[] = "scheduleStatus";
            $value[] = ":scheduleStatus";
        }
        if(!empty($schedule->getId())) {
            $fields[] = "sid";
            $value[] = ":sid";
        }
        array_push($data, $fields);
        array_push($data, $value);
        return $data;
    }
   
    public static function getTemporaryScheduleIDs($s_date, $e_date) {
        $database = new database();
        $database->beginTransaction();
        try {
            
            $IDs = array();
            
            $query = "SELECT "
                            . "t.sid "
                    . "FROM "
                            . "taskandschedule t "
                    . "WHERE "
                            . "("
                                    . "t.scheduleStatus = :scheduleStatus AND t.repeatType_id = :daily AND ( ((t.startDate BETWEEN :s_date AND :e_date) AND (t.endDate BETWEEN :s_date AND :e_date)) OR (t.startDate BETWEEN :s_date AND :e_date) OR (t.endDate BETWEEN :s_date AND :e_date) OR (t.startDate <= :s_date AND t.endDate >= :e_date) )"
                            . ") OR ("
                                    . "t.scheduleStatus = :scheduleStatus AND t.repeatType_id = :weekly AND ( ((t.startDate BETWEEN :s_date AND :e_date) AND (t.endDate BETWEEN :s_date AND :e_date)) OR (t.startDate BETWEEN :s_date AND :e_date) OR (t.endDate BETWEEN :s_date AND :e_date) OR (t.startDate <= :s_date AND t.endDate >= :e_date) )"
                            . ") OR ("
                                    . "t.scheduleStatus = :scheduleStatus AND t.repeatType_id = :oneDay AND t.oneTimeDate >= CURDATE() AND ( ((t.startDate BETWEEN :s_date AND :e_date) AND (t.endDate BETWEEN :s_date AND :e_date)) OR (t.startDate BETWEEN :s_date AND :e_date) OR (t.endDate BETWEEN :s_date AND :e_date) OR (t.startDate <= :s_date AND t.endDate >= :e_date) )"
                            . ")";
            $database->query($query);
            $database->bind(':scheduleStatus', 1);
            $database->bind(':daily', 1);
            $database->bind(':weekly', 2);
            $database->bind(':oneDay', 3);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
            

            if($database->execute()) {
                $reschedule_ID_s = $database->resultset();
                foreach ($reschedule_ID_s as $reschedule_ID) {
                    $IDs[] = $reschedule_ID['sid'];
                }
                return $IDs;
            } else {
                return false;
            }
        }  catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function searchSchedule($loggedUserID,$user,$checkpoint,$s_date,$e_date,$parent,$child,$order,$s_limit,$e_limit,$all=false) {
        $reschedule_IDs = ScheduleDAO::getTemporaryScheduleIDs($s_date, $e_date);
        
        $database = new database();
        $database->beginTransaction();
        try {
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            
            $supervisorSet = $loggedUserID !== false;
            $parentSet = empty($parent) !== true;
            $childSet = empty($child) !== true;
            
            $data = array();
            $days = null;
            
            $query1 = "SELECT "
                            . "u.ID,"
                            . "u.firstName,"
                            . "u.lastName,"
                            . "m.meta_value as img_original "
                    . "FROM user u "
                    . "INNER JOIN user_meta m ON "
                            . "u.ID = m.user_id "
                    . "INNER JOIN staff_supervisor spv ON "
                            . "u.ID = spv.user_id "
                    . "WHERE (role = :role AND meta_key = :key) AND (u.firstName LIKE :fName OR u.lastName LIKE :lName)";
            
            if($supervisorSet) {
                $query1 = $query1." AND spv.supervisor_id = :spvID";
            }
            
            if($order == false){
                $query1 = $query1." ORDER BY u.ID DESC";
            } else {
                $query1 = $query1." ORDER BY u.ID ASC";
            }
            
            if($all===false){
                if( ($s_limit == 0 || !empty($s_limit)) && !empty($e_limit)){
                    $query1 = $query1. " LIMIT :s_limit,:e_limit";                
                }
            }
            
            $database->query($query1);
            $database->bind(':role', 3);
            $database->bind(':key', 'imagePath');
            $database->bind(':fName', '%'.$user.'%');
            $database->bind(':lName', '%'.$user.'%');
            
            if($supervisorSet) {
                $database->bind(':spvID', $loggedUserID);
            }
            
            if($all === false){
                if(($s_limit == 0 || !empty($s_limit)) && !empty($e_limit)){
                    $database->bind(':s_limit', $s_limit);
                    $database->bind(':e_limit', $e_limit);
                }
            }
            
            $user_s = $database->resultset();
            
            if($user_s) {
                               
                foreach ($user_s as $user) {
                    $query2 = "SELECT "
				. "t.*,"
				. "c.name as checkpointName,"
                                . "l_p.ID as parentID, "
                                . "l_p.name as parentName, "
                                . "l_c.ID as childID, "
                                . "l_c.name as childName "
                            . "FROM "
				. "taskandschedule t "
                            . "INNER JOIN checkpoint c ON "
				. "t.checkpoint_id = c.ID "
                            . "INNER JOIN location l_c ON "
                                . "c.locationid = l_c.ID "
                            . "INNER JOIN location l_p ON "
                                . "l_c.parentid = l_p.ID "
                            . "INNER JOIN user u ON "
                                . "t.user_id = u.ID "
                            . "INNER JOIN staff_supervisor spv ON "
                                . "u.ID = spv.user_id "
                            . "WHERE "
                                . "("
                                    . "("
                                            . "t.user_id = :uid AND (t.scheduleStatus = 1 OR t.scheduleStatus = 0) AND t.repeatType_id = :daily"
                                    . ") OR ("
                                            . "t.user_id = :uid AND (t.scheduleStatus = 1 OR t.scheduleStatus = 0) AND t.repeatType_id = :weekly"
                                    . ") OR ("
                                            . "t.user_id = :uid AND (t.scheduleStatus = 1 OR t.scheduleStatus = 0) AND t.repeatType_id = :oneDay AND t.oneTimeDate >= CURDATE()"
                                    . ")"
                                . ") AND ( "
                                        . "( (t.startDate IS NULL AND t.endDate IS NULL) OR ( ((t.startDate BETWEEN :s_date AND :e_date) AND (t.endDate BETWEEN :s_date AND :e_date)) OR (t.startDate BETWEEN :s_date AND :e_date) OR (t.endDate BETWEEN :s_date AND :e_date) OR (t.startDate <= :s_date AND t.endDate >= :e_date) ) )"
                                    . ")";
//                    (t.startDate IS NULL OR (t.startDate >= :s_date AND t.startDate <= :e_date) ) OR (t.endDate IS NULL OR (t.endDate >= :s_date AND t.endDate <= :e_date) )
                    $supervisorSet = $loggedUserID !== false;
                    if($supervisorSet) {
                        $query2 = $query2." AND spv.supervisor_id = :loggingUserID";
                    }
                    
                    if($parentSet && $childSet){
                        $query2 = $query2." AND l_p.ID = :l_p_ID AND l_c.ID = :l_c_ID";
                    }elseif ($parentSet && !$childSet) {
                        $query2 = $query2." AND l_p.ID = :l_p_ID";
                    }elseif ($childSet && !$parentSet) {
                        $query2 = $query2." AND l_c.ID = :l_c_ID";
                    }
                    
                    $checkpointSet = $checkpoint !== 'All';
                    if($checkpointSet) {
                        $query2 = $query2." AND c.ID = :cid";

                    }
                    
                    if(is_array($reschedule_IDs)) {
                        if(sizeof($reschedule_IDs)>0){
                            $query2 = $query2." AND ( t.ID != ". implode(" AND t.ID != ", $reschedule_IDs).")";
                        }
                    }
                    
                    if($order == false){
                        $query2 = $query2." ORDER BY t.ID DESC";
                    } else {
                        $query2 = $query2." ORDER BY t.ID ASC";
                    }

                    $database->query($query2);
                    $database->bind(':uid', $user['ID']);
                    $database->bind(':daily', 1);
                    $database->bind(':weekly', 2);
                    $database->bind(':oneDay', 3);
                    $database->bind(':s_date', $s_date);
                    $database->bind(':e_date', $e_date);
                    
                    if($supervisorSet) {
                        $database->bind(':loggingUserID', $loggedUserID);
                    }
                    
                    if($parentSet && $childSet){
                        $database->bind(':l_p_ID', $parent);
                        $database->bind(':l_c_ID', $child);
                    }elseif ($parentSet && !$childSet) {
                        $database->bind(':l_p_ID', $parent);
                    }elseif ($childSet && !$parentSet) {
                        $database->bind(':l_c_ID', $child);
                    }
                    
                    if($checkpointSet) {
                        $database->bind(':cid', $checkpoint);
                    }
                    
                    if($database->execute()) {
                        $schedule_s = $database->resultset();
                        $i=0;
                        foreach ($schedule_s as $schedule) {
                            
                            //repeatType Daily
                            if($schedule['repeatType_id'] == 1){
                                $schedule_s[$i]['repeat'] = "Daily";
                            }
                            
                            //repeatType Weekly
                            if($schedule['repeatType_id'] == 2){

                                if($schedule['sunday'] == 1){
                                    $days = $days.'Sunday ';
                                }
                                if($schedule['monday'] == 1){
                                    $days = $days.'Monday ';
                                }
                                if($schedule['tuesday'] == 1){
                                    $days = $days.'Tuesday ';
                                }
                                if($schedule['wednesday'] == 1){
                                    $days = $days.'Wednesday ';
                                }
                                if($schedule['thursday'] == 1){
                                    $days = $days.'Thursday ';
                                }
                                if($schedule['friday'] == 1){
                                    $days = $days.'Friday ';
                                }
                                if($schedule['saturday'] == 1){
                                    $days = $days.'Saturday ';
                                }

                                $schedule_s[$i]['repeat'] = $days;
                                $days = null;
                            }
                            
                            //repeatType One Day
                            if($schedule['repeatType_id'] == 3){                                    
                                $schedule_s[$i]['repeat'] = $schedule['oneTimeDate']; //no need this line 
                            }  
                            $i++;
                        }
                        

                        if(count($schedule_s) > 0) {
                            
                            $imagePath = $user['img_original'];
                
                            if($imagePath !== null){
                                $extension_pos = strrpos($imagePath, '.');

                                $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                                $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);

                                $user['img_thumbnail'] = $thumb;
                                $user['img_medium'] = $medium;
                            } else {
                                $user['img_thumbnail'] = null;
                                $user['img_medium'] = null;
                            }
                            
                            $user['schedule'] = $schedule_s;
                            array_push($data, $user);
                        }
                    }
                }
                $database->endTransaction();
               
                if($data) {
                    return $data;
                } else {
                    return false;
                }
                
            } else {
                return false;
            }
        }  catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function searchScheduleCount($loggedUserID,$user,$order) {        
        $database = new database();
        try {
            
            $supervisorSet = $loggedUserID !== false;
            
            $query1 = "SELECT "
                            . "u.ID,"
                            . "u.firstName,"
                            . "u.lastName,"
                            . "m.meta_value as img_original "
                    . "FROM user u "
                    . "INNER JOIN user_meta m ON "
                            . "u.ID = m.user_id "
                    . "INNER JOIN staff_supervisor spv ON "
                            . "u.ID = spv.user_id "
                    . "WHERE (role = :role AND meta_key = :key) AND (u.firstName LIKE :fName OR u.lastName LIKE :lName)";
            
            if($supervisorSet) {
                $query1 = $query1." AND spv.supervisor_id = :spvID";
            }
            
            if($order == false){
                $query1 = $query1." ORDER BY u.ID DESC";
            } else {
                $query1 = $query1." ORDER BY u.ID ASC";
            }
            
            $database->query($query1);
            $database->bind(':role', 3);
            $database->bind(':key', 'imagePath');
            $database->bind(':fName', '%'.$user.'%');
            $database->bind(':lName', '%'.$user.'%');
            
            if($supervisorSet) {
                $database->bind(':spvID', $loggedUserID);
            }
            
            $count = $database->single();
            
            if($count) {
                return $count;
            } else {
                return false;
            }
        }  catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function singleSchedule($ID) {
        $database = new database();
        try {
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            
            $query = "SELECT
                            t.*,
                            u.firstName,
                            u.lastName,
                            c.name,
                            um.meta_value as img_original
                        FROM
                            taskandschedule t
                        INNER JOIN user u ON
                            t.user_id = u.id
                        INNER JOIN user_meta um ON
                            t.user_id = um.user_id
                        INNER JOIN checkpoint c ON
                            t.checkpoint_id = c.ID 
                        WHERE t.ID = :id AND ( (scheduleStatus = 0 OR scheduleStatus = 1) OR (t.oneTimeDate >= CURDATE()) OR (endDate >= CURDATE()))";
            
            $database->query($query);
            $database->bind(':id', $ID);

            
            if($database->execute()) {
                $schedule = $database->single();
                if($schedule) {
                    
                    $imagePath = $schedule['img_original'];
                
                    if($imagePath !== null){
                        $extension_pos = strrpos($imagePath, '.');

                        $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                        $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);

                        $schedule['img_thumbnail'] = $thumb;
                        $schedule['img_medium'] = $medium;
                    } else {
                        $schedule['img_thumbnail'] = null;
                        $schedule['img_medium'] = null;
                    }                   
                    return $schedule;
                } else {
                    return false;
                }
            } else {
                throw new Exception("Query is not executed properly");
            }
        }  catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getSchedules($userID,$s_date,$e_date) {
        $database = new database();
        try {
            
            
            $query = "SELECT
                            t.*,
                            u.firstName,
                            u.lastName,
                            c.name,
                            um.meta_value as img_original
                        FROM
                            taskandschedule t
                        INNER JOIN user u ON
                            t.user_id = u.id
                        INNER JOIN user_meta um ON
                            t.user_id = um.user_id
                        INNER JOIN checkpoint c ON
                            t.checkpoint_id = c.ID 
                        WHERE t.user_id = :id AND scheduleStatus = 0 AND (t.repeatType_id = 1 OR t.repeatType_id = 2 OR (t.repeatType_id = 3 AND t.oneTimeDate >= :s_date AND :e_date >= t.oneTimeDate ))";
//                        WHERE t.user_id = :id AND scheduleStatus = 0 OR (t.oneTimeDate >= CURDATE()) OR (endDate >= CURDATE()))";
            
            $database->query($query);
            $database->bind(':id', $userID);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);

            
            if($database->execute()) {
                $schedule = $database->resultset();
                if($schedule) {                   
                    return $schedule;
                } else {
                    return false;
                }
            } else {
                throw new Exception("Query is not executed properly");
            }
        }  catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function rescheduleValidation(array $schedule_s) {

        foreach ($schedule_s as $schedule) {

            $schedule_obj = new Schedule($schedule);

            $checkSchedule = ScheduleDAO::checkSchedule($schedule_obj);
            
            if($checkSchedule !== true) {
                if($checkSchedule === false) {
                    throw new Exception("Check user function not working");
                } else {
                    return $checkSchedule;
                }
            }
            
        }
    }

    public static function reSchedule(Schedule $newSchedule) {
//        $checkSchedule = ScheduleDAO::checkSchedule($newSchedule);
        $database = new database();
        $database->beginTransaction();
        try {

//            if($checkSchedule !== true) {
//                if($checkSchedule === false) {
//                    throw new Exception("Check user function not working");
//                } else {
//                    return $checkSchedule;
//                }
//            }
            
            if($newSchedule->getScheduleStatus() == 0) {//this mean permanent reschedule                

                $query1 = "INSERT INTO temp_schedule(schedule_id,user_id,checkpoint_id,repeatType_id,description,startTime,endTime,oneTimeDate,sunday,monday,tuesday,wednesday,thursday,friday,saturday,startDate,endDate,scheduleStatus,sid) "
                        . "SELECT ID,user_id,checkpoint_id,repeatType_id,description,startTime,endTime,oneTimeDate,sunday,monday,tuesday,wednesday,thursday,friday,saturday,startDate,endDate,scheduleStatus,sid FROM taskandschedule WHERE ID = :moveID";

                $database->query($query1);
                $database->bind(':moveID', $newSchedule->getId());
                $tempInsert = $database->execute();
                
                $query2 = "UPDATE taskandschedule SET user_id = :uid, scheduleStatus = :scheduleStatus WHERE ID = :scheduleID";

                $database->query($query2);
                $database->bind(':scheduleID', $newSchedule->getId());
                $database->bind(':uid', $newSchedule->getUser_id());
                $database->bind(':scheduleStatus', 0);
                $scheduleUpdate = $database->execute();
                
                if($tempInsert && $scheduleUpdate) {
                    $database->endTransaction();
                    return true;
                } else {
                    throw new Exception("Permanent reschedule Can Not SET");
                }
            } else {
                $insert_field_var = ScheduleDAO::insertFields($newSchedule);

                $query3 = "INSERT INTO taskandschedule (". implode(", ", $insert_field_var[0]).") VALUES (". implode(", ", $insert_field_var[1]).")";
                $database->query($query3);
                
                $database->bind(':user_id', $newSchedule->getUser_id());
                $database->bind(':check_id', $newSchedule->getCheckpoint_id());
                $database->bind(':repeat_id', $newSchedule->getRepeatType_id());
                $database->bind(':s_time', $newSchedule->getStartTime());
                $database->bind(':e_time', $newSchedule->getEndTime());
                $database->bind(':startDate', $newSchedule->getStartDate());
                $database->bind(':endDate', $newSchedule->getEndDate());
                $database->bind(':scheduleStatus', $newSchedule->getScheduleStatus());
                $database->bind(':sid', $newSchedule->getId());//for identify witch schedule that going to reschedule
                
                if(!empty($newSchedule->getDescription())){
                    $database->bind(':description', $newSchedule->getDescription());
                }
                
                if($newSchedule->getRepeatType_id() == 2) {
     
                    if(!empty($newSchedule->getSunday())) {
                        $database->bind(':sunday', $newSchedule->getSunday());
                    }
                    if(!empty($newSchedule->getMonday())) {
                        $database->bind(':monday', $newSchedule->getMonday());
                    }
                    if(!empty($newSchedule->getTuesday())) {
                        $database->bind(':tuesday', $newSchedule->getTuesday());
                    }
                    if(!empty($newSchedule->getWednesday())) {
                        $database->bind(':wednesday', $newSchedule->getWednesday());
                    }
                    if(!empty($newSchedule->getThursday())) {
                        $database->bind(':thursday', $newSchedule->getThursday());
                    }
                    if(!empty($newSchedule->getFriday())) {
                        $database->bind(':friday', $newSchedule->getFriday());
                    }
                    if(!empty($newSchedule->getSaturday())) {
                        $database->bind(':saturday', $newSchedule->getSaturday());
                    }
                }
                if($newSchedule->getRepeatType_id() == 3) {
                    
                     if(!empty($newSchedule->getOneTimeDate())) {
                        $database->bind(':oneTimeDate', $newSchedule->getOneTimeDate());
                    }
                }
                
                $set_Newschedule_success = $database->execute();
                
                
                if($set_Newschedule_success) {
                    $database->endTransaction();
                    return true;
                } else {
                    throw new Exception("Temporary reschedule Can Not SET");
                }  
            }
        }  catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            $database->cancelTransaction();
            return false;
        }
    }
    
    public static function deleteSchedule($ID) {
        $database = new database();
        $database->beginTransaction();

        try {
            
            $query1 = "INSERT INTO temp_schedule (schedule_id,user_id,checkpoint_id,repeatType_id,description,startTime,endTime,oneTimeDate,sunday,monday,tuesday,wednesday,thursday,friday,saturday,startDate,endDate,scheduleStatus,sid) SELECT t.ID,t.user_id,t.checkpoint_id,t.repeatType_id,t.description,t.startTime,t.endTime,t.oneTimeDate,t.sunday,t.monday,t.tuesday,t.wednesday,t.thursday,t.friday,t.saturday,t.startDate,t.endDate,t.scheduleStatus,t.sid FROM taskandschedule t WHERE t.ID = :moveID";
            
            $database->query($query1);
            $database->bind(':moveID', $ID);

            $tempInsertSuccess = $database->execute();
            
            $query2 = "DELETE FROM taskandschedule WHERE ID = :id";
            
            $database->query($query2);
            $database->bind(':id', $ID);
            
            $deleteSuccess = $database->execute();
            
            if($tempInsertSuccess && $deleteSuccess) {
                $database->endTransaction();
                return true;
            } else {
                $database->cancelTransaction();
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            echo $exc->getMessage();
            return false;
        }
    }
       
    public static function getInvalidOnedayScheduleID() {
        $database = new database();
        try {
            $query = "SELECT * FROM taskandschedule WHERE oneTimeDate < CURDATE()";
            $database->query($query);
            if($database->execute()) {
                $invalidSchedule_s = $database->resultset();
                
                $success = array();
                
                foreach ($invalidSchedule_s as $invalidSchedule) {
                    
                    $success[] = ScheduleDAO::deleteSchedule($invalidSchedule['ID']);
                }
                
                if(in_array(false, $success,TRUE)) {
                    return false;
                } else {
                    return true;
                }
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function listScheduleForUser($user_id,$s_date,$e_date) {
        $reschedule_IDs = ScheduleDAO::getTemporaryScheduleIDs($s_date,$e_date);
        
        $database = new database();
        $database->beginTransaction();
        try {
            
            $data = array();//return array
            
            $day = strtolower(date("l"));//get day of today
            
            $startTime = null;//this two variables for working time period for specific locatin in 12 hour format
            $endTime = null;
            date_default_timezone_set('Asia/Colombo');//set default time zone to sri lanka (no need)
            
            
            $query1 = "SELECT DISTINCT "
                        . "l_p.ID as parentID, "
                        . "l_p.name as parentName, "
                        . "l_c.ID as childID, "
                        . "l_c.name as childName "
                    . "FROM "
                        . "taskandschedule t "
                    . "INNER JOIN checkpoint c ON "
                        . "t.checkpoint_id = c.ID "
                    . "INNER JOIN location l_c ON "
                        . "c.locationid = l_c.ID "
                    . "INNER JOIN location l_p ON "
                        . "l_c.parentid = l_p.ID "
                    . "WHERE t.user_id = :userid "
                    . "ORDER BY parentName, childName ";
            
            $database->query($query1);
            $database->bind(':userid', $user_id);
            
            if($database->execute()) {
                $location_s = $database->resultset();

                if(count($location_s)== 0) {
                    return null;
                }
                
                
                $query3 = "SELECT schedule_id FROM work_list WHERE server_date = CURDATE()";
                $database->query($query3);

                $workTable_scheduleID_s = null;
                if($database->execute()){
                    $workTable_scheduleID_s = $database->resultset();
                }
                
                
                $i=0;//identify the location_s indexces
                foreach ($location_s as $location){
                    
                
                    $query2 = "SELECT "
                                . "t.*,"
                                . "c.name as checkpointName "
                            . "FROM "
                                . "taskandschedule t "
                            . "INNER JOIN checkpoint c ON "
                                . "t.checkpoint_id = c.ID "
                            . "INNER JOIN location l_c ON "
                                . "c.locationid = l_c.ID "
                            . "WHERE t.user_id = :user_id AND ( (repeatType_id = 1) OR ( (repeatType_id = 2) AND (t.".$day." = 1) ) OR ( (repeatType_id = 3) AND (oneTimeDate = CURDATE()) ) ) AND l_c.ID = :childID ";

                    if(is_array($reschedule_IDs)) {
                        if(sizeof($reschedule_IDs)>0){
                            $query2 = $query2."AND ( t.ID != ". implode(" AND t.ID != ", $reschedule_IDs).") ";
                        }
                    }

                    $query2 = $query2."ORDER BY t.startTime ";

                    $database->query($query2);
                    $database->bind(':user_id', $user_id);
                    $database->bind(':childID', $location['childID']);

                    if($database->execute()) {
                        $schedule_s = $database->resultset();

                        if(count($schedule_s) == 0) {
                            unset($location_s[$i]);//$i needed for remove locations that doesnt have schedules
                        } else {
                            
                            $k=0;
                            
                            foreach ($schedule_s as $schedule) {
                            
 
                                $scheduleID = $schedule['ID'];

//                                $key = array_search($scheduleID, array_column($workTable_scheduleID_s, 'schedule_id'));

                                foreach ($workTable_scheduleID_s as $workTable_scheduleID){
                                    
                                    if($scheduleID === $workTable_scheduleID['schedule_id']){
                                        $schedule_s[$k]['isSubmit'] = true;
                                        $isComplete = true;
                                    }
             
                                }                        
                              $k++;
                              
                                
                                if(count($schedule_s) == 1) {//has one schedule

                                    $startTime = date('h:i:s a', strtotime($schedule['startTime']));
                                    $endTime = date('h:i:s a', strtotime($schedule['endTime']));

                                    $location['timeperiod'] = $startTime.' - '.$endTime;
                                }elseif (count($schedule_s) > 1) {//more than one schedule

                                    $startTime = date('h:i:s a', strtotime($schedule_s[0]['startTime']));
                                    $endTime = date('h:i:s a', strtotime($schedule_s[count($schedule_s)-1]['endTime']));

                                    $location['timeperiod'] = $startTime.' - '.$endTime;
                                }
                                
                                

                                $location['schedules'] = $schedule_s;
                            }
                            
                            array_push($data, $location);
                        } 
                    } else {
                        throw new Exception("can not access schedules of given user");
                    }
//                    $i++;
                }
                
                $database->endTransaction();
                
                if(count($data)>0) {
                    return $data;
                } else {
                    throw new Exception("There are no available Schedules");
                }
                
            } else {
                throw new Exception("Can not access locations of given user");
            }
            
        }  catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getScheduleAccordingToCheckpointID($checkpointID){
        $database = new database();
        try {
            $query = "SELECT * FROM taskandschedule WHERE checkpoint_id = :checkpointID";
            $database->query($query);
            $database->bind(':checkpointID', $checkpointID);            
            
            $result = $database->resultset();
            
            if($result){
                return $result;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getSchedule($ID,$s_date,$e_date) {
        $database = new database();
        try {

            $query = "SELECT * FROM taskandschedule WHERE sid = :id AND ( (startDate BETWEEN :s_date AND :e_date) OR (endDate BETWEEN :s_date AND :e_date) )";
            
            $database->query($query);
            $database->bind(':id', $ID);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);

            $schedule = $database->resultset();
                
            if($schedule) {                   
                return $schedule;
            } else {
                return false;
            }            
        }  catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function exportSchedule(){
        $database = new database();
        try {
            date_default_timezone_set('Asia/Colombo');
            $today = date('h:i:s a Y-m-d');
            $filename = "schedule_export_".$today.".csv";
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename='.$filename);
            $output = fopen("php://output", "w");
            fputcsv($output, array('Schedule ID','Staff ID','Staff Name','Checkpoint ID','Checkpoint Name','Schedule Type ID','Schedule Type','Description','Schedule Start Time','Schedule End Time','Date of One Day Schedule','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'));
            
            $query = "SELECT
                        t.ID,
                        t.user_id,
                        CONCAT(u.firstName,' ',u.lastName),                        
                        t.checkpoint_id,
                        c.name,
                        t.repeatType_id,
                        IF(t.repeatType_id=1,'Daily Schedule',( IF(t.repeatType_id=2,'Weekly Schedule', (IF(t.repeatType_id=3,'OneDay Schedule','Invalid Schedule')) ) )),
                        t.description,
                        t.startTime,
                        t.endTime,
                        t.oneTimeDate,
                        t.sunday,
                        t.monday,
                        t.tuesday,
                        t.wednesday,
                        t.thursday,
                        t.friday,
                        t.saturday
                      FROM 
                        taskandschedule t
                      INNER JOIN user u ON
                        t.user_id = u.ID
                      INNER JOIN checkpoint c ON
                        t.checkpoint_id = c.ID";
            
            $database->query($query);

            $result = $database->resultset();

            foreach ($result as $one){
                fputcsv($output, $one);
            }
            fclose($output);   
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
}

