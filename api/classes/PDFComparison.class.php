<?php

class PDFComparison {
    
    function __construct() {
        
    }

    public function printComparison(array $comparison){
        $data = json_encode($comparison,JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        echo $data;
    }
}

