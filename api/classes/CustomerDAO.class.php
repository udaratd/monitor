<?php

class CustomerDAO {
    
    public static function add(Customer $customer){
        $database=new database($_SESSION['DB_NAME']);
        try{
            $imagePath = null;
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            
            $query_1 = "INSERT INTO customer_feedback(checkpoint_id, reaction, feedback, name, phone, feedback_read) VALUES(:checkpoint_id, :reaction, :feedback, :name, :phone, :feedback_read)";

            $database->query($query_1);
            $database->bind(':checkpoint_id', $customer->getCheckpoint_id());
            $database->bind(':reaction', $customer->getReaction());
            $database->bind(':feedback', $customer->getFeedback());
            $database->bind(':name', $customer->getName());
            $database->bind(':phone', $customer->getPhone());
            $database->bind(':feedback_read', $customer->getFeedback_read());
            
            $result1 = $database->execute();

            if($result1){
                $lastInsertedID = $database->lastInsertId();
                
                if($customer->hasImage()) {
                    $image = $customer->getImage();
                    $fileName = $lastInsertedID.'_'.mt_rand(1,5000);
                    $extension = '.jpg';
                                     
                    $path = UPLOAD_CUSTOMER_FEEDBACK_DIR;
                    $imagePath = FileManager::feedBackimageUpload($path, $fileName.$extension, $image);                    
                    if(is_string($imagePath)) {                       
                        FileManager::feedBackimageUpload($path, $fileName.$size_1.$extension, ImageManager::reSize(150, $image));
                        FileManager::feedBackimageUpload($path, $fileName.$size_2.$extension, ImageManager::reSize(500, $image));                    
                    }
                    
                    if($imagePath){
                        $query_2 = "UPDATE customer_feedback SET image = :image WHERE id = :id";
                        $database->query($query_2);
                        $database->bind(':id', $lastInsertedID);
                        $database->bind(':image', $imagePath);
                        $result2 = $database->execute();
                    }
                }
                return $lastInsertedID;
            } else {
                throw new Exception("Can not access last inserted ID");
            }
        } catch (Exception $ex){
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function read(Customer $customer){
        $database=new database();
        try{            
            $query_1 = "UPDATE customer_feedback SET feedback_read = :feedback_read WHERE id = :id";

            $database->query($query_1);
            $database->bind(':id', $customer->getId());
            $database->bind(':feedback_read', $customer->getFeedback_read());
            
            if($database->execute()){
                return true;
            } else {
                return false;
            }          
        } catch (Exception $ex){
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function unreadCount(Customer $customer){
        $database=new database();
        try{            
            $query_1 = "SELECT DISTINCT COUNT(id) as unread_count FROM customer_feedback WHERE feedback_read = :feedback_read";

            $database->query($query_1);
            $database->bind(':feedback_read', $customer->getFeedback_read());
            
            $count = $database->single();
            if($count){
                return $count;
            } else {
                return false;
            }
        } catch (Exception $ex){
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function single(Customer $customeer){
        $database = new database();
        try {
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            $query = "SELECT "
                        . "f.id,"
                        . "f.name,"
                        . "f.phone,"
                        . "f.reaction,"
                        . "IF (f.reaction = 1, 'Bad', IF(f.reaction = 2, 'Good', 'Great') ) as reaction_txt, "
                        . "f.feedback,"                       
                        . "f.feedback_read,"                       
                        . "f.image as fd_image_ori, "
                        . "c.ID as checkpoint_id, "
                        . "c.name as checkpoint_name, "
                        . "c.locationid, "
                        . "c.image as img_original, "
                        . "l_p.name as p_location, "
                        . "l_c.name as c_location "
                    . "FROM customer_feedback f "
                    . "INNER JOIN checkpoint c ON "
                    . "f.checkpoint_id = c.ID "
                    . "INNER JOIN location l_c ON "
                    . "l_c.ID = c.locationid "
                    . "INNER JOIN location l_p ON "
                    . "l_p.ID = l_c.parentid "
                    . "WHERE f.id = :id";

            $database->query($query);
            $database->bind(':id', $customeer->getId()); 
            $checkpoint = $database->single();
            
            if($checkpoint) {
                
                $imagePathFd = $checkpoint['fd_image_ori']; 
                if($imagePathFd !== null){
                    $extension_pos = strrpos($imagePathFd, '.');                       

                    $thumbFd = substr($imagePathFd,0, $extension_pos).$size_1.substr($imagePathFd, $extension_pos);
                    $mediumFd = substr($imagePathFd,0, $extension_pos).$size_2.substr($imagePathFd, $extension_pos);
                    $largeFd = substr($imagePathFd,0, $extension_pos).$size_3.substr($imagePathFd, $extension_pos);

                    $checkpoint['fd_img_thumbnail'] = $thumbFd;
                    $checkpoint['fd_img_medium'] = $mediumFd;
                    $checkpoint['fd_img_large'] = $largeFd;
                } else {
                    $checkpoint['fd_img_thumbnail'] = null;
                    $checkpoint['fd_img_medium'] = null;
                    $checkpoint['fd_img_large'] = null;
                }  
                
                $imagePath = $checkpoint['img_original']; 
                if($imagePath !== null){
                    $extension_pos = strrpos($imagePath, '.');                       

                    $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                    $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);
                    $large = substr($imagePath,0, $extension_pos).$size_3.substr($imagePath, $extension_pos);

                    $checkpoint['img_thumbnail'] = $thumb;
                    $checkpoint['img_medium'] = $medium;
                    $checkpoint['img_large'] = $large;
                } else {
                    $checkpoint['img_thumbnail'] = null;
                    $checkpoint['img_medium'] = null;
                    $checkpoint['img_large'] = null;
                }             
                
                return $checkpoint;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            echo $exc->getMessage();
            return false;
        }
    }

    public static function allCount(Customer $customer){
        $database = new database();
        try {
            
            $query = "SELECT DISTINCT "
                        . "COUNT(f.id) as count "
                    . "FROM customer_feedback f "
                    . "INNER JOIN checkpoint c ON "
                    . "f.checkpoint_id = c.ID "
                    . "INNER JOIN location l_c ON "
                    . "l_c.ID = c.locationid "
                    . "INNER JOIN location l_p ON "
                    . "l_p.ID = l_c.parentid "
                    . "WHERE (f.name LIKE :search OR f.phone LIKE :search)";
            
            if(!empty($customer->getReaction())){
                $query = $query . " AND f.reaction = :reaction";
            }
            
            $database->query($query);
            $database->bind(':search', '%'.$customer->getSearch().'%');
            
            if(!empty($customer->getReaction())){
                $database->bind(':reaction', $customer->getReaction()); 
            }   
            
            $feedbacksCount = $database->single();   
            if($feedbacksCount) {
                return $feedbacksCount;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            echo $exc->getMessage();
            return false;
        }
    }
    
    public static function all(Customer $customer){
        $database = new database();
        try {
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            $query = "SELECT DISTINCT "
                        . "f.id,"
                        . "f.name,"
                        . "f.phone,"
                        . "f.reaction,"
                        . "IF (f.reaction = 1, 'Bad', IF(f.reaction = 2, 'Good', 'Great') ) as reaction_txt, "
                        . "f.feedback,"                       
                        . "f.feedback_read,"                       
                        . "f.image as fd_image_ori, "
                        . "c.ID as checkpoint_id, "
                        . "c.name as checkpoint_name, "
                        . "c.locationid, "
                        . "c.image as img_original, "
                        . "l_p.name as p_location, "
                        . "l_c.name as c_location "
                    . "FROM customer_feedback f "
                    . "INNER JOIN checkpoint c ON "
                    . "f.checkpoint_id = c.ID "
                    . "INNER JOIN location l_c ON "
                    . "l_c.ID = c.locationid "
                    . "INNER JOIN location l_p ON "
                    . "l_p.ID = l_c.parentid "
                    . "WHERE (f.name LIKE :search OR f.phone LIKE :search OR c.name LIKE :search)";
            
            if(!empty($customer->getReaction())){
                $query = $query . " AND f.reaction = :reaction";
            }
            
            $query = $query . " ORDER BY f.id DESC ";
            
            if(!$customer->getLimit()){
                $query = $query. " LIMIT :s_limit,:e_limit";                                
            }

            $database->query($query);
            $database->bind(':search', '%'.$customer->getSearch().'%');
            
            if(!empty($customer->getReaction())){
                $database->bind(':reaction', $customer->getReaction()); 
            }   
            
            if(!$customer->getLimit()){
                $database->bind(':s_limit', $customer->getS_limit());
                $database->bind(':e_limit', $customer->getE_limit());                              
            }
            
            $feedbacks = $database->resultset();  
            
            if($feedbacks) {
                
                $i=0;
                foreach ($feedbacks as $feedbackSingle){
                    
                    $imagePathFd = $feedbackSingle['fd_image_ori']; 
                    if($imagePathFd !== null){
                        $extension_pos = strrpos($imagePathFd, '.');                       

                        $thumbFd = substr($imagePathFd,0, $extension_pos).$size_1.substr($imagePathFd, $extension_pos);
                        $mediumFd = substr($imagePathFd,0, $extension_pos).$size_2.substr($imagePathFd, $extension_pos);
                        $largeFd = substr($imagePathFd,0, $extension_pos).$size_3.substr($imagePathFd, $extension_pos);

                        $feedbacks[$i]['fd_img_thumbnail'] = $thumbFd;
                        $feedbacks[$i]['fd_img_medium'] = $mediumFd;
                        $feedbacks[$i]['fd_img_large'] = $largeFd;
                    } else {
                        $feedbacks[$i]['fd_img_thumbnail'] = null;
                        $feedbacks[$i]['fd_img_medium'] = null;
                        $feedbacks[$i]['fd_img_large'] = null;
                    }  

                    $imagePath = $feedbackSingle['img_original']; 
                    if($imagePath !== null){
                        $extension_pos = strrpos($imagePath, '.');                       

                        $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                        $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);
                        $large = substr($imagePath,0, $extension_pos).$size_3.substr($imagePath, $extension_pos);

                        $feedbacks[$i]['img_thumbnail'] = $thumb;
                        $feedbacks[$i]['img_medium'] = $medium;
                        $feedbacks[$i]['img_large'] = $large;
                    } else {
                        $feedbacks[$i]['img_thumbnail'] = null;
                        $feedbacks[$i]['img_medium'] = null;
                        $feedbacks[$i]['img_large'] = null;
                    }
                    
                    $i++;
                }
                return $feedbacks;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            echo $exc->getMessage();
            return false;
        }
    }
    
    public static function getCheckpoint(Customer $customeer) {
        $database = new database($_SESSION['DB_NAME']);
        try {
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            $query = "SELECT c.ID,c.name,c.locationid,c.description,c.image as img_original,l_p.name as p_location,l_c.name as c_location "
                    . "FROM checkpoint c "
                    . "INNER JOIN location l_c ON "
                    . "l_c.ID = c.locationid "
                    . "INNER JOIN location l_p ON "
                    . "l_p.ID = l_c.parentid "
                    . "WHERE c.ID = :id";
            $database->query($query);
            $database->bind(':id', $customeer->getCheckpoint_id());
            $checkpoint = $database->single();
            
            if($checkpoint) {
                
                $imagePath = $checkpoint['img_original'];               
                if($imagePath !== null){
                    $extension_pos = strrpos($imagePath, '.');                       

                    $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                    $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);
                    $large = substr($imagePath,0, $extension_pos).$size_3.substr($imagePath, $extension_pos);

                    $checkpoint['img_thumbnail'] = $thumb;
                    $checkpoint['img_medium'] = $medium;
                    $checkpoint['img_large'] = $large;
                } else {
                    $checkpoint['img_thumbnail'] = null;
                    $checkpoint['img_medium'] = null;
                    $checkpoint['img_large'] = null;
                }             
                
                return $checkpoint;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }
}

