<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Schedule {
    private $id;
    private $user_id;
    private $checkpoint_id;
    private $repeatType_id;
    private $description;
    private $startTime;
    private $endTime;
    private $oneTimeDate;
    private $sunday=0;
    private $monday=0;
    private $tuesday=0;
    private $wednesday=0;
    private $thursday=0;
    private $friday=0;
    private $saturday=0;
    private $startDate;
    private $endDate;
    private $scheduleStatus;
            
    function __construct(Array $data=null) {
        if($data){
            if(isset($data['ID'])) {
                $this->id = $data['ID'];
            }
            
            if(isset($data['user_id'])){
                $this->user_id = $data['user_id'];
            }
            
            if(isset($data['checkpoint_id'])) {
                $this->checkpoint_id = $data['checkpoint_id'];
            }

            if(isset($data['repeatType_id'])) {
                $this->repeatType_id = $data['repeatType_id'];
            }

            if(isset($data['startTime'])) {
                $this->startTime = Schedule::timeConvert($data['startTime']);
            }

            if(isset($data['endTime'])) {
                $this->endTime = Schedule::timeConvert($data['endTime']);
            }
            
            if(isset($data['description'])) {
                $this->description = $data['description'];
            }
            
            if(isset($data['sunday'])) {
                $this->sunday = $data['sunday'];
            }
            
            if(isset($data['monday'])) {
                $this->monday = $data['monday'];
            }
            
            if(isset($data['tuesday'])) {
                $this->tuesday = $data['tuesday'];
            }
            
            if(isset($data['wednesday'])) {
                $this->wednesday = $data['wednesday'];
            }
            
            if(isset($data['thursday'])) {
                $this->thursday = $data['thursday'];
            }
            
            if(isset($data['friday'])) {
                $this->friday = $data['friday'];
            }
            
            if(isset($data['saturday'])) {
                $this->saturday = $data['saturday'];
            }
            
            if(isset($data['oneTimeDate'])) {
                $this->oneTimeDate = Schedule::dateConvert($data['oneTimeDate']);
            }
            
            if(isset($data['startDate'])) {
                $this->startDate = Schedule::dateConvert($data['startDate']);
            }
            
            if(isset($data['endDate'])) {
                $this->endDate = Schedule::dateConvert($data['endDate']);
            }
            
            if(isset($data['scheduleStatus'])) {
                $this->scheduleStatus = $data['scheduleStatus'];
            }
            
        }
    }

    function getId() {
        return $this->id;
    }

    function getUser_id() {
        return $this->user_id;
    }

    function getCheckpoint_id() {
        return $this->checkpoint_id;
    }

    function getRepeatType_id() {
        return $this->repeatType_id;
    }

    function getDescription() {
        return $this->description;
    }

    function getStartTime() {
        return $this->startTime;
    }

    function getEndTime() {
        return $this->endTime;
    }

    function getOneTimeDate() {
        return $this->oneTimeDate;
    }

    function getSunday() {
        return $this->sunday;
    }

    function getMonday() {
        return $this->monday;
    }

    function getTuesday() {
        return $this->tuesday;
    }

    function getWednesday() {
        return $this->wednesday;
    }

    function getThursday() {
        return $this->thursday;
    }

    function getFriday() {
        return $this->friday;
    }

    function getSaturday() {
        return $this->saturday;
    }

    function getStartDate() {
        return $this->startDate;
    }

    function getEndDate() {
        return $this->endDate;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUser_id($user_id) {
        $this->user_id = $user_id;
    }

    function setCheckpoint_id($checkpoint_id) {
        $this->checkpoint_id = $checkpoint_id;
    }

    function setRepeatType_id($repeatType_id) {
        $this->repeatType_id = $repeatType_id;
    }

    function setDescription($description) {
        $this->description = $description;
    }
    
    function setStartTimebyString($startTimeString) {
        //convert strin gto object and set it
        $this->startTime = Schedule::timeConvert($startTimeString);
    }
//    function setStartTime($startTimeObj) {
//        //validate obje
////        if(preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9])/", $startTimeObj)) {
//            $this->startTime = $startTimeObj;
////        }
//    }

    function setEndTimebyString($endTimeString) {
        $this->endTime = Schedule::timeConvert($endTimeString);
    }
    
//    function setEndTime($endTime) {
//        $this->endTime = $endTime;
//    }

    function setOneTimeDate($oneTimeDate) {
        $this->oneTimeDate = Schedule::dateConvert($oneTimeDate);
    }

    function setSunday($sunday) {
        $this->sunday = $sunday;
    }

    function setMonday($monday) {
        $this->monday = $monday;
    }

    function setTuesday($tuesday) {
        $this->tuesday = $tuesday;
    }

    function setWednesday($wednesday) {
        $this->wednesday = $wednesday;
    }

    function setThursday($thursday) {
        $this->thursday = $thursday;
    }

    function setFriday($friday) {
        $this->friday = $friday;
    }

    function setSaturday($saturday) {
        $this->saturday = $saturday;
    }

    function setStartDate($startDate) {
        $this->startDate = Schedule::dateConvert($startDate);
    }

    function setEndDate($endDate) {
        $this->endDate = Schedule::dateConvert($endDate);
    }
    
    function getScheduleStatus() {
        return $this->scheduleStatus;
    }

    function setScheduleStatus($scheduleStatus) {
        $this->scheduleStatus = $scheduleStatus;
    }
    
    static function timeConvert($timeString) {
        $temp = strtotime($timeString);
        $time = date("H:i:s",$temp);
        return $time;
    }
    
    static function dateConvert($dateString) {
        $temp = strtotime($dateString);
        $date = date("Y-m-d",$temp);
        return $date;
    }
}