<?php

class Customer {
    
    private $id;
    private $name;
    private $phone;
    private $image;
    private $checkpoint_id;
    private $reaction;// 1 :- Bad, 2 :- Good, 3 :- Great
    private $feedback;
    private $feedback_read;
    private $search;
    private $s_limit;
    private $e_limit;
    private $limit;
    private $pageNumber;
            
    function __construct($data = null) {
        if($data){
            if(isset($data['id'])){
                $this->id = $data['id'];
            }
            if(isset($data['name'])){
                $this->name = $data['name'];
            }
            if(isset($data['phone'])){
                $this->phone = $data['phone'];
            }
            if(isset($data['image'])){
                $this->image = $data['image'];
            }
            if(isset($data['checkpoint_id'])){
                $this->checkpoint_id = $data['checkpoint_id'];
            }
            if(isset($data['reaction'])){
                $this->reaction = $data['reaction'];
            }
            if(isset($data['feedback'])){
                $this->feedback = $data['feedback'];
            }
            if(isset($data['feedback_read'])){
                $this->feedback_read = $data['feedback_read'];
            }
            if(isset($data['search'])){
                $this->search = $data['search'];
            }
            if(isset($data['s_limit'])){
                $this->s_limit = $data['s_limit'];
            }
            if(isset($data['e_limit'])){
                $this->e_limit = $data['e_limit'];
            }
            if(isset($data['limit'])){
                $this->limit = $data['limit'];
            }
            if(isset($data['page_number'])){
                $this->pageNumber = $data['page_number'];
            }
        }
    }
    
    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getPhone() {
        return $this->phone;
    }

    function getImage() {
        return $this->image;
    }

    function getCheckpoint_id() {
        return $this->checkpoint_id;
    }

    function getReaction() {
        return $this->reaction;
    }
        
    function getFeedback() {
        return $this->feedback;
    }
    
    function getFeedback_read() {
        return $this->feedback_read;
    }
        
    function getSearch() {
        return $this->search;
    }

    function getS_limit() {
        return $this->s_limit;
    }

    function getE_limit() {
        return $this->e_limit;
    }

    function getLimit() {
        return $this->limit;
    }

    function getPageNumber() {
        return $this->pageNumber;
    }
    
    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setPhone($phone) {
        $this->phone = $phone;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setCheckpoint_id($checkpoint_id) {
        $this->checkpoint_id = $checkpoint_id;
    }
    
    function setReaction($reaction) {
        $this->reaction = $reaction;
    }

    function setFeedback($feedback) {
        $this->feedback = $feedback;
    }
    
    function setFeedback_read($feedback_read) {
        $this->feedback_read = $feedback_read;
    }
    
    function setSearch($search) {
        $this->search = $search;
    }

    function setS_limit($s_limit) {
        $this->s_limit = $s_limit;
    }

    function setE_limit($e_limit) {
        $this->e_limit = $e_limit;
    }

    function setLimit($limit) {
        $this->limit = $limit;
    }

    function setPageNumber($pageNumber) {
        $this->pageNumber = $pageNumber;
    }
    
    function hasImage(){
        if(empty($this->image)||$this->image==null){//validate image String
            return false;
        }else{
            return true;
        }
    }
}

