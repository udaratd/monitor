<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Work {
    
    private $id;
    private $checkpointID;
    private $deviceDate;
    private $deviceTime;
    private $serverDate;
    private $serverTime;
    private $userID;
    private $status;
    private $rework;
    private $originalWorkID;
    private $scheduleID;
    private $image;
    private $stf_comment;
            
    function __construct($data = null) {
        
        if($data) {
            
            if(isset($data['id'])) {
                $this->id = $data['id'];
            }
            if(isset($data['checkpointID'])) {
                $this->checkpointID = $data['checkpointID'];
            }
            if(isset($data['deviceDate'])) {
                $this->deviceDate = Work::dateConvert($data['deviceDate']);
            }
            if(isset($data['deviceTime'])) {
                $this->deviceTime = Work::timeConvert($data['deviceTime']);
            }
            if(isset($data['serverDate'])) {
                $this->serverDate = $data['serverDate'];
            }
            if(isset($data['serverTime'])) {
                $this->serverTime = $data['serverTime'];
            }
            if(isset($data['userID'])) {
                $this->userID = $data['userID'];
            }
            if(isset($data['status'])) {
                $this->status = $data['status'];
            }
            if(isset($data['rework'])) {
                $this->rework = $data['rework'];
            }
            if(isset($data['originalWorkID'])) {
                $this->originalWorkID = $data['originalWorkID'];
            }
            if(isset($data['scheduleID'])) {
                $this->scheduleID = $data['scheduleID'];
            }
            if(isset($data['image'])) {
                $this->image = $data['image'];
            }
            if(isset($data['stf_comment'])) {
                $this->stf_comment = $data['stf_comment'];
            }
        }
    }
    
    function getId() {
        return $this->id;
    }

    function getCheckpointID() {
        return $this->checkpointID;
    }

    function getDeviceDate() {
        return $this->deviceDate;
    }

    function getDeviceTime() {
        return $this->deviceTime;
    }

    function getServerDate() {
        return $this->serverDate;
    }

    function getServerTime() {
        return $this->serverTime;
    }

    function getUserID() {
        return $this->userID;
    }

    function getStatus() {
        return $this->status;
    }

    function getRework() {
        return $this->rework;
    }

    function getOriginalWorkID() {
        return $this->originalWorkID;
    }

    function getScheduleID() {
        return $this->scheduleID;
    }
    
    function getImage() {
        return $this->image;
    }

    function getStf_comment() {
        return $this->stf_comment;
    }
    
    function setId($id) {
        $this->id = $id;
    }

    function setCheckpointID($checkpointID) {
        $this->checkpointID = $checkpointID;
    }

    function setDeviceDate($deviceDate) {
        $this->deviceDate = Work::dateConvert($deviceDate);
    }

    function setDeviceTime($deviceTime) {
        $this->deviceTime = Work::timeConvert($deviceTime);
    }

    function setServerDate($serverDate) {
        $this->serverDate = $serverDate;
    }

    function setServerTime($serverTime) {
        $this->serverTime = $serverTime;
    }

    function setUserID($userID) {
        $this->userID = $userID;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setRework($rework) {
        $this->rework = $rework;
    }

    function setOriginalWorkID($originalWorkID) {
        $this->originalWorkID = $originalWorkID;
    }

    function setScheduleID($scheduleID) {
        $this->scheduleID = $scheduleID;
    }
    function setImage($image) {
        $this->image = $image;
    }

    function setStf_comment($stf_comment) {
        $this->stf_comment = $stf_comment;
    }
    
    static function timeConvert($timeString) {
        $temp = strtotime($timeString);
        $time = date("H:i:s",$temp);
        return $time;
    }
    
    static function dateConvert($dateString) {
        $temp = strtotime($dateString);
        $date = date("Y-m-d",$temp);
        return $date;
    }

    function hasImage(){
        if(empty($this->image)||$this->image==null){
            return false;
        }else{
            if(base64_decode($this->image,TRUE)){
                return true; 
            } else {
                return false;
            }
        }
    }
}
