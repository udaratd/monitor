<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Validation {
    
    /**
     * This function validate email
     * 
     * @param type $email String Email
     * @return boolean return True on success False on Failure
     */
    public static function emailValidation($email) {
        if(filter_var($email,FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * This function validate Sri Lankan NIC number
     * 
     * @param type $nic String NIC number
     * @return boolean return True on success False on failure 
     */
    public static function nicValidation($nic) {
        
        ////// OLD NIC VALIDATION //////////////////
//        $result = true;       
//        if(strlen($nic) <> 10) {
//            $result = false;
//        }       
//        $nic_9 = substr($nic, 0,9);       
//        if(!is_numeric($nic_9)) {
//            $result = false;
//        }       
//        $nic_v = substr($nic, 9,1);       
//        if(is_numeric($nic_v)) {
//            $result = false;
//        }
        ////// OLD NIC VALIDATION END //////////////////
        
        $result = preg_match('/^([0-9]{9}[x|X|v|V]|[0-9]{12})$/', $nic);
        
        return $result;
    }
    
    public static function phoneValidation($phone) {
        $value = $phone;
        $length = strlen($value);
        if($length === 14) {
            if( (substr($value, 0,1) === '(') && (substr($value, 1,1) === '+') && (substr($value, 4,1) === ')') && (is_numeric(substr($value, 2,2))) && (is_numeric(substr($value, 5,14))) ) {
                return true;
            } else {
                return false;
            }
        } else if ($length === 12) {
            if( (substr($value, 0,1) === '+') && (is_numeric(substr($value, 1,10))) ) {
                return true;
            } else {
                return false;
            }
        } else if ($length === 10) {
            if(substr($value, 0,1) === '0' && is_numeric($value)) {
                return true;
            } else {
                return false;
            }
        } else if($length === 9){
            if(is_numeric($value)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
        
//        $value = $phone; 
//        if(preg_match('^\(?\+?([0-9]{1,4})\)?[-\. ]?(\d{9})$', trim($value))) {//edit d{3} to d{2}
//            return true;
//        } else {
//            return false;
//        }

//        $value = $phone; 
//        if(preg_match('/^\(?\+?([0-9]{1,4})\)?[-\. ]?(\d{2})[-\. ]?([0-9]{7})$/', trim($value))) {//edit d{3} to d{2}
//            return true;
//        } else {
//            return false;
//        }
    }
    
    /**
     * 
     * @param array $vars
     * @return boolean|array
     */
    public static function variableValidation(array $vars,$ignore=array()) {
        $errors = array();
        foreach($vars as $key => $value) {
            if(!in_array($key, $ignore)){
                if( empty($value) ) {
                    array_push($errors, $key);
                }
            }
            
        }
        if(count($errors)>0) {
            return $errors;
        } else {
            return true;
        }
    }
    
    public static function arrayKeyExists(array $keys,array $checkArray) {
        $count = 0;
        for($i=0;$i<count($keys);$i++){
            if(array_key_exists($keys[$i], $checkArray)){
                $count++;
            }
        }
        return $count;
    }

    
    public static function validateBase64String($data){
        if ( base64_encode(base64_decode($data, true)) === $data){
            return true;
        } else {
            return false;
        }
    }
    
    public static function removeSpecialCharactersFromString($string){
        $characters = str_replace(' ', '-', $string);
        return preg_replace('/[^A-Za-z0-9\_-]/', '', $characters);
    }
    
    public static function validatePassword($password){
        if(strlen($password)<6){
            return false;
        }
        
        //Upper case validation
        preg_match_all("/[A-Z]/", $password,$up_match);
        $up_match_count = count($up_match[0]);
        if($up_match_count < 1){
            return false;
        }

        //Lower case validation
        preg_match_all("/[a-z]/", $password,$low_match);
        $low_match_count = count($low_match[0]);
        if($low_match_count < 1){
            return false;
        }

        //Digit validation
        preg_match_all("/[0-9]/", $password,$digit_match);
        $digit_match_count = count($digit_match[0]);
        if($digit_match_count < 1){
            return false;
        }

        return true;
    }
    
    public static function checkStringForLettersAndNumbers($string){
        if (!preg_match('/[^A-Za-z0-9]/', $string)) { 
            return true;
        } else {
            return false;
        }
    }
    
    public static function checkULR($string){
        
        if(filter_var($string, FILTER_VALIDATE_URL)){ 
            return true;
        } else {
            return false;
        }
    }
}
