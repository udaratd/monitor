<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserDAO 
 *
 * @author user
 */
class UserDAO {
    
    /**
     * This function add new user and return True or False
     * 
     * @param User $user Enter user object
     * @return boolean Return True on Success False on Failure
     */
    public static function addUser(User $user,$db = null){
        $database=new database($db);
        $database->beginTransaction();
        try{
            $imageUploadSuccess = null;//use to check image upload success or unsuccess
            $imagePath = null;//user image, image path, name of the image
            $supv_inst_success = null;//use to allocate supervisor for the staff
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            
            $query_1 = "INSERT INTO user(firstName,lastName,phone,email,password,role,note) VALUES(:fName, :lName, :phone, :email, :password, :role, :note)";

            $database->query($query_1);
            $database->bind(':fName', $user->getFName());
            $database->bind(':lName', $user->getLName());
            $database->bind(':phone', $user->getPhone());
            $database->bind(':email', $user->getEmail());
            $database->bind(':password', $user->getPasswordHash());
            $database->bind(':role', $user->getRole());
            $database->bind(':note', $user->getNote());
            
            $result1 = $database->execute();
            
            $lastInsertedID = $database->lastInsertId();
            
            if(isset($lastInsertedID)){
                $query_2 = "INSERT INTO user_nic(user_id,nic) VALUES(:userID, :nic)";

                $database->query($query_2);
                $database->bind(':userID', $lastInsertedID);
                $database->bind(':nic', $user->getNic());

                $result2 = $database->execute();

                $query_3 = "INSERT INTO user_meta(user_id,meta_key,meta_value) VALUES(:userID, :key, :value)";

                $database->query($query_3);
                $database->bind(':userID', $lastInsertedID);
                $keyValue = "imagePath";//user_meta table key name
                $database->bind(':key', $keyValue);

                if($user->hasImage()) {
                    $image = $user->getImage();
                    $fileName = $lastInsertedID.'_'.mt_rand(1,5000);
                    $extension = '.jpg';
                                     
                    $path = UPLOAD_USER_DIR;
                    $imagePath = FileManager::imageUpload($path, $fileName.$extension, $image);
                    
                    if(is_string($imagePath)) {
                        $database->bind(':value', $imagePath);
                        $imageUploadSuccess = true;
                        
                        FileManager::imageUpload($path, $fileName.$size_1.$extension, ImageManager::reSize(150, $image));
                        FileManager::imageUpload($path, $fileName.$size_2.$extension, ImageManager::reSize(500, $image));
                    
                    } else {
                        $imagePath = null;
                        $imageUploadSuccess = false;
                        System::log(new Log("Can not upload ".$imagePath, LOG_CRITICAL));
                        return false;
                    }
                } 
                
                $database->bind(':value', $imagePath);

                $result3 = $database->execute();
                
                $supervisorID = $user->getSupervisorID();
                
                if(is_numeric($supervisorID) && $supervisorID != 0 && !empty($supervisorID)) {
                    $query_4 = "INSERT INTO staff_supervisor (supervisor_id,user_id) VALUES(:supervisorID, :staffID)";
                    $database->query($query_4);
                    $database->bind(':supervisorID', $supervisorID);
                    $database->bind(':staffID', $lastInsertedID);
                    
                    if($database->execute()) {
                        $supv_inst_success = true;
                    } else {
                        $supv_inst_success = false;
                        throw new Exception("Can not allocate supervisor for user");
                    }
                }
                

                if( $result1 && $result2 && $result3 && ( ($imageUploadSuccess === null) || ($imageUploadSuccess === true) ) && ( ($supv_inst_success === null) || ($supv_inst_success === true) ) ) {
                    $database->endTransaction();
                    return true;
                } else {
                    if($imageUploadSuccess === true) {
                        if(FileManager::deleteFile($imagePath) === false) {
                            System::log(new Log("Can not delete ".$imagePath, LOG_ERROR));
                        } else {
                            $extension_pos = strrpos($imagePath, '.');
                        
                            $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                            $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);                           

                            if(FileManager::fileExist($thumb)){
                                FileManager::deleteFile($thumb);
                            }
                            if(FileManager::fileExist($medium)){
                                FileManager::deleteFile($medium);
                            }                           
                        }
                    }
                    throw new Exception("Can not Insert Data");
                }
            } else {
                throw new Exception("Can not access last inserted ID");
            }
        } catch (Exception $ex){
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            echo $ex->getMessage();
            $database->cancelTransaction();
            return false;
        }
    }
    
    /**
     * This function validate the login and return true or false
     * 
     * @param String $email Enter login Email
     * @param String $password Enter password
     * @return boolean Returns True on Success false on Failure
     */
    public static function checkLogin($nic,$password){
        $database=new database($_SESSION['DB_NAME']);        
        try{
   
            $query = "SELECT user.ID,user.firstName,user.lastName,user.phone,user.role,user.email,user_nic.nic,user_meta.meta_value "
                    . "FROM user "
                    . "INNER JOIN user_nic "
                    . "ON user.ID = user_nic.user_id "
                    . "INNER JOIN user_meta "
                    . "ON user.ID = user_meta.user_id "
                    . "WHERE user_nic.nic = :nic AND "
                    . "user.password = :password ";

            $database->query($query);
            $database->bind(':nic',$nic);
            $database->bind(':password', hash('sha256',$password));

            if($database->execute()) {
                $logginData = $database->single();              
                return $logginData;
            } else {
                throw new Exception("Invalid User");
            }          
        } catch (Exception $ex) {
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            $ex->getMessage();
            return false;
        }
    }
    
    public static function checkPassword($ID){
        $database=new database();
        try{
            
            $query = "SELECT * "
                    . "FROM user "
                    . "WHERE ID = :id ";

            $database->query($query);
            $database->bind(':id',$ID);

            $singleUser = $database->single();
            if($singleUser) {             
                return $singleUser;
            } else {
                return false;
            }          
        } catch (Exception $ex) {
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            $ex->getMessage();
            return false;
        }
    }
    
    public static function changePassword($ID,$newPass,$dbname = null) {
        $database = new database($dbname);
        try {
            $query = "UPDATE user SET password = :password WHERE ID = :id";
            $database->query($query);
            $database->bind(':id', $ID);
            $database->bind(':password', hash('sha256',$newPass));
            
            if($database->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }

    /**
     * This function return All staff with their details
     * 
     * @return boolean return User Array on Success False on failure
     */
    public static function listStaff($loggedUser_ID) {
       $database = new database();
        try {
           
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            
            $supervisorSet = $loggedUser_ID !== false;
            
            $query = "SELECT
                         user.ID,
                         user.firstName,
                         user.lastName,
                         user.phone,
                         user.role,
                         user_nic.nic,
                         user_meta.meta_value as img_original
                     FROM
                         user
                     INNER JOIN user_nic ON user.ID = user_nic.user_id
                     INNER JOIN user_meta ON user.ID = user_meta.user_id";
            
            if($supervisorSet){
                $query = $query." INNER JOIN staff_supervisor ON user.ID = staff_supervisor.user_id";
            }
                     
            $query = $query." WHERE user.role = 3";

            
            if($supervisorSet) {
                $query = $query." AND staff_supervisor.supervisor_id = :supID";
            }

            $database->query($query);

            if($supervisorSet) {
                $database->bind(':supID', $loggedUser_ID);
            }

            $users = $database->resultset();

            if($users){
                $i=0;
                 foreach ($users as $user){

                     $imagePath = $user['img_original'];
                     if($imagePath !== null){
                         $extension_pos = strrpos($imagePath, '.');

                         $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                         $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);

                         $users[$i]['img_thumbnail'] = $thumb;
                         $users[$i]['img_medium'] = $medium;
                     } else {
                         $users[$i]['img_thumbnail'] = null;
                         $users[$i]['img_medium'] = null;
                     }

                     $i++;
                 }
                return $users;
            } else {
                return false;
            }
        } catch (Exception $exc) {
           System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
           $exc->getMessage();
           return false;
        }
    }
    
    /**
     * This function return user according to entering value
     * 
     * @param String $value this value can be user name,nic,phone
     * @return boolean return User Array on success False on failure
     */
    public static function searchUser($loggedUser_ID,$value,$role,$s_limit,$e_limit,$all=false) {
        $database = new database();
        try {
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            
            $query = "SELECT DISTINCT
                        user.ID,
                        user.firstName,
                        user.lastName,
                        user.phone,
                        user.role,
                        user_nic.nic,
                        user_meta.meta_value as img_original
                    FROM
                        user
                    INNER JOIN user_nic ON user.ID = user_nic.user_id
                    INNER JOIN user_meta ON user.ID = user_meta.user_id
                    LEFT JOIN staff_supervisor ON user.ID = staff_supervisor.user_id
                    WHERE
                        (
                            user.firstName LIKE :fName OR user.lastName LIKE :lName OR user.phone LIKE :phone OR user_nic.nic LIKE :nic
                        )";

            $roleset = $role !== 'All';
            if($roleset) {
                $query = $query." AND user.role = :role";

            } 
            
            $supervisorSet = $loggedUser_ID !== false;
            if($supervisorSet) {
                $query = $query." AND staff_supervisor.supervisor_id = :supID";
            }
            
            if($all===false){
                if( ($s_limit == 0 || !empty($s_limit)) && !empty($e_limit)){
                    $query = $query. " LIMIT :s_limit,:e_limit";                
                }
            }
            
            $database->query($query);
            $database->bind(':fName', '%'.$value.'%');
            $database->bind(':lName', '%'.$value.'%');
            $database->bind(':phone', '%'.$value.'%');
            $database->bind(':nic', '%'.$value.'%');
            
            if($roleset) {    
                $database->bind(':role', $role);
            }
             
            if($supervisorSet) {
                $database->bind(':supID', $loggedUser_ID);
            }
            
            if($all === false){
                if(($s_limit == 0 || !empty($s_limit)) && !empty($e_limit)){
                    $database->bind(':s_limit', $s_limit);
                    $database->bind(':e_limit', $e_limit);
                }
            }
            
            $users = $database->resultset();
            if($users) {
                
                $i=0;
                foreach ($users as $user){

                    $imagePath = $user['img_original'];
                    if($imagePath !== null){
                        $extension_pos = strrpos($imagePath, '.');

                        $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                        $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);

                        $users[$i]['img_thumbnail'] = $thumb;
                        $users[$i]['img_medium'] = $medium;
                    } else {
                        $users[$i]['img_thumbnail'] = null;
                        $users[$i]['img_medium'] = null;
                    }
                    
                    $i++;
                }
                
                return $users;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function searchUserCount($loggedUser_ID,$value,$role) {
        $database = new database();
        try {
            
            $query = "SELECT DISTINCT
                        COUNT(user.ID) as count
                    FROM
                        user
                    INNER JOIN user_nic ON user.ID = user_nic.user_id
                    INNER JOIN user_meta ON user.ID = user_meta.user_id
                    LEFT JOIN staff_supervisor ON user.ID = staff_supervisor.user_id
                    WHERE
                        (
                            user.firstName LIKE :fName OR user.lastName LIKE :lName OR user.phone LIKE :phone OR user_nic.nic LIKE :nic
                        )";

            $roleset = $role !== 'All';
            if($roleset) {
                $query = $query." AND user.role = :role";

            } 
            
            $supervisorSet = $loggedUser_ID !== false;
            if($supervisorSet) {
                $query = $query." AND staff_supervisor.supervisor_id = :supID";
            }
            
            $database->query($query);
            $database->bind(':fName', '%'.$value.'%');
            $database->bind(':lName', '%'.$value.'%');
            $database->bind(':phone', '%'.$value.'%');
            $database->bind(':nic', '%'.$value.'%');
            
            if($roleset) {    
                $database->bind(':role', $role);
            }
             
            if($supervisorSet) {
                $database->bind(':supID', $loggedUser_ID);
            }
            
            $count = $database->single();
            if($count) {
                return $count;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    /**
     * This function return User Array on success False on Fail
     * 
     * @param type $ID Integer ID
     * @return boolean return user Array on Success False on Failure
     */
    public static function getUser($loggedUser_ID,$ID,$dbname = null) {
        $database = new database($dbname);
        try {
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            
            $supervisorSet = $loggedUser_ID !== false;
            
            $query = "SELECT user.ID,user.firstName,user.lastName,user.phone,user.email,user.role,user.note,user_nic.nic,user_meta.meta_value as img_original,s_f_spv.ID as supervisorID,CONCAT(s_f_spv.firstName,' ',s_f_spv.lastName) as supervisorName "
                   . "FROM user "
                   . "INNER JOIN user_nic "
                   . "ON user.ID = user_nic.user_id "
                   . "INNER JOIN user_meta "
                   . "ON user.ID = user_meta.user_id "
                   . "LEFT JOIN staff_supervisor spv "
                   . "ON user.ID = spv.user_id "
                   . "LEFT JOIN user s_f_spv "
                   . "ON spv.supervisor_id = s_f_spv.ID "
                   . "WHERE user.ID = :id";
            
            if($supervisorSet){
                $query = $query." AND spv.supervisor_id = :spvID";
            }
            
            $database->query($query);
            $database->bind(':id', $ID);
            
            if($supervisorSet){
                $database->bind(':spvID', $loggedUser_ID);
            }
            
            $result = $database->single();
            
            if($result) {

                $imagePath = $result['img_original'];
                
                if($imagePath !== null){
                    $extension_pos = strrpos($imagePath, '.');

                    $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                    $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);

                    $result['img_thumbnail'] = $thumb;
                    $result['img_medium'] = $medium;
                } else {
                    $result['img_thumbnail'] = null;
                    $result['img_medium'] = null;
                }
                
                return $result;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    /**
     * This function check registered user
     * 
     * @param String $email
     * @return boolean return ID on success False on failure
     */
    public static function checkUser($email,$dbname = null) {
        $database = new database($dbname);
        try {
            $query = "SELECT * FROM user WHERE email = :email";
            $database->query($query);
            $database->bind(':email', $email);
            
            if($database->execute()) {
                $result = $database->single();
                if($result) {
                    return $result['ID'];
                } else {
                    return false;
                }
            } else {
                return false; 
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getUserAccordingToRole($role) {
        $database = new database();
        try {
            $query = "SELECT * FROM user WHERE role = :role";
            $database->query($query);
            $database->bind(':role', $role);
            
            if($database->execute()) {
                $result = $database->resultset();
                if($result) {
                    return $result;
                } else {
                    return false;
                }
            } else {
                return false; 
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function identifyUpdateFields(User $user) {
        $fields = array();
        
        if(!empty($user->getFName())) {
            $fields[] = "firstName = :fName";
        }
        if(!empty($user->getLName())) {
            $fields[] = "lastName = :lName";
        }
        if(!empty($user->getEmail())) {
            $fields[] = "email = :email";
        }
        if(!empty($user->getPhone())) {
            $fields[] = "phone = :phone";
        }
        if(!empty($user->getPassword())) {
            $fields[] = "password = :password";
        }
        if(!empty($user->getNote())) {
            $fields[] = "note = :note";
        }

        return $fields;
    }

    /**
     * This function Update user and return True or False
     * 
     * @param User $user Enter User Object
     * @return boolean return True on success False on failure
     */
    public static function updateUser(User $user,$loggedUser_ID,$loggedUser_Role) {
//        $oldData = UserDAO::getUser($loggedUser_ID,$user->getId());
//        $previousJobrole = $oldData['role'];
        
        $database = new database();
        $database->beginTransaction();
        try {

            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            
            $success = null;
            $result4 = null;
            
            $updateFields = UserDAO::identifyUpdateFields($user);
            
            $query1 = "UPDATE user SET ".implode(", ", $updateFields)." WHERE ID = :id";

            $database->query($query1);
            
            $database->bind(':id', $user->getId());

            if(!empty($user->getFName())){
                $database->bind(':fName', $user->getFName());
            } 
            if(!empty($user->getLName())){
                $database->bind(':lName', $user->getLName());
            } 
            if(!empty($user->getPhone())){
                $database->bind(':phone', $user->getPhone());
            }
            if(!empty($user->getEmail())){
                $database->bind(':email', $user->getEmail());
            }
            if(!empty($user->getPassword())){
                $database->bind(':password', $user->getPasswordHash());
            }
            if(!empty($user->getNote())){
                $database->bind(':note', $user->getNote());
            }

            $result1 = $database->execute();
            
            $query2 = "UPDATE user_nic SET nic = :nic WHERE user_id = :id";
            $database->query($query2);

            $database->bind(':id', $user->getId());
            $database->bind(':nic', $user->getNic());
            
            $result2 = $database->execute();

            $key = 'imagePath';
            $query3 = "SELECT meta_value FROM user_meta WHERE user_id = :id AND meta_key = :key";
            $database->query($query3);
            
            $database->bind(':id', $user->getId());
            $database->bind(':key', $key);
            
            $result3 = $database->single();

            $oldImage = $result3['meta_value'];
            $imagePath = $oldImage;
            
            if($user->hasImage()) {
                
                $image = $user->getImage();
                $fileName = $user->getId().'_'.mt_rand(5000,10000);
                $extension = '.jpg';
 
                $path = UPLOAD_USER_DIR;
                $imagePath = FileManager::imageUpload($path, $fileName.$extension, $image);
                
                
                if(is_string($imagePath)) {

                    FileManager::imageUpload($path, $fileName.$size_1.$extension, ImageManager::reSize(150, $image));
                    FileManager::imageUpload($path, $fileName.$size_2.$extension, ImageManager::reSize(500, $image));
                    
                    $success = true;//image successfuly uploaded

                    $query4 = "UPDATE user_meta SET meta_value = :imagePath WHERE meta_key = :key AND  user_id = :id";
                    $database->query($query4);

                    $database->bind(':id', $user->getId());
                    $database->bind(':key', $key);
                    $database->bind(':imagePath', $imagePath);

                    $result4 = $database->execute();

                    if($result4) {
                        if(!empty($oldImage)){
                            if(FileManager::fileExist($oldImage) === true){
                                if(FileManager::deleteFile($oldImage) === true) {
                                    $extension_pos = strrpos($oldImage, '.');

                                    $thumb = substr($oldImage,0, $extension_pos).$size_1.substr($oldImage, $extension_pos);
                                    $medium = substr($oldImage,0, $extension_pos).$size_2.substr($oldImage, $extension_pos);                           

                                    if(FileManager::fileExist($thumb)){
                                        if(FileManager::deleteFile($thumb)===false){
                                            System::log(new Log("Cant Delete ".$thumb, LOG_EXCEPTION));
                                        }
                                    }
                                    if(FileManager::fileExist($medium)){
                                        if(FileManager::deleteFile($medium)===false){
                                            System::log(new Log("Cant Delete ".$medium, LOG_EXCEPTION));
                                        }
                                    }
                                } else {
                                    System::log(new Log("Cant Delete ".$oldImage, LOG_EXCEPTION));
                                }
                            }
                        }
                    } else {
                        if($success === true) {//image uploaded but data not updated
                            
                            if(!empty($imagePath) && ($imagePath !== 1) && ($imagePath !== false)){
                                
                                if(FileManager::fileExist($imagePath) === true){
                                    
                                    if(FileManager::deleteFile($imagePath) === true) {
                                        $extension_pos = strrpos($imagePath, '.');

                                        $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                                        $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);                           

                                        if(FileManager::fileExist($thumb)){
                                            if(FileManager::deleteFile($thumb)===false){
                                                System::log(new Log("Cant Delete ".$thumb, LOG_EXCEPTION));
                                            }
                                        }
                                        if(FileManager::fileExist($medium)){
                                            if(FileManager::deleteFile($medium)===false){
                                                System::log(new Log("Cant Delete ".$medium, LOG_EXCEPTION));
                                            }
                                        }
                                    } else {
                                        System::log(new Log("Cant Delete ".$imagePath, LOG_EXCEPTION));
                                    }
                                }
                            }
                        }
                        $result4 = false;
                    }
                } else {
                    $success = false;//image not uploaded
                }
            }
            
            
            $supervisorUpdate_success = null;
            if($loggedUser_Role == 1){
                if(!empty($user->getSupervisorID())) {

                    $query5 = "UPDATE staff_supervisor SET supervisor_id = :supID WHERE user_id = :uid";                    
                    $database->query($query5);
                    $database->bind(':supID', $user->getSupervisorID());
                    $database->bind(':uid', $user->getId());
                                    
                    if($database->execute()) {
                        $supervisorUpdate_success = true;
                    } else {
                        $supervisorUpdate_success = false;
                    }

                }
            }

            if($result1 && $result2 && $result3 && ($result4===null || $result4===true) && ($success===null || $success === true) && ($supervisorUpdate_success===null || $supervisorUpdate_success === true) ) {
                $database->endTransaction();
                return true;
            } else {
                throw new Exception("Can not Update Data");
            }          
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            $database->cancelTransaction();
            return false;
        }
    }
    
    public static function updatField($ID,$fieldName,$value) {
        $oldImage = UserDAO::getUserImagePath($ID);
        $database = new database();
        $database->beginTransaction();
        $query = null;
        
        try {
            
            //begin validation
            if($fieldName === "password") {
                throw new Exception("Can not Update password in here");
            }
            if($fieldName === "nic") {
                if(Validation::nicValidation($value)) {
                    $user_id = UserDAO::checkUserNIC($value);
                    if( ($user_id !== $ID) && ($user_id !== false) ){//This nic number used by another user
                        throw new Exception("This nic number used by another user");
                    }
                } else {
                    throw new Exception("Invalid NIC number");
                }
            }
            if($fieldName === "email") {
                if(Validation::emailValidation($value)) {
                    $user_id = UserDAO::checkUser($value);
                    if( ($user_id !== $ID) && ($user_id !== false) ) {//This email used by another user
                        throw new Exception("This Email used by another user");
                    }
                } else {
                    throw new Exception("Invalid Email");
                }
            }
            if($fieldName === "phone") {
                if(Validation::phoneValidation($value) === false) {
                    throw new Exception("Invalid Phone number");
                }
            }
            //End Validation
            
            switch (UserDAO::getTable($fieldName)) {
                case 'user':
                    $query = "UPDATE user SET ".$fieldName." = :value WHERE ID = :id";
                    break;                    
                case 'user_nic':
                    $query = "UPDATE user_nic SET ".$fieldName." = :value WHERE user_id = :id";
                    break;
                case 'user_meta':
                    $query = "UPDATE user_meta SET ".$fieldName." = :value WHERE user_id = :id";
                    
                    $image = $value;
                    $fileName = $ID.'_'.mt_rand(5000,10000).'.jpg';

                    $path = UPLOAD_USER_DIR;
                    $imagePath = FileManager::imageUpload($path, $fileName, $image);
                    
                    if(is_string($imagePath)){
                        $value = $imagePath;
                        if(!is_null($oldImage)){
                            System::log(new Log("DELETE THIS IMAGE ".$oldImage, LOG_ERROR));
                        }
                    } else {
                        throw new Exception("Can not upload image");
                    }
                    break;
                case 'staff_supervisor';
                    $query = "UPDATE staff_supervisor SET ".$fieldName." = :value WHERE user_id = :id";
                    break;
                default :
                    throw new Exception("No such table in system");
            }
            
            if($query !== null) {
                $database->query($query);
                $database->bind(':id', $ID);
                $database->bind(':value', $value);

                if($database->execute()) {
                    $database->endTransaction();
                    return true;
                } else {
                    throw new Exception("Can not update ".$fieldName." in user table");
                }
            } else {
                throw new Exception("No such table in system");
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            $database->cancelTransaction();
            return false;
        }
    }
    
    public static function getTable($field) {
        $table = null;
        switch ($field) {
            case 'nic':
                $table = 'user_nic';
                break;
            case 'meta_value':
                $table = 'user_meta';
                break;
            case 'firstName':
            case 'lastName':
            case 'phone':
            case 'email':
            case 'role':
                $table = 'user';
                break;
            case 'supervisor_id':
                $table = 'staff_supervisor';
            default :
                break;
        }
        return $table;
    }
    /**
     * This function delete User
     * 
     * @param type $id integer ID
     * @return boolean return True on Success False on Failure
     */
    public static function deleteUser($id) {
        $path = UserDAO::getUserImagePath($id);
        $database = new database();

//        $database->beginTransaction();
        try {

            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            
            $query = "DELETE FROM user WHERE ID = :id";
            $database->query($query);
            $database->bind(':id', $id);
            $result1 = $database->execute();

            if($result1){
//                $database->endTransaction();
                if(!empty($path) && $path!==false){
                    if(FileManager::fileExist($path)===true) {

                        if(FileManager::deleteFile($path)===true){

                            $extension_pos = strrpos($path, '.');

                            $thumb = substr($path,0, $extension_pos).$size_1.substr($path, $extension_pos);
                            $medium = substr($path,0, $extension_pos).$size_2.substr($path, $extension_pos);                           

                            if(FileManager::fileExist($thumb)){
                                if(FileManager::deleteFile($thumb)===false){
                                    System::log(new Log("Can not Delete ".$thumb, LOG_ERROR));
                                }
                            }

                            if(FileManager::fileExist($medium)){
                                if(FileManager::deleteFile($medium)===false){
                                    System::log(new Log("Can not Delete ".$medium, LOG_ERROR));
                                }
                            }
                        } else {
                            System::log(new Log("Can not Delete ".$path, LOG_ERROR));
                        }

                    }
                }
                return true;
            } else {
                System::log(new Log("Data not Deleted", LOG_CRITICAL));
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
//            $database->cancelTransaction();
            return false;
        }
    }
    
    /**
     * This function return User image path
     * @param type $id Integer ID
     * @return boolean return image path on Success False on failure
     */
    public static function getUserImagePath($id) {
        $database = new database();       
        try {
            $query = "SELECT meta_value FROM user_meta WHERE user_id = :id";
            $database->query($query);
            $database->bind(':id', $id);
            $result = $database->single();

            if($result) {
                $value = $result['meta_value'];
                return $value;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    /**
     * This function check if User is existed
     * 
     * @param type $ID Integer ID
     * @return boolean return True on Success False on Failure
     */
    public static function checkUserID($ID) {
        $database = new database();
        try {
            $query = "SELECT ID FROM user WHERE ID = :id";
            $database->query($query);
            $database->bind(':id', $ID);
            $result = $database->single();
            
            if($result) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    /**
     * Function check user NIC is already in the System
     * @param type $nic nic String
     * @return boolean return ID on Success False on Failure
     */
    public static function checkUserNIC($nic) {
        $database = new database();
        try {
            $query = "SELECT * FROM user_nic WHERE nic = :nic";
            $database->query($query);
            $database->bind(':nic', $nic);
            
            if($database->execute()) {
            $result = $database->single();
                if($result) {
                    return $result['user_id'];
                } else {
                    return false;
                }
            } else {
                System::log(new Log("Can not get user ID",LOG_EXCEPTION));
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getSupervisor() {
        $database = new database();
        try {
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            
            $query = "SELECT user.ID,user.firstName,user.lastName,user.phone,user.role,user_nic.nic,user_meta.meta_value as img_original "
                   . "FROM user "
                   . "LEFT JOIN user_nic "
                   . "ON user.ID = user_nic.user_id "
                   . "LEFT JOIN user_meta "
                   . "ON user.ID = user_meta.user_id "
                   . "WHERE user.role = :role";
            $database->query($query);
            $database->bind(':role', 2);
            
            $supervisors = $database->resultset();
            
            if($supervisors) {
                                               
                $i=0;
                foreach ($supervisors as $supervisor){

                    $imagePath = $supervisor['img_original'];
                    if($imagePath !== null){
                        $extension_pos = strrpos($imagePath, '.');

                        $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                        $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);

                        $supervisors[$i]['img_thumbnail'] = $thumb;
                        $supervisors[$i]['img_medium'] = $medium;
                    } else {
                        $supervisors[$i]['img_thumbnail'] = null;
                        $supervisors[$i]['img_medium'] = null;
                    }
                    
                    $i++;
                }
                
                return $supervisors;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getAlocatedSupervisor($id) {
        $database = new database();
        try {
  
            $query = "SELECT user_id "
                   . "FROM staff_supervisor "
                   . "WHERE supervisor_id = :id";
            $database->query($query);
            $database->bind(':id', $id);
            
            $supervisors = $database->resultset();
            
            if($supervisors) {                                                              
                return $supervisors;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function singleUserWithCheckpoint($id,$loggedUserID,$loggedUserRole) {
        $database = new database();
        try {
            $data = array();
            
            $supevisorSet = $loggedUserID !== false;
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            
            $query1="SELECT
                        u.ID,
                        u.firstName,
                        u.lastName,
                        u.phone,
                        u.role,
                        un.nic,
                        um.meta_value as img_original
                    FROM 
                        user u
                    INNER JOIN user_nic un ON
                        u.ID = un.user_id
                    INNER JOIN user_meta um ON
                        u.ID = um.user_id
                    LEFT JOIN staff_supervisor spv_u ON
                        u.ID = spv_u.user_id
                    WHERE u.ID = :userID";
            
            if($supevisorSet) {
                $query1 = $query1." AND spv_u.supervisor_id = :supervisorID";
            }
            
            $database->query($query1);
            $database->bind(':userID', $id);
            
            if($supevisorSet) {
                $database->bind(':supervisorID', $loggedUserID);
            }
            
            $staffPrifileData = $database->single();
            
            if($staffPrifileData) {
                
                $imagePath = $staffPrifileData['img_original'];
                
                if($imagePath !== null){
                    $extension_pos = strrpos($imagePath, '.');

                    $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                    $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);

                    $staffPrifileData['img_thumbnail'] = $thumb;
                    $staffPrifileData['img_medium'] = $medium;
                } else {
                    $staffPrifileData['img_thumbnail'] = null;
                    $staffPrifileData['img_medium'] = null;
                }
                
                if($loggedUserRole == 1 || $loggedUserRole == 2) {
                    $query2="SELECT DISTINCT
                                l_p.ID as Location_parentID,
                                l_p.name as Location_parentName,
                                l_c.ID as Location_childID,
                                l_c.name as Location_childName
                            FROM 
                                user u
                            LEFT JOIN taskandschedule t ON
                                u.ID = t.user_id
                            LEFT JOIN checkpoint c ON
                                t.checkpoint_id = c.ID
                            LEFT JOIN location l_c ON
                                c.locationid = l_c.ID
                            LEFT JOIN location l_p ON
                                l_c.parentid = l_p.ID
                            WHERE u.ID = :user_ID AND (t.repeatType_id = 1 OR t.repeatType_id = 2) AND t.scheduleStatus = 0 ORDER BY Location_parentName, Location_childName";

                    $database->query($query2);
                    $database->bind(':user_ID', $staffPrifileData['ID']);

                    if($database->execute()) {
                        $location_s = $database->resultset();
                        
                        $temp = array();//assign locations
                        
                        foreach ($location_s as $location) {

                            $query3 = "SELECT DISTINCT
                                            c.ID as checkpointID,
                                            c.name as checkpointName
                                        FROM 
                                            checkpoint c
                                        WHERE c.locationid = :cid OR c.locationid = :pid";

                            $database->query($query3);
                            $database->bind(':cid', $location['Location_childID']);
                            $database->bind(':pid', $location['Location_parentID']);

                            if($database->execute()) {
                                $scheduleChckpoints = $database->resultset();
                                
                                $location['checkpoints'] = $scheduleChckpoints;                               
                                array_push($temp, $location);

                            } else {
                                throw new Exception("No scheduled checkpoints for this user");
                            }
                        }
                        
                        if(count($temp)>0) {
                            $staffPrifileData['locations'] = $temp;
                        }
                        
                        array_push($data, $staffPrifileData);
                        return $data;
                    } else {
                        throw new Exception("No scheduled locations for this user");
                    }
                }
            } else {
                throw new Exception("No such staff");
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getUserRole(){
        $roles_1 = array("id"=>"1","label"=>"Admin");
        $roles_2 = array("id"=>"2","label"=>"Supervisor");
        $roles_3 = array("id"=>"3","label"=>"Staff");
        
        $roles = array($roles_1,$roles_2,$roles_3);
        
        return $roles;
    }
    
    public static function getStaffAccordingToSupervisor($supervisorID){
        
        $database = new database();
        try {
            $query = "SELECT 
                        u.ID as staffID,
                        CONCAT(u.firstName,' ',u.lastName) as fullName
                     FROM 
                        user u
                     INNER JOIN staff_supervisor spv ON
                        u.ID = spv.user_id
                     WHERE spv.supervisor_id = :spvID";
            
            $database->query($query);
            $database->bind(':spvID', $supervisorID);
            
            $result = $database->resultset();

            if($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function exportUser(){
        $database = new database();
        try {
            date_default_timezone_set('Asia/Colombo');
            $today = date('h:i:s a Y-m-d');
            $filename = "user_export_".$today.".csv";
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename='.$filename);
            $output = fopen("php://output", "w");
            fputcsv($output, array('ID','firstName','lastName','phone','email','role','nic','supervisor_id'));
            
            $query = "SELECT
                        u.ID,
                        u.firstName,
                        u.lastName,
                        u.phone,
                        u.email,
                        u.role,
                        n.nic,
                        s.supervisor_id
                      FROM 
                        user u
                      INNER JOIN user_nic n ON
                        u.ID = n.user_id
                      LEFT JOIN staff_supervisor s ON
                        u.ID = s.user_id";
            
            $database->query($query);

            $result = $database->resultset();

            foreach ($result as $one){
                fputcsv($output, $one);
            }
            fclose($output);   
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getPasswordResetData($email,$dbname = null) {
        $database = new database($dbname);
        try {
            $query = "SELECT * FROM user WHERE email = :email";
            $database->query($query);
            $database->bind(':email', $email);
            
            $result = $database->single();
            if($result) {
                return $result;
            } else {
                return false; 
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function addResetPassword($user_id,$email,$secret,$dbname = null){
        $database = new database($dbname);       
        try {

            $today = date('Y-m-d H:i:s');
            
            $query = "INSERT INTO reset_password(user_id,email,secret,date_time) VALUES(:user_id,:email,:secret,:date_time)";
            $database->query($query);
            $database->bind(':user_id', $user_id);
            $database->bind(':email', $email);
            $database->bind(':secret', $secret);
            $database->bind(':date_time', $today);
            
            $staffDetails = $database->execute();
            if($staffDetails){
                return $database->lastInsertId();
            } else {
                return false;
            }            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function getResetData($id,$dbname = null){
        $database = new database($dbname);       
        try {            
            $query = "SELECT * FROM reset_password WHERE ID = :id";
            $database->query($query);
            $database->bind(':id', $id);
            
            $resetDetails = $database->single();
            if($resetDetails){
                return $resetDetails;
            } else {
                return false;
            }            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function checkSupervisor($ID) {
        $database = new database();
        try {
            $query = "SELECT * FROM user WHERE ID = :id";
            $database->query($query);
            $database->bind(':id', $ID);
            $result = $database->single();
            
            if($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
}
