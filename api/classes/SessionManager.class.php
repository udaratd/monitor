<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SessionManager {
    
    private static $userID;
    private static $jobRole;
    private static $email;
    
    public static function createSession($userID,$role,$email) {

        $response = new Response();
        
        if(!empty($userID) && !empty($role) && !empty($email)){

            self::$userID = $userID;
            self::$jobRole = $role;
            self::$email = $email;

            $_SESSION['ID'] = self::$userID;
            $_SESSION['role'] = self::$jobRole;
            $_SESSION['email'] = self::$email;

        }else{
            $response->create(500, 'can not create Session (check session variables)', false);
            return false;
        }
        
    }

    public static function createSessionVar($key,$value) {
        
        $response = new Response();
        
        if(!empty($key) && !empty($value)) {
            $_SESSION[$key] = $value;
        } else {
            $response->create(500, 'can not create Session (check session variables)', false);
            return false;
        }
    }
    
    public static function validateSession() {
        if(!empty($_SESSION['ID']) && !empty($_SESSION['role']) && !empty($_SESSION['email'])){
            return true;
        }else{
            return false;
        }
    }
    
    public static function checkTimeoutSession() {
        $time = $_SERVER['REQUEST_TIME'];
        
        $timeout_duration = 86400;
        echo $time - $_SESSION['LAST_ACTIVITY'];
        echo "\r\n";
        echo $_SESSION['LAST_ACTIVITY'];
        if (isset($_SESSION['LAST_ACTIVITY']) && ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
            session_unset();
            session_destroy();
            session_start();
        }

        $_SESSION['LAST_ACTIVITY'] = $time;
        
    }
    
    public static function is_admin() {
        if(isset($_SESSION['role'])) {
            if($_SESSION['role'] == 1){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public static function is_supervisor() {
        if(isset($_SESSION['role'])) {
            if($_SESSION['role'] == 2){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public static function is_staff() {
        if(isset($_SESSION['role'])) {
            if($_SESSION['role'] == 3){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public static function is_superAdmin() {
        if(isset($_SESSION['role'])) {
            if($_SESSION['role'] == 100){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public static function loguot() {
        session_unset();//remove all sessionh variables
        
        session_destroy();//destory the session
    }

    public static function getUserID() {
        return self::$userID;
    }

    public static function getJobRole() {
        return self::$jobRole;
    }

    public static function getEmail() {
        return self::$email;
    }

}