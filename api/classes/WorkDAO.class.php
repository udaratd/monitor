<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class WorkDAO {
    
    public static function addWorkList(Work $work) {
        $database = new database();
        $database->beginTransaction();
        try {
            
            if($work->hasImage() === false) {
                throw new Exception("Upload valid image");
            }
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            $query1="INSERT INTO work_list(
                        checkpoint_id,
                        device_date,
                        device_time,
                        server_date,
                        server_time,
                        user_id,
                        work_status,
                        rework,
                        original_work_id,
                        schedule_id,
                        stf_comment
                    )
                    VALUES(
                        :checkpointID,
                        :deviceDate,
                        :deviceTime,
                        :serverDate,
                        :serverTime,
                        :userID,
                        :status,
                        :rework,
                        :originalWorkID,
                        :scheduleID,
                        :stf_comment
                    )";
            
            $database->query($query1);
            
            $database->bind(':checkpointID', $work->getCheckpointID());
            $database->bind(':deviceDate', $work->getDeviceDate());
            $database->bind(':deviceTime', $work->getDeviceTime());
            $database->bind(':serverDate', $work->getServerDate());
            $database->bind(':serverTime', $work->getServerTime());
            $database->bind(':userID', $work->getUserID());
            $database->bind(':status', $work->getStatus());
            $database->bind(':stf_comment', $work->getStf_comment());
            
            
            if(empty($work->getId())){//compleate this work in first time
                $database->bind(':rework', 0);
                $database->bind(':originalWorkID', NULL);
            } else {
                if(empty($work->getOriginalWorkID())) {
                    $database->bind(':rework', $work->getRework());
                    $database->bind(':originalWorkID', $work->getId());//first time rework
                } else {
                    $database->bind(':rework', $work->getRework());
                    $database->bind(':originalWorkID', $work->getOriginalWorkID());//more than one time rework
                }
            }
            
            $database->bind(':scheduleID', $work->getScheduleID());
            
            $work_completed = $database->execute();
            
            $lastInsertedID = $database->lastInsertId();
            
            $imageUploadSuccess = null;
            
            $image = $work->getImage();
            $fileName = $lastInsertedID . '_' . mt_rand(1,5000);
            $extension = '.jpg';
            
            $path = UPLOAD_WORK_DIR;
            $imagePath = FileManager::imageUpload($path, $fileName.$extension, $image);
            
            if(is_string($imagePath)) {
                $imageUploadSuccess = true;
            } else {
                $imageUploadSuccess = false;
                throw new Exception("Can not upload image to server");
            }
            
            $query2 = "UPDATE work_list SET image = :path WHERE ID = :id";
            $database->query($query2);
            $database->bind(':path', $imagePath);
            $database->bind(':id', $lastInsertedID);
            
            $imagePathUpdateSuccess = $database->execute();
            
            if($work_completed && $imageUploadSuccess && $imagePathUpdateSuccess) {
                
                FileManager::imageUpload($path, $fileName.$size_1.$extension, ImageManager::reSize(150, $image));
                FileManager::imageUpload($path, $fileName.$size_2.$extension, ImageManager::reSize(500, $image));
                FileManager::imageUpload($path, $fileName.$size_3.$extension, ImageManager::reSize(1024, $image));
                
                $database->endTransaction();
                return $lastInsertedID;// edit return true to last insert id :- 2019-1-1
            } else {
                if($imageUploadSuccess) {
                    if(FileManager::deleteFile($imagePath) === false) {
                        System::log(new Log("Can not delete ".$imagePath, LOG_ERROR));
                    } else {
                        $extension_pos = strrpos($imagePath, '.');
                        
                        $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                        $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);
                        $large = substr($imagePath,0, $extension_pos).$size_3.substr($imagePath, $extension_pos);
                        
                        if(FileManager::fileExist($thumb)){
                            FileManager::deleteFile($thumb);
                        }
                        if(FileManager::fileExist($medium)){
                            FileManager::deleteFile($medium);
                        }
                        if(FileManager::fileExist($large)){
                            FileManager::deleteFile($large);
                        }
                    }
                }
                throw new Exception("Can not Insert Data");
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            $database->cancelTransaction();
            return false;
        }
    }
    
    public static function listOriginalWorkList($loggedUserID,$serchValue,$s_date,$e_date,$staffID,$locationID,$status,$ordre,$s_limit,$e_limit,$all=false) {
        $database = new database();
        try {
            $data = array();
            $temp = array("rework"=>"Not done yet");
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            $query1 = "SELECT
                            w.*,
                            c.name as checkpointName,
                            l_p.ID as parentID,
                            l_p.name as parentName,
                            l_c.ID as childID,
                            l_c.name as childName,
                            u.ID as staffID,
                            u.firstName as staff_FName,
                            u.lastName as staff_LName,
                            spv.ID as supervisorID,
                            spv.firstName as supervisorFName,
                            spv.lastName as supervisorLName
                        FROM
                            work_list w
                        INNER JOIN checkpoint c ON
                            w.checkpoint_id = c.ID
                        INNER JOIN location l_c ON
                            c.locationid = l_c.ID
                        INNER JOIN location l_p ON
                            l_c.parentid = l_p.ID
                        INNER JOIN user u ON
                            w.user_id = u.ID
                        INNER JOIN staff_supervisor s_f ON
                            u.ID = s_f.user_id
                        INNER JOIN user spv ON
                            s_f.supervisor_id = spv.ID 
                        WHERE w.rework = :rework AND server_date BETWEEN :s_date AND :e_date";
            
            $supevisorSet = $loggedUserID !== false;
            if($supevisorSet) {
                $query1 = $query1." AND s_f.supervisor_id = :supervisorID";
            }

            $staffSet = $staffID !== 'All';
            if($staffSet) {
                $query1 = $query1." AND w.user_id = :user_id";
            }
            
            $locationSet = $locationID !== 'All Location';
            if($locationSet) {
                $query1 = $query1." AND ( (l_p.ID = :parentID) OR (l_c.ID = :childID) )";
            }
            
            $statusSet = $status !== 'All Action';
            if($statusSet) {
                $query1 = $query1." AND w.work_status = :status";
            }
            
            $query1= $query1." AND (
                                c.name LIKE :checkpointName OR  
                                l_p.name LIKE :parentName OR  
                                l_c.name LIKE :childName OR  
                                u.firstName LIKE :staffFname OR 
                                u.lastName LIKE :staffLName OR 
                                w.work_status LIKE :statusVal
                                )";
            
            if($ordre == false){
                $query1 = $query1." ORDER BY w.ID DESC";
            } else {
                $query1 = $query1." ORDER BY w.ID ASC";
            }
            
            if($all===false){
                if( ($s_limit == 0 || !empty($s_limit)) && !empty($e_limit)){
                    $query1 = $query1. " LIMIT :s_limit,:e_limit";                
                }
            }
            
            $database->query($query1);
            $database->bind(':rework', 0);            
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
            
            if($supevisorSet) {
                $database->bind(':supervisorID', $loggedUserID);
            }
            
            if($staffSet) {
                $database->bind(':user_id', $staffID);
            }
            
            if($locationSet) {
                $database->bind(':parentID', $locationID);
                $database->bind(':childID', $locationID);
            }
            
            if($statusSet) {
                $database->bind(':status', $status);
            }
            
            $database->bind(':checkpointName', '%'.$serchValue.'%');
            $database->bind(':parentName', '%'.$serchValue.'%');
            $database->bind(':childName', '%'.$serchValue.'%');
            $database->bind(':staffFname', '%'.$serchValue.'%');
            $database->bind(':staffLName', '%'.$serchValue.'%');
            $database->bind(':statusVal', '%'.$serchValue.'%');
            
            if($all === false){
                if(($s_limit == 0 || !empty($s_limit)) && !empty($e_limit)){
                    $database->bind(':s_limit', $s_limit);
                    $database->bind(':e_limit', $e_limit);
                }
            }
            
            $original_workList = $database->resultset();
            if($original_workList) {
                

                $i=0;//identify original work list indexces
                foreach ($original_workList as $work) {
                    
                    
                    $imagePath = $work['image'];
                     if($imagePath !== null){
                         $extension_pos = strrpos($imagePath, '.');

                         $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                         $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);
                         $large = substr($imagePath,0, $extension_pos).$size_3.substr($imagePath, $extension_pos);

                         $original_workList[$i]['img_thumbnail'] = $thumb;
                         $original_workList[$i]['img_medium'] = $medium;
                         $original_workList[$i]['img_large'] = $large;
                     } else {
                         $original_workList[$i]['img_thumbnail'] = null;
                         $original_workList[$i]['img_medium'] = null;
                         $original_workList[$i]['img_large'] = null;
                     }
                    
                    $original_work_id = $work['ID'];

                    $rework_workList = WorkDAO::listReworkWorkList($original_work_id);
                    
                    if(!$supevisorSet) {//Logged user is Admin
                        $original_workList[$i]['loggingUserRole'] = "Admin";
                    }
                    
                    if($rework_workList){// new condition to wrapp below content 2020-06-30 By Udara
                        
                        if(count($rework_workList) == 0) {
                            if($work['work_status'] === "Rework") {

                                $original_workList[$i]['Rework'] = $temp;                           
                            }                        
                        } elseif(count($rework_workList) > 0) {

                            $original_workList[$i]['Rework'] = $rework_workList;
                        } else {
                            throw new Exception("listReworkWorkList() function not working");
                        }
                    }
                    $i++;
                }
                
                
                if(count($original_workList) > 0){
//                    array_push($data, $original_workList);
                    return $original_workList;
                } else {
                    return false;
                }
            } else {
                throw new Exception("listOriginalWorkList() function not working");
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function listOriginalWorkListCount($loggedUserID,$serchValue,$s_date,$e_date,$staffID,$locationID,$status,$ordre) {
        $database = new database();
        try {
            
            $query1 = "SELECT
                            w.*,
                            c.name as checkpointName,
                            l_p.ID as parentID,
                            l_p.name as parentName,
                            l_c.ID as childID,
                            l_c.name as childName,
                            u.ID as staffID,
                            u.firstName as staff_FName,
                            u.lastName as staff_LName,
                            spv.ID as supervisorID,
                            spv.firstName as supervisorFName,
                            spv.lastName as supervisorLName
                        FROM
                            work_list w
                        INNER JOIN checkpoint c ON
                            w.checkpoint_id = c.ID
                        INNER JOIN location l_c ON
                            c.locationid = l_c.ID
                        INNER JOIN location l_p ON
                            l_c.parentid = l_p.ID
                        INNER JOIN user u ON
                            w.user_id = u.ID
                        INNER JOIN staff_supervisor s_f ON
                            u.ID = s_f.user_id
                        INNER JOIN user spv ON
                            s_f.supervisor_id = spv.ID 
                        WHERE w.rework = :rework AND server_date BETWEEN :s_date AND :e_date";
            
            $supevisorSet = $loggedUserID !== false;
            if($supevisorSet) {
                $query1 = $query1." AND s_f.supervisor_id = :supervisorID";
            }

            $staffSet = $staffID !== 'All';
            if($staffSet) {
                $query1 = $query1." AND w.user_id = :user_id";
            }
            
            $locationSet = $locationID !== 'All Location';
            if($locationSet) {
                $query1 = $query1." AND ( (l_p.ID = :parentID) OR (l_c.ID = :childID) )";
            }
            
            $statusSet = $status !== 'All Action';
            if($statusSet) {
                $query1 = $query1." AND w.work_status = :status";
            }
            
            $query1= $query1." AND (
                                c.name LIKE :checkpointName OR  
                                l_p.name LIKE :parentName OR  
                                l_c.name LIKE :childName OR  
                                u.firstName LIKE :staffFname OR 
                                u.lastName LIKE :staffLName OR 
                                w.work_status LIKE :statusVal
                                )";
            
            if($ordre == false){
                $query1 = $query1." ORDER BY w.ID DESC";
            } else {
                $query1 = $query1." ORDER BY w.ID ASC";
            }
            
            $database->query($query1);
            $database->bind(':rework', 0);            
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
            
            if($supevisorSet) {
                $database->bind(':supervisorID', $loggedUserID);
            }
            
            if($staffSet) {
                $database->bind(':user_id', $staffID);
            }
            
            if($locationSet) {
                $database->bind(':parentID', $locationID);
                $database->bind(':childID', $locationID);
            }
            
            if($statusSet) {
                $database->bind(':status', $status);
            }
            
            $database->bind(':checkpointName', '%'.$serchValue.'%');
            $database->bind(':parentName', '%'.$serchValue.'%');
            $database->bind(':childName', '%'.$serchValue.'%');
            $database->bind(':staffFname', '%'.$serchValue.'%');
            $database->bind(':staffLName', '%'.$serchValue.'%');
            $database->bind(':statusVal', '%'.$serchValue.'%');
            
            $original_workList = $database->resultset();
            if($original_workList) {

                if(count($original_workList) > 0){
//                    array_push($data, $original_workList);
                    return $original_workList;
                } else {
                    return false;
                }
            } else {
                throw new Exception("listOriginalWorkList() function not working");
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function listReworkWorkList($original_work_id) {
        $database = new database();
        try {
            $query1 = "SELECT
                            w.*,
                            c.name as checkpointName,
                            c.image as sampleImage,
                            l_p.ID as parentID,
                            l_p.name as parentName,
                            l_c.ID as childID,
                            l_c.name as childName,
                            u.ID as staffID,
                            u.firstName as staff_FName,
                            u.lastName as staff_LName,
                            spv.ID as supervisorID,
                            spv.firstName as supervisorFName,
                            spv.lastName as supervisorLName
                        FROM
                            work_list w
                        INNER JOIN checkpoint c ON
                            w.checkpoint_id = c.ID
                        INNER JOIN location l_c ON
                            c.locationid = l_c.ID
                        INNER JOIN location l_p ON
                            l_c.parentid = l_p.ID
                        INNER JOIN user u ON
                            w.user_id = u.ID
                        INNER JOIN staff_supervisor s_f ON
                            u.ID = s_f.user_id 
                        INNER JOIN user spv ON
                            s_f.supervisor_id = spv.ID 
                        WHERE w.rework = :rework AND w.original_work_id = :original_work_id";
            
            
            $database->query($query1);
            $database->bind(':rework', 1);
            $database->bind(':original_work_id', $original_work_id);
              
            $reWork_workList = $database->resultset();
            if($reWork_workList) {               
                return $reWork_workList;
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function updateStatus($workID,$status,$comment) {
        $database = new database();
        try {
            
            $query = "UPDATE work_list SET work_status = :status,comment = :comment WHERE ID = :workID";
            $database->query($query);
            $database->bind(':status', $status);
            $database->bind(':workID', $workID);
            $database->bind(':comment', $comment);
            
            if($database->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function deleteWork($workID) {
        $database = new database();
        try {
            
            $query = "DELETE FROM work_list WHERE ID = :workID";
            $database->query($query);
            $database->bind(':workID', $workID);
            
            if($database->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function singleWork($workID,$loggedUserID) {
        
        $pendingWorkLimit = WorkDAO::getPendingWorkLimit($loggedUserID);
        
        $database = new database();
        try {
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            $supervisorSet = $loggedUserID !== false;
            
            $query = "SELECT
                        w.ID as workID,
                        w.image as w_img_original,
                        w.work_status as status,
                        w.comment as comment,
                        c.ID as checkpointID,
                        c.name as checkpointName,
                        l_p.ID as loc_parentID,
                        l_p.name as loc_parentName,
                        l_c.ID as loc_childID,
                        l_c.name as loc_childName,
                        w.server_date as uploadServerDate,
                        w.server_time as uploadServerTime,
                        w.device_time as uploadDeviceTime,
                        w.original_work_id as originalWorkID,
                        w.stf_comment as stf_comment,
                        t.description as schedule_Description,
                        u.ID as staffID,
                        u.firstName as staff_fName,
                        u.lastName as staff_lName,
                        c.description as check_Description,
                        c.image as c_img_original,
                        t.endTime as scheduleEndTime,
                        t.ID as scheduleID,
                        t.scheduleStatus as scheduleStatus,
                        IF(t.endTime>w.server_time, 'YES', 'NO') as early,
                        SEC_TO_TIME(TIMESTAMPDIFF(SECOND,t.endTime,w.server_time)) as timeDiff
                      FROM
                        work_list w 
                      INNER JOIN checkpoint c ON 
                        w.checkpoint_id = c.ID 
                      INNER JOIN location l_c ON 
                        c.locationid = l_c.ID 
                      INNER JOIN location l_p ON
                        l_c.parentid = l_p.ID 
                      INNER JOIN user u ON 
                        w.user_id = u.ID 
                      INNER JOIN taskandschedule t ON 
                        w.schedule_id = t.ID 
                      INNER JOIN staff_supervisor spv ON 
                        spv.user_id = u.ID 
                      WHERE w.ID = :workID";
            
            if($supervisorSet) {
                $query = $query." AND spv.supervisor_id = :spvID";
            }
            
            $database->query($query);
            $database->bind(':workID', $workID);
            
            if($supervisorSet) {
                $database->bind(':spvID', $loggedUserID);
            }
            
            $singleWork = $database->single();
            if($singleWork) {
                
                
                $imagePath = $singleWork['w_img_original'];
                if($imagePath !== null){
                    
                    if(FileManager::fileExist($imagePath)){
                        $singleWork['w_img_original_string'] = base64_encode(file_get_contents($imagePath));
                    } else {
                        $singleWork['w_img_original_string'] = null;
                    }
                    
                    $extension_pos = strrpos($imagePath, '.');

                    $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                    $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);
                    $large = substr($imagePath,0, $extension_pos).$size_3.substr($imagePath, $extension_pos);

                    $singleWork['w_img_thumbnail'] = $thumb;
                    $singleWork['w_img_medium'] = $medium;
                    $singleWork['w_img_large'] = $large;
                } else {
                    $singleWork['w_img_thumbnail'] = null;
                    $singleWork['w_img_medium'] = null;
                    $singleWork['w_img_large'] = null;
                }
                
                $imagePath1 = $singleWork['c_img_original'];
                if($imagePath1 !== null){
                    
                    if(FileManager::fileExist($imagePath1)){
                        $singleWork['c_img_original_string'] = base64_encode(file_get_contents($imagePath1));
                    } else {
                        $singleWork['c_img_original_string'] = null;
                    }
                    
                    $extension_pos = strrpos($imagePath1, '.');

                    $thumb = substr($imagePath1,0, $extension_pos).$size_1.substr($imagePath1, $extension_pos);
                    $medium = substr($imagePath1,0, $extension_pos).$size_2.substr($imagePath1, $extension_pos);
                    $large = substr($imagePath1,0, $extension_pos).$size_3.substr($imagePath1, $extension_pos);

                    $singleWork['c_img_thumbnail'] = $thumb;
                    $singleWork['c_img_medium'] = $medium;
                    $singleWork['c_img_large'] = $large;
                } else {
                    $singleWork['c_img_thumbnail'] = null;
                    $singleWork['c_img_medium'] = null;
                    $singleWork['c_img_large'] = null;
                }
                
                if(count($pendingWorkLimit) == 2){
                    $singleWork["next"] = $pendingWorkLimit[1]['workID'];
                } else {
                    $singleWork["next"] = "NO";
                }
                
                if($singleWork['originalWorkID'] !== null){//get original work
                    
                    $query2 = "SELECT
                        w.ID as workID,
                        w.image as w_img_original,
                        w.work_status as status,
                        c.ID as checkpointID,
                        c.name as checkpointName,
                        l_p.ID as loc_parentID,
                        l_p.name as loc_parentName,
                        l_c.ID as loc_childID,
                        l_c.name as loc_childName,
                        w.server_time as uploadServerTime,
                        w.device_time as uploadDeviceTime,
                        w.original_work_id as originalWorkID,
                        w.stf_comment as stf_comment,
                        t.description as schedule_Description,
                        u.ID as staffID,
                        u.firstName as staff_fName,
                        u.lastName as staff_lName,
                        c.description as check_Description,
                        c.image as c_img_original,
                        t.endTime as scheduleEndTime,
                        IF(t.endTime>w.server_time, 'YES', 'NO') as early,
                        SEC_TO_TIME(TIMESTAMPDIFF(SECOND,t.endTime,w.server_time)) as timeDiff
                      FROM
                        work_list w 
                      INNER JOIN checkpoint c ON 
                        w.checkpoint_id = c.ID 
                      INNER JOIN location l_c ON 
                        c.locationid = l_c.ID 
                      INNER JOIN location l_p ON
                        l_c.parentid = l_p.ID 
                      INNER JOIN user u ON 
                        w.user_id = u.ID 
                      INNER JOIN taskandschedule t ON 
                        w.schedule_id = t.ID 
                      INNER JOIN staff_supervisor spv ON 
                        spv.user_id = u.ID 
                      WHERE w.ID = :workID";
            
                    if($supervisorSet) {
                        $query2 = $query2." AND spv.supervisor_id = :spvID";
                    }

                    $database->query($query2);
                    $database->bind(':workID', $singleWork['originalWorkID']);

                    if($supervisorSet) {
                        $database->bind(':spvID', $loggedUserID);
                    }
                    
                    $originalWork = $database->single();
                    if($originalWork){
                        
                        
                        $imagePath = $singleWork['w_img_original'];
                        if($imagePath !== null){
                            
                            if(FileManager::fileExist($imagePath)){
                                $singleWork['w_img_original_string'] = base64_encode(file_get_contents($imagePath));
                            } else {
                                $singleWork['w_img_original_string'] = null;
                            }
                            
                            $extension_pos = strrpos($imagePath, '.');

                            $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                            $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);
                            $large = substr($imagePath,0, $extension_pos).$size_3.substr($imagePath, $extension_pos);

                            $singleWork['w_img_thumbnail'] = $thumb;
                            $singleWork['w_img_medium'] = $medium;
                            $singleWork['w_img_large'] = $large;
                        } else {
                            $singleWork['w_img_thumbnail'] = null;
                            $singleWork['w_img_medium'] = null;
                            $singleWork['w_img_large'] = null;
                        }

                        $imagePath1 = $singleWork['c_img_original'];
                        if($imagePath1 !== null){
                            
                            if(FileManager::fileExist($imagePath1)){
                                $singleWork['c_img_original_string'] = base64_encode(file_get_contents($imagePath1));
                            } else {
                                $singleWork['c_img_original_string'] = null;
                            }
                            
                            $extension_pos = strrpos($imagePath1, '.');

                            $thumb = substr($imagePath1,0, $extension_pos).$size_1.substr($imagePath1, $extension_pos);
                            $medium = substr($imagePath1,0, $extension_pos).$size_2.substr($imagePath1, $extension_pos);
                            $large = substr($imagePath1,0, $extension_pos).$size_3.substr($imagePath1, $extension_pos);

                            $singleWork['c_img_thumbnail'] = $thumb;
                            $singleWork['c_img_medium'] = $medium;
                            $singleWork['c_img_large'] = $large;
                        } else {
                            $singleWork['c_img_thumbnail'] = null;
                            $singleWork['c_img_medium'] = null;
                            $singleWork['c_img_large'] = null;
                        }
                        
                        $singleWork["originalWork"] = $originalWork;
                    }
                    
                }
                
                
                if($singleWork['originalWorkID'] === null && $singleWork['status'] === "Rework"){//get rework if original work has a rework
                    
                    $query3 = "SELECT
                        w.ID as workID,
                        w.image as w_img_original,
                        w.work_status as status,
                        c.ID as checkpointID,
                        c.name as checkpointName,
                        l_p.ID as loc_parentID,
                        l_p.name as loc_parentName,
                        l_c.ID as loc_childID,
                        l_c.name as loc_childName,
                        w.server_time as uploadServerTime,
                        w.device_time as uploadDeviceTime,
                        w.original_work_id as originalWorkID,
                        w.stf_comment as stf_comment,
                        t.description as schedule_Description,
                        u.ID as staffID,
                        u.firstName as staff_fName,
                        u.lastName as staff_lName,
                        c.description as check_Description,
                        c.image as c_img_original,
                        t.endTime as scheduleEndTime,
                        IF(t.endTime>w.server_time, 'YES', 'NO') as early,
                        SEC_TO_TIME(TIMESTAMPDIFF(SECOND,t.endTime,w.server_time)) as timeDiff
                      FROM
                        work_list w 
                      INNER JOIN checkpoint c ON 
                        w.checkpoint_id = c.ID 
                      INNER JOIN location l_c ON 
                        c.locationid = l_c.ID 
                      INNER JOIN location l_p ON
                        l_c.parentid = l_p.ID 
                      INNER JOIN user u ON 
                        w.user_id = u.ID 
                      INNER JOIN taskandschedule t ON 
                        w.schedule_id = t.ID 
                      INNER JOIN staff_supervisor spv ON 
                        spv.user_id = u.ID 
                      WHERE w.original_work_id = :workID";
            
                    if($supervisorSet) {
                        $query3 = $query3." AND spv.supervisor_id = :spvID";
                    }

                    $database->query($query3);
                    $database->bind(':workID', $singleWork['workID']);

                    if($supervisorSet) {
                        $database->bind(':spvID', $loggedUserID);
                    }
                    
                    $rework = $database->single();
                    if($rework){                      
                        
                        $imagePath = $singleWork['w_img_original'];
                        if($imagePath !== null){
                            
                            if(FileManager::fileExist($imagePath)){
                                $singleWork['w_img_original_string'] = base64_encode(file_get_contents($imagePath));
                            } else {
                                $singleWork['w_img_original_string'] = null;
                            }
                            
                            $extension_pos = strrpos($imagePath, '.');

                            $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                            $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);
                            $large = substr($imagePath,0, $extension_pos).$size_3.substr($imagePath, $extension_pos);

                            $singleWork['w_img_thumbnail'] = $thumb;
                            $singleWork['w_img_medium'] = $medium;
                            $singleWork['w_img_large'] = $large;
                        } else {
                            $singleWork['w_img_thumbnail'] = null;
                            $singleWork['w_img_medium'] = null;
                            $singleWork['w_img_large'] = null;
                        }

                        $imagePath1 = $singleWork['c_img_original'];
                        if($imagePath1 !== null){
                            
                            if(FileManager::fileExist($imagePath1)){
                                $singleWork['c_img_original_string'] = base64_encode(file_get_contents($imagePath1));
                            } else {
                                $singleWork['c_img_original_string'] = null;
                            }
                            
                            $extension_pos = strrpos($imagePath1, '.');

                            $thumb = substr($imagePath1,0, $extension_pos).$size_1.substr($imagePath1, $extension_pos);
                            $medium = substr($imagePath1,0, $extension_pos).$size_2.substr($imagePath1, $extension_pos);
                            $large = substr($imagePath1,0, $extension_pos).$size_3.substr($imagePath1, $extension_pos);

                            $singleWork['c_img_thumbnail'] = $thumb;
                            $singleWork['c_img_medium'] = $medium;
                            $singleWork['c_img_large'] = $large;
                        } else {
                            $singleWork['c_img_thumbnail'] = null;
                            $singleWork['c_img_medium'] = null;
                            $singleWork['c_img_large'] = null;
                        }
                        
                        if(count($rework)>0){
                            $singleWork["rework"] = $rework;
                        }
                    }                    
                }                
                return $singleWork;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function checkWork($workID) {
        $database = new database();
        try {
            
            $query = "SELECT * FROM work_list WHERE ID = :workID";
            $database->query($query);
            $database->bind(':workID', $workID);
            
            $result = $database->single();
            if($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getPendingWorkLimit($loggedUserID) {
        $database = new database();
        try {
            
            $workStatus = "Pending";          
            
            $supervisorSet = $loggedUserID !== false;
            
            $query = "SELECT
                        w.ID as workID
                      FROM
                        work_list w  
                      INNER JOIN user u ON 
                        w.user_id = u.ID 
                      INNER JOIN taskandschedule t ON 
                        w.schedule_id = t.ID 
                      INNER JOIN staff_supervisor spv ON 
                        spv.user_id = u.ID
                      WHERE w.work_status = :workStatus";
            
            if($supervisorSet) {
                $query = $query." AND spv.supervisor_id = :spvID";
            }
            
            $query = $query." LIMIT 2";
            
            $database->query($query);
            $database->bind(':workStatus', $workStatus);
            
            if($supervisorSet) {
                $database->bind(':spvID', $loggedUserID);
            }
            
            $result = $database->resultset();
            if($result) {
                return $result;
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getPendingWorkCount($loggedUserID) {
        $database = new database();
        try {
            
            $workStatus = "Pending";
            
            $supervisorSet = $loggedUserID !== false;
            
            $query = "SELECT
                        w.ID as firstWorkID,
                        COUNT(w.ID) as workCount
                      FROM
                        work_list w  
                      INNER JOIN user u ON 
                        w.user_id = u.ID 
                      INNER JOIN taskandschedule t ON 
                        w.schedule_id = t.ID 
                      INNER JOIN staff_supervisor spv ON 
                        spv.user_id = u.ID
                      WHERE w.work_status = :workStatus";
            
            if($supervisorSet) {
                $query = $query." AND spv.supervisor_id = :spvID";
            }
            
            $database->query($query);
            $database->bind(':workStatus', $workStatus);
            
            if($supervisorSet) {
                $database->bind(':spvID', $loggedUserID);
            }
            
            $result = $database->resultset();
            if($result) {
                return $result;
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getPendingWork($loggedUserID) {
        $database = new database();
        try {
            
            $workStatus = "Pending";
            
            $supervisorSet = $loggedUserID !== false;
            
            $query = "SELECT
                        w.ID as workID,
                        w.image as workImage,
                        c.ID as checkpointID,
                        c.name as checkpointName,
                        l_p.ID as loc_parentID,
                        l_p.name as loc_parentName,
                        l_c.ID as loc_childID,
                        l_c.name as loc_childName,
                        w.server_time as uploadServerTime,
                        w.device_time as uploadDeviceTime,
                        u.ID as staffID,
                        u.firstName as staff_fName,
                        u.lastName as staff_lName,
                        c.description as check_Description,
                        c.image as check_sampleImage,
                        t.endTime as scheduleEndTime,
                        IF(t.endTime>w.server_time, 'YES', 'NO') as beforeORafter,
                        SEC_TO_TIME(TIMESTAMPDIFF(SECOND,t.endTime,w.server_time)) as timeDiff
                      FROM
                        work_list w 
                      INNER JOIN checkpoint c ON 
                        w.checkpoint_id = c.ID 
                      INNER JOIN location l_c ON 
                        c.locationid = l_c.ID 
                      INNER JOIN location l_p ON
                        l_c.parentid = l_p.ID 
                      INNER JOIN user u ON 
                        w.user_id = u.ID 
                      INNER JOIN taskandschedule t ON 
                        w.schedule_id = t.ID 
                      INNER JOIN staff_supervisor spv ON 
                        spv.user_id = u.ID
                      WHERE w.work_status = :workStatus";
            
            if($supervisorSet) {
                $query = $query." AND spv.supervisor_id = :spvID";
            }
            
            $database->query($query);
            $database->bind(':workStatus', $workStatus);
            
            if($supervisorSet) {
                $database->bind(':spvID', $loggedUserID);
            }
            
            $result = $database->resultset();
            if($result) {
                return $result;
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getPendingWorkIDs($loggedUserID) {
        $database = new database();
        try {
            
            $workStatus = "Pending";
            
            $supervisorSet = $loggedUserID !== false;
            
            $query = "SELECT
                        w.ID as workID
                      FROM
                        work_list w 
                      INNER JOIN checkpoint c ON 
                        w.checkpoint_id = c.ID 
                      INNER JOIN location l_c ON 
                        c.locationid = l_c.ID 
                      INNER JOIN location l_p ON
                        l_c.parentid = l_p.ID 
                      INNER JOIN user u ON 
                        w.user_id = u.ID 
                      INNER JOIN taskandschedule t ON 
                        w.schedule_id = t.ID 
                      INNER JOIN staff_supervisor spv ON 
                        spv.user_id = u.ID
                      WHERE w.work_status = :workStatus";
            
            if($supervisorSet) {
                $query = $query." AND spv.supervisor_id = :spvID";
            }
            
            $database->query($query);
            $database->bind(':workStatus', $workStatus);
            
            if($supervisorSet) {
                $database->bind(':spvID', $loggedUserID);
            }
            
            $result = $database->resultset();
            if($result) {
                return $result;
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
        
    public static function getReworkedWork($loggedUserID) {
        $database = new database();
        try {
            
            $workStatus = "Rework";
            
            $query = "SELECT
                        w.ID as workID,
                        w.image as workImage,
                        c.ID as checkpointID,
                        c.name as checkpointName,
                        l_p.ID as loc_parentID,
                        l_p.name as loc_parentName,
                        l_c.ID as loc_childID,
                        l_c.name as loc_childName,
                        w.server_time as uploadServerTime,
                        w.device_time as uploadDeviceTime,
                        w.schedule_id as scheduleID,
                        u.ID as staffID,
                        u.firstName as staff_fName,
                        u.lastName as staff_lName,
                        c.description as check_Description,
                        c.image as check_sampleImage,
                        t.endTime as scheduleEndTime,
                        spv_u.ID as supervisorID,
                        spv_u.firstName as supervisor_fName,
                        spv_u.lastName as supervisor_lName,
                        IF(t.endTime>w.server_time, 'YES', 'NO') as beforeORafter,
                        SEC_TO_TIME(TIMESTAMPDIFF(SECOND,t.endTime,w.server_time)) as timeDiff
                      FROM
                        work_list w 
                      INNER JOIN checkpoint c ON 
                        w.checkpoint_id = c.ID 
                      INNER JOIN location l_c ON 
                        c.locationid = l_c.ID 
                      INNER JOIN location l_p ON
                        l_c.parentid = l_p.ID 
                      INNER JOIN user u ON 
                        w.user_id = u.ID 
                      INNER JOIN taskandschedule t ON 
                        w.schedule_id = t.ID 
                      INNER JOIN staff_supervisor spv ON 
                        spv.user_id = u.ID
                      INNER JOIN user spv_u ON
                        spv.supervisor_id = spv_u.ID
                      WHERE w.work_status = :workStatus AND u.ID = :staffID";
            
            
            $database->query($query);
            $database->bind(':workStatus', $workStatus);
            $database->bind(':staffID', $loggedUserID);
            
            
            $result = $database->resultset();
            if($result) {
                return $result;
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getTodayRework($loggedUserID) {
        $database = new database();
        try {
            
            $workStatus = "Rework";
            
            $query = "SELECT
                        w.ID as workID,
                        w.image as workImage,
                        c.ID as checkpointID,
                        c.name as checkpointName,
                        l_p.ID as loc_parentID,
                        l_p.name as loc_parentName,
                        l_c.ID as loc_childID,
                        l_c.name as loc_childName,
                        w.server_time as uploadServerTime,
                        w.device_time as uploadDeviceTime,
                        w.schedule_id as scheduleID,
                        u.ID as staffID,
                        u.firstName as staff_fName,
                        u.lastName as staff_lName,
                        c.description as check_Description,
                        c.image as check_sampleImage,
                        t.endTime as scheduleEndTime,
                        spv_u.ID as supervisorID,
                        spv_u.firstName as supervisor_fName,
                        spv_u.lastName as supervisor_lName,
                        IF(t.endTime>w.server_time, 'YES', 'NO') as beforeORafter,
                        SEC_TO_TIME(TIMESTAMPDIFF(SECOND,t.endTime,w.server_time)) as timeDiff
                      FROM
                        work_list w 
                      INNER JOIN checkpoint c ON 
                        w.checkpoint_id = c.ID 
                      INNER JOIN location l_c ON 
                        c.locationid = l_c.ID 
                      INNER JOIN location l_p ON
                        l_c.parentid = l_p.ID 
                      INNER JOIN user u ON 
                        w.user_id = u.ID 
                      INNER JOIN taskandschedule t ON 
                        w.schedule_id = t.ID 
                      INNER JOIN staff_supervisor spv ON 
                        spv.user_id = u.ID
                      INNER JOIN user spv_u ON
                        spv.supervisor_id = spv_u.ID
                      WHERE w.work_status = :workStatus AND u.ID = :staffID AND w.server_date = CURDATE() AND w.ID NOT IN (SELECT original_work_id FROM work_list WHERE original_work_id IS NOT NULL AND server_date = CURDATE())";
            
            
            $database->query($query);
            $database->bind(':workStatus', $workStatus);
            $database->bind(':staffID', $loggedUserID);
            
            
            $result = $database->resultset();
            if($result) {
                return $result;
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getTodayReworkAndFlag($loggedUserID) {
        $database = new database();
        try {
            
            $r_workStatus = "Rework";
            $f_workStatus = "Flagged";
            
            $query = "SELECT
                        w.ID as workID,
                        w.image as workImage,
                        w.work_status as workStatus,
                        c.ID as checkpointID,
                        c.name as checkpointName,
                        l_p.ID as loc_parentID,
                        l_p.name as loc_parentName,
                        l_c.ID as loc_childID,
                        l_c.name as loc_childName,
                        w.server_time as uploadServerTime,
                        w.device_time as uploadDeviceTime,
                        w.schedule_id as scheduleID,
                        u.ID as staffID,
                        u.firstName as staff_fName,
                        u.lastName as staff_lName,
                        c.description as check_Description,
                        c.image as check_sampleImage,
                        t.endTime as scheduleEndTime,
                        spv_u.ID as supervisorID,
                        spv_u.firstName as supervisor_fName,
                        spv_u.lastName as supervisor_lName,
                        IF(t.endTime>w.server_time, 'YES', 'NO') as beforeORafter,
                        SEC_TO_TIME(TIMESTAMPDIFF(SECOND,t.endTime,w.server_time)) as timeDiff
                      FROM
                        work_list w 
                      INNER JOIN checkpoint c ON 
                        w.checkpoint_id = c.ID 
                      INNER JOIN location l_c ON 
                        c.locationid = l_c.ID 
                      INNER JOIN location l_p ON
                        l_c.parentid = l_p.ID 
                      INNER JOIN user u ON 
                        w.user_id = u.ID 
                      INNER JOIN taskandschedule t ON 
                        w.schedule_id = t.ID 
                      INNER JOIN staff_supervisor spv ON 
                        spv.user_id = u.ID
                      INNER JOIN user spv_u ON
                        spv.supervisor_id = spv_u.ID
                      WHERE (w.work_status = :r_workStatus OR w.work_status = :f_workStatus) AND u.ID = :staffID AND w.server_date = CURDATE() AND w.ID NOT IN (SELECT original_work_id FROM work_list WHERE original_work_id IS NOT NULL AND server_date = CURDATE())";
            
            
            $database->query($query);
            $database->bind(':r_workStatus', $r_workStatus);
            $database->bind(':f_workStatus', $f_workStatus);
            $database->bind(':staffID', $loggedUserID);
            
            $result = $database->resultset();
            if($result) {
                return $result;
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getReworkedWorkforDashBoard(Report $report) {
        $database = new database();
        try {
            
            $s_date = $report->getS_date();
            $e_date = $report->getE_date();
            $staff_id = $report->getStaffID();
            $supervisor_id = $report->getSupervisorID();
            $checkpoint_id = $report->getCheckpointID();
            
            $workStatus = "Rework";
            
            $query = "SELECT
                        c.name as checkpointName,
                        CONCAT(l_p.name,' > ',l_c.name) as location,
                        CONCAT(u.firstName,' ',u.lastName) as staffName,
                        w.server_date as uploadServerDate,
                        w.ID as workID,
                        c.ID as checkpointID,                        
                        l_p.ID as loc_parentID,
                        l_c.ID as loc_childID,
                        w.schedule_id as scheduleID,
                        u.ID as staffID                        
                      FROM
                        work_list w 
                      INNER JOIN checkpoint c ON 
                        w.checkpoint_id = c.ID 
                      INNER JOIN location l_c ON 
                        c.locationid = l_c.ID 
                      INNER JOIN location l_p ON
                        l_c.parentid = l_p.ID 
                      INNER JOIN user u ON 
                        w.user_id = u.ID 
                      WHERE w.work_status = :workStatus AND (w.server_date BETWEEN :s_date AND :e_date)
                      ORDER BY uploadServerDate DESC";
            
            
            $database->query($query);
            $database->bind(':workStatus', $workStatus);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
            
            $result = $database->resultset();
            if($result) {
                return $result;
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getAllNotification($loggedUserID,$s_date,$e_date) {
        $database = new database();
        try {
            
//            $workStatus = "Pending";
            
            $supervisorSet = $loggedUserID !== false;
            
            $query = "SELECT
                        w.ID as workID,
                        w.image as workImage,
                        c.ID as checkpointID,
                        c.name as checkpointName,
                        l_p.ID as loc_parentID,
                        l_p.name as loc_parentName,
                        l_c.ID as loc_childID,
                        l_c.name as loc_childName,
                        w.server_time as uploadServerTime,
                        w.device_time as uploadDeviceTime,
                        u.ID as staffID,
                        u.firstName as staff_fName,
                        u.lastName as staff_lName,
                        w.original_work_id as rework,
                        w.notification as notification
                      FROM
                        work_list w 
                      INNER JOIN checkpoint c ON 
                        w.checkpoint_id = c.ID 
                      INNER JOIN location l_c ON 
                        c.locationid = l_c.ID 
                      INNER JOIN location l_p ON
                        l_c.parentid = l_p.ID 
                      INNER JOIN user u ON 
                        w.user_id = u.ID 
                      INNER JOIN taskandschedule t ON 
                        w.schedule_id = t.ID 
                      INNER JOIN staff_supervisor spv ON 
                        spv.user_id = u.ID
                      WHERE (w.server_date BETWEEN :s_date AND :e_date) AND (w.notification = 0 OR w.notification = 1)";
            
            if($supervisorSet) {
                $query = $query." AND spv.supervisor_id = :spvID";
            }
            
            $query = $query." ORDER BY w.notification ASC, w.ID ASC";
            
            $database->query($query);
//            $database->bind(':workStatus', $workStatus);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
            
            if($supervisorSet) {
                $database->bind(':spvID', $loggedUserID);
            }
            
            if($database->execute()) {
                return $database->resultset();
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getUnreadNotification($loggedUserID,$s_date,$e_date) {
        $database = new database();
        try {
            
//            $workStatus = "Pending";
            
            $supervisorSet = $loggedUserID !== false;
            
            $query = "SELECT
                        w.ID as workID,
                        w.image as workImage,
                        c.ID as checkpointID,
                        c.name as checkpointName,
                        l_p.ID as loc_parentID,
                        l_p.name as loc_parentName,
                        l_c.ID as loc_childID,
                        l_c.name as loc_childName,
                        w.server_time as uploadServerTime,
                        w.device_time as uploadDeviceTime,
                        u.ID as staffID,
                        u.firstName as staff_fName,
                        u.lastName as staff_lName,
                        w.original_work_id as rework,
                        w.notification as notification
                      FROM
                        work_list w 
                      INNER JOIN checkpoint c ON 
                        w.checkpoint_id = c.ID 
                      INNER JOIN location l_c ON 
                        c.locationid = l_c.ID 
                      INNER JOIN location l_p ON
                        l_c.parentid = l_p.ID 
                      INNER JOIN user u ON 
                        w.user_id = u.ID 
                      INNER JOIN taskandschedule t ON 
                        w.schedule_id = t.ID 
                      INNER JOIN staff_supervisor spv ON 
                        spv.user_id = u.ID
                      WHERE (w.server_date BETWEEN :s_date AND :e_date) AND w.notification = 0";
            
            if($supervisorSet) {
                $query = $query." AND spv.supervisor_id = :spvID";
            }
            
            $database->query($query);
//            $database->bind(':workStatus', $workStatus);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
            
            if($supervisorSet) {
                $database->bind(':spvID', $loggedUserID);
            }
            
            if($database->execute()) {
                return $database->resultset();
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getUnreadNotificationCount($loggedUserID,$s_date,$e_date) {
        $database = new database();
        try {
            
//            $workStatus = "Pending";
            
            $supervisorSet = $loggedUserID !== false;
            
            $query = "SELECT
                        COUNT(w.ID) as notificationCount                        
                      FROM
                        work_list w 
                      INNER JOIN checkpoint c ON 
                        w.checkpoint_id = c.ID 
                      INNER JOIN location l_c ON 
                        c.locationid = l_c.ID 
                      INNER JOIN location l_p ON
                        l_c.parentid = l_p.ID 
                      INNER JOIN user u ON 
                        w.user_id = u.ID 
                      INNER JOIN taskandschedule t ON 
                        w.schedule_id = t.ID 
                      INNER JOIN staff_supervisor spv ON 
                        spv.user_id = u.ID
                      WHERE (w.server_date BETWEEN :s_date AND :e_date) AND w.notification = 0";
            
            if($supervisorSet) {
                $query = $query." AND spv.supervisor_id = :spvID";
            }
            
            $database->query($query);
//            $database->bind(':workStatus', $workStatus);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
            
            if($supervisorSet) {
                $database->bind(':spvID', $loggedUserID);
            }
            
            if($database->execute()) {
                return $database->single();
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function readingNotification($workID) {
        $database = new database();
        try {
            
//            $workStatus = "Pending";
            
            $query = "UPDATE work_list SET notification = 1 WHERE ID = :work_id";            
            $database->query($query);
            $database->bind(':work_id', $workID);
//            $database->bind(':workStatus', $workStatus);
            
            if($database->execute()) {
                return true;
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function workDuplicateValidation(Work $work){
        $database = new database();
        try {
            
            $query = "SELECT
                        *                        
                      FROM
                        work_list 
                      WHERE checkpoint_id = :checkpoint_id AND server_date = :server_date AND user_id = :user_id AND rework = :rework AND schedule_id = :schedule_id";
            
            $database->query($query);
            $database->bind(':checkpoint_id', $work->getCheckpointID());
            $database->bind(':server_date', $work->getServerDate());
            $database->bind(':user_id', $work->getUserID());
            $database->bind(':rework', $work->getRework());
            $database->bind(':schedule_id', $work->getScheduleID());
            
            
            if($database->execute()) {
                return $database->single();
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
     public static function getWorkByScheduleID($scheduleID){
        $database = new database();
        try {
            
            $query = "SELECT
                        *                        
                      FROM
                        work_list 
                      WHERE schedule_id = :schedule_id";
            
            $database->query($query);
            $database->bind(':schedule_id', $scheduleID);
            
            $result = $database->resultset();
            if($result) {
                return $result;
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
}
