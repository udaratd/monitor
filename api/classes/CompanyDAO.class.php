<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class CompanyDAO {
    
    public static function addCompany(Company $company) {
        $database = new database();
        $database->beginTransaction();
        try {
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';

            $query1="INSERT INTO company(name,image) VALUES(:name,:image)";
            $database->query($query1);
            
            $database->bind(':name', $company->getName());
            $database->bind(':image', null);
            $database->bind(':email', $company->getEmail());

            $company_success = $database->execute();
            
            $lastInsertedID = $database->lastInsertId();

            if(!empty($company->getImage())){
                
                $image = $company->getImage();
                $fileName = $lastInsertedID.'_'.mt_rand(1,5000);
                $extension = '.jpg';

                $path = UPLOAD_COMPANY_DIR;
                $imagePath = FileManager::imageUpload($path, $fileName.$extension, $image);
                
                if(is_string($imagePath)){
                    $query2="UPDATE company SET image = :image WHERE ID = :id";
                    $database->query($query2);

                    $database->bind(':id', $lastInsertedID);
                    $database->bind(':image', $imagePath);
                    
                    $imageUpdate = $database->execute();
                    
                    if($imageUpdate && $company_success){
                        FileManager::imageUpload($path, $fileName.$size_1.$extension, ImageManager::reSize(150, $image));
                        FileManager::imageUpload($path, $fileName.$size_2.$extension, ImageManager::reSize(500, $image));
                        FileManager::imageUpload($path, $fileName.$size_3.$extension, ImageManager::reSize(1024, $image));
                        
                        $database->endTransaction();
                        return true;
                    } else {
                        if(FileManager::fileExist($imagePath)){
                            FileManager::deleteFile($imagePath);
                        }
                        
                        $database->cancelTransaction();
                        return false;
                    }
                }   
            } else {
                if($company_success){
                    $database->endTransaction();
                    return true;
                }
            } 
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            $database->cancelTransaction();
            return false;
        }
    }
    
    public static function updateCompany(Company $company) {
        $oldImagePath = CompanyDAO::getImage($company->getId());
        $database = new database();
        try {
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            $imagePath = $oldImagePath;
            $success = null;
            
            if(!empty($company->getImage()) && !filter_var($company->getImage(), FILTER_VALIDATE_URL)) {
                
                $image = $company->getImage();
                $fileName = $company->getId() . '_' . mt_rand(5000, 10000);
                $extension = '.jpg';
                
                $path = UPLOAD_COMPANY_DIR;
                $imagePath = FileManager::imageUpload($path, $fileName.$extension, $image);
                
                if(is_string($imagePath) && ($imagePath !== 1) && ($imagePath !== false) ) {//check valid image
                    
                    FileManager::imageUpload($path, $fileName.$size_1.$extension, ImageManager::reSize(150, $image));
                    FileManager::imageUpload($path, $fileName.$size_2.$extension, ImageManager::reSize(500, $image));
                    FileManager::imageUpload($path, $fileName.$size_3.$extension, ImageManager::reSize(1024, $image));
                    
                    $success = true;
                    
                    if(!empty($oldImagePath) && $oldImagePath !== false){
                        
                        if(FileManager::fileExist($oldImagePath) === true){
                            if(FileManager::deleteFile($oldImagePath) === true) {                        
                                $extension_pos = strrpos($oldImagePath, '.');

                                $thumb = substr($oldImagePath,0, $extension_pos).$size_1.substr($oldImagePath, $extension_pos);
                                $medium = substr($oldImagePath,0, $extension_pos).$size_2.substr($oldImagePath, $extension_pos);
                                $large = substr($oldImagePath,0, $extension_pos).$size_3.substr($oldImagePath, $extension_pos);

                                if(FileManager::fileExist($thumb)){
                                    if(FileManager::deleteFile($thumb)===false){
                                        System::log(new Log('can not delete '.$thumb, LOG_ERROR));
                                    }
                                }
                                if(FileManager::fileExist($medium)){
                                    if(FileManager::deleteFile($medium)===false){
                                        System::log(new Log('can not delete '.$medium, LOG_ERROR));
                                    }
                                }
                                if(FileManager::fileExist($large)){
                                    if(FileManager::deleteFile($large)===false){
                                        System::log(new Log('can not delete '.$large, LOG_ERROR));
                                    }
                                }
                            } else {                                
                                System::log(new Log('can not delete '.$oldImagePath, LOG_ERROR));
                            }
                        }
                    }
                } else {
                    $success = false;
                }                    
            }
            
            $database->beginTransaction();

            $updateFields = CompanyDAO::updateFields($company);
            if($updateFields===false){
                return false;
            }
            
            $query = "UPDATE company SET ".implode(", ", $updateFields)." WHERE ID = :id";
            $database->query($query);
            $database->bind(':id', $company->getId());
            
            if(!empty($company->getName())){
                $database->bind(':name', $company->getName());
            }
            if(!empty($company->getImage()) && !filter_var($company->getImage(), FILTER_VALIDATE_URL)){
                $database->bind(':image', $imagePath);
            }
            if(!empty($company->getEmail())){
                $database->bind(':email', $company->getEmail());
            }

            $result = $database->execute();

            if($result && ($success === null || $success === true)) {
                $database->endTransaction();
                return true;
            } else {
                throw new Exception("Something went wrong, Can not Update Data");
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            $database->cancelTransaction();
            return false;
        }
    }
    
    public static function updateFields(Company $company) {
        $fields = array();
        if(!empty($company->getName())) {
            $fields[] = "name = :name";
        }
        if(!empty($company->getImage()) && !filter_var($company->getImage(), FILTER_VALIDATE_URL)) {
            $fields[] = "image = :image";
        }
        if(!empty($company->getEmail())) {
            $fields[] = "email = :email";
        }
        
        if(!empty($fields)){
            return $fields;
        } else {
            return false;
        }
    }
    
    public static function getImage($id) {
        $database = new database();
        try {
            $query = "SELECT image FROM company WHERE ID = :id";
            $database->query($query);
            $database->bind(':id', $id);
            $result = $database->single();

            $path = $result['image'];
            if($result) {
                return $path;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }
    
    public static function getCompany($id) {
        $database = new database();
        try {
            $query = "SELECT ID,name,logo,email,phone FROM company WHERE ID = :id";
            $database->query($query);
            $database->bind(':id', $id);
            $result = $database->single();

            if($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }
    
    public static function getCompanyCount() {
        $database = new database();
        try {
            $query = "SELECT COUNT(ID) as count FROM company";
            $database->query($query);

            $result = $database->single();

            if($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }
    
    public static function getCompanyID() {
        $database = new database();
        try {
            $query = "SELECT * FROM company";
            $database->query($query);

            $result = $database->resultset();

            if($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }
}
