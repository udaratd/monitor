<?php

class AdminDAO {
    
    public static function addNewCompany(Admin $admin){
        $database = new database();
        $database->beginTransaction();
        try {
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            $query="INSERT INTO company(name,address,dbname,username,phone,email,f_name,l_name,logo,bucket_name,active,expiry_date,user,skip_validation) "
                    . "VALUES(:name,:address,:dbname,:username,:phone,:email,:f_name,:l_name,:logo,:bucket_name,:active,:expiry_date,:user,:skip_validation)";
            $database->query($query);
            
            $database->bind(':name', $admin->getCom_name());
            $database->bind(':address', $admin->getCom_address());
            $database->bind(':dbname', $admin->getDbName());
            $database->bind(':username', $admin->getUsername());           
            $database->bind(':phone', $admin->getPhone());           
            $database->bind(':email', $admin->getEmail());           
            $database->bind(':f_name', $admin->getFName());           
            $database->bind(':l_name', $admin->getLName());           
            $database->bind(':logo', null);           
            $database->bind(':bucket_name', $admin->getBucketName());                     
            $database->bind(':active', 1);
            $database->bind(':expiry_date', $admin->getExpiryDate());
            $database->bind(':user', $admin->getCheckFirstUser());
            $database->bind(':skip_validation', $admin->getSkip_validation());

            $company_success = $database->execute();
            
            $lastInsertedID = $database->lastInsertId();
            
            if(!empty($admin->getLogo())){
                
                $image = $admin->getLogo();
                $fileName = $lastInsertedID.'_'.mt_rand(1,5000);
                $extension = '.jpg';

                $path = COMPANY_LOGO;
                $imagePath = FileManager::imageUpload($path, $fileName.$extension, $image);
                
                if(is_string($imagePath)){
                    $query2="UPDATE company SET logo = :logo WHERE ID = :id";
                    $database->query($query2);

                    $database->bind(':id', $lastInsertedID);
                    $database->bind(':logo', $imagePath);
                    
                    $imageUpdate = $database->execute();
                    
                    if($imageUpdate && $company_success){
                        FileManager::imageUpload($path, $fileName.$size_1.$extension, ImageManager::reSize(150, $image));
                        FileManager::imageUpload($path, $fileName.$size_2.$extension, ImageManager::reSize(500, $image));
                        FileManager::imageUpload($path, $fileName.$size_3.$extension, ImageManager::reSize(1024, $image));
                        
                        $database->endTransaction();
                        return $lastInsertedID;
                    } else {
                        if(FileManager::fileExist($imagePath)){
                            FileManager::deleteFile($imagePath);
                        }
                        
                        $database->cancelTransaction();
                        return false;
                    }
                }   
            } else {
                if($company_success){
                    $database->endTransaction();
                    return $lastInsertedID;
                } else {
                    $database->cancelTransaction();
                    return false;
                }
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $database->cancelTransaction();
            $exc->getMessage();
            return false;
        }
    }
    
    public static function checkCompany($username,$dbname = null){
        $database = new database($dbname);
        try {

            $query="SELECT * FROM company WHERE username = :username";
            $database->query($query);
            
            $database->bind(':username', $username);
            
            $companyName = $database->single();
            
            if($companyName){
                return $companyName;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }    
    
    public static function getSingleByUsername(Admin $admin){
        $database = new database(COMPANY_DB_NAME);
        try {

            $query="SELECT * FROM company WHERE username = :username";
            $database->query($query);
            
            $database->bind(':username', $admin->getUsername());
            
            $dbName = $database->single();
            
            if($dbName){
                return $dbName;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function single(Admin $admin){
        $database = new database();
        try {

            $query="SELECT * FROM company WHERE ID = :id";
            $database->query($query);
            
            $database->bind(':id', $admin->getId());
            
            $companyDetails = $database->single();
            
            if($companyDetails){
                return $companyDetails;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function checkEmail(Admin $admin){
        $database = new database();
        try {

            $query="SELECT * FROM company WHERE email = :email";
            $database->query($query);
            
            $database->bind(':email', $admin->getEmail());
            
            $companyDetails = $database->single();
            
            if($companyDetails){
                return $companyDetails;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function listCompany(){
        $database = new database();
        try {

            $query="SELECT * FROM company";
            $database->query($query);

            $companyList = $database->resultset();
            
            if($companyList){
                return $companyList;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function createDatabase($databaseName){
        $database = new database();
        try {

            $query="CREATE DATABASE $databaseName";

            $database->query($query);

            $result = $database->execute();
            
            if($result){
                return true;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function createTables($databaseName){
        $database = new database($databaseName);
        try {
            $sql = file_get_contents(DATABASE_TABLE);
            
            $query=$sql;
            
            $database->query($query);
            
            $result = $database->execute();
            
            if($result){
                return true;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function createNewTable(DatabaseTable $table){
        $database = new database(COMPANY_DB_NAME);
        try {
            $fields = $table->getFields();
            $query='CREATE TABLE IF NOT EXISTS '.$table->getDatabase_name().'.'.$table->getTable_name().' (';
            
            foreach ($fields as $field){              
                if($field['type'] === 'p'){
                    if($field['auto_increment']){
                        $query = $query . $field['field'] . ' INT AUTO_INCREMENT PRIMARY KEY, ';
                    } else {
                        $query = $query . $field['field'] . ' INT PRIMARY KEY, ';
                    }                   
                }
                
                if($field['type'] === 'i'){
                    if($field['allow_null']){
                        $query = $query . $field['field'] . ' INT, ';
                    } else {
                        $query = $query . $field['field'] . ' INT NOT NULL, ';
                    }                   
                }
                
                if($field['type'] === 'v'){
                    $length = $field['length'];
                    if($field['allow_null']){
                        $query = $query . $field['field'] . ' VARCHAR('.$length.'), ';
                    } else {
                        $query = $query . $field['field'] . ' VARCHAR('.$length.') NOT NULL, ';
                    }                   
                }
                
                if($field['type'] === 'd'){
                    if($field['allow_null']){
                        $query = $query . $field['field'] . ' DATE, ';
                    } else {
                        $query = $query . $field['field'] . ' DATE NOT NULL, ';
                    }                   
                }
                
                if($field['type'] === 'dt'){
                    if($field['allow_null']){
                        $query = $query . $field['field'] . ' DATETIME, ';
                    } else {
                        $query = $query . $field['field'] . ' DATETIME NOT NULL, ';
                    }                   
                }
                
                if($field['type'] === 't'){
                    if($field['allow_null']){
                        $query = $query . $field['field'] . ' TEXT, ';
                    } else {
                        $query = $query . $field['field'] . ' TEXT NOT NULL, ';
                    }                   
                }
                
                if($field['type'] === 'lt'){
                    if($field['allow_null']){
                        $query = $query . $field['field'] . ' LONGTEXT, ';
                    } else {
                        $query = $query . $field['field'] . ' LONGTEXT NOT NULL, ';
                    }                   
                }
                
                if($field['type'] === 'ts'){
                    if($field['allow_null']){
                        $query = $query . $field['field'] . ' TIMESTAMP, ';
                    } else {
                        $query = $query . $field['field'] . ' TIMESTAMP NOT NULL, ';
                    }                   
                }
                
                if($field['type'] === 'b'){
                    $query = $query . $field['field'] . ' TINYINT(1), ';                
                }                
            }
                   
            $query = rtrim(trim($query), ',');

            $query = $query . ') ENGINE=INNODB;';

            $database->query($query);
            
            $result = $database->execute();           
            if($result){
                return true;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }

    public static function showDatabases(){
        $databases = array();
        $database = new database(COMPANY_DB_NAME);
        try {
            
            $query='show databases';
            
            $database->query($query);
            
            $result = $database->resultset();
            
            if($result){
                
                foreach ($result as $singleDatabase){
                    array_push($databases, $singleDatabase['Database']);
                }
                return $databases;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function updateCompany(Admin $admin){
        $database = new database();
        $database->beginTransaction();
        try {
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            $query="UPDATE company SET name = :name,address = :address,username = :username,phone = :phone,email = :email,f_name = :f_name,l_name = :l_name,active = :active,skip_validation = :skip_validation WHERE ID = :id";
            
            $database->query($query);
            
            $database->bind(':id', $admin->getId());
            $database->bind(':name', $admin->getCom_name());
            $database->bind(':address', $admin->getCom_address());
            $database->bind(':username', $admin->getUsername());           
            $database->bind(':phone', $admin->getPhone());           
            $database->bind(':email', $admin->getEmail());           
            $database->bind(':f_name', $admin->getFName());                                         
            $database->bind(':l_name', $admin->getLName());                                         
            $database->bind(':active', $admin->getActive());           
            $database->bind(':skip_validation', $admin->getSkip_validation());           

            $company_success = $database->execute();
            
            $lastInsertedID = $admin->getId();
            
            if(!empty($admin->getLogo())){
                
                if(!Validation::checkULR($admin->getLogo()) && Validation::validateBase64String($admin->getLogo())){
                    $image = $admin->getLogo();
                    $fileName = $lastInsertedID.'_'.mt_rand(1,5000);
                    $extension = '.jpg';

                    $path = COMPANY_LOGO;
                    $imagePath = FileManager::imageUpload($path, $fileName.$extension, $image);

                    if(is_string($imagePath)){
                        $query2="UPDATE company SET logo = :logo WHERE ID = :id";
                        $database->query($query2);

                        $database->bind(':id', $lastInsertedID);
                        $database->bind(':logo', $imagePath);

                        $imageUpdate = $database->execute();

                        if($imageUpdate && $company_success){
                            FileManager::imageUpload($path, $fileName.$size_1.$extension, ImageManager::reSize(150, $image));
                            FileManager::imageUpload($path, $fileName.$size_2.$extension, ImageManager::reSize(500, $image));
                            FileManager::imageUpload($path, $fileName.$size_3.$extension, ImageManager::reSize(1024, $image));

                            $database->endTransaction();
                            return true;
                        } else {
                            if(FileManager::fileExist($imagePath)){
                                FileManager::deleteFile($imagePath);
                            }

                            $database->cancelTransaction();
                            return false;
                        }
                    }
                } else {
                    if($company_success){
                        $database->endTransaction();
                        return true;
                    } else {
                        $database->cancelTransaction();
                        return false;
                    }
                }                
            } else {
                if($company_success){
                    $database->endTransaction();
                    return true;
                } else {
                    $database->cancelTransaction();
                    return false;
                }
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $database->cancelTransaction();
            $exc->getMessage();
            return false;
        }
    }
    
    public static function login(Admin $admin){
        $database=new database();        
        try{
   
            $query = "SELECT ID,f_name,l_name,email FROM super_admin WHERE email = :email AND password = :password";

            $database->query($query);
            $database->bind(':email',$admin->getEmail());
            $database->bind(':password', $admin->getPasswordHash());

            $logginData = $database->single(); 
            if($logginData) {                             
                return $logginData;
            } else {
                return false;
            }          
        } catch (Exception $ex) {
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            $ex->getMessage();
            return false;
        }
    }
    
    public static function updateSuperAdmin(Admin $admin){
        $database=new database();
        try {

            $fields = AdminDAO::identifyUpdateFields($admin);
            if(!$fields){
                return false;
            }
            
            $query = "UPDATE super_admin SET ". implode(" ,", $fields)." WHERE ID = :id";
            $database->query($query);
            
            $database->bind(":id", $admin->getId());
            
            if(!empty($admin->getFName())){
                $database->bind(":f_name", $admin->getFName());                
            }
            if(!empty($admin->getLName())){
                $database->bind(":l_name", $admin->getLName());                
            }
            if(!empty($admin->getEmail())){
                $database->bind(":email", $admin->getEmail());                
            }
            if(!empty($admin->getPassword()) && !empty($admin->getNewPassword())){
                $database->bind(":password", $admin->getNewPasswordHash());                
            }

            if($database->execute()){
                return true;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
    
    public static function identifyUpdateFields(Admin $admin) {
        $fields = array();
        
        if(!empty($admin->getFName())) {
            $fields[] = "f_name = :f_name";
        }
        if(!empty($admin->getLName())) {
            $fields[] = "l_name = :l_name";
        }
        if(!empty($admin->getEmail())) {
            $fields[] = "email = :email";
        }
        if(!empty($admin->getPassword()) && !empty($admin->getNewPassword())) {
            $fields[] = "password = :password";
        }

        return $fields;
    }
    
    public static function checkUser(Admin $admin) {
        $database = new database();
        try {
            $query = "SELECT ID,f_name,l_name,email FROM super_admin WHERE email = :email";
            $database->query($query);
            $database->bind(':email', $admin->getEmail());
            
            $result = $database->single();
                
            if($result) {
                return $result;
            } else {
                return false;
            }

            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getPassword($user_id){
        $database=new database();
        try{            
            $query = "SELECT password FROM super_admin WHERE ID = :id";

            $database->query($query);
            $database->bind(':id', $user_id);
            
            $result = $database->single();
            if($result){
                return $result;
            } else {
                return false;
            }           
        } catch (Exception $ex) {
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function resetPassword(Admin $admin){
        $database=new database();
        try {
            
            $query = "UPDATE super_admin SET password = :password WHERE ID = :id";
            $database->query($query);
            $database->bind(":id", $admin->getId());
            $database->bind(":password", $admin->getPasswordHash());
           
            if($database->execute()){
                return true;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public static function addResetPassword($userID,$email,$secret){
        $database = new database();       
        try {

            $today = date('Y-m-d H:i:s');
            
            $query = "INSERT INTO reset_password(user_id,email,secret,d_t) VALUES(:user_id,:email,:secret,:d_t)";
            $database->query($query);
            $database->bind(':user_id', $userID);
            $database->bind(':email', $email);
            $database->bind(':secret', $secret);
            $database->bind(':d_t', $today);
            
            $staffDetails = $database->execute();
            if($staffDetails){
                return $database->lastInsertId();
            } else {
                return false;
            }            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function getResetData($id){
        $database = new database();       
        try {            
            $query = "SELECT * FROM reset_password WHERE ID = :id";
            $database->query($query);
            $database->bind(':id', $id);
            
            $resetDetails = $database->single();
            if($resetDetails){
                return $resetDetails;
            } else {
                return false;
            }            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function checkNewlyAddedUserForNewlyRegisteredCompany($dbName){
        $database = new database($dbName);       
        try {            
            $query = "SELECT u.*,un.* FROM user u INNER JOIN user_nic un";
            $database->query($query);
            
            $resetDetails = $database->resultset();
            if($resetDetails){
                return $resetDetails;
            } else {
                return false;
            }            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function updateCheckFirstUser(Admin $admin){
        $database=new database();
        try {

            $query = "UPDATE company SET user = :user WHERE username = :username";
            $database->query($query);
            
            $database->bind(":username", $admin->getUsername());
            $database->bind(":user", $admin->getCheckFirstUser());

            if($database->execute()){
                return true;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
    
    public static function debugEnableDisable(Admin $admin){
        $database = new database();
        try {
            
            $query="UPDATE company SET debug = :debug WHERE ID = :id";
            
            $database->query($query);
            
            $database->bind(':id', $admin->getId());
            $database->bind(':debug', $admin->getDebug());          

            $debug = $database->execute();
            if($debug){
                return true;
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
}

