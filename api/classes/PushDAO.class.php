<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class PushDAO{
    
    /**
     * 
     * @param Push $push object of Push class
     * @param type $userID String user id
     * 
     * @return type Description
     */
    public static function SendMultiplePush(Push $push,$userID){
        
        $multiplePush = $push->getPush();

        $devicetoken = MobileDAO::getDevice($userID);
        
        $firebase = new Firebase();
        return $firebase->send($devicetoken, $multiplePush);
    }
    
}
