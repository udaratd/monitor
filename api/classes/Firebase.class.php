<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Firebase {
    
    function __construct() {
        
    }

    /**
     * Will call sendPushNotification() function and return detailed result on Success FALSE on Failure
     * 
     * @param type $registrationIDs String device token 
     * @param type $message array of message (title,message,images,hidden data)
     * @return type
     */
    public function send($registrationIDs, $message){
        $fields = array(
            'registration_ids' => $registrationIDs,
            'data' => $message,
            'time_to_live' =>  2419200,
        );
        return $this->sendPushNotification($fields);
    }
    
    /**
     * Send notification and return detailed result on Success FALSE on failure
     * 
     * @param type $fields array of fields (fields contain registration id and message)
     * @return type detailed result on Success FALSE on Failure
     */
    private function sendPushNotification($fields){
        
        $URL = 'https://fcm.googleapis.com/fcm/send';
        
        $headers = array(
            'Authorization: key='.FIREBASE_API_KEY,
            'Content-Type: application/json'
        );
        
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, $URL);
        
        curl_setopt($ch, CURLOPT_POST, true);
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        
        $result = curl_exec($ch);
        if($result === false){
            die('Curl failed: '.curl_errno($ch));
        }
        
        curl_close($ch);
        
        return $result;
    }
}
