<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Push{
    private $title;
    private $message;
    private $image;
    private $data = array();
//    private 
            
    function __construct($title, $message, $image) {
        $this->title = $title;
        $this->message = $message;
        $this->image = $image;
    }
    
    function getData() {
        return $this->data;
    }

    function setData($data) {
        $this->data = $data;
    }

    public function getPush(){
        $notification = array();
        $notification['data']['title'] = $this->title;
        $notification['data']['message'] = $this->message;
        $notification['data']['image'] = $this->image;
        $notification['data']['data'] = $this->data;
        
        return $notification;
    }
}
