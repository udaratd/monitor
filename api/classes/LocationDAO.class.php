<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class LocationDAO {
    
    /**
     * This function Insert new Location to System
     * 
     * @param Location $location Location location
     * @return boolean return True on Success False on Fail
     */
    public static function addLocation(Location $location) {
        $database = new database();
        try {
                        
            $database->beginTransaction();
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            $imagePath = null;
            $query1 = "INSERT INTO location(name, description, image, parentid) VALUES(:name, :description, :image, :parentid)";
            
            $database->query($query1);
            
            $database->bind(':name', $location->getName());
            $database->bind(':description', $location->getDescription());
            $database->bind(':parentid', $location->getParentID());
            $database->bind(':image', $imagePath);
            
            $result1 = $database->execute();
            $result2 = null;
            
            $ID = $database->lastInsertId();

            if($location->hasImage()){
                
                $image = $location->getImage();            
                $fileName = $ID . '_'.mt_rand(1, 5000);
                $extension = '.jpg';
                
                $path = UPLOAD_LOCATION_DIR;
                $imagePath = FileManager::imageUpload($path, $fileName.$extension, $image);

                if(is_string($imagePath)) {
                
                    $query2 = "UPDATE location SET image = :image WHERE ID = :id";
                    $database->query($query2);
                    $database->bind(':id', $ID);
                    $database->bind(':image', $imagePath);
                    
                    $result2 = $database->execute();
                    
                    FileManager::imageUpload($path, $fileName.$size_1.$extension, ImageManager::reSize(150, $image));
                    FileManager::imageUpload($path, $fileName.$size_2.$extension, ImageManager::reSize(500, $image));
                    FileManager::imageUpload($path, $fileName.$size_3.$extension, ImageManager::reSize(1024, $image));
                } else {
                    throw new Exception("Can not Upload Image to server");
                }
            }
            
            if($result1 === true && ($result2 === null || $result2 === true) ){
                $database->endTransaction();
                return $ID;
            } else {
                if($result2 === false){
                    if(FileManager::deleteFile($imagePath) === false) {
                        throw new Exception("Can not delete ".$imagePath);//can not throw exception here, need to execute rest
                    } else {
                        $extension_pos = strrpos($imagePath, '.');
                        
                        $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                        $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);
                        $large = substr($imagePath,0, $extension_pos).$size_3.substr($imagePath, $extension_pos);

                        if(FileManager::fileExist($thumb)){
                            FileManager::deleteFile($thumb);
                        }
                        if(FileManager::fileExist($medium)){
                            FileManager::deleteFile($medium);
                        }
                        if(FileManager::fileExist($large)){
                            FileManager::deleteFile($large);
                        }
                    }
                }
                throw new Exception("Data not Inserted to System");//include this line above line 61
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            $database->cancelTransaction();
            return false;
        }
    }
    
    /**
     * This function update a specific location for given ID
     * 
     * @param Location $location Location location
     * @return boolean return True On success False on Fail
     */
    public static function updateLocation (Location $location) {
        $oldImagePath = LocationDAO::getImage($location->getId());
        $database = new database();
        try {
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            $imagePath = $oldImagePath;
            $success = null;
            
            if($location->hasImage()) {
                
                $image = $location->getImage();
                $fileName = $location->getId() . '_' . mt_rand(5000, 10000);
                $extension = '.jpg';
                
                $path = UPLOAD_LOCATION_DIR;
                $imagePath = FileManager::imageUpload($path, $fileName.$extension, $image);
                
                if(is_string($imagePath) && ($imagePath !== 1) && ($imagePath !== false) ) {//check valid image
                    
                    FileManager::imageUpload($path, $fileName.$size_1.$extension, ImageManager::reSize(150, $image));
                    FileManager::imageUpload($path, $fileName.$size_2.$extension, ImageManager::reSize(500, $image));
                    FileManager::imageUpload($path, $fileName.$size_3.$extension, ImageManager::reSize(1024, $image));
                    
                    $success = true;
                    
                    if(!empty($oldImagePath) && $oldImagePath !== false){
                        
                        if(FileManager::fileExist($oldImagePath) === true){
                            if(FileManager::deleteFile($oldImagePath) === true) {                        
                                $extension_pos = strrpos($oldImagePath, '.');

                                $thumb = substr($oldImagePath,0, $extension_pos).$size_1.substr($oldImagePath, $extension_pos);
                                $medium = substr($oldImagePath,0, $extension_pos).$size_2.substr($oldImagePath, $extension_pos);
                                $large = substr($oldImagePath,0, $extension_pos).$size_3.substr($oldImagePath, $extension_pos);

                                if(FileManager::fileExist($thumb)){
                                    if(FileManager::deleteFile($thumb)===false){
                                        System::log(new Log('can not delete '.$thumb, LOG_ERROR));
                                    }
                                }
                                if(FileManager::fileExist($medium)){
                                    if(FileManager::deleteFile($medium)===false){
                                        System::log(new Log('can not delete '.$medium, LOG_ERROR));
                                    }
                                }
                                if(FileManager::fileExist($large)){
                                    if(FileManager::deleteFile($large)===false){
                                        System::log(new Log('can not delete '.$large, LOG_ERROR));
                                    }
                                }
                            } else {                                
                                System::log(new Log('can not delete '.$oldImagePath, LOG_ERROR));
                            }
                        }
                    }
                } else {
                    $success = false;
                }                    
            }
            
            $database->beginTransaction();

            $query = "UPDATE location SET ".implode(", ", $location->getQ())." WHERE ID = :id";
            $database->query($query);
            $database->bind(':id', $location->getId());
            
            if($location->getName() !== ""){
                $database->bind(':name', $location->getName());
            }
            if($location->getDescription() !== ""){
                $database->bind(':description', $location->getDescription());
            }
            if($location->getParentID() !== ""){
                $database->bind(':pid', $location->getParentID());
            }
            if($location->getImage() !== ""){
                $database->bind(':image', $imagePath);
            }

            $result = $database->execute();

            if($result && ($success === null || $success === true)) {
                $database->endTransaction();
                return true;
            } else {
                throw new Exception("Something went wrong, Can not Update Data");
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            $database->cancelTransaction();
            return false;
        }
    }
    
    /**
     * This function delete location for given ID
     * 
     * @param type $ID Integer ID
     * @return boolean return True on Success False on Fail
     */
    public static function deleteLocation($ID) {
        $imagePath = LocationDAO::getImage($ID);
        $childImagePaths = LocationDAO::getChildImage($ID);
        
        $database = new database();
        
        $database->beginTransaction();
        
        try {
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            $query1 = "DELETE FROM location WHERE ID = :id";
            $database->query($query1);
            $database->bind(':id', $ID);

            $result1 = $database->execute();
            $result2 = null;
            
            if(is_array($childImagePaths) && $childImagePaths !== false) {//delete child

                $query2 = "DELETE FROM location WHERE parentid = :pid";
                $database->query($query2);
                $database->bind(':pid', $ID);

                $result2 = $database->execute();

                foreach ($childImagePaths as $row){

                    $childImage = $row['image'];
                    if(!empty($childImage)){
                        if(FileManager::fileExist($childImage)){
                            if(FileManager::deleteFile($childImage) === true) {                               
                                
                                $extension_pos = strrpos($childImage, '.');

                                $thumb = substr($childImage,0, $extension_pos).$size_1.substr($childImage, $extension_pos);
                                $medium = substr($childImage,0, $extension_pos).$size_2.substr($childImage, $extension_pos);
                                $large = substr($childImage,0, $extension_pos).$size_3.substr($childImage, $extension_pos);

                                if(FileManager::fileExist($thumb)){
                                    if(FileManager::deleteFile($thumb)===false){
                                        System::log(new Log("can not delete ".$thumb, LOG_ERROR));
                                    }
                                }
                                if(FileManager::fileExist($medium)){
                                    if(FileManager::deleteFile($medium)===false){
                                        System::log(new Log("can not delete ".$medium, LOG_ERROR));
                                    }
                                }
                                if(FileManager::fileExist($large)){
                                    if(FileManager::deleteFile($large)==false){
                                        System::log(new Log("can not delete ".$large, LOG_ERROR));
                                    }
                                }
                                
                            } else {
                                System::log(new Log("can not delete ".$childImage, LOG_ERROR));
                            }
                        }
                    }
                }
            }//end delete child
            
            if( ($result1===true) && (($result2===null) || ($result2 === true)) ) {
                $database->endTransaction();
                if(!empty($imagePath) && $imagePath!==false){
                    
                    if(FileManager::fileExist($imagePath)===true){
                        
                        if(FileManager::deleteFile($imagePath) === true) {                                                   
                            
                            $extension_pos = strrpos($imagePath, '.');

                            $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                            $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);
                            $large = substr($imagePath,0, $extension_pos).$size_3.substr($imagePath, $extension_pos);

                            if(FileManager::fileExist($thumb)){
                                if(FileManager::deleteFile($thumb)===false){
                                    System::log(new Log("can not delete ".$thumb, LOG_ERROR));
                                }
                            }
                            if(FileManager::fileExist($medium)){
                                if(FileManager::deleteFile($medium)===false){
                                    System::log(new Log("can not delete ".$medium, LOG_ERROR));
                                }
                            }
                            if(FileManager::fileExist($large)){
                                if(FileManager::deleteFile($large)===false){
                                    System::log(new Log("can not delete ".$large, LOG_ERROR));
                                }
                            }
                        } else {
                            System::log(new Log("can not delete ".$imagePath, LOG_ERROR));
                        }
                    }
                }
                return true;
            } else {                
                throw new Exception("Queries Not Executed");
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            $database->cancelTransaction();
            return false;
        }
    }

    
    /**
     * This function return location for given ID
     * 
     * @param type $ID Integer ID
     * @return boolean return location Array on Success False on Fail
     */
    public static function getLocation($ID) {
        $database = new database();
        try {
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            $query = "SELECT "
                        . "l.ID, "
                        . "l.name, "
                        . "l.description, "
                        . "l.image as img_original, "
                        . "l.parentid, "
                        . "IF(l.parentid != 0, (SELECT p.name FROM location p WHERE p.ID = l.parentid),'null') as parentName "
                    . "FROM "
                        . "location l "
                    . "WHERE l.ID = :id";

            $database->query($query);
            $database->bind(':id', $ID);
     
            $location = $database->single();
            
            if($location) {

                $imagePath = $location['img_original'];               
                if($imagePath !== null){
                    $extension_pos = strrpos($imagePath, '.');                       

                    $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                    $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);
                    $large = substr($imagePath,0, $extension_pos).$size_3.substr($imagePath, $extension_pos);

                    $location['img_thumbnail'] = $thumb;
                    $location['img_medium'] = $medium;
                    $location['img_large'] = $large;
                } else {
                    $location['img_thumbnail'] = null;
                    $location['img_medium'] = null;
                    $location['img_large'] = null;
                }
                
                return $location;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }
    
    /**
     * This function search location return location Array
     * 
     * @param type $name String Location name
     * @return boolean return location Array on Success False on Fail
     */
    public static function searchLocation($name) {
        $database = new database();
        try {
            $query = "SELECT * FROM location WHERE name LIKE :name";
            $database->query($query);
            $database->bind(':name', '%'.$name.'%');
     
            if($database->execute()) {
                $result = $database->resultset();
                return $result;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }

    /**
     * This function return image path of given id
     * 
     * @param type $id Integer id
     * @return boolean Return image path on Success False on Fail
     */
    public static function getImage($id) {
        $database = new database();
        try {
            $query = "SELECT image FROM location WHERE ID = :id";
            $database->query($query);
            $database->bind(':id', $id);
            $result = $database->single();

            $path = $result['image'];
            if($result) {
                return $path;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }
    
    /**
     * return child images for given parent location ID
     * 
     * @param type $pid String parent ID
     * @return boolean return array of images on success false on failure
     */
    public static function getChildImage($pid) {
        $database = new database();
        try {
            $query = "SELECT image FROM location WHERE parentid = :pid";
            $database->query($query);
            $database->bind(':pid', $pid);
            $result = $database->resultset();
            
            if($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }
    
    
    public static function getChild($pid) {
        $database = new database();
        try {
            $query = "SELECT ID,name FROM location WHERE parentid = :pid";
            $database->query($query);
            $database->bind(':pid', $pid);
            $result = $database->resultset();
            
            if($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }
    
    /**
     * This function check if location exist or not
     * 
     * @param type $ID Integer ID
     * @return boolean return True on Success False on Fail
     */
    public static function locationExists($ID) {
        $database = new database();
        try {
            $query = "SELECT * FROM location WHERE ID = :id";
            $database->query($query);
            $database->bind(':id', $ID);
            
            if($database->execute()) {
                return $database->single();
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }
    
    /**
     * This function return parent and child both
     * 
     * @return boolean return parent child Array on Success False on Fail
     */
    public static function getParentChild() {
        $database = new database();
        $database->beginTransaction();
        try {
            
            $query1 = "SELECT ID,name,description,image FROM location WHERE parentid = :pid";           
            $database->query($query1);
            $database->bind(':pid', 0);
            
            $data = array();
            
            if($database->execute()) {
                $result1 = $database->resultset();
                foreach ($result1 as $row1) {
                    $query2 = "SELECT ID,name,description,image FROM location WHERE parentid = :id";
                    $database->query($query2);
                    $database->bind(':id', $row1['ID']);
                    
                    if($database->execute()) {
                        $result2 = $database->resultset();
                        
                        if(count($result2)>0){
                            $row1['childs'] = $result2;
                        }
                        array_push($data, $row1);
                    }
                }
                $database->endTransaction();
                return $data;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }
    
    /**
     * return locations array check location already in the system
     * @param type $name String location name
     * @param type $parentID String location ID
     * @return boolean return location array on success False on failure
     */
    public static function checkLocation($name,$parentID) {
        $database = new database();
        try {
            $query = "SELECT * FROM location WHERE name = :name AND parentid = :pid";
            $database->query($query);
            $database->bind(':name', $name);
            $database->bind(':pid', $parentID);
            
            if($database->execute()) {
                $result = $database->single();
                return $result;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }
    
    /**
     * return locations array
     * @param type $parentID String location ID
     * @return boolean return location array on success False on failure
     */
    public static function checkParentLocation($parentID) {
        $database = new database();
        try {
            $query = "SELECT * FROM location WHERE ID = :id AND parentid = :pid";
            $database->query($query);
            $database->bind(':id', $parentID);
            $database->bind(':pid', 0);
            
            $result = $database->single();
            if($result) {               
                return $result;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }

    /**
     * This function return child ID according to parent
     * 
     * @param type $location String location
     * @return boolean return location ID on Success False on Fail
     */
    public static function getChildID($location) {
        $database = new database();
        try {
            list($parent, $child) = split('[>]', $location);

            $parent = substr($parent, 0, strlen($parent)-1);
            $child = substr($child, 1, strlen($child)-1);

            $query = "SELECT b.ID "
                    . "FROM location a "
                    . "INNER JOIN location b ON "
                    . "a.name = :parent AND "
                    . "b.name = :child AND "
                    . "a.ID = b.parentid";
            $database->query($query);
            $database->bind(':parent', $parent);
            $database->bind(':child', $child);
            
            if($database->execute()) {
                $result = $database->single();
                return $result['ID'];
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }
    
    public static function exportLocation(){
        $database = new database();
        try {
            date_default_timezone_set('Asia/Colombo');
            $today = date('h:i:s a Y-m-d');
            $filename = "location_export_".$today.".csv";
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename='.$filename);
            $output = fopen("php://output", "w");
            fputcsv($output, array('Location ID','Location Name','Location description','Location Parent ID'));
            
            $query = "SELECT
                        ID,
                        name,
                        description,
                        parentid                        
                      FROM 
                        location";
            
            $database->query($query);

            $result = $database->resultset();
            
            foreach ($result as $one){
                fputcsv($output, $one);
            }
            fclose($output);   
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getDefaultData(Location $location) {
        $database = new database();
        try {
            $query = "SELECT * FROM location WHERE name = :name";
            $database->query($query);
            $database->bind(':name', $location->getName());
            
            $result = $database->single();
            if($result) {               
                return $result;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }
}
