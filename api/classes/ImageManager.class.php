<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ImageManager{
    
    private $imageString;
    
    function __construct() {
        
    }

    function getImageString() {
        return $this->imageString;
    }

    function setImageString($imageString) {
        $this->imageString = $imageString;
    }

    public static function checkImageSize($imageString){
        
        $data = base64_decode($imageString);
        
        $im = imagecreatefromstring($data);
        
        $width = false;
        if($im === false){
            $width = false;
        } else {
            $width = imagesx($im);
        }
        
        
        return $width;
    }
    
    public static function reSize($width,$imageString){
        
        $data = base64_decode($imageString);
        
        $image = imagecreatefromstring($data);
        $image = imagescale($image, $width);
        
        ob_start();
        imagejpeg($image);
        $contents = ob_get_contents();
        ob_end_clean();
        
        $new_data = base64_encode($contents);
        
        imagedestroy($image);
        
        return $new_data;
    }
}