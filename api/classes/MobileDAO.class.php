<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MobileDAO
 *
 * @author user
 */
class MobileDAO {
    
    
    public static function checkCompleteWork($user_id,$s_date,$e_date){
        
        $todaySchedulesForOneStaffmember = ScheduleDAO::listScheduleForUser($user_id, $s_date, $e_date);
               
        $value = false;
        $complete = array();
        
        $i=0;
        if($todaySchedulesForOneStaffmember){
            foreach ($todaySchedulesForOneStaffmember as $schedules){

                $complete = array();
                foreach ($schedules['schedules'] as $schedule){
                    $key = array_key_exists("isSubmit", $schedule);
                    if($key){
                        $value = true;
                        $complete[] = $value;

                    } else {
                        $value = false;
                        $complete[] = $value;

                    }
                }

               if(in_array(false, $complete,TRUE)){
                    $todaySchedulesForOneStaffmember[$i]['complete'] = false;     
                } else {
                    $todaySchedulesForOneStaffmember[$i]['complete'] = true;
                } 
                $i++;
            }
        }
        return $todaySchedulesForOneStaffmember;
        
    }
    
    public static function addDevice($userID,$token){
        
        $database=new database($_SESSION['DB_NAME']);
        $database->beginTransaction();
        try{
            
            
            $query = "INSERT INTO device(user_id,token) VALUES(:user_id, :token)";

            $database->query($query);
            $database->bind(':user_id', $userID);
            $database->bind(':token', $token);
            
            if($database->execute()){
                $database->endTransaction();
                return true;
            } else {
                return false;
            }
            
        } catch (Exception $ex){
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            $database->cancelTransaction();
            return false;
        }
    }
    
    public static function getDevice($userID){
        
        $database=new database();
        try{
                       
            $query = "SELECT token FROM device WHERE user_id = :userID";

            $database->query($query);
            $database->bind(':userID', $userID);
            
            //do not change code below this line
            $result = $database->resultset();
            $tokens = array();
            if($result){
                foreach ($result as $token){
                    array_push($tokens, $token['token']);
                }
                return $tokens;               
            } else {
                return false;
            }
            
        } catch (Exception $ex){
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function checkDevice($token){
        
        $database=new database($_SESSION['DB_NAME']);
        try{
                       
            $query = "SELECT * FROM device WHERE token = :token";

            $database->query($query);
            $database->bind(':token', $token);
            
            $result = $database->resultset();
            if($result){
                return $result;               
            } else {
                return false;
            }
            
        } catch (Exception $ex){
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function checkUser($userID){
        
        $database=new database($_SESSION['DB_NAME']);
        try{
                       
            $query = "SELECT ID,user_id,token FROM device WHERE user_id = :userid";

            $database->query($query);
            $database->bind(':userid', $userID);
            
            $result = $database->single();
            if($result){
                return $result;               
            } else {
                return false;
            }
            
        } catch (Exception $ex){
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function updateDevice($userID,$token){
        
        $database=new database();
        try{
                       
            $query = "UPDATE device SET token = :token WHERE user_id = :userID";

            $database->query($query);
            $database->bind(':userID', $userID);
            $database->bind(':token', $token);
            
            if($database->execute()){
                return true;
            } else {
                return false;
            }
            
        } catch (Exception $ex){
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function deleteDevice($ID){
        
        $database=new database($_SESSION['DB_NAME']);
        try{
                       
            $query = "DELETE FROM device WHERE ID = :deviceID";

            $database->query($query);
            $database->bind(':deviceID', $ID);
            
            if($database->execute()){
                return true;
            } else {
                return false;
            }
            
        } catch (Exception $ex){
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function deleteDeviceByToken($token){
        
        $database=new database($_SESSION['DB_NAME']);
        try{
                       
            $query = "DELETE FROM device WHERE token = :token";

            $database->query($query);
            $database->bind(':token', $token);
            
            if($database->execute()){
                return true;
            } else {
                return false;
            }
            
        } catch (Exception $ex){
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
    
    public static function addDeviceInfo($workID,$info){
        
        $database=new database($_SESSION['DB_NAME']);
        $database->beginTransaction();
        try{

            $query = "INSERT INTO device_info(work_id,info) VALUES(:work_id, :info)";

            $database->query($query);
            $database->bind(':work_id', $workID);
            $database->bind(':info', $info);
            
            if($database->execute()){
                $database->endTransaction();
                return true;
            } else {
                return false;
            }
            
        } catch (Exception $ex){
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            $database->cancelTransaction();
            return false;
        }
    }
    
    public static function getDeviceInfo($workID){       
        $database=new database('devmoni');
        try{
            $query = "SELECT * FROM device_info WHERE work_id = :work_id";

            $database->query($query);
            $database->bind(':work_id', $workID);
            
            $result = $database->single();
            if($result){
                return $result;
            } else {
                return false;
            }
            
        } catch (Exception $ex){
            System::log(new Log($ex->getMessage(), LOG_CRITICAL));
            return false;
        }
    }
}
