<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ReportDAO {
    
    public static function getCompletedTask(Report $report) {
        $database = new database();
        try {
            
            $s_date = $report->getS_date();
            $e_date = $report->getE_date();
            $staff_id = $report->getStaffID();
            $supervisor_id = $report->getSupervisorID();
            $checkpoint_id = $report->getCheckpointID();
            
            
            
            //staff set
            $staffSet = !empty($staff_id);
            
            //checkpoint set
            $checkpointSet = !empty($checkpoint_id);
            
            //supervisor set
            $supervisorSet = !empty($supervisor_id);
            
            $query="SELECT 
                        COUNT(w.id) as completedTaskCount,
                        COUNT(DISTINCT w.user_id) as staffCount
                    FROM
                        work_list w";
            
                    if($staffSet && $supervisorSet){    
                        $query = $query." INNER JOIN user u ON
                                            w.user_id = u.ID
                                          INNER JOIN staff_supervisor s_user ON
                                            u.ID = s_user.user_id";
                    }
                        
            $query = $query." WHERE (w.server_date BETWEEN :s_date AND :e_date)";
            
            if($staffSet && $supervisorSet) {
                $query = $query." AND w.user_id = :user_id AND s_user.supervisor_id = :spv_id";
            }
            
            if($checkpointSet) {
                $query = $query." AND w.checkpoint_id = :checkpoint_id";
            }

            $database->query($query);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);          
            
            if($staffSet) {
                $database->bind(':user_id', $staff_id);
            }
            
            if($checkpointSet) {
                $database->bind(':checkpoint_id', $checkpoint_id);
            }
            
            if($supervisorSet) {
                $database->bind(':spv_id', $supervisor_id);
            }
            
            $completedTaskCount = $database->single();
            if($completedTaskCount) {                
                return $completedTaskCount;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getScheduleCount(Report $report) {
        $database = new database();
        try {
            
            $s_date = $report->getS_date();
            $e_date = $report->getE_date();
            $staff_id = $report->getStaffID();
            $supervisor_id = $report->getSupervisorID();
            $checkpoint_id = $report->getCheckpointID();
            
            
            
            //staff set
            $staffSet = !empty($staff_id);
            
            //checkpoint set
            $checkpointSet = !empty($checkpoint_id);
            
            //supervisor set
            $supervisorSet = !empty($supervisor_id);
            
            $query="SELECT 
                        COUNT(w.id) as completedTaskCount,
                        COUNT(DISTINCT w.user_id) as staffCount
                    FROM
                        work_list w";
                    if($staffSet && $supervisorSet){    
                    $query = $query." INNER JOIN user u ON
                            w.user_id = u.ID
                        INNER JOIN staff_supervisor s_user ON
                            u.ID = s_user.user_id";
                    }
                        
            $query = $query." WHERE (w.server_date BETWEEN :s_date AND :e_date)";
            
            if($staffSet) {
                $query = $query." AND w.user_id = :user_id";
            }
            
            if($checkpointSet) {
                $query = $query." AND w.checkpoint_id = :checkpoint_id";
            }
            
            if($staffSet) {
                $query = $query." AND s_user.supervisor_id = :spv_id";
            }

            $database->query($query);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);          
            
            if($staffSet) {
                $database->bind(':user_id', $staff_id);
            }
            
            if($checkpointSet) {
                $database->bind(':checkpoint_id', $checkpoint_id);
            }
            
            if($supervisorSet) {
                $database->bind(':spv_id', $supervisor_id);
            }
            
            $completedTaskCount = $database->single();
            if($completedTaskCount) {                
                return $completedTaskCount;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getOnTimeTask(Report $report) {
        $database = new database();
        
        $completedTasks = ReportDAO::getCompletedTask($report);
        try {
            
            $s_date = $report->getS_date();
            $e_date = $report->getE_date();
            $staff_id = $report->getStaffID();
            $supervisor_id = $report->getSupervisorID();
            $checkpoint_id = $report->getCheckpointID();
            
            //staff set
            $staffSet = !empty($staff_id);
            
            //checkpoint set
            $checkpointSet = !empty($checkpoint_id);
            
            //supervisor set
            $supervisorSet = !empty($supervisor_id);
            
            $query="SELECT 
                        COUNT(w.id) as onTimeTasks
                    FROM
                        work_list w";
                    
                    if($staffSet && $supervisorSet){    
                        $query = $query." INNER JOIN user u ON
                                            w.user_id = u.ID
                                          INNER JOIN staff_supervisor s_user ON
                                            u.ID = s_user.user_id";
                    }
                    
                        
            $query = $query." INNER JOIN taskandschedule t ON
                                w.schedule_id = t.ID
                              WHERE (w.server_date BETWEEN :s_date AND :e_date)  AND DATE_ADD(t.endTime, INTERVAL 59 second) > w.server_time";
            
            if($staffSet) {
                $query = $query." AND w.user_id = :user_id";
            }
            
            if($checkpointSet) {
                $query = $query." AND w.checkpoint_id = :checkpoint_id";
            }
            
            if($supervisorSet){
                $query = $query." AND s_user.supervisor_id = :spv_id";
            }
//            echo $query;
            $database->query($query);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
                       
            if($staffSet) {
                $database->bind(':user_id', $staff_id);
            }
            
            if($checkpointSet) {
                $database->bind(':checkpoint_id', $checkpoint_id);
            }
            
            if($supervisorSet){
                $database->bind(':spv_id', $supervisor_id);
            }
            
            $ontimeTasks = $database->single();
            if($ontimeTasks) {
                $totalTaskCount = $completedTasks['completedTaskCount'];
                $ontimeTasks['completedTaskCount'] = $totalTaskCount;

                return $ontimeTasks;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getReworkRate(Report $report) {
        $database = new database();
        
        $completedTasks = ReportDAO::getCompletedTask($report);
        try {
            
            $s_date = $report->getS_date();
            $e_date = $report->getE_date();
            $staff_id = $report->getStaffID();
            $supervisor_id = $report->getSupervisorID();
            $checkpoint_id = $report->getCheckpointID();
            
            //staff set
            $staffSet = !empty($staff_id);
            
            //checkpoint set
            $checkpointSet = !empty($checkpoint_id);
            
            //supervisor set
            $supervisorSet = !empty($supervisor_id);
            
            $query="SELECT 
                        COUNT(w.id) as reworkTask
                    FROM
                        work_list w";
            
            if($staffSet && $supervisorSet){    
                $query = $query." INNER JOIN user u ON
                                    w.user_id = u.ID
                                INNER JOIN staff_supervisor s_user ON
                                    u.ID = s_user.user_id";
            }
                    
                            
            $query = $query." WHERE (w.server_date BETWEEN :s_date AND :e_date)  AND w.work_status = :status";
            
            if($staffSet) {
                $query = $query." AND w.user_id = :user_id";
            }
            
            if($checkpointSet) {
                $query = $query." AND w.checkpoint_id = :checkpoint_id";
            }
            
            if($supervisorSet){
                $query = $query." AND s_user.supervisor_id = :spv_id";
            }
            
            $database->query($query);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);           
            $database->bind(':status', "Rework");
            
            if($staffSet) {
                $database->bind(':user_id', $staff_id);
            }
            
            if($checkpointSet) {
                $database->bind(':checkpoint_id', $checkpoint_id);
            }
            
            if($supervisorSet){
                $database->bind(':spv_id', $supervisor_id);
            }
            
            $reworkTasks = $database->single();
            if($reworkTasks) {
                $totalTaskCount = $completedTasks['completedTaskCount'];
                $reworkTasks['completedTaskCount'] = $totalTaskCount;
                
                return $reworkTasks;
            } else {
                echo false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getFlaggedRate(Report $report) {
        $database = new database();
        
        $completedTasks = ReportDAO::getCompletedTask($report);
        try {
            
            $s_date = $report->getS_date();
            $e_date = $report->getE_date();
            $staff_id = $report->getStaffID();
            $supervisor_id = $report->getSupervisorID();
            $checkpoint_id = $report->getCheckpointID();
            
            //staff set
            $staffSet = !empty($staff_id);
            
            //checkpoint set
            $checkpointSet = !empty($checkpoint_id);
            
            //supervisor set
            $supervisorSet = !empty($supervisor_id);
            
            $query="SELECT 
                        COUNT(w.id) as flaggedTask
                    FROM
                        work_list w";
            
            if($staffSet && $supervisorSet){    
                $query = $query." INNER JOIN user u ON
                                    w.user_id = u.ID
                                INNER JOIN staff_supervisor s_user ON
                                    u.ID = s_user.user_id";
            }
                    
                            
            $query = $query." WHERE (w.server_date BETWEEN :s_date AND :e_date) AND w.work_status = :status";
            
            if($staffSet) {
                $query = $query." AND w.user_id = :user_id";
            }
            
            if($checkpointSet) {
                $query = $query." AND w.checkpoint_id = :checkpoint_id";
            }
            
            if($supervisorSet){
                $query = $query." AND s_user.supervisor_id = :spv_id";
            }
            
            $database->query($query);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);           
            $database->bind(':status', "Flagged");
            
            if($staffSet) {
                $database->bind(':user_id', $staff_id);
            }
            
            if($checkpointSet) {
                $database->bind(':checkpoint_id', $checkpoint_id);
            }
            
            if($supervisorSet){
                $database->bind(':spv_id', $supervisor_id);
            }
            
            $flaggedTasks = $database->single();
            if($flaggedTasks) {
                
                $totalTaskCount = $completedTasks['completedTaskCount'];
                $flaggedTasks['completedTaskCount'] = $totalTaskCount;
                
                return $flaggedTasks;
            } else {
                echo false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getReworkStaff(Report $report) {
        $database = new database();
        
        try {
            
            $size_1 = '_thumbnail';
            
            $s_date = $report->getS_date();
            $e_date = $report->getE_date();
            $staff_id = $report->getStaffID();
            $supervisor_id = $report->getSupervisorID();
            $checkpoint_id = $report->getCheckpointID();
            

            $query_1 = "SELECT 
                            w.user_id as staffID                       
                        FROM
                            work_list w                    
                        WHERE (w.server_date BETWEEN :s_date AND :e_date) AND w.work_status = :status";
            
            
            $database->query($query_1);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);           
            $database->bind(':status', "Rework");
            
            $reworkStaff = $database->resultset();
            $userID = array();

            if($reworkStaff) {
                
                foreach ($reworkStaff as $user){
                    $userID[] = $user['staffID']; 
                }

                $countSameStaffID = array_count_values($userID);//count the same staff id
                
                arsort($countSameStaffID);//sort that counted value by Descending Order According to Value
                
                $output = array_slice($countSameStaffID, 0, 10, true);
                

                $data = array();
                foreach ($output as $key=>$value){
                    $user_id = $key;
                    
                    $query_2="SELECT
                                u.ID as staffID,
                                CONCAT(u.firstName,' ',u.lastName) as fullName,
                                um.meta_value as img_profile
                            FROM
                                user u 
                            INNER JOIN user_meta um ON
                                um.user_id = u.ID
                            WHERE u.ID = :user_id AND um.meta_key = :path";
                    
                    $database->query($query_2);
                    $database->bind(':user_id', $user_id);
                    $database->bind(':path', "imagePath");
                    
                    $data[] = $database->single();
                }
                
                $i=0;
                foreach ($data as $d){
                    
                    $imagePath = $d['img_profile'];
                    $u_id = $d['staffID'];
                    
                    $extension_pos = strrpos($imagePath, '.');
                    $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                    
                    $data[$i]['img_profile'] = $thumb;
                    $data[$i]['reworkCheckpointCount'] = $output[$u_id];
                    
                    $i++;
                }
                return $data;
            } else {
                echo false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getReworkCheckpoint(Report $report) {
        $database = new database();
        
        try {
            
            $s_date = $report->getS_date();
            $e_date = $report->getE_date();
            $staff_id = $report->getStaffID();
            $supervisor_id = $report->getSupervisorID();
            $checkpoint_id = $report->getCheckpointID();
            
            
            $query_1 = "SELECT 
                            w.checkpoint_id as checkpointID                      
                        FROM
                            work_list w                    
                        WHERE (w.server_date BETWEEN :s_date AND :e_date) AND w.work_status = :status";
            
            
            $database->query($query_1);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
            $database->bind(':status', "Rework");
            
            $reworkCheckpoints = $database->resultset();
            $checkpointIDs = array();

            if($reworkCheckpoints) {
                
                foreach ($reworkCheckpoints as $checkpoint){
                    $checkpointIDs[] = $checkpoint['checkpointID']; 
                }

                $countSameCheckpointID = array_count_values($checkpointIDs);//count the same checkpoint id
                
                arsort($countSameCheckpointID);//sort that counted value by Descending Order According to Value
                
                $output = array_slice($countSameCheckpointID, 0, 10, true);
                

                $data = array();
                foreach ($output as $key=>$value){
                    $checkpoint_id = $key;
                    
                    $query_2="SELECT
                                c.ID as checkpointID,
                                c.name as checkpointName
                            FROM
                                checkpoint c                             
                            WHERE c.ID = :checkpoint_id";
                    
                    $database->query($query_2);
                    $database->bind(':checkpoint_id', $checkpoint_id);
                    
                    $data[] = $database->single();
                }
                
                $i=0;
                foreach ($data as $d){
                    
                    $c_id = $d['checkpointID'];

                    $data[$i]['reworkCheckpointCount'] = $output[$c_id];
                    
                    $i++;
                }
                return $data;
            } else {
                echo false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getLateSubmitedCheckpoint(Report $report) {
        $database = new database();
        
        try {
            
            $s_date = $report->getS_date();
            $e_date = $report->getE_date();
            $staff_id = $report->getStaffID();
            $supervisor_id = $report->getSupervisorID();
            $checkpoint_id = $report->getCheckpointID();
            
//            //staff set
//            $staffSet = $staff_id !== "All Staff";
//            
//            //checkpoint set
//            $checkpointSet = $checkpoint_id !== "All Checkpoints";
            
            $query="SELECT 
                        CONCAT(l_p.name,' > ',l_c.name,' > ',c.name) as checkpointName,
                        w.server_time as uploadServerTime,
                        t.endTime as scheduleEndTime,
                        SEC_TO_TIME(TIMESTAMPDIFF(SECOND,t.endTime,w.server_time)) as timeDiff
                    FROM
                        work_list w
                    INNER JOIN user u ON
                        w.user_id = u.ID
                    INNER JOIN checkpoint c ON
                        w.checkpoint_id = c.ID
                    INNER JOIN location l_c ON
                        c.locationid = l_c.ID
                    INNER JOIN location l_p ON
                        l_c.parentid = l_p.ID
                    INNER JOIN staff_supervisor s_user ON
                        u.ID = s_user.user_id
                    INNER JOIN taskandschedule t ON
                        w.schedule_id = t.ID
                    WHERE (w.server_date BETWEEN :s_date AND :e_date) AND DATE_ADD(t.endTime, INTERVAL 59 second) < w.server_time";
            
            
//            if($staffSet) {
//                $query = $query." AND w.user_id = :user_id";
//            }
//            
//            if($checkpointSet) {
//                $query = $query." AND w.checkpoint_id = :checkpoint_id";
//            }
            
            $query = $query." ORDER BY timeDiff DESC";
            
            $database->query($query);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
            
//            $database->bind(':spv_id', $supervisor_id);
//            AND s_user.supervisor_id = :spv_id
//            if($staffSet) {
//                $database->bind(':user_id', $staff_id);
//            }
//            
//            if($checkpointSet) {
//                $database->bind(':checkpoint_id', $checkpoint_id);
//            }
            
            $lateSubmitedCheckpoints = $database->resultset();
            if($lateSubmitedCheckpoints) {
                
                return $lateSubmitedCheckpoints;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function get_Checkpoint_With_MoreThan_OneRework(Report $report) {
        $database = new database();
        
        try {
            
            $s_date = $report->getS_date();
            $e_date = $report->getE_date();
            $staff_id = $report->getStaffID();
            $supervisor_id = $report->getSupervisorID();
            $checkpoint_id = $report->getCheckpointID();

            
            $query_1 = "SELECT 
                            w.checkpoint_id as checkpointID                      
                        FROM
                            work_list w                    
                        WHERE (w.server_date BETWEEN :s_date AND :e_date) AND w.work_status = :status";
            
            
            $database->query($query_1);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
            $database->bind(':status', "Rework");
            
            $moreThanOneRework = $database->resultset();
            $checkpointIDs = array();
            if($moreThanOneRework) {
                
                foreach ($moreThanOneRework as $checkpoint){
                    $checkpointIDs[] = $checkpoint['checkpointID']; 
                }

                $countSameCheckpointID = array_count_values($checkpointIDs);//count the same checkpoint id
                
                arsort($countSameCheckpointID);//sort that counted value by Descending Order According to Value
                
                
                $keys = array_keys($countSameCheckpointID, 1);//get checkpoint id with only one rework
                
                foreach ($keys as $k=>$v){
                    unset($countSameCheckpointID[$v]);//remove rework with one rework
                }

                $data = array();
                
                foreach ($countSameCheckpointID as $key=>$value){
                    $checkpoint_id = $key;

                    $query_2="SELECT
                                w.checkpoint_id as checkpointID,
                                CONCAT(l_p.name,' > ',l_c.name,' > ',c.name) as fullName
                            FROM
                                work_list w
                            INNER JOIN checkpoint c ON
                                w.checkpoint_id = c.ID
                            INNER JOIN location l_c ON
                                c.locationid = l_c.ID
                            INNER JOIN location l_p ON
                                l_c.parentid = l_p.ID
                            WHERE w.checkpoint_id = :checkpoint_id";
                    
                    $database->query($query_2);
                    $database->bind(':checkpoint_id', $checkpoint_id);
                    
                    $data[] = $database->single();
                }

                $i=0;
                foreach ($data as $d){
                    
                    $c_id = $d['checkpointID'];

                    $data[$i]['reworkCount'] = $countSameCheckpointID[$c_id];
                    
                    $i++;
                }
                return $data;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getCompletedCheckpoint($staff_id,$s_date,$e_date){
        $database = new database();
        try {
            
            $query = "SELECT
                        *                        
                      FROM
                        work_list 
                      WHERE (server_date BETWEEN :s_date AND :e_date) AND user_id = :user_id";
            
            $database->query($query);
            $database->bind(':user_id', $staff_id);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
            
            if($database->execute()) {
                return $database->resultset();
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getReworkedCheckpointCount($staff_id,$s_date,$e_date) {
        $database = new database();
        
        try {
            
//            $supervisor_id = $report->getSupervisorID();
//            $checkpoint_id = $report->getCheckpointID();
//            
//            //staff set
//            $staffSet = !empty($staff_id);
//            
//            //checkpoint set
//            $checkpointSet = !empty($checkpoint_id);
//            
//            //supervisor set
//            $supervisorSet = !empty($supervisor_id);
            
            $query="SELECT 
                        COUNT(w.id) as reworkTask
                    FROM
                        work_list w
                    WHERE (w.server_date BETWEEN :s_date AND :e_date)  AND w.work_status = :status AND w.user_id = :user_id";
            
//            if($staffSet && $supervisorSet){    
//                $query = $query." INNER JOIN user u ON
//                                    w.user_id = u.ID
//                                INNER JOIN staff_supervisor s_user ON
//                                    u.ID = s_user.user_id";
//            }
                    
                            
//            $query = $query." WHERE (w.server_date BETWEEN :s_date AND :e_date)  AND w.work_status = :status";
//            
//            if($staffSet) {
//                $query = $query." AND w.user_id = :user_id";
//            }
//            
//            if($checkpointSet) {
//                $query = $query." AND w.checkpoint_id = :checkpoint_id";
//            }
//            
//            if($supervisorSet){
//                $query = $query." AND s_user.supervisor_id = :spv_id";
//            }
            
            $database->query($query);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);           
            $database->bind(':status', "Rework");
            $database->bind(':user_id', $staff_id);
            
//            if($staffSet) {
//                $database->bind(':user_id', $staff_id);
//            }
//            
//            if($checkpointSet) {
//                $database->bind(':checkpoint_id', $checkpoint_id);
//            }
//            
//            if($supervisorSet){
//                $database->bind(':spv_id', $supervisor_id);
//            }
            
            $reworkTasks = $database->single();
            if($reworkTasks) {               
                return $reworkTasks;
            } else {
                echo false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }    

    public static function getOnTimeCompletionRate($staff_id,$s_date,$e_date) {
        $database = new database();
        
        try {
            
//            $supervisor_id = $report->getSupervisorID();
//            $checkpoint_id = $report->getCheckpointID();
//            
//            //staff set
//            $staffSet = !empty($staff_id);
//            
//            //checkpoint set
//            $checkpointSet = !empty($checkpoint_id);
//            
//            //supervisor set
//            $supervisorSet = !empty($supervisor_id);
            
            $query="SELECT 
                        COUNT(w.id) as onTimeTasks
                    FROM
                        work_list w";
                    
//                    if($staffSet && $supervisorSet){    
//                        $query = $query." INNER JOIN user u ON
//                                            w.user_id = u.ID
//                                          INNER JOIN staff_supervisor s_user ON
//                                            u.ID = s_user.user_id";
//                    }
                    
                        
            $query = $query." INNER JOIN taskandschedule t ON
                                w.schedule_id = t.ID
                              WHERE (w.server_date BETWEEN :s_date AND :e_date)  AND (DATE_ADD(t.endTime, INTERVAL 59 second) > w.server_time) AND w.user_id = :user_id";
            
//            if($staffSet) {
//                $query = $query." AND w.user_id = :user_id";
//            }
//            
//            if($checkpointSet) {
//                $query = $query." AND w.checkpoint_id = :checkpoint_id";
//            }
//            
//            if($supervisorSet){
//                $query = $query." AND s_user.supervisor_id = :spv_id";
//            }

            $database->query($query);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
            $database->bind(':user_id', $staff_id);
                       
//            if($staffSet) {
//                $database->bind(':user_id', $staff_id);
//            }
//            
//            if($checkpointSet) {
//                $database->bind(':checkpoint_id', $checkpoint_id);
//            }
//            
//            if($supervisorSet){
//                $database->bind(':spv_id', $supervisor_id);
//            }
            
            $ontimeTasks = $database->single();
            if($ontimeTasks) {
                return $ontimeTasks;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    
    public static function getOnTimeCompletionRateAllWork($staff_id,$s_date,$e_date) {
        $database = new database();
        
        try {
            
//            $supervisor_id = $report->getSupervisorID();
//            $checkpoint_id = $report->getCheckpointID();
//            
//            //staff set
//            $staffSet = !empty($staff_id);
//            
//            //checkpoint set
//            $checkpointSet = !empty($checkpoint_id);
//            
//            //supervisor set
//            $supervisorSet = !empty($supervisor_id);
            
            $query="SELECT 
                        COUNT(w.id) as allTasks
                    FROM
                        work_list w";
                    
//                    if($staffSet && $supervisorSet){    
//                        $query = $query." INNER JOIN user u ON
//                                            w.user_id = u.ID
//                                          INNER JOIN staff_supervisor s_user ON
//                                            u.ID = s_user.user_id";
//                    }
                    
                        
            $query = $query." INNER JOIN taskandschedule t ON
                                w.schedule_id = t.ID
                              WHERE (w.server_date BETWEEN :s_date AND :e_date)  AND w.user_id = :user_id";
            
//            if($staffSet) {
//                $query = $query." AND w.user_id = :user_id";
//            }
//            
//            if($checkpointSet) {
//                $query = $query." AND w.checkpoint_id = :checkpoint_id";
//            }
//            
//            if($supervisorSet){
//                $query = $query." AND s_user.supervisor_id = :spv_id";
//            }

            $database->query($query);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
            $database->bind(':user_id', $staff_id);
                       
//            if($staffSet) {
//                $database->bind(':user_id', $staff_id);
//            }
//            
//            if($checkpointSet) {
//                $database->bind(':checkpoint_id', $checkpoint_id);
//            }
//            
//            if($supervisorSet){
//                $database->bind(':spv_id', $supervisor_id);
//            }
            
            $ontimeTasks = $database->single();
            if($ontimeTasks) {
                return $ontimeTasks;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    //only use for process rework rate function
    public static function processRework($s_date, $e_date, $location_id = false,$staff_id=false) {
        $database = new database();
        try {           
            $locationSet = $location_id !== false;
            
            $staffSet = $staff_id !== false;
            
            $query = "SELECT "
                        . "w.*, "
                        . "A.name as checkpointName, "
                        . "P.id as parent_ID, "
                        . "P.name as parentName, "
                        . "C.id as child_ID, "                        
                        . "C.name as childName,"
                        . "CONCAT(u.firstName,' ',u.lastName) as staffName "
                    . "FROM "
                        . "work_list w "
                    . "INNER JOIN checkpoint A ON "
                        . "w.checkpoint_id = A.ID "
                    . "INNER JOIN location C ON "
                        . "A.locationid = C.id "
                    . "INNER JOIN location P ON "
                        . "P.id = C.parentid "
                    . "INNER JOIN user u ON "
                        . "w.user_id = u.ID "
                    . "WHERE w.work_status = :rework AND (w.server_date BETWEEN :s_date AND :e_date) ";

            
            if($locationSet){
                $query = $query."AND C.id = :location_id ";
            }
            if($staffSet){
                $query = $query."AND u.ID = :staff_id ";
            }
                
            $query = $query. "ORDER BY A.ID ASC";

            $database->query($query);
            $database->bind(':rework', "Rework");
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
            
            if($locationSet){
                $database->bind(':location_id', $location_id);
            }
            if($staffSet){
                $database->bind(':staff_id', $staff_id);
            }
            
            $result = $database->resultset();
            if($result){                
                return $result;
            } else {
                return false;
            }

        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function processWork($s_date, $e_date, $location_id = false,$staff_id=false) {
        $database = new database();
        try {           
            $locationSet = $location_id !== false;
            
            $staffSet = $staff_id !== false;
            
            $query = "SELECT "
                        . "w.*, "
                        . "A.name as checkpointName, "
                        . "P.id as parent_ID, "
                        . "P.name as parentName, "
                        . "C.id as child_ID, "                        
                        . "C.name as childName,"
                        . "CONCAT(u.firstName,' ',u.lastName) as staffName "
                    . "FROM "
                        . "work_list w "
                    . "INNER JOIN checkpoint A ON "
                        . "w.checkpoint_id = A.ID "
                    . "INNER JOIN location C ON "
                        . "A.locationid = C.id "
                    . "INNER JOIN location P ON "
                        . "P.id = C.parentid "
                    . "INNER JOIN user u ON "
                        . "w.user_id = u.ID "
                    . "WHERE (w.server_date BETWEEN :s_date AND :e_date) ";

            
            if($locationSet){
                $query = $query."AND C.id = :location_id ";
            }
            if($staffSet){
                $query = $query."AND u.ID = :staff_id ";
            }
                
            $query = $query. "ORDER BY A.ID ASC";

            $database->query($query);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
            
            if($locationSet){
                $database->bind(':location_id', $location_id);
            }
            if($staffSet){
                $database->bind(':staff_id', $staff_id);
            }
            
            $result = $database->resultset();
            if($result){                
                return $result;
            } else {
                return false;
            }

        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function customer($s_date,$e_date){
        $database = new database();
        try {           
            $size_1 = '_thumbnail';
            
            $query = "SELECT "
                        . "w.*, "
                        . "c.name as checkpointName, "
                        . "c.image as sampleImage, "
                        . "l_p.id as parent_ID, "
                        . "l_c.id as child_ID, "                        
                        . "CONCAT(l_p.name,' > ',l_c.name) as location "
                    . "FROM "
                        . "work_list w "
                    . "INNER JOIN checkpoint c ON "
                        . "w.checkpoint_id = c.ID "
                    . "INNER JOIN location l_c ON "
                        . "c.locationid = l_c.id "
                    . "INNER JOIN location l_p ON "
                        . "l_p.id = l_c.parentid "
                    . "WHERE (w.server_date BETWEEN :s_date AND :e_date) AND w.work_status = :status "
                    . "ORDER BY w.server_date ASC";


            $database->query($query);
            $database->bind(':s_date', $s_date);
            $database->bind(':e_date', $e_date);
            $database->bind(':status', 'Approved');
            
            $result = $database->resultset();
            if($result){
                
                $i=0;
                foreach ($result as $single){
                    
                    $sample_imagePath = $single['sampleImage'];
                    if($sample_imagePath !== null){
                        $sample_extension_pos = strrpos($sample_imagePath, '.');
                        $sample_thumb = substr($sample_imagePath,0, $sample_extension_pos).$size_1.substr($sample_imagePath, $sample_extension_pos);
                        $result[$i]['sample_img_thumbnail'] = $sample_thumb;
                    } else {
                        $result[$i]['sample_img_thumbnail'] = null;
                    }
                    
                    $work_imagePath = $single['image'];
                    if($work_imagePath !== null){
                        $work_extension_pos = strrpos($work_imagePath, '.');
                        $work_thumb = substr($work_imagePath,0, $work_extension_pos).$size_1.substr($work_imagePath, $work_extension_pos);
                        $result[$i]['work_img_thumbnail'] = $work_thumb;
                    } else {
                        $result[$i]['work_img_thumbnail'] = null;
                    }
                    
                    $i++;
                }
                return $result;
            } else {
                return false;
            }

        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    
    public static function get_approved_work($date,$checkpointID) {
        $database = new database();
        try {
            $size_1 = '_thumbnail';
            
            $query = "SELECT "
                        . "w.*, "
                        . "c.name as checkpointName, "
                        . "c.image as sampleImage, "
                        . "l_p.id as parent_ID, "
                        . "l_c.id as child_ID, "                        
                        . "CONCAT(l_p.name,' > ',l_c.name) as location "
                    . "FROM "
                        . "work_list w "
                    . "INNER JOIN checkpoint c ON "
                        . "w.checkpoint_id = c.ID "
                    . "INNER JOIN location l_c ON "
                        . "c.locationid = l_c.id "
                    . "INNER JOIN location l_p ON "
                        . "l_p.id = l_c.parentid "
                    . "WHERE (w.server_date < :date) AND w.work_status = :status AND w.checkpoint_id = :checkpoint_id "
                    . "ORDER BY w.server_date ASC";


            $database->query($query);
            $database->bind(':date', $date);
            $database->bind(':checkpoint_id', $checkpointID);
            $database->bind(':status', 'Approved');
            
            $result = $database->single();
            if($result){
                
                $work_imagePath = $result['image'];
                if($work_imagePath !== null){
                    $work_extension_pos = strrpos($work_imagePath, '.');
                    $work_thumb = substr($work_imagePath,0, $work_extension_pos).$size_1.substr($work_imagePath, $work_extension_pos);
                    $result['work_img_thumbnail'] = $work_thumb;
                } else {
                    $result['work_img_thumbnail'] = null;
                }
                
                return $result['work_img_thumbnail'];
            } else {
                return false;
            }
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    
    
//    public static function getReworkStaff(Report $report) {
//        $database = new database();
//        
//        try {
//            
//            $size_1 = '_thumbnail';
//            
//            $s_date = $report->getS_date();
//            $e_date = $report->getE_date();
//            $staff_id = $report->getStaffID();
//            $supervisor_id = $report->getSupervisorID();
//            $checkpoint_id = $report->getCheckpointID();
//            
////            //staff set
////            $staffSet = !empty($staff_id);
////            
////            //checkpoint set
////            $checkpointSet = !empty($checkpoint_id);
////            
////            //supervisor set
////            $supervisorSet = !empty($supervisor_id);
//
//            $query="SELECT DISTINCT
//                        u.ID as staffID,
//                        CONCAT(u.firstName,' ',u.lastName) as fullName,
//                        um.meta_value as img_profile
//                    FROM
//                        work_list w 
//                    INNER JOIN user u ON
//                        w.user_id = u.ID
//                    INNER JOIN user_meta um ON
//                        um.user_id = u.ID
//                    INNER JOIN staff_supervisor s_user ON
//                        u.ID = s_user.user_id
//                    WHERE (w.server_date BETWEEN :s_date AND :e_date) AND w.work_status = :status AND um.meta_key = :path";
//            
////            if($staffSet) {
////                $query = $query." AND w.user_id = :user_id";
////            }
////            
////            if($checkpointSet) {
////                $query = $query." AND w.checkpoint_id = :checkpoint_id";
////            }
////            
////            if($supervisorSet){
////                $query = $query." AND s_user.supervisor_id = :spv_id";
////            }
//            
//            $query = $query." LIMIT 10";
//            
//            $database->query($query);
//            $database->bind(':s_date', $s_date);
//            $database->bind(':e_date', $e_date);           
//            $database->bind(':status', "Rework");
//            $database->bind(':path', "imagePath");
//            
////            if($staffSet) {
////                $database->bind(':user_id', $staff_id);
////            }
////            
////            if($checkpointSet) {
////                $database->bind(':checkpoint_id', $checkpoint_id);
////            }
////            
////            if($supervisorSet){
////                $database->bind(':spv_id', $supervisor_id);
////            }
//            
//            $reworkStaff = $database->resultset();
//            if($reworkStaff) {
//                
//                $i=0;
//                foreach($reworkStaff as $user){
//                    
//                    $userID = $user['staffID'];
//                    $imagePath = $user['img_profile'];
//                    
//                    $query_1 = "SELECT COUNT(w.checkpoint_id) as checkpointCount FROM work_list w WHERE w.user_id = :userID AND (w.server_date BETWEEN :s_date AND :e_date) AND w.work_status = :status";
//                    
//                    $database->query($query_1);
//                    $database->bind(':userID', $userID);
//                    $database->bind(':s_date', $s_date);
//                    $database->bind(':e_date', $e_date);           
//                    $database->bind(':status', "Rework");
//                    
//                    $checkpointCount = $database->single();
//                    
//                    $reworkStaff[$i]['checkpointCount'] = $checkpointCount['checkpointCount'];
//                    
//                    $extension_pos = strrpos($imagePath, '.');
//                    $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
//                    $reworkStaff[$i]['img_profile'] = $thumb;
//                    
//                    $i++;
//                }
//                
//                foreach ($reworkStaff as $c=>$key){
//                    $sort_numeric[] = $key['checkpointCount'];
//                }
//                
//                array_multisort($sort_numeric, SORT_DESC,$reworkStaff);//sort according to rework checkpoint count
//                
//                return $reworkStaff;
//            } else {
//                echo false;
//            }
//        } catch (Exception $exc) {
//            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
//            $exc->getMessage();
//            return false;
//        }
//    }
}