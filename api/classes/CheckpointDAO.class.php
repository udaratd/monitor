<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CheckpointDAO 
 * This class handle all database transaction of Checkpoint
 * @author doweby 
 */

use chillerlan\QRCode\{QRCode, QROptions};

class CheckpointDAO {
    
    /**
     * This function add new checkpoint and return true or false
     * @param Checkpoint $checkpoint
     * @return boolean Return True on success False on failure
     */
    public static function addCheckpoint(Checkpoint $checkpoint){
        $database = new database();
        $database->beginTransaction();
        try {
            
//            if($checkpoint->hasImage() === false) {
//                throw new Exception("You have to set Checkpoint Image");
//            }
            
            $imagePath = null;

            
            $query1 = "INSERT INTO checkpoint(name,locationid,description,image) VALUES(:name,:locationid,:description,:image)";
            $database->query($query1);
            $database->bind(':name', $checkpoint->getChekPoinName());
            $database->bind(':locationid', $checkpoint->getLocationid());
            $database->bind(':description', $checkpoint->getDescription());
            $database->bind(':image', $imagePath);
            
            $result1 = $database->execute();
            $result2 = null;
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            if($checkpoint->hasImage()) {
                
                $ID = $database->lastInsertId();
                
                $image = $checkpoint->getChekImage();            
                $fileName = $ID.'_'.mt_rand(1, 5000);
                $extension = '.jpg';
                
                $path = UPLOAD_CHECK_DIR;
                $imagePath = FileManager::imageUpload($path, $fileName.$extension, $image);
                
                if(is_string($imagePath)){
                    
                    $query2 = "UPDATE checkpoint SET image = :image WHERE ID = :id";
                    $database->query($query2);
                    $database->bind(':id', $ID);
                    $database->bind(':image', $imagePath);
                    
                    $result2 = $database->execute();

                    FileManager::imageUpload($path, $fileName.$size_1.$extension, ImageManager::reSize(150, $image));
                    FileManager::imageUpload($path, $fileName.$size_2.$extension, ImageManager::reSize(500, $image));
                    FileManager::imageUpload($path, $fileName.$size_3.$extension, ImageManager::reSize(1024, $image));
                } else {
                    $result2 = false;
                }
            }
            
      
            if($result1 && ($result2 === null || $result2 === true) ) {
                $database->endTransaction();
                
                $qrName = $ID.'.jpg';            
                
                $qc = new QrCode();
                $qc->TEXT($ID);
                $qc->QRCODE(400,$qrName);
                
                return true;
            } else {
                if($result2 === false && ($imagePath !== null || $imagePath !== false || $imagePath !== 1)){
                    if(FileManager::deleteFile($imagePath) === false) {
                        System::log(new Log("Can not delete ".$imagePath, LOG_ERROR));
                    } else {
                        $extension_pos = strrpos($imagePath, '.');
                        
                        $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                        $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);
                        $large = substr($imagePath,0, $extension_pos).$size_3.substr($imagePath, $extension_pos);
                        
                        if(FileManager::fileExist($thumb)){
                            FileManager::deleteFile($thumb);
                        }
                        if(FileManager::fileExist($medium)){
                            FileManager::deleteFile($medium);
                        }
                        if(FileManager::fileExist($large)){
                            FileManager::deleteFile($large);
                        }  
                    }
                }
                
                throw new Exception("Data not Inserted");
            }
                        
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            $database->cancelTransaction();
            return false;
        }
    }
    
    public static function imageUpload(Checkpoint $checkpoint){
        $database = new database();
        try {
            
            $imagePath = null;
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            if($checkpoint->hasImage()) {
                
                $ID = $checkpoint->getId();
                
                $image = $checkpoint->getChekImage();            
                $fileName = $ID.'_'.mt_rand(1, 5000);
                $extension = '.jpg';
                
                $path = UPLOAD_CHECK_DIR;
                $imagePath = FileManager::imageUpload($path, $fileName.$extension, $image);
                
                if(is_string($imagePath)){
                    
                    $query = "UPDATE checkpoint SET image = :image WHERE ID = :id";
                    $database->query($query);
                    $database->bind(':id', $ID);
                    $database->bind(':image', $imagePath);
                    
                    $result = $database->execute();

                    FileManager::imageUpload($path, $fileName.$size_1.$extension, ImageManager::reSize(150, $image));
                    FileManager::imageUpload($path, $fileName.$size_2.$extension, ImageManager::reSize(500, $image));
                    FileManager::imageUpload($path, $fileName.$size_3.$extension, ImageManager::reSize(1024, $image));
                    
                    if($result){
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
                        
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            $database->cancelTransaction();
            return false;
        }
    }

    /**
     * Function return Array of checkpoints on Success False on Failure
     * 
     * @return boolean return checkpoint array on Success False On failure
     */
    public static function listCkeckpoint($loggedUser_ID,$order) {
        $database = new database();
        try {
            
            $supervisorSet = $loggedUser_ID !== false;
            
            $query = "SELECT "
                        . "A.ID, "
                        . "A.name, "
                        . "A.description, "
                        . "A.image, "
                        . "P.id as parent_ID, "
                        . "P.name as parentName, "
                        . "C.id as child_ID, "                        
                        . "C.name as childName, "
                        . "u.ID as staffID, "
                        . "u.firstName as staffFname, "
                        . "u.lastName as staffLname "
                    . "FROM "
                        . "checkpoint A "
                    . "INNER JOIN location C ON "
                        . "A.locationid = C.id "
                    . "INNER JOIN location P ON "
                        . "P.id = C.parentid "
                    . "LEFT JOIN work_list w ON "
                        . "A.ID = w.checkpoint_id "
                    . "LEFT JOIN user u ON "
                        . "w.user_id = u.ID ";
            
            if($order == false){
                $query = $query." ORDER BY A.ID DESC";
            } else {
                $query = $query." ORDER BY A.ID ASC";
            }
//            if($supervisorSet) {
//                
//            }
            
            $database->query($query);
            
//            if($supervisorSet) {
//                
//            }
            
            if($database->execute()){
                $result = $database->resultset();
                return $result;
            } else {
                return false;
            }

        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    /**
     * This function update checkpoint and return True or False
     * 
     * @param Checkpoint $checkpoint Checkpoint object
     * @return boolean return True on Success False on failure
     */
    public static function updateCheckpoint(Checkpoint $checkpoint) {
        $oldImagePath = CheckpointDAO::getCheckImage($checkpoint->getId());
        $database = new database();
        try {
            
            $imagePath = null;
            $imageUpload = null;
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            if($checkpoint->hasImage()) {
                
                $image = $checkpoint->getChekImage();
                $fileName = $checkpoint->getId() . '_' . mt_rand(5000, 10000);
                $extension = '.jpg';

                $path = UPLOAD_CHECK_DIR;

                $imagePath = FileManager::imageUpload($path, $fileName.$extension, $image);
                
                if(is_string($imagePath)) {
                    
                    FileManager::imageUpload($path, $fileName.$size_1.$extension, ImageManager::reSize(150, $image));
                    FileManager::imageUpload($path, $fileName.$size_2.$extension, ImageManager::reSize(500, $image));
                    FileManager::imageUpload($path, $fileName.$size_3.$extension, ImageManager::reSize(1024, $image));
                    
                    $imageUpload = true;
                } else {
                    $imageUpload = false;
                    throw new Exception("Image can not upload to server");
                }
            }          

            $database->beginTransaction();

            $query = "UPDATE checkpoint SET ".implode(", ", $checkpoint->getQ())." WHERE ID = :id";
            $database->query($query);
            
            $database->bind(':id', $checkpoint->getId());
            
            if(!empty($checkpoint->getChekPoinName())){
                $database->bind(':name', $checkpoint->getChekPoinName());
            }
            if(!empty($checkpoint->getLocationid())){
                $database->bind(':locationid', $checkpoint->getLocationid());
            }
            if(!empty($checkpoint->getDescription())){
                $database->bind(':description', $checkpoint->getDescription());  
            }
            if(!empty($checkpoint->getChekImage())){
                $database->bind(':image', $imagePath);
            }
            
            $result = $database->execute();
            
            if($result && ($imageUpload === true || $imageUpload === null) ) {
               $database->endTransaction();
                if(!empty($oldImagePath) && $oldImagePath !== false){
                    
                    if(FileManager::fileExist($oldImagePath)===true){
                        
                        if(FileManager::deleteFile($oldImagePath) === true){
                             $extension_pos = strrpos($oldImagePath, '.');

                             $thumb = substr($oldImagePath,0, $extension_pos).$size_1.substr($oldImagePath, $extension_pos);
                             $medium = substr($oldImagePath,0, $extension_pos).$size_2.substr($oldImagePath, $extension_pos);
                             $large = substr($oldImagePath,0, $extension_pos).$size_3.substr($oldImagePath, $extension_pos);

                             if(FileManager::fileExist($thumb)){
                                 if(FileManager::deleteFile($thumb)===false){
                                     System::log(new Log("Can not delete ".$thumb, LOG_ERROR));
                                 }
                             }
                             if(FileManager::fileExist($medium)){
                                 if(FileManager::deleteFile($medium)===false){
                                     System::log(new Log("Can not delete ".$medium, LOG_ERROR));
                                 }
                             }
                             if(FileManager::fileExist($large)){
                                 if(FileManager::deleteFile($large)==false){
                                     System::log(new Log("Can not delete ".$large, LOG_ERROR));
                                 }
                             }
                        } else {                             
                            System::log(new Log("Can not delete ".$oldImagePath, LOG_ERROR));
                        }
                    }
                }
                
               return true;
            } else {
                $database->cancelTransaction();
                if($imageUpload === true){
                    
                    if(!empty($imagePath) && ($imagePath !== 1) && ($imagePath !== false)){
                        
                        if(FileManager::fileExist($imagePath)===true){
                            if(FileManager::deleteFile($imagePath) === true) {
                                $extension_pos = strrpos($imagePath, '.');

                                $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                                $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);
                                $large = substr($imagePath,0, $extension_pos).$size_3.substr($imagePath, $extension_pos);

                                if(FileManager::fileExist($thumb)){
                                    if(FileManager::deleteFile($thumb)===false){
                                        System::log(new Log("Can not delete ".$thumb, LOG_ERROR));
                                    }
                                }
                                if(FileManager::fileExist($medium)){
                                    if(FileManager::deleteFile($medium)===false){
                                        System::log(new Log("Can not delete ".$medium, LOG_ERROR));
                                    }
                                }
                                if(FileManager::fileExist($large)){
                                    if(FileManager::deleteFile($large)===false){
                                        System::log(new Log("Can not delete ".$large, LOG_ERROR));
                                    }
                                }
                            } else {                                
                                System::log(new Log("Can not delete ".$imagePath, LOG_ERROR));
                            }
                        }
                    }
                }
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $exc->getMessage();
            return false;
        }
    }
    
    /**
     * This function check matching image for given id
     * 
     * @param type $id Integer ID
     * @return boolean return image path on success False on failure
     */
    public static function getCheckImage($id) {
        $database = new database();
        try {
            $query = "SELECT image FROM checkpoint WHERE ID = :id";
            $database->query($query);
            $database->bind(':id', $id);
            $result = $database->single();
            
            $path = $result['image'];
            if($result) {
                return $path;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    /**
     * This function delete checkpoint
     * 
     * @param type $id Integer ID
     * @return boolean return True on success False on failure
     */
    public static function deleteCheckpoint($id) {
        $image = CheckpointDAO::getCheckImage($id);
        $database = new database();
        try {

            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';

            $query = "DELETE FROM checkpoint WHERE ID = :id";
            $database->query($query);
            $database->bind(':id', $id);
            $result = $database->execute();

            if($result) {
                if(!empty($image) && $image!==false){
                    
                    if(FileManager::fileExist($image)=== true){
                        
                        if(FileManager::deleteFile($image) === true) {
                            
                            $extension_pos = strrpos($image, '.');

                            $thumb = substr($image,0, $extension_pos).$size_1.substr($image, $extension_pos);
                            $medium = substr($image,0, $extension_pos).$size_2.substr($image, $extension_pos);
                            $large = substr($image,0, $extension_pos).$size_3.substr($image, $extension_pos);

                            if(FileManager::fileExist($thumb)){
                                if(FileManager::deleteFile($thumb)===false){
                                    System::log(new Log("Can not Delete ".$thumb, LOG_EXCEPTION));
                                }
                            }
                            if(FileManager::fileExist($medium)){
                                if(FileManager::deleteFile($medium)===false){
                                    System::log(new Log("Can not Delete ".$medium, LOG_EXCEPTION));
                                }
                            }
                            if(FileManager::fileExist($large)){
                                if(FileManager::deleteFile($large)==false){
                                    System::log(new Log("Can not Delete ".$large, LOG_EXCEPTION));
                                }
                            }
                            
                        } else {
                            System::log(new Log("Can not Delete ".$image, LOG_EXCEPTION));  
                        }
                    }
                }
                return true;
            } else {                
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    /**
     * Function check is there any checkpoint for providing ID
     * 
     * @param type $ID Integer ID
     * @return boolean return True on Success False on Failure
     */
    public static function checkpointExists($ID) {
        $database = new database();
        try {
            $query = "SELECT ID FROM checkpoint WHERE ID = :id";
            $database->query($query);
            $database->bind(':id', $ID);
            $result = $database->single();
            
            if($result) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    /**
     * function search checkpoint for giving name,location
     * 
     * @param type $name String checkpoint name
     * @param type $location String checkpoint location
     * @return boolean return checkpoint Array on Success False on Failure
     */
    public static function searchCkeckpoint($loggedUser_ID,$name,$locationid,$order,$s_limit,$e_limit,$all=false) {
        $database = new database();
        try {
            
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            $supervisorSet = $loggedUser_ID !== false;
            
            $locationSet = $locationid !== 'All';
            
            $query = "SELECT DISTINCT "
                        . "A.ID as checkPID, "
                        . "A.name as checkPName, "
                        . "A.description as checkPDescription, "
                        . "A.image as img_original, "
                        . "P.id as Loc_parent_ID, "
                        . "P.name as Loc_parentName, "
                        . "C.id as Loc_child_ID, "                        
                        . "C.name as Loc_childName, "
                        . "u.ID as staffID, "
                        . "u.firstName as staffFname, "
                        . "u.lastName as staffLname "
                    . "FROM "
                        . "checkpoint A "
                    . "INNER JOIN location C ON "
                        . "A.locationid = C.id "
                    . "INNER JOIN location P ON "
                        . "P.id = C.parentid "
                    . "LEFT JOIN work_list w ON "
                        . "A.ID = w.checkpoint_id "
                    . "LEFT JOIN user u ON "
                        . "w.user_id = u.ID "
                    . "LEFT JOIN staff_supervisor spv ON "
                        . "u.ID = spv.user_id "
                    . "WHERE A.name LIKE :name ";

            
            if($supervisorSet) {
               // $query = $query."AND spv.supervisor_id = :spvID ";    commented by thilina 2018-04-28 supovisors shoud be able to see all available check points.
            }
            
            if($locationSet) {
                $query = $query."AND A.locationid = :locationid ";    
            }
        
            if($order == false){
                $query = $query."ORDER BY A.ID DESC ";
            } else {
                $query = $query."ORDER BY A.ID ASC ";
            }
            
            if($all===false){
                if( ($s_limit == 0 || !empty($s_limit)) && !empty($e_limit)){
                    $query = $query. "LIMIT :s_limit,:e_limit";                
                }
            }
            
            $database->query($query);
            $database->bind(':name', '%'.$name.'%');
            
            if($supervisorSet) {
                // $database->bind(':spvID', $loggedUser_ID);commented by thilina 2018-04-28
            }
            
            if($locationSet) {
                $database->bind(':locationid', $locationid);
            }

            if($all === false){
                if(($s_limit == 0 || !empty($s_limit)) && !empty($e_limit)){
                    $database->bind(':s_limit', $s_limit);
                    $database->bind(':e_limit', $e_limit);
                }
            }
            
            $checkpoints = $database->resultset();

            
            if($checkpoints) {
                
                $i=0;
                foreach ($checkpoints as $checkpoint){

                    $imagePath = $checkpoint['img_original'];
                    if($imagePath !== null){
                        $extension_pos = strrpos($imagePath, '.');                       
                        
                        $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                        $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);
                        $large = substr($imagePath,0, $extension_pos).$size_3.substr($imagePath, $extension_pos);

                        $checkpoints[$i]['img_thumbnail'] = $thumb;
                        $checkpoints[$i]['img_medium'] = $medium;
                        $checkpoints[$i]['img_large'] = $large;
                    } else {
                        $checkpoints[$i]['img_thumbnail'] = null;
                        $checkpoints[$i]['img_medium'] = null;
                        $checkpoints[$i]['img_large'] = null;
                    }
                    
                    $i++;
                }
             
                return $checkpoints;
            } else {
                return false;
            }    
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function searchCkeckpointCount($loggedUser_ID,$name,$locationid,$order) {
        $database = new database();
        try {
            
            $supervisorSet = $loggedUser_ID !== false;
            
            $locationSet = $locationid !== 'All';
            
            $query = "SELECT DISTINCT "
                        . "A.ID as checkPID, "
                        . "A.name as checkPName, "
                        . "A.description as checkPDescription, "
                        . "A.image as img_original, "
                        . "P.id as Loc_parent_ID, "
                        . "P.name as Loc_parentName, "
                        . "C.id as Loc_child_ID, "                        
                        . "C.name as Loc_childName, "
                        . "u.ID as staffID, "
                        . "u.firstName as staffFname, "
                        . "u.lastName as staffLname "
                    . "FROM "
                        . "checkpoint A "
                    . "INNER JOIN location C ON "
                        . "A.locationid = C.id "
                    . "INNER JOIN location P ON "
                        . "P.id = C.parentid "
                    . "LEFT JOIN work_list w ON "
                        . "A.ID = w.checkpoint_id "
                    . "LEFT JOIN user u ON "
                        . "w.user_id = u.ID "
                    . "LEFT JOIN staff_supervisor spv ON "
                        . "u.ID = spv.user_id "
                    . "WHERE A.name LIKE :name ";

            
            if($supervisorSet) {
               // $query = $query."AND spv.supervisor_id = :spvID ";    commented by thilina 2018-04-28 supovisors shoud be able to see all available check points.
            }
            
            if($locationSet) {
                $query = $query."AND A.locationid = :locationid ";    
            }
        
            if($order == false){
                $query = $query."ORDER BY A.ID DESC";
            } else {
                $query = $query."ORDER BY A.ID ASC";
            }
            
            $database->query($query);
            $database->bind(':name', '%'.$name.'%');
            
            if($supervisorSet) {
                // $database->bind(':spvID', $loggedUser_ID);commented by thilina 2018-04-28
            }
            
            if($locationSet) {
                $database->bind(':locationid', $locationid);
            }

            $count = $database->resultset();

            
            if($count) {
                return $count;
            } else {
                return false;
            }    
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    /**
     * This function return checkpoint array for given ID False on fail
     * 
     * @param type $ID Integer ID
     * @return boolean return checkpoint Array on success False on fail
     */
    public static function getCheckpoint($ID,$scheduleID) {
        $database = new database();
        try {
            $size_1 = '_thumbnail';
            $size_2 = '_medium';
            $size_3 = '_large';
            
            $query = "SELECT c.ID,c.name,c.locationid,c.description,c.image as img_original,l_p.name as p_location,l_c.name as c_location "
                    . "FROM checkpoint c "
                    . "INNER JOIN location l_c ON "
                    . "l_c.ID = c.locationid "
                    . "INNER JOIN location l_p ON "
                    . "l_p.ID = l_c.parentid "
                    . "WHERE c.ID = :id";
            $database->query($query);
            $database->bind(':id', $ID);
            $checkpoint = $database->single();
            
            if(!empty($scheduleID)){
                $query_2 = "SELECT description,startTime,endTime FROM taskandschedule WHERE ID = :scheduleID";
                $database->query($query_2);
                $database->bind(':scheduleID', $scheduleID);
                
                $schedule = $database->single();
                
                $query_3 = "SELECT comment FROM work_list WHERE schedule_id = :scheduleID";
                $database->query($query_3);
                $database->bind(':scheduleID', $scheduleID);
                
                $work = $database->single();   
            }
            
            if($checkpoint) {
                
                $imagePath = $checkpoint['img_original'];               
                if($imagePath !== null){
                    $extension_pos = strrpos($imagePath, '.');                       

                    $thumb = substr($imagePath,0, $extension_pos).$size_1.substr($imagePath, $extension_pos);
                    $medium = substr($imagePath,0, $extension_pos).$size_2.substr($imagePath, $extension_pos);
                    $large = substr($imagePath,0, $extension_pos).$size_3.substr($imagePath, $extension_pos);

                    $checkpoint['img_thumbnail'] = $thumb;
                    $checkpoint['img_medium'] = $medium;
                    $checkpoint['img_large'] = $large;
                } else {
                    $checkpoint['img_thumbnail'] = null;
                    $checkpoint['img_medium'] = null;
                    $checkpoint['img_large'] = null;
                }

                if(!empty($schedule['description'])){
                    $checkpoint['schedule_des'] = $schedule['description'];
                } else {
                    $checkpoint['schedule_des'] = "";
                }
                
                if(!empty($schedule['startTime']) && !empty($schedule['endTime'])){
                    $startTime = date('h:i:s a', strtotime($schedule['startTime']));
                    $endTime = date('h:i:s a', strtotime($schedule['endTime']));
                    $timeperiod = $startTime.' - '.$endTime;
                    $checkpoint['schedule_time'] = $timeperiod;
                } else {
                    $checkpoint['schedule_time'] = "";
                }
                
                if(!empty($work['comment'])){
                    $checkpoint['comment_des'] = $work['comment'];
                } else {
                    $checkpoint['comment_des'] = "";
                }
                
                return $checkpoint;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            echo $exc->getMessage();
            return false;
        }
    }
    
    public static function checkpointsForSchedule($search,$unassigned) {
        $database = new database();
        try {
            
            $statusSet = $unassigned === false;
            
            $query = "SELECT DISTINCT "
                        . "c.ID as checkpointID, "
                        . "c.name as checkpointName, "
                        . "l_p.ID as location_parentID, "
                        . "l_p.name as location_parentName, "
                        . "l_c.ID as location_childID, "
                        . "l_c.name as location_childName "
                    . "FROM "
                        . "checkpoint c "
                    . "INNER JOIN location l_c ON "
                        . "c.locationid = l_c.ID "
                    . "INNER JOIN location l_p ON "
                        . "l_c.parentid = l_p.ID "
                    . "LEFT JOIN taskandschedule t ON "
                        . "c.ID = t.checkpoint_id "
                    . "WHERE ("
                        . "c.name LIKE :checkName OR "
                        . "l_p.name LIKE :loc_parentName OR "
                        . "l_c.name LIKE :loc_childName "
                    . ")";
            
            if($statusSet) {
                $query = $query." AND t.checkpoint_id IS NULL";
            }
            
            $database->query($query);
            $database->bind(':checkName', '%'.$search.'%');
            $database->bind(':loc_parentName', '%'.$search.'%');
            $database->bind(':loc_childName', '%'.$search.'%');

            if($database->execute()) {
                return $database->resultset();
            } else {
                return false;
            }
        } catch (Exception $exc) {
             System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
             $exc->getMessage();
            return false;
        }
    }
    
    public static function getCheckpointsAccordingToLocation($ID) {
        $database = new database();
        try {
            $query = "SELECT ID,name FROM checkpoint WHERE locationid = :id";
            $database->query($query);
            $database->bind(':id', $ID);
            $result = $database->resultset();

            if($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getCheckpoints(){
        $database = new database();
        try {
            $query = "SELECT ID,name FROM checkpoint";
            $database->query($query);
            $result = $database->resultset();

            if($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function exportCheckpoint(){
        $database = new database();
        try {
            date_default_timezone_set('Asia/Colombo');
            $today = date('h:i:s a Y-m-d');
            $filename = "checkpoint_export_".$today.".csv";
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename='.$filename);
            $output = fopen("php://output", "w");
            fputcsv($output, array('ID','name','locationid','locationName','description'));
            
            $query = "SELECT
                        c.ID,
                        c.name,
                        c.locationid,
                        CONCAT(l_p.name,' > ',l_c.name) as location,
                        c.description                        
                      FROM 
                        checkpoint c
                      INNER JOIN location l_c ON
                        c.locationid = l_c.ID
                      INNER JOIN location l_p ON
                        l_c.parentid = l_p.ID";
            
            $database->query($query);

            $result = $database->resultset();
            
            foreach ($result as $one){
                fputcsv($output, $one);
            }
            fclose($output);   
            
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            $exc->getMessage();
            return false;
        }
    }
    
    public static function getCheckpointForQR(array $ID_S, $oldway = false){
        $database = new database();
        try {

            $query = "SELECT
                        c.ID,
                        c.name,
                        CONCAT(l_p.name,' > ',l_c.name) as location
                      FROM 
                        checkpoint c
                      INNER JOIN location l_c ON
                        c.locationid = l_c.ID
                      INNER JOIN location l_p ON
                        l_c.parentid = l_p.ID ";
            if(!empty($ID_S)){
                $query = $query ."WHERE c.ID = ".implode(" OR c.ID = ", $ID_S);
            }
            $database->query($query);
            $result = $database->resultset();

            if($result){
                
                $i=0;
                foreach ($result as $checkpoint){
                    $checkpointID = $checkpoint['ID'];
                    $qrName = $checkpointID.'.jpg';                   
                    if($oldway){
                        $qc = new QrCode();
                        $qc->TEXT($checkpointID);
                        $image = $qc->QRCODE(150);
                        $result[$i]['qr'] = $image;
                    } else {
                        $result[$i]['qr'] = (new QRCode)->render($checkpointID);
                    }
                    $i++;                
                }
                CheckpointDAO::generateBulkQR($result, $oldway);
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }
    
    public static function getCheckpointForFeedbackQR(array $ID_S, $oldway = false){
        $database = new database();
        try {

            $query = "SELECT
                        c.ID,
                        c.name,
                        CONCAT(l_p.name,' > ',l_c.name) as location
                      FROM 
                        checkpoint c
                      INNER JOIN location l_c ON
                        c.locationid = l_c.ID
                      INNER JOIN location l_p ON
                        l_c.parentid = l_p.ID ";
            if(!empty($ID_S)){
                $query = $query ."WHERE c.ID = ".implode(" OR c.ID = ", $ID_S);
            }
            
            $database->query($query);
            $result = $database->resultset();

            $host = $_SERVER['HTTP_HOST'];
            $company_un = $_SESSION['COMPANY_NAME'];                              

            if($result){
                $i=0;
                foreach ($result as $checkpoint){
                    $qrName = $checkpoint['ID'].'.jpg';
                    $url = 'https://'.$host.'/feed/index.php?name='.$company_un.'&id='.$checkpoint['ID'];
                    if($oldway){
                        $qc = new QrCode();
                        $qc->URL($url); 
                        $image = $qc->QRCODE(150);
                        $result[$i]['qr'] = $image;
                    } else {
                        $result[$i]['qr'] = (new QRCode)->render($url);
                    }
                    
                    $i++;               
                }
                CheckpointDAO::generateBulkQR($result, $oldway);
            } else {
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_EXCEPTION));
            return false;
        }
    }
    
    public static function generateBulkQR(array $checkpoints, $oldway){
        ob_start();
        $pdfhtml = new PDFHTML();
        $pdfhtml->htmlGenerator($checkpoints,$oldway);
        $html = ob_get_clean();
        
        date_default_timezone_set('Asia/Colombo');
        $today = date("Y-m-d");
        $currentTime = date("H:i:s");
        $currentTime = str_replace(':', '-', $currentTime);
        $date = $today.'_'.$currentTime;
        
        $dompdf = new Dompdf\Dompdf();
//        $dompdf->set_option('defaultFont', 'Courier');
        $dompdf->set_option('isHtml5ParserEnabled', true);
        $dompdf->set_option('isRemoteEnabled', true);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream('checkpoint_QR_'.$date.'.pdf',array('Attachment'=>0)); 
               
    }
    
    public static function addBulkCheckpoint(Checkpoint $checkpoint, $count){
        $database = new database();
        $database->beginTransaction();
        try {
            
            $successCount = 0;
            for($i=0;$i<$count;$i++){
                $query = "INSERT INTO checkpoint(name,locationid) VALUES(:name,:locationid)";
                $database->query($query);
                $database->bind(':name', $checkpoint->getChekPoinName());
                $database->bind(':locationid', $checkpoint->getLocationid());

                $result = $database->execute();
                if($result){
                    $successCount++;
                }
            }
            
            if($count == $successCount){
                $database->endTransaction();
                return true;
            } else {
                $database->cancelTransaction();
                return false;
            }
        } catch (Exception $exc) {
            System::log(new Log($exc->getMessage(), LOG_CRITICAL));
            $database->cancelTransaction();
            return false;
        }
    }
}

