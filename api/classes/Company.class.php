<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Company {
    
    private $id;
    private $name;
    private $image;
    private $email;
            
    function __construct($data = null) {
        if($data){
            if(!empty($data['id'])){
                $this->id = $data['id'];
            }
            if(!empty($data['name'])){
                $this->name = $data['name'];
            }
            if(!empty($data['image'])){
                $this->image = $data['image'];
            }
            if(!empty($data['email'])){
                $this->email = $data['email'];
            }
        }
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getImage() {
        return $this->image;
    }

    function getEmail() {
        return $this->email;
    }
    
    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setImage($image) {
        $this->image = $image;
    }
    
    function setEmail($email) {
        $this->email = $email;
    }
}
