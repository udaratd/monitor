<?php

class LogActivity {
    
    private $id;
    private $activity;
    private $by;
    private $date;
    private $time;
    private $business_id;
    /**
     *
     * if(type == 1){<br>
     *  admin<br>
     * }elseif(type == 2){<br>
     *  supervisor<br>
     * }elseif(type == 3){<br>
     *  staff<br>
     * }
     * 
     * @var type 
     */
    private $userType;
            
    function __construct($data=null) {
        if($data){
            if(isset($data['id'])){
                $this->id = $data['id'];
            }
            if(isset($data['activity'])){
                $this->activity = $data['activity'];
            }
            if(isset($data['by'])){
                $this->by = $data['by'];
            }
            if(isset($data['date'])){
                $this->date = DateAndTimeManager::dateConvert($data['date']);
            }
            if(isset($data['time'])){
                $this->time = DateAndTimeManager::timeConvert($data['time']);
            }
            if(isset($data['business_id'])){
                $this->business_id = $data['business_id'];
            }
            if(isset($data['type'])){
                $this->userType = $data['type'];
            }
        }
    }
    
    function getId() {
        return $this->id;
    }

    function getActivity() {
        return $this->activity;
    }

    function getBy() {
        return $this->by;
    }

    function getDate() {
        if(!empty($this->date)){
            return $this->date;
        } else {
            return date('Y-m-d');
        }
    }

    function getTime() {
        if(!empty($this->time)){
            return $this->time;
        } else {
            return date('H:i:s');
        }
    }

    function getBusiness_id() {
        return $this->business_id;
    }

    function getUserType() {
        return $this->userType;
    }
    
    function setId($id) {
        $this->id = $id;
    }

    function setActivity($activity) {
        $this->activity = $activity;
    }

    function setBy($by) {
        $this->by = $by;
    }

    function setDate($date) {
        $this->date = DateAndTimeManager::dateConvert($date);
    }

    function setTime($time) {
        $this->time = DateAndTimeManager::timeConvert($time);
    }

    function setBusiness_id($business_id) {
        $this->business_id = $business_id;
    }

    function setUserType($userType) {
        $this->userType = $userType;
    }

}

